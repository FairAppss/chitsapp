package com.fairsoft.chitfund.imagestopdf;

import java.io.File;

public interface CreatePDFListener {
    void onPDFGenerated(File pdfFile, int numOfImages);
}
