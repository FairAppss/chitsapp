package com.fairsoft.chitfund.imagestopdf;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;

import java.io.File;
import java.util.ArrayList;


public class PDFEngine {
    private static PDFEngine ourInstance = new PDFEngine();

    public static PDFEngine getInstance() {

        return ourInstance;
    }

    private PDFEngine() {
    }

    public void createPDF(Context context, Bitmap[] files, String pdfFileName, CreatePDFListener createPDFListener) {
        new CreatePDFTask(context, files, pdfFileName, createPDFListener).execute();
    }

    public void openPDF(Context context, File pdfFile) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(pdfFile), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION|
                Intent.FLAG_ACTIVITY_NO_HISTORY);
        context.startActivity(intent);
    }



    public boolean checkIfPDFExists(ArrayList<File> files, String pdfFileName) {
        if (pdfFileName != null && pdfFileName.equals(Utils.getPDFName(files))) {
            File pdfFile = new File(Utils.PDFS_PATH + File.separator + pdfFileName);
            return pdfFile.exists();
        }

        return false;
    }

    public void sharePDF(Context context, File pdfFile) {
        Intent intentShareFile = new Intent(Intent.ACTION_SEND);

        if (pdfFile.exists()) {
            intentShareFile.setType("application/pdf");
            intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse(pdfFile.getPath()));
            intentShareFile.putExtra(Intent.EXTRA_SUBJECT, "Shared via Document Scanner");

            context.startActivity(Intent.createChooser(intentShareFile, "Share PDF"));
        }
    }
}
