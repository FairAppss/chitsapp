package com.fairsoft.chitfund.imagestopdf;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AlertDialog;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;

public class Consts {

    public static final double PLACEHOLDER_0_0 = 0; // check string placeholder_0_0

    public interface Prefs {
        String USER_ID = "USER_ID";
        String MAIN_COMPANY_ID = "main_c_ID";
        String MOBILE = "mobile";
        String EMAIL = "email";
        String DATE = "date";
        String PASS = "pass";
        String COMPANY_NAME = "company_names";

        String ONCE = "once";

    }

    public static void appendLog(String text) {
        File logFile = new File(android.os.Environment.getExternalStorageDirectory(), "log.txt");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {

                e.printStackTrace();
            }
        }
        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public static boolean checkNetworkAndShowError(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean isConnected = cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
        if (!isConnected) {
            showNetWorkError(context);
        }
        return isConnected;
    }

    private static void showNetWorkError(Context context) {
        new AlertDialog.Builder(context)
                .setPositiveButton("ok", null)
                .setMessage("Please check Internet Connection!!")
                .show();
    }

    public static double get2DecValue(double val) {
        // return Double.parseDouble(new DecimalFormat("#.##").format(val));
        return Double.parseDouble(String.format(Locale.getDefault(), "%.2f", val));
    }

    public static String get2DecValueInString(double val) {
        return new DecimalFormat("##,##,##0.00").format(val);
        //
        // return String.format(Locale.getDefault(), "%.2f", val);
    }

    public static double getDoubleFromString(String str) {
        return Double.parseDouble(str.replace(",", ""));

    }
}
