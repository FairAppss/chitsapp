package com.fairsoft.chitfund.imagestopdf;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.util.Log;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class CreatePDFTask extends AsyncTask<String, Integer, File> {

    private final Context context;
    private Bitmap[] files;
    private CreatePDFListener createPDFListener;
    private String pdfFileName;
    private ProgressDialog progressDialog;

    public CreatePDFTask(Context context, Bitmap[] files, String pdfFileName, CreatePDFListener createPDFListener) {
        this.context = context;
        this.files = files;
        this.pdfFileName = pdfFileName;
        this.createPDFListener = createPDFListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();


        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please wait...");
        progressDialog.setMessage("Creating pdf...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setIndeterminate(false);
        progressDialog.setMax(100);
        progressDialog.setCancelable(true);
        progressDialog.show();
    }

    @Override
    protected void onPostExecute(File s) {
        super.onPostExecute(s);
        progressDialog.dismiss();

        if (createPDFListener != null)
            createPDFListener.onPDFGenerated(s, files.length);
    }

    @Override
    protected File doInBackground(String... params) {

        File outputFile = Utils.getOutputMediaFile(Utils.PDFS_PATH, pdfFileName);

        Log.v("stage 1", "store the pdf in sd card");
        //t.append("store the pdf in sd card\n");

        Document document = new Document(PageSize.A4, 38, 38, 50, 38);

        Log.v("stage 2", "Document Created");
        //t.append("Document Created\n");
        Rectangle documentRect = document.getPageSize();

        try {

            PdfWriter pdfW = PdfWriter.getInstance(document, new FileOutputStream(outputFile.getPath()));
            pdfW.setFullCompression();

            Log.v("Stage 3", "Pdf writer");
            //  t.append("Pdf writer\n");

            document.open();

            Log.v("Stage 4", "Document opened");
            // t.append("Document opened\n");

            // resizeBitmaptoWidth(document.getPageSize().getWidth() - 76);
            // Bitmap[] files = joinAndSplitBitmapsbyA4Height(
            // document.getPageSize().getHeight() - 88);
            if (files[0].getHeight() / (float) files[0].getWidth() > 1.414) {
                files = splitIntoPages();
            }
            for (int i = 0; i < files.length; i++) {
                Log.v("Stage 4.1", "" + i);

                Bitmap bmp = files[i];

                // context.getContentResolver().delete(files.get(i), null, null);

//                bmp = ImagePicker.getResizedBitmap(bmp, (int) PageSize.A3.getWidth(), (int) PageSize.A3.getHeight());
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 80, stream);

                Image image = Image.getInstance(stream.toByteArray());

                float a4Scale = PageSize.A4.getHeight() / PageSize.A4.getWidth();
                float imageScale = (float) bmp.getHeight() / (float) bmp.getWidth();
                float scaler;
                if (a4Scale > imageScale) {
                    scaler = ((document.getPageSize().getWidth()) / bmp.getWidth()) * 100;
                } else {
                    scaler = ((document.getPageSize().getHeight()) / bmp.getHeight()) * 100;
                }
                image.scalePercent(scaler);
                image.setBorder(Image.BOX);
                image.setBorderWidth(1f);

                Log.v("Stage 6", "Image path adding");

                image.setAbsolutePosition(0, documentRect.getHeight() - image.getScaledHeight());

                Log.e("TAG", "doInBackground: " + (documentRect.getWidth() - image.getScaledWidth()) / 2);
                Log.e("TAG", "doInBackground: " +
                        (documentRect.getHeight() - image.getScaledHeight()) / 2);

                /*image.setAbsolutePosition((documentRect.getWidth() - image.getScaledWidth()) / 2,
                        (documentRect.getHeight() - image.getScaledHeight()) / 2);
                */
                Log.v("Stage 7", "Image Alignments");

//                image.setBorder(Image.BOX);
//
//                image.setBorderWidth(15);
                image.setCompressionLevel(8);
                document.add(image);

                document.newPage();

                publishProgress(i);

            }

            Log.v("Stage 8", "Image adding");
            // t.append("Image adding\n");

            document.close();

            Log.v("Stage 7", "Document Closed" + outputFile.getPath());
            //   t.append("Document Closed\n");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        document.close();

        return outputFile;
    }

    private Bitmap[] splitIntoPages() {
        int bitmapWidth = files[0].getWidth();
        int bitmapHeight = files[0].getHeight();
        double hwr = 2;

        double newHeight = bitmapWidth * hwr;

        int pages = (int) Math.ceil(bitmapHeight / newHeight);

        Bitmap[] newBitmaps = new Bitmap[pages];
        for (int i = 0; i < pages; i++) {
            if (i < pages - 1) {

            }
            int startHeight;
            if (i == 0) {
                startHeight = 0;
            } else {
                startHeight = (int) (i * newHeight);
                startHeight -= 50;
            }
            if (bitmapHeight - startHeight < newHeight) {
                newHeight = bitmapHeight - startHeight;
            }
            newBitmaps[i] = Bitmap.createBitmap(files[0],
                    0,
                    startHeight,
                    bitmapWidth,
                    (int) newHeight);
        }
        return newBitmaps;
    }

    private Bitmap[] joinAndSplitBitmapsbyA4Height(float height) {
        int w = 0, h = 0;
        for (Bitmap bitmap : files) {
            w = bitmap.getWidth();
            h += bitmap.getHeight();
        }
        Bitmap result = Bitmap.createBitmap(w,
                h,
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(result);
        canvas.drawColor(Color.WHITE);

        Paint paint = new Paint();
        w = 0;
        h = 0;
        for (Bitmap file : files) {
            canvas.drawBitmap(file,
                    w,
                    h,
                    paint);
            // w = file.getWidth();
            h += file.getHeight();
        }
        int totalRows = result.getHeight();

        int pages = (int) Math.ceil(totalRows / height);
        Bitmap[] newBitmap = new Bitmap[pages];
        for (int i = 0; i < pages; i++) {
            if (totalRows > height) {
                totalRows -= height;
                totalRows += 20;
            } else {
                height = totalRows;
            }

            newBitmap[i] = Bitmap.createBitmap(result,
                    0,
                    (int) (i * (height - 20)),
                    result.getWidth(),
                    (int) height);
        }
        return newBitmap;
    }

    private void resizeBitmaptoWidth(float width) {
        for (int i = 0; i < files.length; i++) {
            float aspectRatio = files[i].getWidth() /
                    (float) files[i].getHeight();
            int height = Math.round(width / aspectRatio);

            files[i] = Bitmap.createScaledBitmap(
                    files[i], (int) width, height, false);
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        progressDialog.setProgress((values[0] + 1) * 100 / files.length);
        progressDialog.setTitle("Processing images (" + (values[0] + 1) + "/" + files.length + ")");

    }
}