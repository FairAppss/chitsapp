package com.fairsoft.chitfund.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fairsoft.chitfund.R;
import com.fairsoft.chitfund.api.Urls;
import com.fairsoft.chitfund.model.LoginResponseModelItem;

import java.util.ArrayList;
import java.util.List;

public class SpashActivity extends AppCompatActivity {
    private List<LoginResponseModelItem> loginResponseModelItems;
    private String json;
    String rest;
    String jsonStr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spash);

        postDeviceIdDataUsingVolley(getDeviceId());

    }

    private void postDeviceIdDataUsingVolley(String devideid) {
        RequestQueue queue = Volley.newRequestQueue(SpashActivity.this);

        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_Login_DEVID_ID+"/"+devideid, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                loginResponseModelItems = new ArrayList<LoginResponseModelItem>();

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    if (rest.equals("0")) {

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(SpashActivity.this);
                        builder1.setMessage(" Your Device  " + devideid + "  Not  Registered");
                        builder1.setCancelable(false);
                        builder1.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });


                        AlertDialog alert11 = builder1.create();
                        alert11.show();


                    }else {
                        Intent i = new Intent(SpashActivity.this,LoginActivity.class);
                        i.putExtra("userNmae", response.toString());
                    }


                } catch (Exception e) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(SpashActivity.this);
                    builder1.setMessage(" Your Device  " + devideid + "  Not  Registered");
                    builder1.setCancelable(false);

                    builder1.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });


                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }


            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(SpashActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                Log.e("error is", error.toString());
            }
        });

        queue.add(request);


    }

    private String getDeviceId() {
        // testing code
        if (true) {
            return "c5f8b70f653852cd";
        }
        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return android_id;
    }





}