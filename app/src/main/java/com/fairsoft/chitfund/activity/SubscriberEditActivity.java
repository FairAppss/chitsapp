package com.fairsoft.chitfund.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fairsoft.chitfund.R;

import com.fairsoft.chitfund.adapter.SubscriberAdapter;
import com.fairsoft.chitfund.api.Urls;

import com.fairsoft.chitfund.model.SubscriberDetailsModelItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SubscriberEditActivity extends AppCompatActivity implements SubscriberAdapter.onItemClickListener {
    SubscriberAdapter subscriberAdapter;
    RecyclerView recyclerView;
    private String json;
    String rest;
    String jsonStr;
    CardView editCardView;
    Integer chitIdStr;


    private List<SubscriberDetailsModelItem> subscriberDetailsList;
    String spChitCode, spMemberId, spDate;
    String compId, userId, userName, compName;
    SharedPreferences pref;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscriber_edit);
        getSupportActionBar().setTitle(" Subscriber Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();


        compId = String.valueOf(pref.getInt("compId", 0));
        userId = String.valueOf(pref.getInt("userId", 0));
        userName = String.valueOf(pref.getString("userName", null));
        compName = String.valueOf(pref.getString("compName", null));


        editCardView = findViewById(R.id.editCardView);
        editCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubscriberEditActivity.this, SubscriberActivity.class);
                startActivity(intent);
            }
        });


        getSubScriberDetails(chitIdStr);
    }

    private void getSubScriberDetails(Integer chitId) {
        RequestQueue queue = Volley.newRequestQueue(SubscriberEditActivity.this);
        // api/Chits/SubscriberDetails/{compid}/{userid}/{chitid}/{subscriberid}
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_SUBSCRIBER_DETAILS + "/" + compId + "/" + userId + "/" + 1 + "/" + 0, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                subscriberDetailsList = new ArrayList<SubscriberDetailsModelItem>();

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
                        SubscriberDetailsModelItem memeberDefine = new SubscriberDetailsModelItem();
                        JSONObject object = (JSONObject) array.get(i);
                        memeberDefine.setReference(object.getString("reference"));
                        memeberDefine.setChitid(object.getInt("chitid"));
                        memeberDefine.setCreatedby(object.getInt("createdby"));
                        memeberDefine.setCreateddate(object.getString("createddate"));
                        memeberDefine.setCity(object.getString("city"));
                        memeberDefine.setChitcode(object.getString("chitcode"));
                        memeberDefine.setName(object.getString("member"));
                        memeberDefine.setSubscriberid(object.getInt("subscriberid"));
                        memeberDefine.setMemberid(object.getInt("memberid"));
                        memeberDefine.setStatus(object.getString("status"));
                        subscriberDetailsList.add(memeberDefine);
                    }


                } catch (Exception e) {
                    Toast.makeText(SubscriberEditActivity.this, "error is" + e.toString(), Toast.LENGTH_SHORT).show();

                }
              /*  recyclerView = findViewById(R.id.subscriberRecyclerView);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SubscriberEditActivity.this);
                recyclerView.setLayoutManager(linearLayoutManager);
                subscriberAdapter = new SubscriberAdapter(SubscriberEditActivity.this, subscriberDetailsList, SubscriberEditActivity.this::onItemClick,SubscriberActivity.);
                recyclerView.setAdapter(subscriberAdapter);
*/
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(SubscriberEditActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        queue.add(request);

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_menu, menu);
        return true;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logOut) {
            Intent intent1 = new Intent(SubscriberEditActivity.this, LoginActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
            startActivity(intent1);
            finish();


        }
        return super.onOptionsItemSelected(item);


    }

    @Override
    public void onItemClick(Integer chitId, Integer subScriberId, String chitCode,String member) {

    }
}