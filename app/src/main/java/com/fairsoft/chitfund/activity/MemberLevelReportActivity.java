package com.fairsoft.chitfund.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fairsoft.chitfund.imagestopdf.PDFEngine;
import com.fairsoft.chitfund.R;

import com.fairsoft.chitfund.adapter.MemberLevelReportAdapter;
import com.fairsoft.chitfund.api.Urls;

import com.fairsoft.chitfund.model.MemberLevelReportModelItem;
import com.fairsoft.chitfund.model.MemberModelItem1;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MemberLevelReportActivity extends AppCompatActivity {
    AutoCompleteTextView autoCompleteTextView;
    Spinner select_member;
    Integer chitIdStr;
    Integer chitCodePos;
    String chitCodeStr;
    Integer chitIdStrUp;
    private String json;
    String rest;
    String jsonStr;
    ArrayAdapter chitaa;
    private List<MemberLevelReportModelItem> memberLevelReportModelItems;
    RecyclerView recyclerView;
    MemberLevelReportAdapter memberLevelReportAdapter;
    TextView nodataTxt;
    LinearLayout totalLL;
    String compId, userId, userName, compName;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String date,time;
    public final static String PREFS_NAME = "my_prefs";
    File pdfFile = null;
    Button btn_send_wa_all_print, btn_send_mail_all_print, btn_save_all_print,send, sendWhatsapp, btnSendMultiple;
    String EMAIL = "email";
    File finalPdfFile;
    File finalPdfFile1;
    String fileName;
    Uri uri;

    ProgressDialog pd;
    String encoded;
    public static final int MY_DEFAULT_TIMEOUT = 15000;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_level_report);
        getSupportActionBar().setTitle("Member Level Reports");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();

        compId = String.valueOf(pref.getInt("compId", 0));
        userId = String.valueOf(pref.getInt("userId", 0));
        userName = String.valueOf(pref.getString("userName", null));
        compName = String.valueOf(pref.getString("compName", null));
        select_member = findViewById(R.id.select_member);
        nodataTxt = findViewById(R.id.noDataMember);
        totalLL = findViewById(R.id.totalLL);

        btn_send_wa_all_print = findViewById(R.id.btn_send_wa_all_print);
        btn_send_mail_all_print = findViewById(R.id.btn_send_mail_all_print);
        btn_save_all_print = findViewById(R.id.btn_save_all_print);
/*
        autoCompleteTextView=findViewById(R.id.autoCompleteTextView);
*/

        send = findViewById(R.id.send);
        sendWhatsapp = findViewById(R.id.sendWhatsapp);
        btnSendMultiple = findViewById(R.id.btnSendMultiple);


        getMemberid();
        btn_send_wa_all_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finalPdfFile == null) {
                    Toast.makeText(MemberLevelReportActivity.this, "PDF File Not Found Click On Create Pdf", Toast.LENGTH_SHORT).show();
                    return;
                }

                Uri uri = FileProvider.getUriForFile(MemberLevelReportActivity.this, getApplication().getPackageName() + ".provider", finalPdfFile);
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("application/pdf");
                share.putExtra(Intent.EXTRA_STREAM, uri);
                share.setPackage("com.whatsapp");
                share.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                startActivity(share);
            }
        });
        btn_send_mail_all_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finalPdfFile1 == null) {
                    Toast.makeText(MemberLevelReportActivity.this, "PDF File Not Found Click On Create Pdf", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setType("application/pdf");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
                        new String[]{getStr(EMAIL)});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Report Generated from Day Sheet App");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
                emailIntent.putExtra(Intent.EXTRA_STREAM,
                        // Uri.fromFile(file));
                        FileProvider.getUriForFile(MemberLevelReportActivity.this, getPackageName() + ".provider", finalPdfFile1));
                emailIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            }
        });
        btn_save_all_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkCheckPermission()) {
                    // savePetrolData();
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    createPDF();

                }
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*
                String numberTxt = mobileNumber.getText().toString();
*/

                addContact("918142323365");
            }
        });

        sendWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*
                String numberTxt = mobileNumber.getText().toString();
*/


                sendMsgToWhatsapp("918142323365");
            }
        });
        btnSendMultiple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*
                String numberTxt = mobileNumber.getText().toString();
*/

                if (finalPdfFile == null) {
                    Toast.makeText(MemberLevelReportActivity.this, "PDF File Not Found Click On Create Pdf", Toast.LENGTH_SHORT).show();
                    return;
                } else {


                    encoded = encodeFileToBase64Binary(finalPdfFile);
/*
                    if (encoded != null) {
                        uploadDocs(encoded);
                    }
*/
                }

                uri = FileProvider.getUriForFile(MemberLevelReportActivity.this, getApplication().getPackageName() + ".provider", finalPdfFile);

                sendMsgToMultipleWhatsapp("918142323365", String.valueOf(encoded));
            }
        });

    }

    class MemberSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            // Toast.makeText(v.getContext(), "Your choose :" + chit[position], Toast.LENGTH_SHORT).show();
            chitIdStr = select_member.getSelectedItemPosition();
            chitCodeStr = select_member.getItemAtPosition(position).toString();
            chitCodePos = select_member.getSelectedItemPosition();
            MemberModelItem1 chit = (MemberModelItem1) parent.getSelectedItem();
            chitIdStrUp = chit.getMemberid();
            getMemberReportsDetails(chitIdStrUp);


        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private void getMemberid() {
        RequestQueue queue = Volley.newRequestQueue(MemberLevelReportActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_MEMBER_DETAILS + "/" + compId + "/" + userId + "/" + "0", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // chitDetailsList = new ArrayList<ChitCompleDetailsModelItem>();
                ArrayList<MemberModelItem1> memberList = new ArrayList<>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {


                        JSONObject object = (JSONObject) array.get(i);

                        String membername = object.getString("name");
                        Integer memberid = object.getInt("memberid");
                        memberList.add(new MemberModelItem1(memberid, membername));

                    }
                 /*   MemberAutoCompleteCountryAdapter adapter = new MemberAutoCompleteCountryAdapter(MemberLevelReportActivity.this, memberList);
                    autoCompleteTextView.setAdapter(adapter);


                    autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            chitIdStrUp = adapter.getItem(i).getMemberid();
                            getMemberReportsDetails(chitIdStrUp);



                        }
                    });*/


                    chitaa = new ArrayAdapter(MemberLevelReportActivity.this, android.R.layout.simple_spinner_item, memberList);
                    chitaa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    //Setting the ArrayAdapter data on the Spinner
                    select_member.setAdapter(chitaa);
                    select_member.setOnItemSelectedListener(new MemberLevelReportActivity.MemberSpinnerClass());

                } catch (Exception e) {
                    Toast.makeText(MemberLevelReportActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(MemberLevelReportActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy( MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }

    private void getMemberReportsDetails(Integer chitId) {
        RequestQueue queue = Volley.newRequestQueue(MemberLevelReportActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_MEMBER_LEVEL_REPORTS + "/" + compId + "/" + userId + "/" + chitId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                memberLevelReportModelItems = new ArrayList<MemberLevelReportModelItem>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                 /*   String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");*/
                    if (rest.equals("")) {
                        nodataTxt.setVisibility(View.VISIBLE);


                    } else {
                        jsonStr = rest.replace("\\", "");
                        JSONArray array = new JSONArray(jsonStr);
                        for (int i = 0; i < array.length(); i++) {
                            MemberLevelReportModelItem auctionDefine = new MemberLevelReportModelItem();
                            JSONObject object = (JSONObject) array.get(i);
                            auctionDefine.setReceivables(object.getString("Receivables"));
                            auctionDefine.setReceived(object.getString("Received"));
                            auctionDefine.setRecPending(object.getString("RecPending"));
                            auctionDefine.setPaid(object.getString("Paid"));
                            auctionDefine.setPayable(object.getString("Payable"));
                            auctionDefine.setPayPending(object.getString("PayPending"));
                            auctionDefine.setChitCode(object.getString("ChitCode"));
                            memberLevelReportModelItems.add(auctionDefine);

                        }
                    }
                    nodataTxt.setVisibility(View.GONE);


                } catch (Exception e) {

/*
                    nodataTxt.setVisibility(View.VISIBLE);
*/
/*
                    totalLL.setVisibility(View.GONE);
*/

                    Toast.makeText(MemberLevelReportActivity.this, "error is" + e.toString(), Toast.LENGTH_SHORT).show();

                }
                if (memberLevelReportModelItems.isEmpty()) {
                    nodataTxt.setVisibility(View.VISIBLE);
                }

                recyclerView = findViewById(R.id.chitLevelRecyclerView);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MemberLevelReportActivity.this);
                recyclerView.setLayoutManager(linearLayoutManager);
                memberLevelReportAdapter = new MemberLevelReportAdapter(MemberLevelReportActivity.this, memberLevelReportModelItems);
                recyclerView.setAdapter(memberLevelReportAdapter);
                memberLevelReportAdapter.notifyDataSetChanged();


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(MemberLevelReportActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy( MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_menu, menu);
        return true;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logOut) {
            Intent intent1 = new Intent(MemberLevelReportActivity.this, LoginActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
            startActivity(intent1);
            finish();


        }
        return super.onOptionsItemSelected(item);


    }

    public String getStr(String key) {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
        return prefs.getString(key, "");
    }


    private boolean checkCheckPermission() {
        if (ContextCompat.checkSelfPermission(
                MemberLevelReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                MemberLevelReportActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            // You can use the API that requires the permission.
            return true;
        } else {
            // You can directly ask for the permission.
            // The registered ActivityResultCallback gets the result of this request.
            ActivityCompat.requestPermissions(MemberLevelReportActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1011);
            return false;
        }
    }

    private void createPDF() {
        date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        time = new SimpleDateFormat("HH-mm-ss", Locale.getDefault()).format(new Date());

        View u = findViewById(R.id.scrollPdf);

        HorizontalScrollView z = findViewById(R.id.scrollPdf);
        int totalHeight = z.getChildAt(0).getHeight();
        int totalWidth = z.getChildAt(0).getWidth();

        Bitmap bitmap = takeScreenShot(u, totalHeight, totalWidth);
/*
        Bitmap bitmap = takeScreenShot();
*/
        Bitmap[] bitmaps = new Bitmap[]{bitmap};
        fileName = "MemberLevelReport_" + date +"-"+time + ".pdf";


        PDFEngine.getInstance().createPDF(MemberLevelReportActivity.this,
                bitmaps,
                fileName,
                (pdfFile, numOfImages) -> {
                    if (pdfFile == null) {
                        Toast.makeText(MemberLevelReportActivity.this, "Error in PDF Generation Error.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Toast.makeText(MemberLevelReportActivity.this, "PDF File generated", Toast.LENGTH_SHORT).show();
                    finalPdfFile = pdfFile;

                    PDFEngine.getInstance().openPDF(MemberLevelReportActivity.this,finalPdfFile);

                    btn_send_wa_all_print.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (finalPdfFile == null) {
                                Toast.makeText(MemberLevelReportActivity.this, "PDF File Not Found", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            Uri uri = FileProvider.getUriForFile(MemberLevelReportActivity.this, getApplication().getPackageName() + ".provider", finalPdfFile);
                            Intent share = new Intent(Intent.ACTION_SEND);
                            share.setType("application/pdf");
                            share.putExtra(Intent.EXTRA_STREAM, uri);
                            share.setPackage("com.whatsapp");
                            share.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                            startActivity(share);
                        }
                    });
                    finalPdfFile1 = pdfFile;
                    btn_send_mail_all_print.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (finalPdfFile1 == null) {
                                Toast.makeText(MemberLevelReportActivity.this, "PDF File Not Found", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                            emailIntent.setType("application/pdf");
                            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
                                    new String[]{getStr(EMAIL)});
                            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Report Generated from Day Sheet App");
                            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
                            emailIntent.putExtra(Intent.EXTRA_STREAM,
                                    // Uri.fromFile(file));
                                    FileProvider.getUriForFile(MemberLevelReportActivity.this, getPackageName() + ".provider", finalPdfFile1));
                            emailIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                        }
                    });


                    pdfFile = pdfFile;
                    // PDFEngine.getInstance().sharePDF(MainActivity.this, pdfFile);
                });
    }

    protected Bitmap takeScreenShot(View view, int totalHeight, int totalWidth) {

        Bitmap returnedBitmap = Bitmap.createBitmap(totalWidth, totalHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return returnedBitmap;


    }

    private void addContact(String number) {

        RequestQueue queue = Volley.newRequestQueue(MemberLevelReportActivity.this);
        StringRequest request = new StringRequest(Request.Method.GET, Urls.URL_WHATSAPP_CONTACT_ADD + "/" + number + "/" + "Rajitha", new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Toast.makeText(MemberLevelReportActivity.this, response.toString(), Toast.LENGTH_SHORT).show();


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(MemberLevelReportActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy( MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void sendMsgToWhatsapp(String number) {

        RequestQueue queue = Volley.newRequestQueue(MemberLevelReportActivity.this);
        StringRequest request = new StringRequest(Request.Method.GET, Urls.URL_SEND_WHATSAP + "/" + number + "/" + "Rajitha" + "/" + "Hello", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(MemberLevelReportActivity.this, response.toString(), Toast.LENGTH_SHORT).show();

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(MemberLevelReportActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {


        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy( MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void sendMsgToMultipleWhatsapp(String number, String binaryString) {


        RequestQueue queue = Volley.newRequestQueue(MemberLevelReportActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_SEND_WHATSAP_FILE, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(MemberLevelReportActivity.this, response.toString(), Toast.LENGTH_SHORT).show();

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(MemberLevelReportActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {


            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("WANo", "918142323365");
                params.put("msg", "hello");
                params.put("fileName", String.valueOf(fileName));
                params.put("filebinary", String.valueOf(binaryString));


                return params;
            }


        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy( MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private String encodeFileToBase64Binary(File yourFile) {
        int size = (int) yourFile.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(yourFile));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        String encoded = Base64.encodeToString(bytes, Base64.NO_WRAP);
        appendLog(encoded);
        return encoded;
    }

    public static void appendLog(String text) {
        File logFile = new File(android.os.Environment.getExternalStorageDirectory(), "log.txt");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {

                e.printStackTrace();
            }
        }
        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }





}