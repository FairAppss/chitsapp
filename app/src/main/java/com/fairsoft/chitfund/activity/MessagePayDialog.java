package com.fairsoft.chitfund.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fairsoft.chitfund.api.Urls;
import com.fairsoft.chitfund.model.SubscriberModelItem;
import com.fairsoft.chitfund.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;


public class MessagePayDialog extends DialogFragment {
    View view;
    ImageView closeImageView;
    Integer customerId;
    String mobileList, chitCodep,auctionamountp,auctionDatep,phonestr,nameStr,name;
    TextView noData;
    ProgressBar progressBar;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    CardView clearCardView;
    public static final int MY_DEFAULT_TIMEOUT = 15000;
    ArrayList<SubscriberModelItem> memberList1;
    CardView whatsappButton,messageCardView;



    public interface onInputListener {
        void sendInput(String input);
    }

    public onInputListener inputListener;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_rec_dialog, null, false);
        pref = getActivity().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        if (getArguments() != null) {
            mobileList = getArguments().getString("mobileList", "");
            chitCodep = getArguments().getString("chitCodep", "");
            name = getArguments().getString("name", "");

            auctionamountp = getArguments().getString("auctionamountp", "");
            auctionDatep = getArguments().getString("auctionDatep", "");



        }
        // creating a variable for gson.
        Gson gson = new Gson();

        // below line is to get to string present from our
        // shared prefs if not present setting it as null.
        String json = pref.getString("openCashModel", null);

        // below line is to get the type of our array list.

        // in below line we are getting data from gson
        // and saving it to our array list

        // checking below if the array list is empty or not


        init();


        closeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();

            }
        });
        whatsappButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    phonestr = "91" + mobileList;
/*
                phonestr = "91" + "8142323365";
*/

                nameStr = name;
                    String messagestr = "Payment Amount For this" + " " + chitCodep + " " + auctionamountp + " " + "on" + " " + auctionDatep;

                openWhatsApp(phonestr.toString(), messagestr, nameStr);

/*
                    sendSmsMsgFnc(phonestr,messagestr);
*/


            }
        });
        messageCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    phonestr = "91" + mobileList;
/*
                phonestr = "91" + "8074051219";
*/

                nameStr = name;
                    String messagestr = "Payment Amount For this" + " " + chitCodep + " " + auctionamountp + " " + "on" + " " + auctionDatep;

/*
                    sendMsgToWhatsapp(phonestr.toString(), messagestr, nameStr);
*/

                    sendSmsMsgFnc(phonestr,messagestr);


            }
        });




        return view;
    }
    void sendSmsMsgFnc(String mblNumVar, String smsMsgVar)
    {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED)
        {
            try
            {
                SmsManager smsMgrVar = SmsManager.getDefault();
                smsMgrVar.sendTextMessage(mblNumVar, null, smsMsgVar, null, null);
                getDialog().dismiss();

/*
                Toast.makeText(AuctionActivity.this, "Message Sent",
                        Toast.LENGTH_LONG).show();
*/
            }
            catch (Exception ErrVar)
            {
                Toast.makeText(getActivity(),ErrVar.getMessage().toString(),
                        Toast.LENGTH_LONG).show();
                ErrVar.printStackTrace();
            }
        }
        else
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                requestPermissions(new String[]{Manifest.permission.SEND_SMS}, 10);
            }
        }

    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = 600;
            dialog.getWindow().setLayout(width, height);
        }
    }


    public void init() {
        closeImageView = view.findViewById(R.id.closeImageView);
        progressBar = view.findViewById(R.id.progressBar);
        whatsappButton=view.findViewById(R.id.whatsappButton);
        messageCardView=view.findViewById(R.id.messageCardView);

    }
    private void sendMsgToWhatsapp(String number, String message, String name) {

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_SEND_AUCTION_WHATSAP, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                getDialog().dismiss();



            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // below line we are creating a map for
                // storing our values in key and value pair.
                Map<String, String> params = new HashMap<String, String>();

                // on below line we are passing our key
                // and value pair to our parameters.

                params.put("WANo", String.valueOf(number));
                params.put("name", name);
                params.put("msg", String.valueOf(message));


                // at last we are
                // returning our params.
                return params;
            }


        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }
    public void openWhatsApp(String number, String message, String name) {
        try {
            String text = message;// Replace with your message.

            String toNumber = number; // Replace with mobile phone number without +Sign or leading zeros, but with country code
            //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.


            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + toNumber + "&text=" + text));
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            inputListener = (onInputListener) getActivity();

        } catch (ClassCastException e) {

        }
    }


    public static boolean checkNetworkAndShowError(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean isConnected = cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
        if (!isConnected) {
            showNetWorkError(context);
        }

        return isConnected;
    }

    private static void showNetWorkError(Context context) {
        new AlertDialog.Builder(context)
                .setPositiveButton("ok", null)
                .setMessage("Please check Internet Connection!!")
                .show();

    }


}