package com.fairsoft.chitfund.activity;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AlertDialog;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Consts {

    public interface Prefs {
        String USER_NAME = "USERNAME";
        String SUB_CODE = "S_ID";
        String USER_ID = "USER_ID";
        String MAIN_COMPANY_ID = "main_c_ID";
        String ROLE = "ROLE";
        String FLASH = "FLASH";

        String ONCE = "once";

    }

    public static void appendLog(String text) {
        File logFile = new File(android.os.Environment.getExternalStorageDirectory(), "log.txt");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {

                e.printStackTrace();
            }
        }
        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

//    public static int a4pageWidth = (int) (596 * 1);
//    public static int a4pageHeight = (int) (842 * 1);



    public static boolean checkNetworkAndShowError(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean isConnected = cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
        if (!isConnected) {
            showNetWorkError(context);
        }
        return isConnected;
    }

    private static void showNetWorkError(Context context) {
        new AlertDialog.Builder(context)
                .setPositiveButton("ok", null)
                .setMessage("Please check Internet Connection!!")
                .show();
    }
}
