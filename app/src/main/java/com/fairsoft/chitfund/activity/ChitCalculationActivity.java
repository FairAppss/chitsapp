package com.fairsoft.chitfund.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fairsoft.chitfund.R;
import com.fairsoft.chitfund.adapter.ChitBidCalculationAdapter;
import com.fairsoft.chitfund.adapter.ChitInterestCalculationAdapter;

import java.text.DecimalFormat;
import java.util.ArrayList;

import java.util.List;


public class ChitCalculationActivity extends AppCompatActivity {
    Button bidBtn, interestBtn, bidCalCulate, interestCalculate, clearBidTxt, clearIntTxt, cancel_button;
    Spinner incrementSpn, incrementIntSpn;
    EditText bidAmt, bidMnths, bidCutMnths, bidCommn, bidFrom, bidTo;
    String bidAmtStr, bidMnthsStr, bidCutMnthStr, bidCommnStr, bidFromStr, bidToStr;
    Double resultInterest, bidCalcStr, bidCommnStrResult, interestCommnStrResult, interestCalcStr, incrementbyIntType, resultInt;
    String interestAmtStr, interestMnthsStr, interestCutMnthStr, interestCommnStr, interestFromStr, interestToStr, interestStr;
    EditText interestAmt, interestMnths, interestCutMnths, interestCommn, interestFrom, interestTo;
    String[] increment = {"1000", "500", "100"};
    String[] incrementbyInt = {".1", ".2", ".5"};
    double num1, num2;

    String incrementType;
    LinearLayout bidLL, interestLL;
    TextView bidTxt, interestTxt;
    private List<Double> monthlistData = new ArrayList<Double>();
    private List<Double> interestBid = new ArrayList<Double>();

    private List<Double> interestlistData = new ArrayList<Double>();
    private List<Double> bidInterest = new ArrayList<Double>();


    RecyclerView recyclerView;
    ChitBidCalculationAdapter chitBidCalculationAdapter;
    ChitInterestCalculationAdapter chitInterestCalculationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chit_calculation);
        getSupportActionBar().setTitle("Chit Calculator");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();

        bidBtn.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                Drawable buttonDrawable = bidBtn.getBackground();
                buttonDrawable = DrawableCompat.wrap(buttonDrawable);
                //the color is a direct color int and not a color resource
                DrawableCompat.setTint(buttonDrawable, Color.RED);
                bidBtn.setBackground(buttonDrawable);


                Drawable buttonDrawable1 = interestBtn.getBackground();
                buttonDrawable1 = DrawableCompat.wrap(buttonDrawable1);
                //the color is a direct color int and not a color resource
                DrawableCompat.setTint(buttonDrawable1, Color.GRAY);
                interestBtn.setBackground(buttonDrawable1);
                bidLL.setVisibility(View.VISIBLE);
                interestLL.setVisibility(View.GONE);


            }
        });

        interestBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                Drawable buttonDrawable = interestBtn.getBackground();
                buttonDrawable = DrawableCompat.wrap(buttonDrawable);
                //the color is a direct color int and not a color resource
                DrawableCompat.setTint(buttonDrawable, Color.RED);
                interestBtn.setBackground(buttonDrawable);

                Drawable buttonDrawable1 = bidBtn.getBackground();
                buttonDrawable1 = DrawableCompat.wrap(buttonDrawable1);
                //the color is a direct color int and not a color resource
                DrawableCompat.setTint(buttonDrawable1, Color.GRAY);
                bidBtn.setBackground(buttonDrawable1);
                bidLL.setVisibility(View.GONE);
                interestLL.setVisibility(View.VISIBLE);


            }
        });

        //adding Increment spinner
        ArrayAdapter selectAuction = new ArrayAdapter(this, R.layout.spinner_text, increment);
        selectAuction.setDropDownViewResource(R.layout.spinner_text);
        incrementSpn.setAdapter(selectAuction);
        incrementSpn.setOnItemSelectedListener(new IncrementSpinnerClass());

        //adding Increment spinner
        ArrayAdapter selectIncremeent = new ArrayAdapter(this, R.layout.spinner_text, incrementbyInt);
        selectIncremeent.setDropDownViewResource(R.layout.spinner_text);
        incrementIntSpn.setAdapter(selectIncremeent);
        incrementIntSpn.setOnItemSelectedListener(new IncrementbyIntSpinnerClass());

        bidCalCulate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bidAmtStr = bidAmt.getText().toString();
                bidMnthsStr = bidMnths.getText().toString();
                bidCutMnthStr = bidCutMnths.getText().toString();
                bidFromStr = bidFrom.getText().toString();
                bidToStr = bidTo.getText().toString();
                bidCommnStr = bidCommn.getText().toString();
                validateBidFields();


            }
        });
        interestCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interestAmtStr = interestAmt.getText().toString();
                interestMnthsStr = interestMnths.getText().toString();
                interestCutMnthStr = interestCutMnths.getText().toString();
                interestFromStr = interestFrom.getText().toString();
                interestToStr = interestTo.getText().toString();
                interestCommnStr = interestCommn.getText().toString();
                interestStr = interestFromStr + incrementbyIntType;
                validateInterestFields();


            }


        });
        clearBidTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bidAmt.setText("");
                bidMnths.setText("");
                bidCutMnths.setText("");
                bidFrom.setText("");
                bidTo.setText("");
                bidCommn.setText("");


            }
        });
        clearIntTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interestAmt.setText("");
                interestMnths.setText("");
                interestCutMnths.setText("");
                interestFrom.setText("");
                interestTo.setText("");
                interestCommn.setText("");


            }
        });


    }

    public void openInterestDialog() {
        AlertDialog.Builder alertDialog = new
                AlertDialog.Builder(this);
        alertDialog.setCancelable(true);
        alertDialog.setPositiveButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //put your code that needed to be executed when okay is clicked
                        dialog.cancel();
                        interestlistData.clear(); // clear list
                        bidInterest.clear(); // clear list
                        chitInterestCalculationAdapter.notifyDataSetChanged();


                    }
                });
        View rowList = getLayoutInflater().inflate(R.layout.activity_interest_result_dialog, null);
        recyclerView = rowList.findViewById(R.id.interestRecycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ChitCalculationActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        chitInterestCalculationAdapter = new ChitInterestCalculationAdapter(ChitCalculationActivity.this, interestlistData, bidInterest);
        recyclerView.setAdapter(chitInterestCalculationAdapter);
        chitInterestCalculationAdapter.notifyDataSetChanged();


        alertDialog.setView(rowList);
        AlertDialog dialog = alertDialog.create();
        dialog.show();
    }

    public void openBidDialog() {
        AlertDialog.Builder alertDialog = new
                AlertDialog.Builder(this);
        alertDialog.setCancelable(true);
        alertDialog.setPositiveButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //put your code that needed to be executed when okay is clicked
                        dialog.cancel();
                        monthlistData.clear(); // clear list
                        interestBid.clear(); // clear list
                        chitBidCalculationAdapter.notifyDataSetChanged();


                    }
                });
        View rowList = getLayoutInflater().inflate(R.layout.activity_bid_result_dialog, null);
        recyclerView = rowList.findViewById(R.id.bidRecycler);
        chitBidCalculationAdapter = new ChitBidCalculationAdapter(ChitCalculationActivity.this, monthlistData, interestBid);
        recyclerView.setAdapter(chitBidCalculationAdapter);
        chitBidCalculationAdapter.notifyDataSetChanged();
        alertDialog.setView(rowList);
        AlertDialog dialog = alertDialog.create();
        dialog.show();


    }

    public void init() {
        bidBtn = findViewById(R.id.bidBtn);
        interestBtn = findViewById(R.id.interestBtn);
        incrementSpn = findViewById(R.id.incrementSpn);
        incrementIntSpn = findViewById(R.id.incrementIntSpn);

        bidLL = findViewById(R.id.bidLL);
        interestLL = findViewById(R.id.interestLL);
        bidCalCulate = findViewById(R.id.bidCalculate);
        interestCalculate = findViewById(R.id.interestCalculate);

        bidAmt = findViewById(R.id.bidAmt);
        bidMnths = findViewById(R.id.bidMnths);
        bidCutMnths = findViewById(R.id.bidCutMnths);
        bidFrom = findViewById(R.id.bidFrom);
        bidTo = findViewById(R.id.bidTo);
        bidCommn = findViewById(R.id.bidCommn);

        interestAmt = findViewById(R.id.interestAmnt);
        interestMnths = findViewById(R.id.interestMnths);
        interestCutMnths = findViewById(R.id.interestCutMnths);
        interestFrom = findViewById(R.id.interestFRom);
        interestTo = findViewById(R.id.interestTo);
        interestCommn = findViewById(R.id.interestCommn);

        bidTxt = findViewById(R.id.bidTxt);
        interestTxt = findViewById(R.id.interestTxt);

        recyclerView = findViewById(R.id.bidRecycler);
        clearBidTxt = findViewById(R.id.clearTxt);
        clearIntTxt = findViewById(R.id.clearIntTxt);


    }

    class IncrementSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            incrementType = incrementSpn.getItemAtPosition(position).toString();


        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }


    }

    class IncrementbyIntSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            incrementbyIntType = Double.valueOf(incrementIntSpn.getItemAtPosition(position).toString());


        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }


    }

    public void validateBidFields() {

        if (bidAmtStr.equals("")) {
            Toast.makeText(ChitCalculationActivity.this, "Please enter Amount", Toast.LENGTH_SHORT).show();

            return;
        }
        if (bidMnthsStr.equals("")) {
            Toast.makeText(ChitCalculationActivity.this, "Please enter Months", Toast.LENGTH_SHORT).show();

            return;
        }
        if (bidCutMnthStr.equals("")) {
            Toast.makeText(ChitCalculationActivity.this, "Please enter CutMonths", Toast.LENGTH_SHORT).show();

            return;
        }
        if (bidCommnStr.equals("")) {
            Toast.makeText(ChitCalculationActivity.this, "Please enter Commn", Toast.LENGTH_SHORT).show();

            return;
        }
        if (bidFromStr.equals("")) {
            Toast.makeText(ChitCalculationActivity.this, "Please enter Bid From", Toast.LENGTH_SHORT).show();

            return;
        }
        if (bidToStr.equals("")) {
            Toast.makeText(ChitCalculationActivity.this, "Please enter Bid To", Toast.LENGTH_SHORT).show();

            return;
        }

        Integer i = Integer.valueOf(incrementType);

        for (double j = Double.parseDouble(bidFromStr); j <= Double.parseDouble(bidToStr); j += i) {
            num1 = j;
            num2 = Double.valueOf(incrementType);
            monthlistData.add(j);
            bidCommnStrResult = Double.valueOf(bidAmtStr) * Double.valueOf(bidCommnStr) / 100;
            bidCalcStr = (j - Double.valueOf(bidCommnStrResult)) / (Double.valueOf(bidMnthsStr) - Double.valueOf(bidCutMnthStr)) / (Double.valueOf(bidAmtStr) - j) * 100;
            resultInt = Double.valueOf(bidCalcStr);
            DecimalFormat df = new DecimalFormat("###.######");
/*
            long l = Long.parseLong(df.format(resultInt));
*/
            interestBid.add(Double.valueOf(String.format("%.2f", resultInt)));

        }
        openBidDialog();


    }

    public void validateInterestFields() {

        if (interestAmtStr.equals("")) {
            Toast.makeText(ChitCalculationActivity.this, "Please enter Amount", Toast.LENGTH_SHORT).show();

            return;
        }
        if (interestMnthsStr.equals("")) {
            Toast.makeText(ChitCalculationActivity.this, "Please enter Months", Toast.LENGTH_SHORT).show();

            return;
        }
        if (interestCutMnthStr.equals("")) {
            Toast.makeText(ChitCalculationActivity.this, "Please enter CutMonths", Toast.LENGTH_SHORT).show();

            return;
        }

        if (interestCommnStr.equals("")) {
            Toast.makeText(ChitCalculationActivity.this, "Please enter Commn", Toast.LENGTH_SHORT).show();

            return;
        }
        if (interestFromStr.equals("")) {
            Toast.makeText(ChitCalculationActivity.this, "Please enter Interest From", Toast.LENGTH_SHORT).show();

            return;
        }
        if (interestToStr.equals("")) {
            Toast.makeText(ChitCalculationActivity.this, "Please enter Interest To", Toast.LENGTH_SHORT).show();

            return;
        }

        Double i = Double.valueOf(String.valueOf(incrementbyIntType));
        Double n = Double.parseDouble(interestToStr) + .1;

        for (double j = Double.parseDouble(interestFromStr); j <= n; j += i) {
            num1 = j;

            num2 = Double.valueOf(incrementbyIntType);
            interestlistData.add(Double.valueOf(String.format("%.2f", j)));
            interestCommnStrResult = Double.valueOf(interestAmtStr) * Double.valueOf(interestCommnStr) / 100;
            interestCalcStr = ((Double.valueOf(interestMnthsStr) - Double.valueOf(interestCutMnthStr)) * j) / (100 + (Double.valueOf(interestMnthsStr) - Double.valueOf(interestCutMnthStr)) * Double.valueOf(j)) * (Double.valueOf(interestAmtStr) - Double.valueOf(interestCommnStrResult)) + Double.valueOf(interestCommnStrResult);
            bidInterest.add(Double.valueOf(String.format("%.2f", interestCalcStr)));

        }
        openInterestDialog();


    }


}