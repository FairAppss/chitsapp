package com.fairsoft.chitfund.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.appcompat.app.AppCompatActivity;

import com.fairsoft.chitfund.R;

import java.io.File;


public class WebViewActivity extends AppCompatActivity {
    String pdfFile;
    File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        WebView web_view = findViewById(R.id.web_view);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();


        if (extras != null) {
            pdfFile = extras.getString("param_pdf_path");
            File storage = Environment.getExternalStorageDirectory();
             file = new File(storage,pdfFile);


        }
        WebSettings settings = web_view.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setAllowUniversalAccessFromFileURLs(true);
        settings.setBuiltInZoomControls(true);
        web_view.setWebChromeClient(new WebChromeClient());
        web_view.loadUrl("file:///android_asset/pdfjs/web/viewer.html?file=" + file.getAbsolutePath() + "#zoom=page-width");
    }
}