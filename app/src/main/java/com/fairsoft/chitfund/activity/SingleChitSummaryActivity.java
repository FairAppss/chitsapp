package com.fairsoft.chitfund.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.fairsoft.chitfund.api.Urls;

import com.fairsoft.chitfund.R;
import com.fairsoft.chitfund.adapter.SigleChitAdapter;
import com.fairsoft.chitfund.imagestopdf.PDFEngine;
import com.fairsoft.chitfund.model.SingleChitResponseModelItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SingleChitSummaryActivity extends AppCompatActivity {
    SigleChitAdapter sigleChitAdapter;
    RecyclerView recyclerView;
    private List<SingleChitResponseModelItem> chitDefineModelItemArrayList;
    List<Integer> install = new ArrayList<Integer>();

    private String json;
    String rest;
    String jsonStr;
    Integer chitId, position;
    TextView nodata;
    String compId, userId, userName, compName,chitCode;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Button btn_save_all_print;
    Bitmap bitmap;
    String date, time;
    File finalPdfFile;

    String fileName;
    Uri uri;
    TextView totalReceivable, totalReceived, totalPending, totalPrized, totalPayable, totalPaid, totalPayPending, chitIdTxt;
    public static final int MY_DEFAULT_TIMEOUT = 15000;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_chit_summary);
        getSupportActionBar().setTitle("Single Chit Summary");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();


        compId = String.valueOf(pref.getInt("compId", 0));
        userId = String.valueOf(pref.getInt("userId", 0));
        userName = String.valueOf(pref.getString("userName", null));
        compName = String.valueOf(pref.getString("compName", null));

        //bundle
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        if (extras != null) {
            chitId = extras.getInt("chitId");
            position = extras.getInt("position");
            chitCode=extras.getString("chitCode");

        }
        //method calling
        init();
        chitIdTxt.setText(chitCode.toString());
        postDataUsingVolley();
        btn_save_all_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkCheckPermission()) {
                    // savePetrolData();
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    createPDF();

                }

            }
        });


    }
    private boolean checkCheckPermission() {
        if (ContextCompat.checkSelfPermission(
                SingleChitSummaryActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                SingleChitSummaryActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            // You can use the API that requires the permission.
            return true;
        } else {
            // You can directly ask for the permission.
            // The registered ActivityResultCallback gets the result of this request.
            ActivityCompat.requestPermissions(SingleChitSummaryActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1011);
            return false;
        }
    }


    private void init() {
        recyclerView = (RecyclerView) findViewById(R.id.saveChiefComplaintsRecyclerView);
        nodata = findViewById(R.id.nodata);
        btn_save_all_print = findViewById(R.id.btn_save_all_print);
        totalReceivable = findViewById(R.id.totalReceivable);
        totalReceived = findViewById(R.id.totalReceived);
        totalPending = findViewById(R.id.totalPending);
        totalPrized = findViewById(R.id.totalPrized);
        totalPayable = findViewById(R.id.totalPayable);
        totalPaid = findViewById(R.id.totalPaid);
        totalPayPending = findViewById(R.id.totalPayPending);
        chitIdTxt = findViewById(R.id.chitId);


    }


    private void postDataUsingVolley() {
        RequestQueue queue = Volley.newRequestQueue(SingleChitSummaryActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_SINGLE_CHIT_SUMMARY__DETAILS + "/" + compId + "/" + userId + "/" + chitId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                chitDefineModelItemArrayList = new ArrayList<SingleChitResponseModelItem>();

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    if (rest.equals("")) {
                        nodata.setVisibility(View.VISIBLE);
                    }else {
                        String json1 = rest.replace("[", "");
                        String json2 = json1.replace("]", "");
                        jsonStr = rest.replace("\\", "");
                        JSONArray array = new JSONArray(jsonStr);
                        for (int i = 0; i < array.length(); i++) {
                            SingleChitResponseModelItem chitDefine = new SingleChitResponseModelItem();
                            JSONObject object = (JSONObject) array.get(i);
                            chitDefine.setReceivables(object.getString("Receivables"));
                            chitDefine.setMemberID(object.getString("MemberID"));
                            chitDefine.setRecPending(object.getString("RecPending"));
                            chitDefine.setPrized(object.getString("prized"));
                            chitDefine.setReceived(object.getString("Received"));
                            chitDefine.setName(object.getString("Name"));
                            chitDefine.setPayPending(object.getString("PayPending"));
                            chitDefine.setPaid(object.getString("Paid"));
                            chitDefine.setPayable(object.getString("Payable"));
                            chitDefineModelItemArrayList.add(chitDefine);


                        }
                    }


                    if (chitDefineModelItemArrayList.isEmpty()) {
                        nodata.setVisibility(View.VISIBLE);
                    }



                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SingleChitSummaryActivity.this);
                    recyclerView.setLayoutManager(linearLayoutManager);
                    sigleChitAdapter = new SigleChitAdapter(SingleChitSummaryActivity.this, chitDefineModelItemArrayList);
                    recyclerView.setAdapter(sigleChitAdapter);
                    Integer totalReceivables = 0;
                    for (int i = 0; i < chitDefineModelItemArrayList.size(); i++) {
                        totalReceivables += Integer.valueOf(chitDefineModelItemArrayList.get(i).getReceivables());
                    }

                    totalReceivable.setText(totalReceivables.toString());
                    Integer totalReceiveds = 0;
                    for (int i = 0; i < chitDefineModelItemArrayList.size(); i++) {
                        totalReceiveds += Integer.valueOf(chitDefineModelItemArrayList.get(i).getReceived());
                    }
                    totalReceived.setText(totalReceiveds.toString());
                    Integer totalPendings = 0;
                    for (int i = 0; i < chitDefineModelItemArrayList.size(); i++) {
                        String valueStr=chitDefineModelItemArrayList.get(i).getRecPending();
                        if (!valueStr.equals("")) {
                            Integer value = Integer.valueOf(chitDefineModelItemArrayList.get(i).getRecPending());
                            if (!value.equals("")) {
                                totalPendings += Integer.valueOf(chitDefineModelItemArrayList.get(i).getRecPending());
                            } else {
                                totalPendings += 0;
                            }
                        }


                    }
                    totalPending.setText(totalPendings.toString());


                    Integer totalPrizeds = 0;
                    for (int i = 0; i < chitDefineModelItemArrayList.size(); i++) {
                        Integer value=Integer.valueOf(chitDefineModelItemArrayList.get(i).getPrized());
                        if (!value.equals("")){
                            totalPrizeds += Integer.valueOf(chitDefineModelItemArrayList.get(i).getPrized());
                        }
                    }
                    totalPrized.setText(totalPrizeds.toString());

                    Integer totalPayables = 0;
                    for (int i = 0; i < chitDefineModelItemArrayList.size(); i++) {
                        totalPayables += Integer.valueOf(chitDefineModelItemArrayList.get(i).getPayable());
                    }
                    totalPayable.setText(totalPayables.toString());

                    Integer totalPaids = 0;
                    for (int i = 0; i < chitDefineModelItemArrayList.size(); i++) {
                        totalPaids += Integer.valueOf(chitDefineModelItemArrayList.get(i).getPaid());
                    }
                    totalPaid.setText(totalPaids.toString());

                    Integer totalPayPendings = 0;
                    for (int i = 0; i < chitDefineModelItemArrayList.size(); i++) {
                        totalPayPendings += Integer.valueOf(chitDefineModelItemArrayList.get(i).getPayPending());
                    }
                    totalPayPending.setText(totalPayPendings.toString());


                } catch (Exception e) {
                    Toast.makeText(SingleChitSummaryActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }
                //  nodata.setVisibility(View.GONE);

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(SingleChitSummaryActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy( MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_menu, menu);
        return true;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logOut) {
            Intent intent1 = new Intent(SingleChitSummaryActivity.this, LoginActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
            startActivity(intent1);
            finish();


        }
        return super.onOptionsItemSelected(item);


    }

    private void createPDF() {
        date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        time = new SimpleDateFormat("HH-mm-ss", Locale.getDefault()).format(new Date());

        View u = findViewById(R.id.scrollPdf);

        HorizontalScrollView z = findViewById(R.id.scrollPdf);
        int totalHeight = z.getChildAt(0).getHeight();
        int totalWidth = z.getChildAt(0).getWidth();

        Bitmap bitmap = takeScreenShot(u, totalHeight, totalWidth);
/*
        Bitmap bitmap = takeScreenShot();
*/
        Bitmap[] bitmaps = new Bitmap[]{bitmap};
        fileName = "RunningChits_" + date + "-" + time + ".pdf";


        PDFEngine.getInstance().createPDF(SingleChitSummaryActivity.this,
                bitmaps,
                fileName,
                (pdfFile, numOfImages) -> {
                    if (pdfFile == null) {
                        Toast.makeText(SingleChitSummaryActivity.this, "Error in PDF Generation Error.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Toast.makeText(SingleChitSummaryActivity.this, "PDF File generated", Toast.LENGTH_SHORT).show();
                    finalPdfFile = pdfFile;
                    PDFEngine.getInstance().openPDF(SingleChitSummaryActivity.this, finalPdfFile);


                    pdfFile = pdfFile;
                    // PDFEngine.getInstance().sharePDF(MainActivity.this, pdfFile);
                });
    }

    public void openPdf() {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + File.separator + ".ChitS" + File.separator + "PDFs", "SingleChit.pdf");
        if (file.exists()) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = FileProvider.getUriForFile(SingleChitSummaryActivity.this, getApplication().getPackageName() + ".provider", file);
            intent.setDataAndType(uri, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION |
                    Intent.FLAG_ACTIVITY_NO_HISTORY);
            try {
                startActivity(intent);

            } catch (ActivityNotFoundException e) {
                Toast.makeText(SingleChitSummaryActivity.this, "No Application For View", Toast.LENGTH_SHORT).show();


            }
        }

    }

    protected Bitmap takeScreenShot(View view, int totalHeight, int totalWidth) {

        Bitmap returnedBitmap = Bitmap.createBitmap(totalWidth, totalHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return returnedBitmap;


    }


}