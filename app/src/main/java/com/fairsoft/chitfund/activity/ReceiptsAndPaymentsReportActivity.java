package com.fairsoft.chitfund.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import android.widget.Button;
import android.widget.DatePicker;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fairsoft.chitfund.adapter.ReceiptsAndPaymentsReportAdapter;
import com.fairsoft.chitfund.api.Urls;
import com.fairsoft.chitfund.imagestopdf.PDFEngine;
import com.fairsoft.chitfund.model.ReceiptsNPaymentsModelItem;
import com.fairsoft.chitfund.R;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ReceiptsAndPaymentsReportActivity extends AppCompatActivity {
    Spinner select_chitCode;

    private String json;
    String rest;
    String jsonStr;
    private List<ReceiptsNPaymentsModelItem> receiptsNPaymentsModelItems;
    RecyclerView recyclerView;
    ReceiptsAndPaymentsReportAdapter receiptsAndPaymentsReportAdapter;
    TextView noDataTxt, startDateTxt, endDateTxt;
    LinearLayout totalLL;
    final Calendar myCalendar = Calendar.getInstance();
    final Calendar myCalendar2 = Calendar.getInstance();
    String start_Date, froDdatePick, endDateStr, tochangeDate;
    RadioButton r_mode, p_mode;
    RadioGroup radioGroup;
    String value, valueUp;
    String selectedIdStr;
    RadioButton radioButton;
    String compId, userId, userName, compName;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    public final static String PREFS_NAME = "my_prefs";
    File pdfFile = null;
    Button btn_send_wa_all_print, btn_send_mail_all_print, btn_save_all_print,send, sendWhatsapp, btnSendMultiple;
    String EMAIL = "email";
    String date,time;
    File finalPdfFile;
    File finalPdfFile1;
    CardView saveButton;

    String fileName;
    Uri uri;

    ProgressDialog pd;
    String encoded;
    public static final int MY_DEFAULT_TIMEOUT = 15000;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rec_pay_level_report);
        getSupportActionBar().setTitle("Receipts And Payments");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();


        compId = String.valueOf(pref.getInt("compId", 0));
        userId = String.valueOf(pref.getInt("userId", 0));
        userName = String.valueOf(pref.getString("userName", null));
        compName = String.valueOf(pref.getString("compName", null));

        init();

        //From Current date picker
        start_Date = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        startDateTxt.setText((start_Date));
        SimpleDateFormat dateFormatprev = new SimpleDateFormat("dd-MM-yyyy");
        Date d = null;
        try {
            d = dateFormatprev.parse(start_Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd");
        froDdatePick = dateFormat.format(d);


        //To Current date picker
        endDateStr = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        endDateTxt.setText((endDateStr));
        SimpleDateFormat dateFormatprev1 = new SimpleDateFormat("dd-MM-yyyy");
        Date d1 = null;
        try {
            d1 = dateFormatprev1.parse(endDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyy-MM-dd");
        tochangeDate = dateFormat1.format(d1);

        //datePick methods
        startDatePicker();
        endDatePicker();
        //callingApiMethod

        value = ((RadioButton) findViewById(radioGroup.getCheckedRadioButtonId()))
                .getText().toString();
        getRecAndPayReportsDetails(froDdatePick, tochangeDate, value);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                value = ((RadioButton) findViewById(radioGroup.getCheckedRadioButtonId()))
                        .getText().toString();
                getRecAndPayReportsDetails(froDdatePick, tochangeDate, value);


            }
        });

      /*  //radioClickListener
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                  @Override
                                                  public void onCheckedChanged(RadioGroup group, int checkedId) {
                                                      value = ((RadioButton) findViewById(radioGroup.getCheckedRadioButtonId()))
                                                              .getText().toString();
                                                      getRecAndPayReportsDetails(datePick, changeDate, value);

                                                  }
                                              }
        );
*/
       /* if (radioGroup.getCheckedRadioButtonId() == -1) {
        } else {
            getRecAndPayReportsDetails(datePick, changeDate, value);
        }*/

        btn_send_wa_all_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finalPdfFile == null) {
                    Toast.makeText(ReceiptsAndPaymentsReportActivity.this, "PDF File Not Found Click On Create Pdf", Toast.LENGTH_SHORT).show();
                    return;
                }

                Uri uri = FileProvider.getUriForFile(ReceiptsAndPaymentsReportActivity.this, getApplication().getPackageName() + ".provider", finalPdfFile);
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("application/pdf");
                share.putExtra(Intent.EXTRA_STREAM, uri);
                share.setPackage("com.whatsapp");
                share.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                startActivity(share);
            }
        });
        btn_send_mail_all_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finalPdfFile1 == null) {
                    Toast.makeText(ReceiptsAndPaymentsReportActivity.this, "PDF File Not Found Click On Create Pdf", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setType("application/pdf");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
                        new String[]{getStr(EMAIL)});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Report Generated from Day Sheet App");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
                emailIntent.putExtra(Intent.EXTRA_STREAM,
                        // Uri.fromFile(file));
                        FileProvider.getUriForFile(ReceiptsAndPaymentsReportActivity.this, getPackageName() + ".provider", finalPdfFile1));
                emailIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            }
        });
        btn_save_all_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkCheckPermission()) {
                    // savePetrolData();
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    createPDF();

                }
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*
                String numberTxt = mobileNumber.getText().toString();
*/

                addContact("918142323365");
            }
        });

        sendWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*
                String numberTxt = mobileNumber.getText().toString();
*/


                sendMsgToWhatsapp("918142323365");
            }
        });
        btnSendMultiple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*
                String numberTxt = mobileNumber.getText().toString();
*/

                if (finalPdfFile == null) {
                    Toast.makeText(ReceiptsAndPaymentsReportActivity.this, "PDF File Not Found Click On Create Pdf", Toast.LENGTH_SHORT).show();
                    return;
                } else {


                    encoded = encodeFileToBase64Binary(finalPdfFile);
/*
                    if (encoded != null) {
                        uploadDocs(encoded);
                    }
*/
                }

                uri = FileProvider.getUriForFile(ReceiptsAndPaymentsReportActivity.this, getApplication().getPackageName() + ".provider", finalPdfFile);

                sendMsgToMultipleWhatsapp("918142323365", String.valueOf(encoded));
            }
        });


    }

    public void init() {
        select_chitCode = findViewById(R.id.select_chitCode);
        noDataTxt = findViewById(R.id.noDataRec);
        totalLL = findViewById(R.id.totalLLRec);
        startDateTxt = findViewById(R.id.startDate);
        endDateTxt = findViewById(R.id.endDate);
        r_mode = findViewById(R.id.r_mode);
        p_mode = findViewById(R.id.p_mode);
        radioGroup = findViewById(R.id.radioButton);
        btn_send_wa_all_print = findViewById(R.id.btn_send_wa_all_print);
        btn_send_mail_all_print = findViewById(R.id.btn_send_mail_all_print);
        btn_save_all_print = findViewById(R.id.btn_save_all_print);
        saveButton = findViewById(R.id.saveButton);

        send = findViewById(R.id.send);
        sendWhatsapp = findViewById(R.id.sendWhatsapp);
        btnSendMultiple = findViewById(R.id.btnSendMultiple);




    }


    @SuppressLint("ClickableViewAccessibility")
    public void startDatePicker() {
        DatePickerDialog.OnDateSetListener startDates = new
                DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar2.set(Calendar.YEAR, year);
                        myCalendar2.set(Calendar.MONTH, monthOfYear);
                        myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        startDate();
                        value = ((RadioButton) findViewById(radioGroup.getCheckedRadioButtonId()))
                                .getText().toString();
/*
                        getRecAndPayReportsDetails(datePick, changeDate, value);
*/

                    }

                };

        startDateTxt.setOnTouchListener(new View.OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(ReceiptsAndPaymentsReportActivity.this, startDates, myCalendar2
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar2.get(Calendar.DAY_OF_MONTH)).show();
                }
                return false;
            }
        });


    }

    //datePicker
    @SuppressLint("ClickableViewAccessibility")
    public void endDatePicker() {
        DatePickerDialog.OnDateSetListener date = new
                DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateLabel();
                        value = ((RadioButton) findViewById(radioGroup.getCheckedRadioButtonId()))
                                .getText().toString();
/*
                        getRecAndPayReportsDetails(datePick, changeDate, value);
*/

                    }

                };

        endDateTxt.setOnTouchListener(new View.OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(ReceiptsAndPaymentsReportActivity.this, date, myCalendar2
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar2.get(Calendar.DAY_OF_MONTH)).show();
                }
                return false;
            }
        });


    }

    private void updateLabel() {

        String myFormat = "dd-MM-yyyy";


        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        endDateStr = sdf.format(myCalendar.getTime());
        endDateTxt.setText((endDateStr));


        SimpleDateFormat dateFormatprev = new SimpleDateFormat("dd-MM-yyyy");
        Date d = null;
        try {
            d = dateFormatprev.parse(endDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd");
        tochangeDate = dateFormat.format(d);


    }

    private void startDate() {
        String myFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        start_Date = sdf.format(myCalendar2.getTime());
        startDateTxt.setText(start_Date);
        SimpleDateFormat dateFormatprev = new SimpleDateFormat("dd-MM-yyyy");
        Date d = null;
        try {
            d = dateFormatprev.parse(start_Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd");
        froDdatePick = dateFormat.format(d);
    }

    private void getRecAndPayReportsDetails(String fromDate, String toDate, String valueStr) {
        RequestQueue queue = Volley.newRequestQueue(ReceiptsAndPaymentsReportActivity.this);
        // api/Chits/ReceiptsNPayments/{compid}/{userid}/{trmode}/{DateFrom}/{DateTo}
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_RECEIPTS_ANDZ_PAYMENTS_REPORTS + "/" + compId + "/" + userId + "/" + valueStr + "/" + fromDate + "/" + toDate, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                receiptsNPaymentsModelItems = new ArrayList<ReceiptsNPaymentsModelItem>();

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
                        ReceiptsNPaymentsModelItem auctionDefine = new ReceiptsNPaymentsModelItem();
                        JSONObject object = (JSONObject) array.get(i);
                        auctionDefine.setChitcode(object.getString("chitcode"));
                        auctionDefine.setChitid(object.getInt("chitid"));
                        auctionDefine.setAmount(object.getInt("amount"));
                        auctionDefine.setCreatedby(object.getInt("createdby"));
                        auctionDefine.setCreateddate(object.getString("createddate"));
                        auctionDefine.setName(object.getString("name"));
                        if (object.isNull("memberid")) {
                            object.put("memberid", "");
                        } else {
                            auctionDefine.setMemberid(object.getInt("memberid"));


                        }
                        if (object.isNull("trtype")) {
                            object.put("trtype", "");
                        } else {
                            auctionDefine.setTrtype(object.getString("trtype"));


                        }
                        if (valueStr == "P") {
                            auctionDefine.setPaymentid(object.getInt("paymentid"));
                            auctionDefine.setSubscriberid(object.getInt("subscriberid"));

                        }
                        if (valueStr == "R") {
                            auctionDefine.setReceiptid(object.getInt("receiptid"));
                            auctionDefine.setTrDate(object.getString("trDate"));
                            auctionDefine.setSubscriberid(object.getInt("subscriberid"));



                        }
                        receiptsNPaymentsModelItems.add(auctionDefine);
                    }


                } catch (Exception e) {
                    Toast.makeText(ReceiptsAndPaymentsReportActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
/*
                    totalLL.setVisibility(View.GONE);
*/


                }
                if (receiptsNPaymentsModelItems.isEmpty()) {
                    noDataTxt.setVisibility(View.VISIBLE);
                } else {
                    noDataTxt.setVisibility(View.GONE);


                }

                recyclerView = findViewById(R.id.chitLevelRecyclerView);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ReceiptsAndPaymentsReportActivity.this);
                recyclerView.setLayoutManager(linearLayoutManager);
                receiptsAndPaymentsReportAdapter = new ReceiptsAndPaymentsReportAdapter(ReceiptsAndPaymentsReportActivity.this, receiptsNPaymentsModelItems);
                recyclerView.setAdapter(receiptsAndPaymentsReportAdapter);
                receiptsAndPaymentsReportAdapter.notifyDataSetChanged();


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ReceiptsAndPaymentsReportActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy( MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_menu, menu);
        return true;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logOut) {
            Intent intent1 = new Intent(ReceiptsAndPaymentsReportActivity.this, LoginActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
            startActivity(intent1);
            finish();


        }
        return super.onOptionsItemSelected(item);


    }

    private boolean checkCheckPermission() {
        if (ContextCompat.checkSelfPermission(
                ReceiptsAndPaymentsReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                ReceiptsAndPaymentsReportActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            // You can use the API that requires the permission.
            return true;
        } else {
            // You can directly ask for the permission.
            // The registered ActivityResultCallback gets the result of this request.
            ActivityCompat.requestPermissions(ReceiptsAndPaymentsReportActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1011);
            return false;
        }
    }

    public String getStr(String key) {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
        return prefs.getString(key, "");
    }

    private void createPDF() {
        date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        time = new SimpleDateFormat("HH-mm-ss", Locale.getDefault()).format(new Date());

        View u = findViewById(R.id.scrollPdf);

        HorizontalScrollView z = findViewById(R.id.scrollPdf);
        int totalHeight = z.getChildAt(0).getHeight();
        int totalWidth = z.getChildAt(0).getWidth();

        Bitmap bitmap = takeScreenShot(u, totalHeight, totalWidth);
/*
        Bitmap bitmap = takeScreenShot();
*/
        Bitmap[] bitmaps = new Bitmap[]{bitmap};
        fileName="ReceiptsNPayments_" + date+"-"+time + ".pdf";


        PDFEngine.getInstance().createPDF(ReceiptsAndPaymentsReportActivity.this,
                bitmaps,
                fileName,
                (pdfFile, numOfImages) -> {
                    if (pdfFile == null) {
                        Toast.makeText(ReceiptsAndPaymentsReportActivity.this, "Error in PDF Generation Error.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Toast.makeText(ReceiptsAndPaymentsReportActivity.this, "PDF File generated", Toast.LENGTH_SHORT).show();
                    finalPdfFile = pdfFile;
                    PDFEngine.getInstance().openPDF(ReceiptsAndPaymentsReportActivity.this,finalPdfFile);

                    btn_send_wa_all_print.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (finalPdfFile == null) {
                                Toast.makeText(ReceiptsAndPaymentsReportActivity.this, "PDF File Not Found", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            Uri uri = FileProvider.getUriForFile(ReceiptsAndPaymentsReportActivity.this, getApplication().getPackageName() + ".provider", finalPdfFile);
                            Intent share = new Intent(Intent.ACTION_SEND);
                            share.setType("application/pdf");
                            share.putExtra(Intent.EXTRA_STREAM, uri);
                            share.setPackage("com.whatsapp");
                            share.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                            startActivity(share);
                        }
                    });
                    finalPdfFile1 = pdfFile;
                    btn_send_mail_all_print.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (finalPdfFile1 == null) {
                                Toast.makeText(ReceiptsAndPaymentsReportActivity.this, "PDF File Not Found", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                            emailIntent.setType("application/pdf");
                            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
                                    new String[]{getStr(EMAIL)});
                            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Report Generated from Day Sheet App");
                            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
                            emailIntent.putExtra(Intent.EXTRA_STREAM,
                                    // Uri.fromFile(file));
                                    FileProvider.getUriForFile(ReceiptsAndPaymentsReportActivity.this, getPackageName() + ".provider", finalPdfFile1));
                            emailIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                        }
                    });


                    pdfFile = pdfFile;
                    // PDFEngine.getInstance().sharePDF(MainActivity.this, pdfFile);
                });
    }

    protected Bitmap takeScreenShot(View view, int totalHeight, int totalWidth) {

        Bitmap returnedBitmap = Bitmap.createBitmap(totalWidth, totalHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return returnedBitmap;


    }

    private void addContact(String number) {

        RequestQueue queue = Volley.newRequestQueue(ReceiptsAndPaymentsReportActivity.this);
        StringRequest request = new StringRequest(Request.Method.GET, Urls.URL_WHATSAPP_CONTACT_ADD + "/" + number + "/" + "Rajitha", new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Toast.makeText(ReceiptsAndPaymentsReportActivity.this, response.toString(), Toast.LENGTH_SHORT).show();


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ReceiptsAndPaymentsReportActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy( MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void sendMsgToWhatsapp(String number) {

        RequestQueue queue = Volley.newRequestQueue(ReceiptsAndPaymentsReportActivity.this);
        StringRequest request = new StringRequest(Request.Method.GET, Urls.URL_SEND_WHATSAP + "/" + number + "/" + "Rajitha" + "/" + "Hello", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(ReceiptsAndPaymentsReportActivity.this, response.toString(), Toast.LENGTH_SHORT).show();

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ReceiptsAndPaymentsReportActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {


        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy( MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void sendMsgToMultipleWhatsapp(String number, String binaryString) {


        RequestQueue queue = Volley.newRequestQueue(ReceiptsAndPaymentsReportActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_SEND_WHATSAP_FILE, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(ReceiptsAndPaymentsReportActivity.this, response.toString(), Toast.LENGTH_SHORT).show();

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ReceiptsAndPaymentsReportActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {


            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("WANo", "918142323365");
                params.put("msg", "hello");
                params.put("fileName", String.valueOf(fileName));
                params.put("filebinary", String.valueOf(binaryString));


                return params;
            }


        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy( MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private String encodeFileToBase64Binary(File yourFile) {
        int size = (int) yourFile.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(yourFile));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        String encoded = Base64.encodeToString(bytes, Base64.NO_WRAP);
        appendLog(encoded);
        return encoded;
    }

    public static void appendLog(String text) {
        File logFile = new File(android.os.Environment.getExternalStorageDirectory(), "log.txt");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {

                e.printStackTrace();
            }
        }
        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


}