package com.fairsoft.chitfund.activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fairsoft.chitfund.adapter.ChitDetailsAdapter;
import com.fairsoft.chitfund.api.Urls;
import com.fairsoft.chitfund.model.ChitCompleDetailsModelItem;
import com.fairsoft.chitfund.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ChitEditActivity extends AppCompatActivity implements ChitDetailsAdapter.onChitItemClickListener, ChitDetailsAdapter.deleteRow {
    //id's assigning
    ChitDetailsAdapter chitDetailsAdapter;
    RecyclerView recyclerView;
    private List<ChitCompleDetailsModelItem> chitDetailsList;
    private String json;
    String rest;
    String jsonStr;
    CardView editCardView;
    String compId, userId, userName, compName, chitCodeStr;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Integer chitIdUp;
    Integer position;
    TextView nodata;
    public static final int MY_DEFAULT_TIMEOUT = 15000;



    String nameup, aadharup, mobile1up, mobile2up, landOffup, landResup, emailup, address1up, address2up, cityup, stateup, pinup, dobup, domup, refByup, refMobup, cComp, cenddt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chit_edit);
        getSupportActionBar().setTitle(" Chit Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //sharedPreference

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        compId = String.valueOf(pref.getInt("compId", 0));
        userId = String.valueOf(pref.getInt("userId", 0));
        userName = String.valueOf(pref.getString("userName", null));
        compName = String.valueOf(pref.getString("compName", null));


//id's intialization
        editCardView = findViewById(R.id.editCardView);
        nodata = findViewById(R.id.nodata);


        editCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChitEditActivity.this, ChitActivity.class);
                startActivity(intent);
            }
        });


        getChitDetails();
    }

    private void getChitDetails() {
        //https://fssservices.bookxpert.co/api/ChitCompleteDetails/compId/userId/0
        RequestQueue queue = Volley.newRequestQueue(ChitEditActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_CHIT_COMPLETE_DETAILS + "/" + compId + "/" + userId + "/" + "0", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                chitDetailsList = new ArrayList<ChitCompleDetailsModelItem>();

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
                        ChitCompleDetailsModelItem memeberDefine = new ChitCompleDetailsModelItem();
                        JSONObject object = (JSONObject) array.get(i);
                        memeberDefine.setChitcode(object.getString("chitcode"));
                        memeberDefine.setChitid(object.getInt("chitid"));
                        memeberDefine.setChitdate(object.getString("chitdate"));
                        memeberDefine.setChitamt(object.getInt("chitamt"));
                        memeberDefine.setNomonths(object.getInt("nomonths"));
                        memeberDefine.setNoofmembers(object.getInt("noofmembers"));
                        memeberDefine.setFrequency(object.getString("frequency"));
                        memeberDefine.setCommn(object.getInt("commn"));
                        memeberDefine.setIsownchit(object.getString("isownchit"));
                        memeberDefine.setDividend(object.getString("dividend"));
                        memeberDefine.setAuctionday(object.getString("auctionday"));
                        memeberDefine.setBidamount(object.getString("bidamount"));
                        memeberDefine.setCompleted(object.getString("completed"));
                        memeberDefine.setReceivable(object.getString("receivable"));
                        memeberDefine.setPayable(object.getString("payable"));
                        memeberDefine.setLastbid(object.getString("lastbid"));
                        memeberDefine.setComp(object.getString("Comp"));
                        memeberDefine.setEnddt(object.getString("enddt"));

                        chitDetailsList.add(memeberDefine);
                    }


                } catch (Exception e) {
                    Toast.makeText(ChitEditActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }
                if (chitDetailsList.isEmpty()) {
                    nodata.setVisibility(View.VISIBLE);

                } else {
                    nodata.setVisibility(View.GONE);


                }
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ChitEditActivity.this);
                recyclerView = findViewById(R.id.memberDetailsRecyclerView);
                recyclerView.setLayoutManager(linearLayoutManager);
                chitDetailsAdapter = new ChitDetailsAdapter(ChitEditActivity.this, chitDetailsList, ChitEditActivity.this::onMemItemClick, ChitEditActivity.this::deleteRowClick);
                recyclerView.setAdapter(chitDetailsAdapter);


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ChitEditActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy( MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    @Override
    public void onMemItemClick(int position, String name, String aadhar, String mobile1, String mobile2, String landOff, String landRes, String email, String address1, String address2, String city, String state, String pin, String dob, String dom, String refBy, String refMob, String comp, String enddt) {
        nameup = name;
        aadharup = aadhar;
        mobile1up = mobile1;
        mobile2up = mobile2;
        landOffup = landOff;
        landResup = landRes;
        emailup = email;
        address1up = address1;
        address2up = address2;
        cityup = city;
        stateup = state;
        pinup = pin;
        dobup = dob;
        domup = dom;
        refByup = refBy;
        refMobup = refMob;
        cComp = comp;
        cenddt = enddt;


    }


    private void getDeleteApi(Integer chitId, String chitCode) {
        // api/Chits/Deletes/{compid}/{userid}/{chitid}/{chitcode}/{type}/{typeID}
        RequestQueue queue = Volley.newRequestQueue(ChitEditActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_DELETE + "/" + compId + "/" + userId + "/" + chitId + "/" + chitCode + "/" + "Chit" + "/" + chitId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    if (rest.equals("Fail")) {

                    } else if (rest.equals("This chit is used in subscribers list")) {
                        Toast.makeText(ChitEditActivity.this, "This chit is used in subscribers list", Toast.LENGTH_SHORT).show();


                    } else {
                        Toast.makeText(ChitEditActivity.this, "Chit Deleted Successfully", Toast.LENGTH_SHORT).show();

                        getChitDetails();


                    }


                } catch (Exception e) {
                    Toast.makeText(ChitEditActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ChitEditActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy( MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_menu, menu);
        return true;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logOut) {
            Intent intent1 = new Intent(ChitEditActivity.this, LoginActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
            startActivity(intent1);
            finish();


        }
        return super.onOptionsItemSelected(item);


    }
    public void onBackPressed() {
        Intent intent = new Intent(ChitEditActivity.this, DashBoardActivity.class);
        startActivity(intent);
    }



    @Override
    public void deleteRowClick(Integer chitId, String chitCode, Integer position) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ChitEditActivity.this);

        // set title
        alertDialogBuilder.setTitle("Delete");

        // set dialog message
        alertDialogBuilder
                .setMessage("Are You Sure To Delete?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        getDeleteApi(chitId, chitCode);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
/*
        chitDetailsAdapter.deleteRow(position);
*/


    }
}