package com.fairsoft.chitfund.activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;


import com.fairsoft.chitfund.imagestopdf.PDFEngine;
import com.fairsoft.chitfund.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class PdfDialog extends DialogFragment {
    View view;
    RecyclerView invoiceDetailsRecyclerView;
    ImageView closeImageView;
    CardView deliverCardView;
    LinearLayout linear;
    HorizontalScrollView scrool;
    private boolean IS_MANY_PDF_FILE;
    private int deviceHeight;
    private int deviceWidth;


    /**
     * This is identify to number of pdf file. If pdf model list size > sector so we have create many file. After that we have merge all pdf file into one pdf file
     */
    private int SECTOR = 100; // Default value for one pdf file.
    private int START;
    private int END = SECTOR;
    private int NO_OF_PDF_FILE = 1;
    private int NO_OF_FILE;
    private int LIST_SIZE, LIST_SIZE1;
    private ProgressDialog progressDialog;
    String docId, companyIdStr, finYearStr, companyNameStr;
    File pdfFile = null;
    String date;
    String  time;
    String fileName;
    File finalPdfFile;



    /**
     * Store all dummy PDF models
     */

    private TextView btnPdfPath;
    /**
     * Share PDF file
     */
    private Button btnSharePdfFile;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String userid, userName, subsriptionid, rolename, subscriptioncode;

    private String json;
    String rest, jsonStr;
    TextView noDataTxt;
    TextView gstinIdTxt, aglWholeTxt, cell1Txt, cell2Txt, cell3Txt, titleHeadTxt, distributersTxt, headerAddressTxt, forTxt;
    TextView grossAmount, cgstAmountTxt, sgstAmount, discountTxt, transportChargeGst, invoiceTotalTxt;
    TextView invoiceNumb, StateCodeTxt, invoiceDate, stateTxt, billMode;
    TextView nameTxt, villageTxt, mandaltxt, districtTxt, cellPhnTxt;
    TextView dcNo, dcDateTxt, veichleTxt, dispatchTxt, creditTxt;
    TextView brokerTxt, state2Txt, mfmsNoTxt, aglNoTxt, gstInTxt;
    private String cgsttotal, totalgss, discount;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_invoice_result_dialog, null, false);

        init();

        //pdfView
        view.findViewById(R.id.btn_create_pdf).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkCheckPermission()) {
                    // savePetrolData();
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    createPDF();

                }


/*
                requestPermission();
*/
            }
        });


        closeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();

            }
        });


/*
        deliverCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
*/


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    public void init() {
/*
        deliverCardView = view.findViewById(R.id.deliverCardView);
*/
        linear = view.findViewById(R.id.downloadLL);
        btnSharePdfFile = (Button) view.findViewById(R.id.btn_share_pdf);
        btnPdfPath = (TextView) view.findViewById(R.id.btn_pdf_path);


    }


    private boolean checkCheckPermission() {
        if (ContextCompat.checkSelfPermission(
                getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            // You can use the API that requires the permission.
            return true;
        } else {
            // You can directly ask for the permission.
            // The registered ActivityResultCallback gets the result of this request.
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1011);
            return false;
        }
    }


    private void createPDF() {
        date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        time = new SimpleDateFormat("HH-mm-ss", Locale.getDefault()).format(new Date());
        View u = view.findViewById(R.id.scrollPdf);

        LinearLayout ll = view.findViewById(R.id.scrollPdf);


        int totalHeight = ll.getChildAt(0).getHeight();
        int totalWidth = ll.getChildAt(0).getWidth();

        Bitmap bitmap = takeScreenShot(u, totalHeight, totalWidth);

/*
        Bitmap bitmap = takeScreenShot();
*/
        Bitmap[] bitmaps = new Bitmap[]{bitmap};
        fileName = "HealthReport_" + date + "-" + time + ".pdf";


        PDFEngine.getInstance().createPDF(getActivity(),
                bitmaps,
                fileName,
                (pdfFile, numOfImages) -> {
                    if (pdfFile == null) {
                        Toast.makeText(getActivity(), "Error in PDF Generation Error.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    btnSharePdfFile.setVisibility(View.VISIBLE);

                    Toast.makeText(getActivity(), "PDF File generated", Toast.LENGTH_SHORT).show();
                     finalPdfFile = pdfFile;
                    btnSharePdfFile.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (finalPdfFile == null) {
                                Toast.makeText(getActivity(), "PDF File Not Found", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            Uri uri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", finalPdfFile);
                            Intent share = new Intent(Intent.ACTION_SEND);
                            share.setType("application/pdf");
                            share.putExtra(Intent.EXTRA_STREAM, uri);
                            share.setPackage("com.whatsapp");
                            share.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                            startActivity(share);
                        }
                    });

                    pdfFile = pdfFile;
                    // PDFEngine.getInstance().sharePDF(MainActivity.this, pdfFile);
                });
    }

    protected Bitmap takeScreenShot() {

        LinearLayout ll = view.findViewById(R.id.scrollable);
        ll.setDrawingCacheEnabled(true);
        ll.layout(1, 1, ll.getMeasuredWidth(), ll.getMeasuredHeight());
        ll.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(ll.getDrawingCache());// clear drawing cache

        return bitmap;


    }
    protected Bitmap takeScreenShot(View view, int totalHeight, int totalWidth) {

        Bitmap returnedBitmap = Bitmap.createBitmap(totalWidth, totalHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return returnedBitmap;


    }



}