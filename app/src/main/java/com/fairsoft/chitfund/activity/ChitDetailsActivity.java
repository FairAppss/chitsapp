package com.fairsoft.chitfund.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fairsoft.chitfund.api.Urls;
import com.fairsoft.chitfund.model.ChitDefineModelItem;

import com.fairsoft.chitfund.R;
import com.fairsoft.chitfund.adapter.DashBoardAdapter;


import org.json.JSONArray;

import org.json.JSONObject;


import java.util.ArrayList;

import java.util.List;


public class ChitDetailsActivity extends AppCompatActivity implements DashBoardAdapter.onItemClickListener {
    //id's assigning
    DashBoardAdapter customAdapter;
    RecyclerView recyclerView;
    private List<ChitDefineModelItem> chitDefineModelItemArrayList;
    private String json;
    String rest;
    String jsonStr;
    String compId, userId, userName, compName;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    public static final int MY_DEFAULT_TIMEOUT = 15000;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chit_detail_layout);
        getSupportActionBar().setTitle("ChitDetails");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();


        compId = String.valueOf(pref.getInt("compId", 0));
        userId = String.valueOf(pref.getInt("userId", 0));
        userName = String.valueOf(pref.getString("userName", null));
        compName = String.valueOf(pref.getString("compName", null));

        recyclerView = (RecyclerView) findViewById(R.id.saveChiefComplaintsRecyclerView);
        //Api calling
        postDataUsingVolley();
    }


    @Override
    public void onItemClick(int position) {
        Toast.makeText(ChitDetailsActivity.this, "position is" + position, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, sampleTable.class);
        intent.putExtra("data", position);


        startActivity(intent);

    }
    // url to post our data

    private void postDataUsingVolley() {

        RequestQueue queue = Volley.newRequestQueue(ChitDetailsActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_CHIT_DETAILS + "/" + compId + "/" + userId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                chitDefineModelItemArrayList = new ArrayList<ChitDefineModelItem>();

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
                        ChitDefineModelItem chitDefine = new ChitDefineModelItem();
                        JSONObject object = (JSONObject) array.get(i);
                        chitDefine.setChitdate(object.getString("chitdate"));
                        chitDefine.setChitid(object.getInt("chitid"));
                        chitDefine.setChitcode(object.getString("chitcode"));
                        chitDefine.setChitamt(object.getDouble("chitamt"));
                        chitDefine.setNomonths(object.getInt("nomonths"));
                        chitDefine.setNoofmembers(object.getInt("noofmembers"));
                        chitDefine.setFrequency(object.getString("frequency"));
                        chitDefine.setCommn(object.getDouble("commn"));
                        chitDefine.setIsownchit(object.getString("isownchit"));
                        chitDefine.setDividend(object.getString("dividend"));
                        chitDefine.setAuctionday(object.getString("auctionday"));
                        chitDefine.setBidamount(object.getString("bidamount"));
                        chitDefine.setCompleted(object.getString("completed"));
                        chitDefine.setReceivable(object.getString("receivable"));
                        chitDefine.setPayable(object.getString("payable"));
                        chitDefine.setLastbid(object.getString("lastbid"));
                        chitDefineModelItemArrayList.add(chitDefine);

                    }


                } catch (Exception e) {
                    Toast.makeText(ChitDetailsActivity.this, "error is" + e.toString(), Toast.LENGTH_SHORT).show();

                }


                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ChitDetailsActivity.this);
                recyclerView.setLayoutManager(linearLayoutManager);
                customAdapter = new DashBoardAdapter(ChitDetailsActivity.this, chitDefineModelItemArrayList);
                recyclerView.setAdapter(customAdapter);

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ChitDetailsActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy( MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_menu, menu);
        return true;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logOut) {
            Intent intent1 = new Intent(ChitDetailsActivity.this, LoginActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
            startActivity(intent1);
            finish();


        }
        return super.onOptionsItemSelected(item);


    }


}