package com.fairsoft.chitfund.activity;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fairsoft.chitfund.model.ChitCompleDetailsModelItem;
import com.fairsoft.chitfund.R;

import com.fairsoft.chitfund.api.Urls;

import com.fairsoft.chitfund.model.ChitUpdateDetailsModelItem;


import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ChitActivity extends AppCompatActivity {
    //id's assigning
    RadioButton yes, no;
    RadioGroup radioGroup;
    EditText codeTxt, amountTxt, membersTxt, commonTxt, auctionDay, paymentAfter, receiptAfter;
    Spinner frequency, dividend, comm_spinner, auction_day_spinner, month_spinner, day_spinner, week_spinner, no_of_months_spinner, ampm_spin, chitType_spinner;
    TextView monthTxt, auctionTime, startDate;
    LinearLayout yes_meansLL, if_selected_day;
    CardView saveBtn, clearCardView;
    final Calendar myCalendar = Calendar.getInstance();
    final Calendar myCalendar2 = Calendar.getInstance();
    TimePickerDialog timePickerDialog;
    Calendar calendar;
    int currentHour;
    int currentMinute;
    int selectedIdStr;
    String start_Date, datePick;
    String value, valueUp;
    String auctionSeType;
    String currentTime;


    private List<ChitCompleDetailsModelItem> chitDetailsList;
    private List<ChitUpdateDetailsModelItem> chitUpdateDetailsList;

    private String json;
    String rest;
    String jsonStr;
    Integer chitIdUp;
    Integer position;
    boolean update = false;
    Integer chitIdPara;
    int t1Hrs, t2min;
    String changedDate, commonType, chitType, auctionMonth, auctionDayse, auctionWeek, noOfMonths, amPmStr, noOfMonthsUp, auctionDayseUp, auctionWeekUp;
    String chitCodeStr, chitAmtStr, noOfMembersStr, frequencySpinStr, commonStr, isOwnChitStr, dividendStr, auctiondayStr, auctiontimeStr, paymentfterStr, receiptafterStr, noMonthsStr;

    String[] frequencys = {"Monthly", "Weekly", "Daily"};
    String[] dividends = {"Same", "Next"};
    private List<Integer> monthlistData = new ArrayList<Integer>();
    HashSet<Integer> listToSet;
    List<Integer> listWithoutDuplicates;

    String[] commnTypes = {"%", "Amount"};
    String[] chitTypes = {"Regular", "Incremental"};

    String[] ampm = {"AM", "PM"};

    String[] auctionTypes = {"Day", "Day Of Month"};
    String[] auctionMonths = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"};
    String[] auctionDays = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    String[] auctionWeeks = {"First Week", "Second Week", "Third Week", "Fourth Week"};
    String compId, userId, userName, compName;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String longval;
    TextView textView1;
    ProgressBar progressBar;
    public static final int MY_DEFAULT_TIMEOUT = 15000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_chit);

        //SharedPreference

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        compId = String.valueOf(pref.getInt("compId", 0));
        userId = String.valueOf(pref.getInt("userId", 0));
        userName = String.valueOf(pref.getString("userName", null));
        compName = String.valueOf(pref.getString("compName", null));

        //bundle
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            chitIdUp = extras.getInt("chitId");
            position = extras.getInt("position");
            update = true;
            getUpdateMemberDetails(chitIdUp);
        }

        //id Initializing
        init();

        if (update) {
            getSupportActionBar().setTitle("Update Chit");
            textView1.setText("Add Updated Chit");

        } else {
            getSupportActionBar().setTitle("New  Chit");
            textView1.setText("Add New Chit");


        }
        //methods calling
        start_Date = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        startDate.setText((start_Date));
        currentTime = new SimpleDateFormat("hh:mm:ss", Locale.getDefault()).format(new Date());
        auctionTime.setText((currentTime));
        auctionDatePicker();
        startDatePicker();


        auctionTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openTimeFragment();
            }
        });


        membersTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.equals("")) {


                } else {
                    monthTxt.setText(s.toString());
                    String month1 = monthTxt.getText().toString();
                    int months = 0;
                    if (!month1.equals("")) {
                        months = Integer.parseInt(month1);
                    }


                    for (int i = 1; i <= months; i++) {
                        monthlistData.add(i);
                        listToSet = new LinkedHashSet<Integer>(monthlistData);
                        listWithoutDuplicates = new ArrayList<Integer>(listToSet);

                    }

                    ArrayAdapter selectNoOfMonthss = new ArrayAdapter(ChitActivity.this, R.layout.spinner_text, listWithoutDuplicates);
                    selectNoOfMonthss.setDropDownViewResource(R.layout.spinner_text);
                    no_of_months_spinner.setAdapter(selectNoOfMonthss);
                    no_of_months_spinner.setOnItemSelectedListener(new ChitActivity.NoOfMonthsSpinnerClass());

                }
            }
        });

        amountTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                amountTxt.removeTextChangedListener(this);

                try {
                    String givenstring = s.toString();

                    if (givenstring.contains(",")) {
                        givenstring = givenstring.replaceAll(",", "");
                    }
                    longval = String.valueOf(Long.parseLong(givenstring));
                    DecimalFormat formatter = new DecimalFormat("#,###,###");
                    String formattedString = formatter.format(longval);
                    amountTxt.setText(formattedString);
                    amountTxt.setSelection(amountTxt.getText().length());
                    // to place the cursor at the end of text
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                amountTxt.addTextChangedListener(this);

            }
        });


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.yes) {
                    yes_meansLL.setVisibility(View.VISIBLE);
                    noOfMonthsUp = noOfMonths;
                }

                if (checkedId == R.id.no) {
                    yes_meansLL.setVisibility(View.GONE);
                    noOfMonthsUp = "0";


                }


            }
        });

        //adding Frequency spinner
        ArrayAdapter select = new ArrayAdapter(this, R.layout.spinner_text, frequencys);
        select.setDropDownViewResource(R.layout.spinner_text);
        frequency.setAdapter(select);
        frequency.setOnItemSelectedListener(new FrequencySpinnerClass());


        //adding Dividend spinner
        ArrayAdapter selectDividend = new ArrayAdapter(this, R.layout.spinner_text, dividends);
        selectDividend.setDropDownViewResource(R.layout.spinner_text);
        dividend.setAdapter(selectDividend);
        dividend.setOnItemSelectedListener(new DividendSpinnerClass());

        //adding Commn spinner
        ArrayAdapter selectCommn = new ArrayAdapter(this, R.layout.spinner_text, commnTypes);
        selectCommn.setDropDownViewResource(R.layout.spinner_text);
        comm_spinner.setAdapter(selectCommn);
        comm_spinner.setOnItemSelectedListener(new CommnSpinnerClass());

        //adding ChitType spinner
        ArrayAdapter selectChitType = new ArrayAdapter(this, R.layout.spinner_text, chitTypes);
        selectChitType.setDropDownViewResource(R.layout.spinner_text);
        chitType_spinner.setAdapter(selectChitType);
        chitType_spinner.setOnItemSelectedListener(new ChitTypeSpinnerClass());


        //adding AuctionDay spinner
        ArrayAdapter selectAuction = new ArrayAdapter(this, R.layout.spinner_text, auctionTypes);
        selectAuction.setDropDownViewResource(R.layout.spinner_text);
        auction_day_spinner.setAdapter(selectAuction);
        auction_day_spinner.setOnItemSelectedListener(new AuctionDaySpinnerClass());

        //adding Month spinner
        ArrayAdapter selectAuctionMonth = new ArrayAdapter(this, R.layout.spinner_text, auctionMonths);
        selectAuctionMonth.setDropDownViewResource(R.layout.spinner_text);
        month_spinner.setAdapter(selectAuctionMonth);
        month_spinner.setOnItemSelectedListener(new AuctionMonthSpinnerClass());

        //adding day spinner
        ArrayAdapter selectAuctionDay = new ArrayAdapter(this, R.layout.spinner_text, auctionDays);
        selectAuctionDay.setDropDownViewResource(R.layout.spinner_text);
        day_spinner.setAdapter(selectAuctionDay);
        day_spinner.setOnItemSelectedListener(new AuctionDaysSpinnerClass());

        //adding Week spinner
        ArrayAdapter selectAuctionWeek = new ArrayAdapter(this, R.layout.spinner_text, auctionWeeks);
        selectAuctionWeek.setDropDownViewResource(R.layout.spinner_text);
        week_spinner.setAdapter(selectAuctionWeek);
        week_spinner.setOnItemSelectedListener(new AuctionWeekSpinnerClass());

        //adding ampm spinner
        ArrayAdapter selectTimeAmPm = new ArrayAdapter(this, R.layout.spinner_text, ampm);
        selectTimeAmPm.setDropDownViewResource(R.layout.spinner_text);
        ampm_spin.setAdapter(selectTimeAmPm);
        ampm_spin.setOnItemSelectedListener(new AmPmSpinnerClass());

        //saving data
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                value = ((RadioButton) findViewById(radioGroup.getCheckedRadioButtonId()))
                        .getText().toString();

                chitCodeStr = codeTxt.getText().toString();
                chitAmtStr = amountTxt.getText().toString();
                noOfMembersStr = membersTxt.getText().toString();
                commonStr = commonTxt.getText().toString();
                selectedIdStr = radioGroup.getCheckedRadioButtonId();
                datePick = startDate.getText().toString();
                SimpleDateFormat dateFormatprev = new SimpleDateFormat("dd-MM-yyyy");
                Date d = null;
                try {
                    d = dateFormatprev.parse(datePick);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd");
                changedDate = dateFormat.format(d);

                auctiontimeStr = auctionTime.getText().toString();
                paymentfterStr = paymentAfter.getText().toString();
                receiptafterStr = receiptAfter.getText().toString();
                if (value.equals("No")) {
                    valueUp = "N";
                    noOfMonthsUp = "";
                } else {
                    noOfMonthsUp = noOfMonths;
                }
                if (value.equals("Yes")) {
                    valueUp = "Y";


                }

                if (auctionSeType.equals("Day")) {
                    auctionDayseUp = auctionDayse;
                    auctionWeekUp = auctionWeek;


                } else if (auctionSeType.equals("Day Of Month")) {
                    auctionDayseUp = auctionMonth;
                    auctionWeekUp = "";


                }


                validateFields();


            }
        });
        clearCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* FragmentManager fm = getSupportFragmentManager();
                PdfDialog resourse_share=new PdfDialog();
                resourse_share.show(fm, "Dialog Fragment");
*/
                codeTxt.setText("");
                amountTxt.setText("");
                commonTxt.setText("");
                auctionTime.setText("");
                paymentAfter.setText("");
                receiptAfter.setText("");
                monthTxt.setText("");
                noOfMembersStr = membersTxt.getText().toString();

                if (noOfMembersStr.equals("")) {

                } else {
                    membersTxt.setText("");


                }


            }
        });

    }

    public void init() {
        yes = findViewById(R.id.yes);
        no = findViewById(R.id.no);
        monthTxt = findViewById(R.id.monthTxt);
        frequency = (Spinner) findViewById(R.id.frequency);
        dividend = (Spinner) findViewById(R.id.dividend_spinner);
        saveBtn = findViewById(R.id.saveCardView);
        clearCardView = findViewById(R.id.clearCardView);
        codeTxt = (EditText) findViewById(R.id.codeTxt);
        codeTxt.setHint(Html.fromHtml("ChitCode <font color =\"#cc0029\" >*</font>"));
        chitType_spinner = findViewById(R.id.chitType_spinner);
        amountTxt = (EditText) findViewById(R.id.amountTxt);
        amountTxt.setHint(Html.fromHtml("Chit Amount <font color =\"#cc0029\" >*</font>"));

        membersTxt = (EditText) findViewById(R.id.noMembersTxt);
        membersTxt.setHint(Html.fromHtml("No.of Months <font color =\"#cc0029\" >*</font>"));

        commonTxt = (EditText) findViewById(R.id.commonTxt);
        commonTxt.setHint(Html.fromHtml("Common <font color =\"#cc0029\" >*</font>"));

        auctionTime = (TextView) findViewById(R.id.autionTime);
        auctionTime.setHint(Html.fromHtml("AuctionTime <font color =\"#cc0029\" >*</font>"));

        // auctionDay = (EditText) findViewById(R.id.auctionDay);
        startDate = (TextView) findViewById(R.id.startDate);
        startDate.setHint(Html.fromHtml("Start Date <font color =\"#cc0029\" >*</font>"));

        paymentAfter = (EditText) findViewById(R.id.paymentAfter);
        receiptAfter = (EditText) findViewById(R.id.receiptAfter);
        radioGroup = findViewById(R.id.radioButton);
        yes_meansLL = findViewById(R.id.yes_means);
        comm_spinner = findViewById(R.id.comm_spinner);
        auction_day_spinner = findViewById(R.id.auction_day_spinner);
        month_spinner = findViewById(R.id.month_spinner);
        day_spinner = findViewById(R.id.day_spinner);
        week_spinner = findViewById(R.id.week_spinner);
        if_selected_day = findViewById(R.id.if_selected_day);
        no_of_months_spinner = findViewById(R.id.no_of_months_spinner);
        ampm_spin = findViewById(R.id.ampm_spin);
        textView1 = findViewById(R.id.textView1);
        progressBar = findViewById(R.id.progressBar);


    }

    //datePicker
    @SuppressLint("ClickableViewAccessibility")
    public void auctionDatePicker() {
        DatePickerDialog.OnDateSetListener date = new
                DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        //  updateLabel();
                    }

                };


    }

    @SuppressLint("ClickableViewAccessibility")
    public void startDatePicker() {
        DatePickerDialog.OnDateSetListener startDates = new
                DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar2.set(Calendar.YEAR, year);
                        myCalendar2.set(Calendar.MONTH, monthOfYear);
                        myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        startDate();
                    }

                };

        startDate.setOnTouchListener(new View.OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(ChitActivity.this, startDates, myCalendar2
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar2.get(Calendar.DAY_OF_MONTH)).show();
                }
                return false;
            }
        });


    }

    void openTimeFragment() {
        TimePickerDialog mTimePicker;

        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        mTimePicker = new TimePickerDialog(ChitActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {


                String time = selectedHour + ":" + selectedMinute;

                SimpleDateFormat fmt = new SimpleDateFormat("HH:mm");
                Date date = null;
                try {
                    date = fmt.parse(time);
                } catch (ParseException e) {

                    e.printStackTrace();
                }

                SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm");

                String formattedTime = fmtOut.format(date);

                auctionTime.setText(formattedTime);
            }
        }, hour, minute, false);//No 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }


    private void startDate() {
        String myFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        start_Date = sdf.format(myCalendar2.getTime());


        startDate.setText(start_Date);


    }


    class FrequencySpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            frequencySpinStr = frequency.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }


    }

    class DividendSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            dividendStr = dividend.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }


    }

    class CommnSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            commonType = comm_spinner.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }


    }

    class ChitTypeSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            chitType = chitType_spinner.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }


    }


    class AuctionDaySpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            auctionSeType = auction_day_spinner.getItemAtPosition(position).toString();
            if (auctionSeType.equals("Day")) {
                if_selected_day.setVisibility(View.VISIBLE);
                month_spinner.setVisibility(View.GONE);


            } else {
                if_selected_day.setVisibility(View.GONE);


            }
            if (auctionSeType.equals("Day Of Month")) {
                month_spinner.setVisibility(View.VISIBLE);
                if_selected_day.setVisibility(View.GONE);
                auctionDayseUp = auctionMonth;
                auctionWeek = "0";


            } else {
                month_spinner.setVisibility(View.GONE);


            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }


    }

    class AuctionMonthSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            auctionMonth = month_spinner.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }


    }

    class AuctionDaysSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            auctionDayse = day_spinner.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }


    }

    class AuctionWeekSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            auctionWeek = week_spinner.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }


    }

    class NoOfMonthsSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            noOfMonths = no_of_months_spinner.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }


    }

    class AmPmSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            amPmStr = ampm_spin.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }


    }


    public void validateFields() {
        if (chitCodeStr.isEmpty()) {
            Toast.makeText(ChitActivity.this, "Please enter ChitCode", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);


            return;
        } else if (chitAmtStr.isEmpty()) {
            Toast.makeText(ChitActivity.this, "Please enter ChitAmount", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);


            return;
        } else if (noOfMembersStr.isEmpty()) {
            Toast.makeText(ChitActivity.this, "Please enter No.ofMonths", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);


            return;
        } else if (frequencySpinStr.isEmpty() || frequencySpinStr.equals("select Frequency")) {
            Toast.makeText(ChitActivity.this, "Please select Frequency", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);


            return;
        } else if (commonStr.isEmpty()) {
            Toast.makeText(ChitActivity.this, "Please enter Commn", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);

            return;
        } else if (commonType.isEmpty() || commonType.equals("select")) {
            Toast.makeText(ChitActivity.this, "Please select CommnType", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);

            return;
        } else if (dividendStr.isEmpty() || dividendStr.equals("select Dividend")) {
            Toast.makeText(ChitActivity.this, "Please select dividend", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);


            return;
        } else if (auctionSeType.isEmpty() || auctionSeType.equals("select")) {
            Toast.makeText(ChitActivity.this, "Please select dividend", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);


            return;
        } else if (auctiontimeStr.isEmpty()) {
            Toast.makeText(ChitActivity.this, "Please enter Time", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);


            return;
        } else if (datePick.isEmpty()) {
            Toast.makeText(ChitActivity.this, "Please select date", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);


            return;
        } else if (commonType.equals("%")) {
            Integer commn = Integer.valueOf(commonStr);

            if (commn > 99) {
                Toast.makeText(ChitActivity.this, "Common should be Less than 99", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

                return;
            }
        } else if (commonType.equals("Amount")) {
            Integer commn1 = Integer.valueOf(commonStr);
            Integer chitAmt = Integer.valueOf(chitAmtStr);
            if (commn1 >= chitAmt) {
                Toast.makeText(ChitActivity.this, "Common should be Less than Chit Amount", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

                return;
            }
        }

        if (update) {
            if (amountTxt.isFocused()) {
                longval = longval;

            } else {
                double bill = Double.parseDouble(chitAmtStr);
                long longAuctionAmnt = (long) bill;
                long auctionAmtval = Integer.valueOf(String.valueOf(longAuctionAmnt));
                longval = String.valueOf(Long.valueOf(auctionAmtval));

            }
            getValidateMonth(chitCodeStr, noOfMembersStr);


        } else {
            checkExistingDataUsingVolley(chitCodeStr, longval.toString(), noOfMembersStr, frequencySpinStr, commonStr, String.valueOf(value), dividendStr, changedDate, auctiontimeStr, paymentfterStr, receiptafterStr, noOfMembersStr);
        }


    }

    //getting response fro Api


    private void postDataUsingVolley(String chitCode, String chitAmt, String noOfMembers, String frequencySpin, String common, String isOwmChit, String dividend, String auctionday, String auctiontime, String paymentfter, String receiptafter, String noMonths, String noOfMonthsUp) {
        progressBar.setVisibility(View.VISIBLE);


        String updateData;
        if (update) {
            updateData = "U";
            chitIdPara = chitIdUp;

        } else {
            updateData = "N";
            chitIdPara = 0;

        }
        // creating a new variable for our request queue
        RequestQueue queue = Volley.newRequestQueue(ChitActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_SAVE_CHIT_DETAILS + "/" + compId + "/" + userId + "/" + updateData, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                // on below line we are displaying a success toast message.

                codeTxt.setText("");
                amountTxt.setText("");
                membersTxt.setText("");
                commonTxt.setText("");
                auctionTime.setText("");
                paymentAfter.setText("");
                receiptAfter.setText("");
                monthTxt.setText("");

                if (updateData.equals("U")) {
                    progressBar.setVisibility(View.GONE);

                    Toast.makeText(ChitActivity.this, "Chit Updated SuccessFully", Toast.LENGTH_SHORT).show();

                } else {
                    progressBar.setVisibility(View.GONE);

                    Toast.makeText(ChitActivity.this, "Chit Saved SuccessFully", Toast.LENGTH_SHORT).show();


                }
                Intent intent = new Intent(ChitActivity.this, ChitEditActivity.class);

                startActivity(intent);

              /*  if (update == true) {
                    Intent intent = new Intent(ChitActivity.this, ChitEditActivity.class);
                    startActivity(intent);
                }
                Toast.makeText(ChitActivity.this, "Chit Saves SuccessFully", Toast.LENGTH_SHORT).show();

*/


            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ChitActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // below line we are creating a map for
                // storing our values in key and value pair.
                Map<String, String> params = new HashMap<String, String>();

                // on below line we are passing our key
                // and value pair to our parameters.
                params.put("chitcode", chitCode);
                params.put("chitamt", chitAmt);
                params.put("noofmembers", noOfMembers);
                params.put("frequency", frequencySpin);
                params.put("commn", common);
                params.put("peramount", commonType);
                params.put("isownchit", isOwmChit);
                params.put("ownchitmonth", noOfMonthsUp);
                params.put("samenext", dividendStr);
                params.put("dateorday", auctionSeType);
                params.put("auctweek", auctionWeekUp);
                params.put("auctionday", auctionDayseUp);
                params.put("auctiontime", auctiontime);
                params.put("ampm", amPmStr);
                params.put("paymentafter", paymentfter);
                params.put("receiptafter", receiptafter);
                params.put("nomonths", noOfMembers);
                params.put("startdate", auctionday);
                params.put("chittype", chitType);

/*
                params.put("startdate", auctionday);
*/

                params.put("chitId", String.valueOf(chitIdPara));


                // at last we are
                // returning our params.
                return params;
            }

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }

    private void checkExistingDataUsingVolley(String chitCode, String chitAmt, String noOfMembers, String frequencySpin, String common, String isOwmChit, String dividend, String auctionday, String auctiontime, String paymentfter, String receiptafter, String noMonths) {
        if (!checkNetworkAndShowError(this)) {
            return;
        }
        progressBar.setVisibility(View.VISIBLE);


        String myparam = chitCodeStr;
        String url = Urls.URL_IS_EXISTING_CODE + "/" + compId + "/" + userId + "/" + myparam;
        // creating a new variable for our request queue
        RequestQueue queue = Volley.newRequestQueue(ChitActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String json = response;
                String rest = json.substring(1, json.length() - 1);


                // on below line we are displaying a success toast message.
/*
                Toast.makeText(UserActivity.this, response, Toast.LENGTH_SHORT).show();
*/
                if (rest.equals(chitCode)) {
                    progressBar.setVisibility(View.GONE);

                    Toast.makeText(ChitActivity.this, "This Chit Already Exist", Toast.LENGTH_SHORT).show();
                } else {
                    postDataUsingVolley(chitCodeStr, longval.toString(), noOfMembersStr, frequencySpinStr, commonStr, String.valueOf(valueUp), dividendStr, changedDate, auctiontimeStr + amPmStr, paymentfterStr, receiptafterStr, noMonthsStr, noOfMonthsUp);

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ChitActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        });
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }

    private void getUpdateMemberDetails(Integer chitId) {
        RequestQueue queue = Volley.newRequestQueue(ChitActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_CHIT_COMPLETE_DETAILS + "/" + compId + "/" + userId + "/" + chitId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                chitUpdateDetailsList = new ArrayList<ChitUpdateDetailsModelItem>();

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
                        ChitUpdateDetailsModelItem memeberDefine = new ChitUpdateDetailsModelItem();
                        JSONObject object = (JSONObject) array.get(i);
                        memeberDefine.setChitcode(object.getString("chitcode"));
                        memeberDefine.setChitid(object.getInt("chitid"));
                        memeberDefine.setChitamt(object.getInt("chitamt"));
                        memeberDefine.setNoofmembers(object.getInt("noofmembers"));
                        memeberDefine.setFrequency(object.getString("frequency"));
                        memeberDefine.setCommn(object.getInt("commn"));
                        memeberDefine.setIsownchit(object.getString("isownchit"));
                        memeberDefine.setOwnchitmonth(object.getString("ownchitmonth"));
                        memeberDefine.setPaymentafter(object.getString("paymentafter"));
                        memeberDefine.setReceiptafter(object.getString("receiptafter"));
                        memeberDefine.setAuctweek(object.getString("auctweek"));
                        memeberDefine.setSamenext(object.getString("samenext"));
                        memeberDefine.setDateday(object.getString("dateday"));
                        memeberDefine.setAuctiontime(object.getString("auctiontime"));
                        memeberDefine.setCommnPerAmount(object.getString("commnperamount"));
                        memeberDefine.setStartdate(object.getString("startdate"));
                        memeberDefine.setChittype(object.getString("chittype"));


                        if (object.isNull("dividend")) {
                            object.put("dividend", "");
                        } else {

                            memeberDefine.setDividend(object.getString("dividend"));

                        }
                        memeberDefine.setAuctionday(object.getString("auctionday"));
                        chitUpdateDetailsList.add(memeberDefine);
                    }
                    updatedetails();


                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);

                    Toast.makeText(ChitActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ChitActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void getValidateMonth(String chitCode, String noOfMembersStr) {
        RequestQueue queue = Volley.newRequestQueue(ChitActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_MONTHS_VALID + "/" + compId + "/" + userId + "/" + chitCode + "/" + noOfMembersStr, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    json = response;
                    int rest1 = Integer.parseInt(json.substring(1, json.length() - 1));
                    int members = Integer.parseInt(noOfMembersStr);
                    if (members < rest1) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(ChitActivity.this, "no of members should not be less than subscriber count", Toast.LENGTH_SHORT).show();

                    } else {
                        progressBar.setVisibility(View.GONE);
                        postDataUsingVolley(chitCodeStr, longval.toString(), noOfMembersStr, frequencySpinStr, commonStr, String.valueOf(valueUp), dividendStr, changedDate, auctiontimeStr + amPmStr, paymentfterStr, receiptafterStr, noMonthsStr, noOfMonthsUp);


                    }


                } catch (Exception e) {
                    Toast.makeText(ChitActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);


                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ChitActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }


    private void updatedetails() {
        chitCodeStr = codeTxt.getText().toString();
        chitAmtStr = amountTxt.getText().toString();
        noOfMembersStr = membersTxt.getText().toString();
        commonStr = commonTxt.getText().toString();
        selectedIdStr = radioGroup.getCheckedRadioButtonId();
        datePick = startDate.getText().toString();

        auctiontimeStr = auctionTime.getText().toString();
        paymentfterStr = paymentAfter.getText().toString();
        receiptafterStr = receiptAfter.getText().toString();
        noMonthsStr = monthTxt.getText().toString();


        codeTxt.setText(chitUpdateDetailsList.get(0).getChitcode());
        String chitAmnt = String.valueOf(chitUpdateDetailsList.get(0).getChitamt());

        double billAmnt = Double.parseDouble(String.valueOf(chitAmnt));
        long longAmnt = (long) billAmnt;

        amountTxt.setText(String.valueOf(longAmnt));
        membersTxt.setText(String.valueOf(chitUpdateDetailsList.get(0).getNoofmembers()));
        String common = String.valueOf(chitUpdateDetailsList.get(0).getCommn());
        double bill = Double.parseDouble(String.valueOf(common));
        long longCommn = (long) bill;
        commonTxt.setText(String.valueOf(longCommn));
        auctionTime.setText(String.valueOf(chitUpdateDetailsList.get(0).getAuctiontime()));
        String date = chitUpdateDetailsList.get(0).getStartdate();


        SimpleDateFormat dateFormatprev = new SimpleDateFormat("yyy-MM-dd");
        Date d = null;
        try {
            d = dateFormatprev.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String changedDay = dateFormat.format(d);
        startDate.setText(changedDay);


        paymentAfter.setText(chitUpdateDetailsList.get(0).getPaymentafter());
        receiptAfter.setText(chitUpdateDetailsList.get(0).getReceiptafter());
        monthTxt.setText(String.valueOf(chitUpdateDetailsList.get(0).getNoofmembers()));
        String frequencyStr = String.valueOf(chitUpdateDetailsList.get(0).getFrequency());
        String commisStr = String.valueOf(chitUpdateDetailsList.get(0).getCommnPerAmount());
        String dividendStr = String.valueOf(chitUpdateDetailsList.get(0).getSamenext());
        String ownChitStr = String.valueOf(chitUpdateDetailsList.get(0).getIsownchit());
        String isOwnChitMonth = String.valueOf(chitUpdateDetailsList.get(0).getOwnchitmonth());
        String dateday = String.valueOf(chitUpdateDetailsList.get(0).getDateday());
        String chitType = String.valueOf(chitUpdateDetailsList.get(0).getChittype());


        if (!dividendStr.isEmpty()) {
            dividend.setSelection(getDividendIndex(dividend, dividendStr));
        }


        frequency.setSelection(getFrequencyIndex(frequency, frequencyStr));
        comm_spinner.setSelection(getCommiIndex(comm_spinner, commisStr));

        chitType_spinner.setSelection(getChitTypeIndex(chitType_spinner, chitType));

        if (ownChitStr.equals("N")) {
            radioGroup.check(R.id.no);
        } else {
            radioGroup.check(R.id.yes);
        }
        no_of_months_spinner.setSelection(getisOwnChitMonthndex(no_of_months_spinner, isOwnChitMonth));

        auction_day_spinner.setSelection(getisAuctionDayDate(auction_day_spinner, dateday));

        String weekStr = String.valueOf(chitUpdateDetailsList.get(0).getAuctweek());
        week_spinner.setSelection(getWeekDayDate(week_spinner, weekStr));

        String auctionday = chitUpdateDetailsList.get(0).getAuctionday();
        day_spinner.setSelection(getDayDayDate(day_spinner, auctionday));

        month_spinner.setSelection(getMonthDayDate(month_spinner, auctionday));


    }

    private int getFrequencyIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                return i;
            }
        }

        return 0;
    }

    private int getChitTypeIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                return i;
            }
        }

        return 0;
    }


    private int getCommiIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(String.valueOf(myString))) {
                return i;
            }
        }

        return 0;
    }

    private int getDividendIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(String.valueOf(myString))) {
                return i;
            }
        }

        return 0;
    }

    private int getisOwnChitMonthndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(String.valueOf(myString))) {
                return i;
            }
        }

        return 0;
    }

    private int getisAuctionDayDate(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(String.valueOf(myString))) {
                return i;
            }
        }

        return 0;
    }

    private int getWeekDayDate(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(String.valueOf(myString))) {
                return i;
            }
        }

        return 0;
    }

    private int getDayDayDate(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(String.valueOf(myString))) {
                return i;
            }
        }

        return 0;
    }

    private int getMonthDayDate(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(String.valueOf(myString))) {
                return i;
            }
        }

        return 0;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_menu, menu);
        return true;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logOut) {
            Intent intent1 = new Intent(ChitActivity.this, LoginActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
            startActivity(intent1);
            finish();


        }
        return super.onOptionsItemSelected(item);


    }

    public static boolean checkNetworkAndShowError(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean isConnected = cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
        if (!isConnected) {
            showNetWorkError(context);
        }

        return isConnected;
    }

    private static void showNetWorkError(Context context) {
        new AlertDialog.Builder(context)
                .setPositiveButton("ok", null)
                .setMessage("Please check Internet Connection!!")
                .show();

    }


}