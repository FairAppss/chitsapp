package com.fairsoft.chitfund.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fairsoft.chitfund.adapter.MembersDetailsAdapter;
import com.fairsoft.chitfund.api.Urls;
import com.fairsoft.chitfund.model.MemberIdModelItem;
import com.fairsoft.chitfund.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import java.util.List;


public class MemberEditActivity extends AppCompatActivity implements MembersDetailsAdapter.deleteRow {
    MembersDetailsAdapter membersDetailsAdapter;
    RecyclerView recyclerView;
    private List<MemberIdModelItem> memberDetailsList;
    private String json;
    String rest;
    String jsonStr;
    CardView editCardView, getContactsCardView, getMultipleContactsCardView;
    String compId, userId, userName, compName;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Integer memberId, position;
    TextView nodata;
    public static final int MY_DEFAULT_TIMEOUT = 15000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_edit);
        getSupportActionBar().setTitle(" Member Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


//shared preference data
        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        compId = String.valueOf(pref.getInt("compId", 0));
        userId = String.valueOf(pref.getInt("userId", 0));
        userName = String.valueOf(pref.getString("userName", null));
        compName = String.valueOf(pref.getString("compName", null));
        //initializing id's
        init();
        //details api
        getMemberDetails();

        editCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MemberEditActivity.this, DefineNewMemberActivity.class);
                startActivity(intent);
            }
        });
        getContactsCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MemberEditActivity.this, ListPhoneContactsActivity.class);
                startActivity(intent);

            }
        });
        getMultipleContactsCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MemberEditActivity.this, ListPhoneMultipleContactsActivity.class);
                startActivity(intent);

            }
        });

    }

    public void init() {
        editCardView = findViewById(R.id.editCardView);
        nodata = findViewById(R.id.nodata);
        getContactsCardView = findViewById(R.id.getContactsCardView);
        getMultipleContactsCardView = findViewById(R.id.getMultipleContactsCardView);


    }

    private void getMemberDetails() {
        // https://fssservices.bookxpert.co/api/Chits/MemberDetails/compId/userId/0
        RequestQueue queue = Volley.newRequestQueue(MemberEditActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_MEMBER_DETAILS + "/" + compId + "/" + userId + "/" + "0", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                memberDetailsList = new ArrayList<MemberIdModelItem>();

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
                        MemberIdModelItem memeberDefine = new MemberIdModelItem();
                        JSONObject object = (JSONObject) array.get(i);
                        memeberDefine.setDom1(object.getString("dom1"));
                        memeberDefine.setPhoneres(object.getString("phoneres"));
                        memeberDefine.setCity1(object.getString("city1"));
                        memeberDefine.setDom(object.getString("dom"));
                        memeberDefine.setRefmobile(object.getString("refmobile"));
                        memeberDefine.setCity(object.getString("city"));
                        memeberDefine.setMobile1(object.getString("mobile1"));
                        memeberDefine.setMobile11(object.getString("mobile11"));
                        memeberDefine.setPhoneoff(object.getString("phoneoff"));
                        memeberDefine.setPin(object.getString("pin"));
                        memeberDefine.setDob1(object.getString("dob1"));
                        memeberDefine.setDob(object.getString("dob"));
                        memeberDefine.setMobile2(object.getString("mobile2"));
                        memeberDefine.setName(object.getString("name"));
                        memeberDefine.setAadhar(object.getString("aadhar"));
                        memeberDefine.setState(object.getString("state"));
                        memeberDefine.setAdd2(object.getString("add2"));
                        memeberDefine.setAdd1(object.getString("add1"));
                        memeberDefine.setEmail(object.getString("email"));
                        memeberDefine.setMemberid(object.getInt("memberid"));
                        memeberDefine.setRefby(object.getString("refby"));
                        memberDetailsList.add(memeberDefine);
                    }


                } catch (Exception e) {
                    Toast.makeText(MemberEditActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }
                if (memberDetailsList.isEmpty()) {
                    nodata.setVisibility(View.VISIBLE);

                } else {
                    nodata.setVisibility(View.GONE);


                }
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MemberEditActivity.this);
                recyclerView = findViewById(R.id.memberDetailsRecyclerView);
                recyclerView.setLayoutManager(linearLayoutManager);
                membersDetailsAdapter = new MembersDetailsAdapter(MemberEditActivity.this, memberDetailsList, MemberEditActivity.this::deleteRowClick);
                recyclerView.setAdapter(membersDetailsAdapter);


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(MemberEditActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void getDeleteApi(Integer memberId) {
        // api/Chits/Deletes/{compid}/{userid}/{chitid}/{chitcode}/{type}/{typeID}
        RequestQueue queue = Volley.newRequestQueue(MemberEditActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_DELETE + "/" + compId + "/" + userId + "/" + "0" + "/" + "0" + "/" + "Member" + "/" + memberId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");

                    if (rest.equals("Fail")) {
                        Toast.makeText(MemberEditActivity.this, "This member is used in subscribers list", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(MemberEditActivity.this, "Member Deleted Successfully", Toast.LENGTH_SHORT).show();

                        getMemberDetails();


                    }


                } catch (Exception e) {
                    Toast.makeText(MemberEditActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(MemberEditActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_menu, menu);
        return true;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logOut) {
            Intent intent1 = new Intent(MemberEditActivity.this, LoginActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
            startActivity(intent1);
            finish();


        }
        return super.onOptionsItemSelected(item);


    }


    @Override
    public void deleteRowClick(Integer memberId) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                MemberEditActivity.this);

        // set title
        alertDialogBuilder.setTitle("Delete");

        // set dialog message
        alertDialogBuilder
                .setMessage("Are You Sure To Delete?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        getDeleteApi(memberId);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    public void onBackPressed() {
        Intent intent1 = new Intent(MemberEditActivity.this, DashBoardActivity.class);
        startActivity(intent1);
    }

}