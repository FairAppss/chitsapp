package com.fairsoft.chitfund.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;


import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.fairsoft.chitfund.api.Urls;
import com.fairsoft.chitfund.R;

import com.google.android.material.navigation.NavigationView;

import static com.fairsoft.chitfund.imagestopdf.Consts.checkNetworkAndShowError;


public class DashBoardActivity extends AppCompatActivity {
    public DrawerLayout drawerLayout;
    public NavigationView navigationMenu;
    public ActionBarDrawerToggle actionBarDrawerToggle;
    CardView chitDetails, chitCalculate;
    LinearLayout memberLL, chitLL, schemaLL, addSubscriberLL, auctionLL, receiptLL, paymentLL, litLevelLL, memeberLevelLL, overAllLL, runningGrpLL, receiptandPaymentLL;
    TextView liveChitTxt, turnTxt, receivableTxt, pendingTxt, memberTxt, payableTxt;
    private String json;
    String rest, auctionRes, auctionResVal, CustomerRes, CustomerResVal;
    String type;
    String compId, userId, userName, compName;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    private long backpressedTime;
    public static final int MY_DEFAULT_TIMEOUT = 15000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        getSupportActionBar().setTitle("Dash Board");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();


        compId = String.valueOf(pref.getInt("compId", 0));
        userId = String.valueOf(pref.getInt("userId", 0));
        userName = String.valueOf(pref.getString("userName", null));
        compName = String.valueOf(pref.getString("compName", null));

        //method calling
        init();
        getLiveDetails();
        getTurnDetails();
        getReceivableDetails();
        getAuctionDetails();
        getPayablerDetails();
        getCustomerDetails();


        chitDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashBoardActivity.this, ChitDetailsActivity.class);
                startActivity(intent);

            }
        });
        chitCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashBoardActivity.this, ChitCalculationActivity.class);
                startActivity(intent);

            }
        });

        memberLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(DashBoardActivity.this, MemberEditActivity.class);
                startActivity(intent1);

            }
        });
        chitLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashBoardActivity.this, ChitEditActivity.class);
                startActivity(intent);

            }
        });
        addSubscriberLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(DashBoardActivity.this, SubscriberActivity.class);
                startActivity(intent1);

            }
        });
        auctionLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(DashBoardActivity.this, AuctionActivity.class);
                startActivity(intent1);

            }
        });
        receiptLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(DashBoardActivity.this, ReceiptActivity.class);
                startActivity(intent1);

            }
        });

        paymentLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(DashBoardActivity.this, PaymentActivity.class);
                startActivity(intent1);

            }
        });
        litLevelLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(DashBoardActivity.this, ChitLevelReportActivity.class);
                startActivity(intent1);

            }
        });
        memeberLevelLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(DashBoardActivity.this, MemberLevelReportActivity.class);
                startActivity(intent1);

            }
        });
        overAllLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(DashBoardActivity.this, OverAllLevelReportActivity.class);
                startActivity(intent1);

            }
        });
        runningGrpLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(DashBoardActivity.this, RunningGroupsReportActivity.class);
                startActivity(intent1);

            }
        });
        receiptandPaymentLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(DashBoardActivity.this, ReceiptsAndPaymentsReportActivity.class);
                startActivity(intent1);

            }
        });
        schemaLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(DashBoardActivity.this, SchemeEditActivity.class);
                startActivity(intent1);

            }
        });

    }

    public void init() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
/*
        navigationMenu = (NavigationView) findViewById(R.id.navigation);
*/
        memberLL = findViewById(R.id.memberLL);
        chitLL = findViewById(R.id.chitLL);
        auctionLL = findViewById(R.id.auctionLL);
        addSubscriberLL = findViewById(R.id.addSubscriberLL);
        receiptLL = findViewById(R.id.receipetLL);
        paymentLL = findViewById(R.id.paymentLL);
        chitDetails = findViewById(R.id.chitDetails);
        chitCalculate = findViewById(R.id.chitCalculate);

        liveChitTxt = findViewById(R.id.liveChitTxt);
        turnTxt = findViewById(R.id.turnOverTxt);
        receivableTxt = findViewById(R.id.receivableTxt);
        pendingTxt = findViewById(R.id.pendingTxt);
        memberTxt = findViewById(R.id.memeberTxt);
        payableTxt = findViewById(R.id.payabeTxt);
        litLevelLL = findViewById(R.id.litLevelLL);
        memeberLevelLL = findViewById(R.id.memeberLevelLL);
        overAllLL = findViewById(R.id.overAllLL);
        runningGrpLL = findViewById(R.id.runningGrpLL);
        receiptandPaymentLL = findViewById(R.id.receiptandPaymentLL);
        schemaLL = findViewById(R.id.schemaLL);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_menu, menu);
        return true;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logOut) {
            Intent intent1 = new Intent(DashBoardActivity.this, LoginActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
            startActivity(intent1);
            finish();


        }
        return super.onOptionsItemSelected(item);


      /*  if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }*/


/*
        return super.onOptionsItemSelected(item);
*/
    }
//calling summary api s
    private void getLiveDetails() {
        //https://fssservices.bookxpert.co/api/Chits/ChitSummaries/compId/userId/C
        if (type == "C") {
            //count of live chits
        } else if (type == "T") {
            //turnover
        } else if (type == "M") {
            // no.of members
        } else if (type == "S") {
            //no.of subscribers
        } else if (type == "R") {
            //total receivables
        } else if (type == "P") {
            //total payables
        }
        //myQuery = "select Sum(Payables) Payables  from Auctions;";
        else if (type == "CA") {
        }//current month auctioned
        else if (type == "CP") {  //current month pending
        }
        RequestQueue queue = Volley.newRequestQueue(DashBoardActivity.this);
        if (!checkNetworkAndShowError(this)) {
            return;
        }

        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_CHIT_SUMMARY__DETAILS + "/" + compId + "/" + userId + "/" + "C", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    liveChitTxt.setText("Live Chits:" + "\n" + rest);

                } catch (Exception e) {
                    Toast.makeText(DashBoardActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(DashBoardActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }

    private void getTurnDetails() {
        // https://fssservices.bookxpert.co/api/Chits/ChitSummaries/compId/userId/T

        RequestQueue queue = Volley.newRequestQueue(DashBoardActivity.this);
        if (!checkNetworkAndShowError(this)) {
            return;
        }

        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_CHIT_SUMMARY__DETAILS + "/" + compId + "/" + userId + "/" + "T", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    turnTxt.setText("Turn Over:" + "\n" + rest);

                } catch (Exception e) {
                    Toast.makeText(DashBoardActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(DashBoardActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void getReceivableDetails() {
        //https://fssservices.bookxpert.co/api/Chits/ChitSummaries/compId/userId/R

        RequestQueue queue = Volley.newRequestQueue(DashBoardActivity.this);
        if (!checkNetworkAndShowError(this)) {
            return;
        }

        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_CHIT_SUMMARY__DETAILS + "/" + compId + "/" + userId + "/" + "R", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    receivableTxt.setText("Receivable:" + "\n" + rest);

                } catch (Exception e) {
                    Toast.makeText(DashBoardActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(DashBoardActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void getAuctionDetails() {
        //https://fssservices.bookxpert.co/api/Chits/ChitSummaries/compId/userId/CA

        RequestQueue queue = Volley.newRequestQueue(DashBoardActivity.this);
        if (!checkNetworkAndShowError(this)) {
            return;
        }

        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_CHIT_SUMMARY__DETAILS + "/" + compId + "/" + userId + "/" + "CA", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    json = response;
                    auctionRes = json.substring(1, json.length() - 1);
                    getPendingDetails(auctionRes);

                } catch (Exception e) {
                    Toast.makeText(DashBoardActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(DashBoardActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }


    private void getPendingDetails(String auctionResVal) {
        //https://fssservices.bookxpert.co/api/Chits/ChitSummaries/compId/userId/CP

        RequestQueue queue = Volley.newRequestQueue(DashBoardActivity.this);
        if (!checkNetworkAndShowError(this)) {
            return;
        }

        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_CHIT_SUMMARY__DETAILS + "/" + compId + "/" + userId + "/" + "CP", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    pendingTxt.setText("Cur.Month Auctions/Pending:" + auctionResVal + "/" + rest);

                } catch (Exception e) {
                    Toast.makeText(DashBoardActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(DashBoardActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void getCustomerDetails() {
        //https://fssservices.bookxpert.co/api/Chits/ChitSummaries/compId/userId/S

        RequestQueue queue = Volley.newRequestQueue(DashBoardActivity.this);
        if (!checkNetworkAndShowError(this)) {
            return;
        }

        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_CHIT_SUMMARY__DETAILS + "/" + compId + "/" + userId + "/" + "S", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    json = response;
                    CustomerRes = json.substring(1, json.length() - 1);
                    getMemberDetails(CustomerRes);

                } catch (Exception e) {
                    Toast.makeText(DashBoardActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(DashBoardActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }


    private void getMemberDetails(String CustomerResVal) {
        //https://fssservices.bookxpert.co/api/Chits/ChitSummaries/compId/userId/M

        RequestQueue queue = Volley.newRequestQueue(DashBoardActivity.this);
        if (!checkNetworkAndShowError(this)) {
            return;
        }

        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_CHIT_SUMMARY__DETAILS + "/" + compId + "/" + userId + "/" + "M", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    memberTxt.setText("Member/Customers:" + rest + "/" + CustomerResVal);

                } catch (Exception e) {
                    Toast.makeText(DashBoardActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(DashBoardActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void getPayablerDetails() {
        //https://fssservices.bookxpert.co/api/Chits/ChitSummaries/compId/userId/P

        RequestQueue queue = Volley.newRequestQueue(DashBoardActivity.this);
        if (!checkNetworkAndShowError(this)) {
            return;
        }

        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_CHIT_SUMMARY__DETAILS + "/" + compId + "/" + userId + "/" + "P", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    payableTxt.setText("Payable:" + "\n" + rest);

                } catch (Exception e) {
                    Toast.makeText(DashBoardActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(DashBoardActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                DashBoardActivity.this);

        // set title
        alertDialogBuilder.setTitle("Exit");

        // set dialog message
        alertDialogBuilder
                .setMessage("Do you really want to Logout?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Intent intent1 = new Intent(DashBoardActivity.this, LoginActivity.class);
                        intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
                        startActivity(intent1);
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


}