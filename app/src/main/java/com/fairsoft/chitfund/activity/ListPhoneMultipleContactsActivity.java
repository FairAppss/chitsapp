package com.fairsoft.chitfund.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fairsoft.chitfund.R;
import com.fairsoft.chitfund.adapter.AutoCompleteItemContactSearchAdapter;
import com.fairsoft.chitfund.adapter.CustomAdapter;
import com.fairsoft.chitfund.api.Urls;
import com.fairsoft.chitfund.model.ContactsModelItem;
import com.fairsoft.chitfund.model.CustomerContactModel;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ListPhoneMultipleContactsActivity extends AppCompatActivity {

    // Store all phone contacts list.
    // Each String format is " DisplayName \r\n Phone Number \r\n Phone Type " ( Jerry \r\n 111111 \r\n Home) .
    private List<String> phoneContactsList = new ArrayList<String>();
    // This is the phone contacts list view's data adapter.
    private ArrayAdapter<String> contactsListDataAdapter;
    ListView contactsListView = null;
    private int PERMISSION_REQUEST_CODE_READ_CONTACTS = 1;
    private int PERMISSION_REQUEST_CODE_WRITE_CONTACTS = 2;
    AutoCompleteTextView autocompleteTextView;
    private CustomerContactModel customerContactModel;
    private List<CustomerContactModel> customerContactModelList = new ArrayList<>();
    private List<ContactsModelItem> contactsModelItemArrayList = new ArrayList<>();
    private List<ContactsModelItem> contactsModelItemArrayList1 = new ArrayList<>();

    private ContactsModelItem contactsModelItem;
    boolean update = false;
    Integer memeberIdUp;
    Integer memberIdPara;
    public static final int MY_DEFAULT_TIMEOUT = 15000;




    String[] listItems;
    boolean[] checkedItems;
    ArrayList<Integer> mUserItems = new ArrayList<>();
    int preSelectedIndex = -1;
    CustomAdapter adapter;
    CardView saveCardView;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String compId, userId, userName, compName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_phone_contacts1);
        setTitle("Phone Contacts");
        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();

        compId = String.valueOf(pref.getInt("compId", 0));
        userId = String.valueOf(pref.getInt("userId", 0));
        userName = String.valueOf(pref.getString("userName", null));
        compName = String.valueOf(pref.getString("compName", null));
        autocompleteTextView=findViewById(R.id.autocompleteTextView);
        // Get contacts list view.
        contactsListView = (ListView)findViewById(R.id.display_phone_ocntacts_list_view);
        saveCardView=findViewById(R.id.saveCardView);
        // Create the list view data adapter.
         adapter = new CustomAdapter(this, customerContactModelList);
        contactsListView.setAdapter(adapter);
        contactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                CustomerContactModel model = customerContactModelList.get(i);

                if (model.isSelected())
                    model.setSelected(false);

                else
                    model.setSelected(true);


                customerContactModelList.set(i, model);
                String name=model.getName();
                String mobile=model.getMobile();
                if (mobile.startsWith("+91")) {
                    mobile = mobile.replace("+91", "");


                }else if(mobile.startsWith(" +91 ")) {
                    mobile = mobile.replace(" +91 ", "");


                }
                mobile = mobile.replace(" ","").trim();
                contactsModelItem = new ContactsModelItem(name, mobile);
                contactsModelItemArrayList.add(contactsModelItem);

/*
                customerContactModelList1.set(i,model);
*/

                //now update adapter
                adapter.updateRecords(customerContactModelList);
            }
        });
        saveCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson = new Gson();
                List<CustomerContactModel> todolist1 = new ArrayList<CustomerContactModel>(customerContactModelList);
                String jsonText = gson.toJson(todolist1);
                editor.putString("openCashModel", jsonText);
                editor.apply();

                Iterator iterator = contactsModelItemArrayList.iterator();

                while (iterator.hasNext()) {
                    ContactsModelItem o = (ContactsModelItem) iterator.next();
                    if(!contactsModelItemArrayList1.contains(o)) contactsModelItemArrayList1.add(o);
                }
                for (int i = 0; i < contactsModelItemArrayList.size(); i++) {
                    String name=contactsModelItemArrayList.get(i).getName();
                    String mobile=contactsModelItemArrayList.get(i).getValue();

                    postDataUsingVolley(name,mobile);



                }


            }
        });

/*
        contactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String s = contactsListView.getItemAtPosition(i).toString();
                String[] separated = s.split("\\:");
              String name=  separated[0]; // this will contain "name"
                String mobile=  separated[1]; // this will contain " number"
                if (mobile.startsWith("+91")) {
                    mobile = mobile.replace("+91", "");


                }else if(mobile.startsWith(" +91")) {
                    mobile = mobile.replace(" +91", "");


                }else if(mobile.startsWith(" +91 ")) {
                    mobile = mobile.replace(" +91 ", "");


                }
                    mobile = mobile.replace(" ","").trim();


                Intent intent = new Intent(ListPhoneMultipleContactsActivity.this, DefineNewMemberActivity.class);
                intent.putExtra("name", name);
                intent.putExtra("number", mobile);
                startActivity(intent);

            }
        });
*/
        if(!hasPhoneContactsPermission(Manifest.permission.READ_CONTACTS))
        {
            requestPermission(Manifest.permission.READ_CONTACTS, PERMISSION_REQUEST_CODE_READ_CONTACTS);
        }else
        {
            readPhoneContacts();
        }








        // Click this button start add phone contact activity.
        Button addPhoneContactsButton = (Button)findViewById(R.id.add_phone_contacts_button);
        addPhoneContactsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!hasPhoneContactsPermission(Manifest.permission.WRITE_CONTACTS))
                {
                    requestPermission(Manifest.permission.WRITE_CONTACTS, PERMISSION_REQUEST_CODE_WRITE_CONTACTS);
                }else
                {
                    PhoneActivity.start(getApplicationContext());
                }
            }
        });
        // Click this button to get and display phone contacts in the list view.
        CardView readPhoneContactsButton = (CardView)findViewById(R.id.read_phone_contacts_button);
        readPhoneContactsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!hasPhoneContactsPermission(Manifest.permission.READ_CONTACTS))
                {
                    requestPermission(Manifest.permission.READ_CONTACTS, PERMISSION_REQUEST_CODE_READ_CONTACTS);
                }else
                {
                    readPhoneContacts();
                }
            }
        });
    }
    // Check whether user has phone contacts manipulation permission or not.
    private boolean hasPhoneContactsPermission(String permission)
    {
        boolean ret = false;
        // If android sdk version is bigger than 23 the need to check run time permission.
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // return phone read contacts permission grant status.
            int hasPermission = ContextCompat.checkSelfPermission(getApplicationContext(), permission);
            // If permission is granted then return true.
            if (hasPermission == PackageManager.PERMISSION_GRANTED) {
                ret = true;
            }
        }else
        {
            ret = true;
        }
        return ret;
    }
    // Request a runtime permission to app user.
    private void requestPermission(String permission, int requestCode)
    {
        String requestPermissionArray[] = {permission};
        ActivityCompat.requestPermissions(this, requestPermissionArray, requestCode);
    }
    // After user select Allow or Deny button in request runtime permission dialog
    // , this method will be invoked.
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int length = grantResults.length;
        if(length > 0)
        {
            int grantResult = grantResults[0];
            if(grantResult == PackageManager.PERMISSION_GRANTED) {
                if(requestCode==PERMISSION_REQUEST_CODE_READ_CONTACTS)
                {
                    // If user grant read contacts permission.
                    readPhoneContacts();
                }else if(requestCode==PERMISSION_REQUEST_CODE_WRITE_CONTACTS)
                {
                    // If user grant write contacts permission then start add phone contact activity.
                    PhoneActivity.start(getApplicationContext());
                }
            }else
            {
                Toast.makeText(getApplicationContext(), "You denied permission.", Toast.LENGTH_LONG).show();
            }
        }
    }
    // Read and display android phone contacts in list view.
    private void readPhoneContacts()
    {
        // First empty current phone contacts list data.
        int size = phoneContactsList.size();
        for(int i=0;i<size;i++)
        {
            phoneContactsList.remove(i);
            i--;
            size = phoneContactsList.size();
        }
        // Get query phone contacts cursor object.
        Uri readContactsUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        Cursor cursor = getContentResolver().query(readContactsUri, null, null, null, null);
        if(cursor!=null)
        {
            cursor.moveToFirst();
            // Loop in the phone contacts cursor to add each contacts in phoneContactsList.
            do{
                // Get contact display name.
                int displayNameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                String userDisplayName = cursor.getString(displayNameIndex);
                // Get contact phone number.
                int phoneNumberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String phoneNumber = cursor.getString(phoneNumberIndex);
                // Get contact phone type.
                String phoneTypeStr = "Mobile";
                int phoneTypeColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE);
                int phoneTypeInt = cursor.getInt(phoneTypeColumnIndex);
                if(phoneTypeInt== ContactsContract.CommonDataKinds.Phone.TYPE_HOME)
                {
                    phoneTypeStr = "Home";
                }else if(phoneTypeInt== ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                {
                    phoneTypeStr = "Mobile";
                }else if(phoneTypeInt== ContactsContract.CommonDataKinds.Phone.TYPE_WORK)
                {
                    phoneTypeStr = "Work";
                }
                StringBuffer contactStringBuf = new StringBuffer();
                contactStringBuf.append(userDisplayName+" : "+phoneNumber);
              /*  contactStringBuf.append("\r\n");
                contactStringBuf.append(phoneNumber);
                contactStringBuf.append("\r\n");
                contactStringBuf.append(phoneTypeStr);*/

                customerContactModel = new CustomerContactModel(false,userDisplayName, phoneNumber);
                customerContactModelList.add(customerContactModel);

                phoneContactsList.add(contactStringBuf.toString());
                AutoCompleteItemContactSearchAdapter adapter = new AutoCompleteItemContactSearchAdapter(ListPhoneMultipleContactsActivity.this, customerContactModelList);
                autocompleteTextView.setAdapter(adapter);
                autocompleteTextView.setThreshold(1);
                autocompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        String  mobile = String.valueOf(adapter.getItem(i).getMobile());
                        if (mobile.startsWith("+91")) {
                            mobile = mobile.replace("+91", "");


                        }else if(mobile.startsWith(" +91 ")) {
                            mobile = mobile.replace(" +91 ", "");


                        }
                        mobile = mobile.replace(" ","").trim();

                        String  name = String.valueOf(adapter.getItem(i).getName());

                      /*  Intent intent = new Intent(ListPhoneMultipleContactsActivity.this, DefineNewMemberActivity.class);
                        intent.putExtra("name", name);
                        intent.putExtra("number", mobile);
                        startActivity(intent);
*/

                              /*  CustomerListResponseModelItem chit = (CustomerListResponseModelItem) adapterView.getSelectedItem();
                                custIdStr = String.valueOf(chit.getActid());*/

                    }
                });

            }while(cursor.moveToNext());
            // Refresh the listview to display read out phone contacts.
            adapter.notifyDataSetChanged();
        }
    }

    private void postDataUsingVolley(String nameStr, String mobileOneStr) {
        RequestQueue queue = Volley.newRequestQueue(ListPhoneMultipleContactsActivity.this);
        if (!checkNetworkAndShowError(this)) {
            return;
        }
        String updateData;
        if (update) {
            updateData = "U";
            memberIdPara = memeberIdUp;
        } else {
            updateData = "N";
            memberIdPara = 0;
        }
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_UPDATE_DETAILS + "/" + compId + "/" + userId + "/" + updateData, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {



                if (updateData.equals("U")) {

                    Toast.makeText(ListPhoneMultipleContactsActivity.this, "Member Updated SuccessFully", Toast.LENGTH_SHORT).show();

                } else {

                    Toast.makeText(ListPhoneMultipleContactsActivity.this, "Member Saved SuccessFully", Toast.LENGTH_SHORT).show();

                }


                Intent intent = new Intent(ListPhoneMultipleContactsActivity.this, MemberEditActivity.class);
                startActivity(intent);


            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ListPhoneMultipleContactsActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Name", nameStr);
                params.put("Aadhar", "");
                params.put("Mobile", mobileOneStr);
                params.put("Mobile1", "");
                params.put("Land", "");
                params.put("LandRes", "");
                params.put("Email", "");
                params.put("Address", "");
                params.put("Address1", "");
                params.put("City", "");
                params.put("State", "");
                params.put("PIN", "");
                params.put("Dob", "");
                params.put("Dom", "");
                params.put("Ref", "");
                params.put("RefMobile", "");
                params.put("memberId", String.valueOf(memberIdPara));

                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(request);


    }
    public static boolean checkNetworkAndShowError(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean isConnected = cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
        if (!isConnected) {
            showNetWorkError(context);
        }

        return isConnected;
    }

    private static void showNetWorkError(Context context) {
        new AlertDialog.Builder(context)
                .setPositiveButton("ok", null)
                .setMessage("Please check Internet Connection!!")
                .show();

    }

}