package com.fairsoft.chitfund.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fairsoft.chitfund.imagestopdf.PDFEngine;
import com.fairsoft.chitfund.R;
import com.fairsoft.chitfund.adapter.RunningGroupsReportAdapter;
import com.fairsoft.chitfund.api.Urls;
import com.fairsoft.chitfund.model.ChitModelItem;
import com.fairsoft.chitfund.model.RunningGroupsResponseModelItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class RunningGroupsReportActivity extends AppCompatActivity {
    Spinner select_chitCode;
    Integer chitIdStr;
    Integer chitCodePos;
    String chitCodeStr;
    Integer chitIdStrUp;
    private String json;
    String rest;
    String jsonStr;
    ArrayAdapter chitaa;
    private List<RunningGroupsResponseModelItem> runningGroupsResponseModelItems;
    RecyclerView recyclerView;
    RunningGroupsReportAdapter runningGroupsReportAdapter;
    TextView noDataTxt, startDateTxt, endDateTxt;
    LinearLayout totalLL;
    String compId, userId, userName, compName;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String start_Date, datePick, endDateStr, changeDate;
    final Calendar myCalendar = Calendar.getInstance();
    final Calendar myCalendar2 = Calendar.getInstance();
    RadioButton r_mode, p_mode;
    RadioGroup radioGroup;
    String value, valueUp;
    String date,time;
    public final static String PREFS_NAME = "my_prefs";
    File pdfFile = null;
    Button btn_send_wa_all_print, btn_send_mail_all_print, btn_save_all_print,send, sendWhatsapp, btnSendMultiple;
    String EMAIL = "email";
    File finalPdfFile;
    File finalPdfFile1;
    CardView saveButton;

    String fileName;
    Uri uri;

    ProgressDialog pd;
    String encoded;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_running_group_level_report);

        getSupportActionBar().setTitle("Running Chits");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();


        compId = String.valueOf(pref.getInt("compId", 0));
        userId = String.valueOf(pref.getInt("userId", 0));
        userName = String.valueOf(pref.getString("userName", null));
        compName = String.valueOf(pref.getString("compName", null));

        init();
        postDataUsingVolley(datePick, changeDate, value);


        //From Currentdate
        start_Date = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        startDateTxt.setText((start_Date));
        SimpleDateFormat dateFormatprev = new SimpleDateFormat("dd-MM-yyyy");
        Date d = null;
        try {
            d = dateFormatprev.parse(start_Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd");
        datePick = dateFormat.format(d);


        //To Currentdate

        endDateStr = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        endDateTxt.setText((endDateStr));
        SimpleDateFormat dateFormatprev1 = new SimpleDateFormat("dd-MM-yyyy");
        Date d1 = null;
        try {
            d1 = dateFormatprev1.parse(endDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyy-MM-dd");
        changeDate = dateFormat1.format(d1);

        //datePickers
        startDatePicker();
        endDatePicker();

        value = ((RadioButton) findViewById(radioGroup.getCheckedRadioButtonId()))
                .getText().toString();
        postDataUsingVolley(datePick, changeDate, value);

      /*  if (radioGroup.getCheckedRadioButtonId() == -1) {
        } else {
            postDataUsingVolley(datePick, changeDate, value);
        }
*/

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                value = ((RadioButton) findViewById(radioGroup.getCheckedRadioButtonId()))
                        .getText().toString();
                postDataUsingVolley(datePick, changeDate, value);


            }
        });


        btn_send_wa_all_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finalPdfFile == null) {
                    Toast.makeText(RunningGroupsReportActivity.this, "PDF File Not Found Click On Create Pdf", Toast.LENGTH_SHORT).show();
                    return;
                }

                Uri uri = FileProvider.getUriForFile(RunningGroupsReportActivity.this, getApplication().getPackageName() + ".provider", finalPdfFile);
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("application/pdf");
                share.putExtra(Intent.EXTRA_STREAM, uri);
                share.setPackage("com.whatsapp");
                share.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                startActivity(share);
            }
        });
        btn_send_mail_all_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finalPdfFile1 == null) {
                    Toast.makeText(RunningGroupsReportActivity.this, "PDF File Not Found Click On Create Pdf", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setType("application/pdf");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
                        new String[]{getStr(EMAIL)});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Report Generated from Day Sheet App");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
                emailIntent.putExtra(Intent.EXTRA_STREAM,
                        // Uri.fromFile(file));
                        FileProvider.getUriForFile(RunningGroupsReportActivity.this, getPackageName() + ".provider", finalPdfFile1));
                emailIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            }
        });

        btn_save_all_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkCheckPermission()) {
                    // savePetrolData();

                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    createPDF();

                }
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*
                String numberTxt = mobileNumber.getText().toString();
*/

                addContact("918142323365");
            }
        });

        sendWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*
                String numberTxt = mobileNumber.getText().toString();
*/


                sendMsgToWhatsapp("918142323365");
            }
        });
        btnSendMultiple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*
                String numberTxt = mobileNumber.getText().toString();
*/

                if (finalPdfFile == null) {
                    Toast.makeText(RunningGroupsReportActivity.this, "PDF File Not Found Click On Create Pdf", Toast.LENGTH_SHORT).show();
                    return;
                } else {


                    encoded = encodeFileToBase64Binary(finalPdfFile);
/*
                    if (encoded != null) {
                        uploadDocs(encoded);
                    }
*/
                }

                uri = FileProvider.getUriForFile(RunningGroupsReportActivity.this, getApplication().getPackageName() + ".provider", finalPdfFile);

                sendMsgToMultipleWhatsapp("918142323365", String.valueOf(encoded));
            }
        });


    }

    public void init() {

        select_chitCode = findViewById(R.id.select_chitCode);
        recyclerView = findViewById(R.id.runningRecyclerView);
        noDataTxt = findViewById(R.id.noData);
        totalLL = findViewById(R.id.totalLL);
        startDateTxt = findViewById(R.id.fromDate);
        endDateTxt = findViewById(R.id.toDate);
        r_mode = findViewById(R.id.r_mode);
        p_mode = findViewById(R.id.p_mode);
        radioGroup = findViewById(R.id.radioButton);
        btn_send_wa_all_print = findViewById(R.id.btn_send_wa_all_print);
        btn_send_mail_all_print = findViewById(R.id.btn_send_mail_all_print);
        btn_save_all_print = findViewById(R.id.btn_save_all_print);
        saveButton = findViewById(R.id.saveButton);

        send = findViewById(R.id.send);
        sendWhatsapp = findViewById(R.id.sendWhatsapp);
        btnSendMultiple = findViewById(R.id.btnSendMultiple);



    }

    @SuppressLint("ClickableViewAccessibility")
    public void startDatePicker() {
        DatePickerDialog.OnDateSetListener startDates = new
                DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar2.set(Calendar.YEAR, year);
                        myCalendar2.set(Calendar.MONTH, monthOfYear);
                        myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        startDate();
                        value = ((RadioButton) findViewById(radioGroup.getCheckedRadioButtonId()))
                                .getText().toString();
/*
                        postDataUsingVolley(datePick, changeDate, value);
*/
                    }

                };

        startDateTxt.setOnTouchListener(new View.OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(RunningGroupsReportActivity.this, startDates, myCalendar2
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar2.get(Calendar.DAY_OF_MONTH)).show();
                }
                return false;
            }
        });


    }

    @SuppressLint("ClickableViewAccessibility")
    public void endDatePicker() {
        DatePickerDialog.OnDateSetListener date = new
                DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateLabel();
                        value = ((RadioButton) findViewById(radioGroup.getCheckedRadioButtonId()))
                                .getText().toString();
/*
                        postDataUsingVolley(datePick, changeDate, value);
*/

                    }

                };

        endDateTxt.setOnTouchListener(new View.OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(RunningGroupsReportActivity.this, date, myCalendar2
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar2.get(Calendar.DAY_OF_MONTH)).show();
                }
                return false;
            }
        });


    }

    private void updateLabel() {

        String myFormat = "dd-MM-yyyy";


        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        endDateStr = sdf.format(myCalendar.getTime());
        endDateTxt.setText((endDateStr));


        SimpleDateFormat dateFormatprev = new SimpleDateFormat("dd-MM-yyyy");
        Date d = null;
        try {
            d = dateFormatprev.parse(endDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd");
        changeDate = dateFormat.format(d);


    }

    private void startDate() {
        String myFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        start_Date = sdf.format(myCalendar2.getTime());
        startDateTxt.setText(start_Date);
        SimpleDateFormat dateFormatprev = new SimpleDateFormat("dd-MM-yyyy");
        Date d = null;
        try {
            d = dateFormatprev.parse(start_Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd");
        datePick = dateFormat.format(d);
    }


    class ChitSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            // Toast.makeText(v.getContext(), "Your choose :" + chit[position], Toast.LENGTH_SHORT).show();
            chitIdStr = select_chitCode.getSelectedItemPosition();
            chitCodeStr = select_chitCode.getItemAtPosition(position).toString();
            chitCodePos = select_chitCode.getSelectedItemPosition();
            ChitModelItem chit = (ChitModelItem) parent.getSelectedItem();
            chitIdStrUp = chit.getMemberid();


        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private void getChitid() {
        RequestQueue queue = Volley.newRequestQueue(RunningGroupsReportActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_CHIT_COMPLETE_DETAILS + "/" + compId + "/" + userId + "/" + "/" + "0", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // chitDetailsList = new ArrayList<ChitCompleDetailsModelItem>();
                ArrayList<ChitModelItem> chitList = new ArrayList<>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject object = (JSONObject) array.get(i);

                        String chitname = object.getString("chitcode");
                        Integer chitid = object.getInt("chitid");
                        chitList.add(new ChitModelItem(chitid, chitname));


                    }


                    chitaa = new ArrayAdapter(RunningGroupsReportActivity.this, android.R.layout.simple_spinner_item, chitList);
                    chitaa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    //Setting the ArrayAdapter data on the Spinner
                    select_chitCode.setAdapter(chitaa);
                    select_chitCode.setOnItemSelectedListener(new RunningGroupsReportActivity.ChitSpinnerClass());


                } catch (Exception e) {
                    Toast.makeText(RunningGroupsReportActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(RunningGroupsReportActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        queue.add(request);

    }


    private void postDataUsingVolley(String fromDate, String toDate, String valueStr) {

        RequestQueue queue = Volley.newRequestQueue(RunningGroupsReportActivity.this);
        //api/Chits/RunningGroups/{compid}/{userid}/{trmode}/{DateFrom}/{DateTo}
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_RUNNING_GROUP_REPORTS + "/" + compId + "/" + userId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                runningGroupsResponseModelItems = new ArrayList<RunningGroupsResponseModelItem>();

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    if (jsonStr.equals("No Data Found")) {
                        noDataTxt.setVisibility(View.VISIBLE);
                    } else {
                        noDataTxt.setVisibility(View.GONE);

                        JSONArray array = new JSONArray(jsonStr);
                        for (int i = 0; i < array.length(); i++) {

                            RunningGroupsResponseModelItem runningGroupsResponse = new RunningGroupsResponseModelItem();
                            JSONObject object = (JSONObject) array.get(i);
                            runningGroupsResponse.setAuctiontime(object.getString("auctiontime"));
                            runningGroupsResponse.setCommn(object.getInt("commn"));
                            runningGroupsResponse.setChitamt(object.getInt("chitamt"));
                            runningGroupsResponse.setAuctionday(object.getString("auctionday"));
                            runningGroupsResponse.setStartdate(object.getString("startdate"));
                            runningGroupsResponse.setAuctweek(object.getString("auctweek"));
                            runningGroupsResponse.setNoofmembers(object.getInt("noofmembers"));
                            runningGroupsResponse.setFrequency(object.getString("frequency"));
                            runningGroupsResponse.setComp(object.getString("Comp"));
                            runningGroupsResponse.setNomonths(object.getInt("nomonths"));
                            runningGroupsResponse.setPayable(object.getString("payable"));
                            runningGroupsResponse.setAuctionday1(object.getString("auctionday1"));
                            runningGroupsResponse.setLastbid(object.getString("lastbid"));
                            runningGroupsResponse.setCommnPerAmount(object.getString("commnPerAmount"));
/*
                            runningGroupsResponse.setDividend(object.getInt("dividend"));
*/
                            runningGroupsResponse.setPaymentafter(object.getInt("paymentafter"));
                            runningGroupsResponse.setBidamount(object.getString("bidamount"));
                            runningGroupsResponse.setChitdate(object.getString("chitdate"));
                            runningGroupsResponse.setChitcode(object.getString("chitcode"));
                            runningGroupsResponse.setDateday(object.getString("dateday"));
                            runningGroupsResponse.setCompleted(object.getString("completed"));
                            runningGroupsResponse.setReceivable(object.getString("receivable"));
                            runningGroupsResponse.setColumn1(object.getString("Column1"));
                            runningGroupsResponse.setReceiptafter(object.getInt("receiptafter"));
                            runningGroupsResponse.setChitid(object.getInt("chitid"));
                            runningGroupsResponse.setEnddate(object.getString("enddate"));
                            runningGroupsResponse.setIsownchit(object.getString("isownchit"));
                            runningGroupsResponse.setAucttimeAMPM(object.getString("aucttimeAMPM"));
                            runningGroupsResponse.setSamenext(object.getString("samenext"));
                            runningGroupsResponseModelItems.add(runningGroupsResponse);
                        }

                    }
                    if (runningGroupsResponseModelItems.isEmpty()) {
                        noDataTxt.setVisibility(View.VISIBLE);
                    } else {
                        noDataTxt.setVisibility(View.GONE);


                    }


                } catch (Exception e) {

                    Toast.makeText(RunningGroupsReportActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                }


                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(RunningGroupsReportActivity.this);
                recyclerView.setLayoutManager(linearLayoutManager);
                runningGroupsReportAdapter = new RunningGroupsReportAdapter(RunningGroupsReportActivity.this, runningGroupsResponseModelItems);
                recyclerView.setAdapter(runningGroupsReportAdapter);

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(RunningGroupsReportActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        queue.add(request);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_menu, menu);
        return true;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logOut) {
            Intent intent1 = new Intent(RunningGroupsReportActivity.this, LoginActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
            startActivity(intent1);
            finish();


        }
        return super.onOptionsItemSelected(item);


    }

    public String getStr(String key) {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
        return prefs.getString(key, "");
    }

    private boolean checkCheckPermission() {
        if (ContextCompat.checkSelfPermission(
                RunningGroupsReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                RunningGroupsReportActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            // You can use the API that requires the permission.
            return true;
        } else {
            // You can directly ask for the permission.
            // The registered ActivityResultCallback gets the result of this request.
            ActivityCompat.requestPermissions(RunningGroupsReportActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1011);
            return false;
        }
    }

    private void createPDF() {
        date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        time = new SimpleDateFormat("HH-mm-ss", Locale.getDefault()).format(new Date());

        View u = findViewById(R.id.scrollPdf);

        HorizontalScrollView z = findViewById(R.id.scrollPdf);
        int totalHeight = z.getChildAt(0).getHeight();
        int totalWidth = z.getChildAt(0).getWidth();

        Bitmap bitmap = takeScreenShot(u, totalHeight, totalWidth);
/*
        Bitmap bitmap = takeScreenShot();
*/
        Bitmap[] bitmaps = new Bitmap[]{bitmap};
        fileName="RunningChits_" + date+"-"+time + ".pdf";


        PDFEngine.getInstance().createPDF(RunningGroupsReportActivity.this,
                bitmaps,
                fileName,
                (pdfFile, numOfImages) -> {
                    if (pdfFile == null) {
                        Toast.makeText(RunningGroupsReportActivity.this, "Error in PDF Generation Error.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Toast.makeText(RunningGroupsReportActivity.this, "PDF File generated", Toast.LENGTH_SHORT).show();
                    finalPdfFile = pdfFile;
                    PDFEngine.getInstance().openPDF(RunningGroupsReportActivity.this,finalPdfFile);

                    btn_send_wa_all_print.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (finalPdfFile == null) {
                                Toast.makeText(RunningGroupsReportActivity.this, "PDF File Not Found", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            Uri uri = FileProvider.getUriForFile(RunningGroupsReportActivity.this, getApplication().getPackageName() + ".provider", finalPdfFile);
                            Intent share = new Intent(Intent.ACTION_SEND);
                            share.setType("application/pdf");
                            share.putExtra(Intent.EXTRA_STREAM, uri);
                            share.setPackage("com.whatsapp");
                            share.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                            startActivity(share);
                        }
                    });
                    finalPdfFile1 = pdfFile;
                    btn_send_mail_all_print.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (finalPdfFile1 == null) {
                                Toast.makeText(RunningGroupsReportActivity.this, "PDF File Not Found", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                            emailIntent.setType("application/pdf");
                            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
                                    new String[]{getStr(EMAIL)});
                            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Report Generated from Day Sheet App");
                            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
                            emailIntent.putExtra(Intent.EXTRA_STREAM,
                                    // Uri.fromFile(file));
                                    FileProvider.getUriForFile(RunningGroupsReportActivity.this, getPackageName() + ".provider", finalPdfFile1));
                            emailIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                        }
                    });


                    pdfFile = pdfFile;
                    // PDFEngine.getInstance().sharePDF(MainActivity.this, pdfFile);
                });
    }

    protected Bitmap takeScreenShot(View view, int totalHeight, int totalWidth) {

        Bitmap returnedBitmap = Bitmap.createBitmap(totalWidth, totalHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return returnedBitmap;


    }

    private void addContact(String number) {

        RequestQueue queue = Volley.newRequestQueue(RunningGroupsReportActivity.this);
        StringRequest request = new StringRequest(Request.Method.GET, Urls.URL_WHATSAPP_CONTACT_ADD + "/" + number + "/" + "Rajitha", new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Toast.makeText(RunningGroupsReportActivity.this, response.toString(), Toast.LENGTH_SHORT).show();


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(RunningGroupsReportActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        queue.add(request);

    }

    private void sendMsgToWhatsapp(String number) {

        RequestQueue queue = Volley.newRequestQueue(RunningGroupsReportActivity.this);
        StringRequest request = new StringRequest(Request.Method.GET, Urls.URL_SEND_WHATSAP + "/" + number + "/" + "Rajitha" + "/" + "Hello", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(RunningGroupsReportActivity.this, response.toString(), Toast.LENGTH_SHORT).show();

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(RunningGroupsReportActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {


        };
        // below line is to make
        // a json object request.
        queue.add(request);

    }

    private void sendMsgToMultipleWhatsapp(String number, String binaryString) {


        RequestQueue queue = Volley.newRequestQueue(RunningGroupsReportActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_SEND_WHATSAP_FILE, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(RunningGroupsReportActivity.this, response.toString(), Toast.LENGTH_SHORT).show();

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(RunningGroupsReportActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {


            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("WANo", "918142323365");
                params.put("msg", "hello");
                params.put("fileName", String.valueOf(fileName));
                params.put("filebinary", String.valueOf(binaryString));


                return params;
            }


        };
        // below line is to make
        // a json object request.
        queue.add(request);

    }

    private String encodeFileToBase64Binary(File yourFile) {
        int size = (int) yourFile.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(yourFile));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        String encoded = Base64.encodeToString(bytes, Base64.NO_WRAP);
        appendLog(encoded);
        return encoded;
    }

    public static void appendLog(String text) {
        File logFile = new File(android.os.Environment.getExternalStorageDirectory(), "log.txt");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {

                e.printStackTrace();
            }
        }
        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


}