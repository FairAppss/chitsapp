package com.fairsoft.chitfund.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.fairsoft.chitfund.R;
import com.fairsoft.chitfund.adapter.ChitBidCalculationAdapter;

import java.util.ArrayList;
import java.util.List;

public class BidResultDialog extends AppCompatDialogFragment {
    private List<Double> monthlistData = new ArrayList<Double>();
    private List<Double> interestBid = new ArrayList<Double>();
    RecyclerView recyclerView;
    ChitBidCalculationAdapter chitBidCalculationAdapter;



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        LayoutInflater inflater=getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.activity_bid_result_dialog,null );
        Intent intent = getActivity().getIntent();
        Bundle extras = intent.getExtras();


        if (extras != null) {
            List<Double> savedSelections = (List<Double>) extras.getSerializable("mylist");
            List<Double> savedSelections1 = (List<Double>) extras.getSerializable("myInterestlist");
            monthlistData.addAll(savedSelections);
            interestBid.addAll(savedSelections1);


        }

        recyclerView = getActivity().findViewById(R.id.bidRecycler);

        builder.setView(view).setTitle("Login").setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        chitBidCalculationAdapter = new ChitBidCalculationAdapter(getContext(), monthlistData, interestBid);
        recyclerView.setAdapter(chitBidCalculationAdapter);
        chitBidCalculationAdapter.notifyDataSetChanged();


        return builder.create();
    }


}