package com.fairsoft.chitfund.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fairsoft.chitfund.api.Urls;
import com.fairsoft.chitfund.model.ChitDefineModelItem;
import com.fairsoft.chitfund.model.ChitModelItem;
import com.fairsoft.chitfund.model.MemberModelItem;
import com.fairsoft.chitfund.model.ReceiptCompleteDetailsModelItem;
import com.fairsoft.chitfund.model.SubscriberDetailsModelItem;
import com.fairsoft.chitfund.R;
import com.fairsoft.chitfund.adapter.ReceiptAdapter;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static java.lang.Integer.valueOf;

public class ReceiptActivity extends AppCompatActivity implements ReceiptAdapter.deleteRow,ReceiptAdapter.onItemClickListener {
    ReceiptAdapter receiptAdapter;
    Spinner select_chitCode, memberSpin;
    TextView sendTxt;
    RecyclerView recyclerView;
    Integer chitIdStr;
    Integer chitCodePos;
    String chitCodeStr;
    private String json;
    String rest;
    String jsonStr;
    private List<ReceiptCompleteDetailsModelItem> receiptCompleteDetailsModelItems;
    private List<ChitDefineModelItem> chitDefineModelItemArrayList;
    ArrayAdapter chitaa;
    ArrayAdapter memberbb;
    private List<SubscriberDetailsModelItem> subscriberDetailsList;
    String memberCodeStr, mobile, name, name1;
    Integer memberIdStr;
    Integer memeberIdStrUp, chitIdStrUp;

    private List<String> listData = new ArrayList<String>();
    private List<String> prizedlistData = new ArrayList<String>();
    ArrayList<MemberModelItem> memberList = new ArrayList<>();
    ArrayList<ChitModelItem> chitList;

    boolean update = false;
    String status;
    String payAmount, radioValue, remarksStr;
    String createdBy;
    CardView saveButton;
    EditText dateTxt, recAmountTxt, remarksEd;
    final Calendar myCalendar = Calendar.getInstance();
    String auctiondayStr;
    String changedDate, chitCodeUp;
    Integer chitIdUp, position, receiptIdUp, receiptIdPara;
    String compId, userId, userName, compName;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    RadioButton cash, bank, upi;
    RadioGroup radioGroup;
    TextView nodata;
    String subId, subTick;
    String chit1;
    ProgressBar progressBar;
    public static final int MY_DEFAULT_TIMEOUT = 15000;
    Spinner recType;
    String[] recTypes = {"Fee", "Interest"};
    String recTypeStr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt);
        getSupportActionBar().setTitle("Receipt");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();


        compId = String.valueOf(pref.getInt("compId", 0));
        userId = String.valueOf(pref.getInt("userId", 0));
        userName = String.valueOf(pref.getString("userName", null));
        compName = String.valueOf(pref.getString("compName", null));


        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        init();
        auctiondayStr = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        dateTxt.setText((auctiondayStr));
        getChitid();
        if (extras != null) {
            chitIdUp = extras.getInt("chitId");
            position = extras.getInt("position");
            chitCodeUp = extras.getString("chitCode");
            receiptIdUp = extras.getInt("receiptId");
            update = true;

            getUpdatedReceiptDetails(Integer.valueOf(chitIdUp), receiptIdUp);
        }
        auctionDatePicker();
        updateLabel();
        //adding Commn spinner
        ArrayAdapter selectCommn = new ArrayAdapter(this, R.layout.spinner_text, recTypes);
        selectCommn.setDropDownViewResource(R.layout.spinner_text);
        recType.setAdapter(selectCommn);
        recType.setOnItemSelectedListener(new CommnSpinnerClass());


        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payAmount = recAmountTxt.getText().toString();
                remarksStr = remarksEd.getText().toString();
                radioValue = ((RadioButton) findViewById(radioGroup.getCheckedRadioButtonId()))
                        .getText().toString();

                validateFields();

            }
        });


        sendTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.putExtra("address", "8142323365; 8074051219");
                i.putExtra("sms_body", "Testing you!");
                i.setType("vnd.android-dir/mms-sms");
                startActivity(i);
            }
        });


    }

    public void init() {
        recyclerView = findViewById(R.id.receiptRecyclerView);
        select_chitCode = findViewById(R.id.select_chitCode);
        memberSpin = (Spinner) findViewById(R.id.member);
        sendTxt = findViewById(R.id.sendMsg);
        saveButton = findViewById(R.id.saveButton);
        dateTxt = (EditText) findViewById(R.id.dateTxt);
        recAmountTxt = findViewById(R.id.amountTxt);
        recAmountTxt.setHint(Html.fromHtml("Amount <font color =\"#cc0029\" >*</font>"));

        cash = findViewById(R.id.cash);
        bank = findViewById(R.id.bank);
        upi = findViewById(R.id.upi);
        radioGroup = findViewById(R.id.radioButtonRec);
        remarksEd = findViewById(R.id.remarksED);

        nodata = findViewById(R.id.nodata);
        progressBar = findViewById(R.id.progressBar);
        recType = findViewById(R.id.recType);


    }

    @Override
    public void onItemClick(Integer chitId, Integer receiptId, String chitCode) {
        update = true;
        receiptIdUp=receiptId;

        getUpdatedReceiptDetails(Integer.valueOf(chitId), receiptId);

    }

    class CommnSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            recTypeStr = recType.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }


    }


    //datePicker
    @SuppressLint("ClickableViewAccessibility")
    public void auctionDatePicker() {
        DatePickerDialog.OnDateSetListener date = new
                DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateLabel();
                    }

                };


        dateTxt.setOnTouchListener(new View.OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(ReceiptActivity.this, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
                return false;
            }
        });


    }

    private void updateLabel() {

        String myFormat = "dd-MM-yyyy";

        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        auctiondayStr = sdf.format(myCalendar.getTime());
        dateTxt.setText((auctiondayStr));

        dateTxt.setSelection(dateTxt.getText().length());

        SimpleDateFormat dateFormatprev = new SimpleDateFormat("dd-MM-yyyy");
        Date d = null;
        try {
            d = dateFormatprev.parse(auctiondayStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd");
        changedDate = dateFormat.format(d);


    }

    @Override
    public void deleteRowClick(Integer chitId, Integer receiptId, String chitCode) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ReceiptActivity.this);

        // set title
        alertDialogBuilder.setTitle("Delete");

        // set dialog message
        alertDialogBuilder
                .setMessage("Are You Sure To Delete?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        getDeleteApi(chitId, receiptId, chitCode);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();


    }

    class ChitSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            // Toast.makeText(v.getContext(), "Your choose :" + chit[position], Toast.LENGTH_SHORT).show();
            chitIdStr = select_chitCode.getSelectedItemPosition();
            chitCodeStr = select_chitCode.getItemAtPosition(position).toString();
            chitCodePos = select_chitCode.getSelectedItemPosition();
            ChitModelItem chit = (ChitModelItem) parent.getSelectedItem();
            chitIdStrUp = chit.getMemberid();

            if (position >= 0) {
                memberList.clear();
                getMemberDetails(chitIdStrUp);
                getReceiptDetails(Integer.valueOf(chitIdStrUp));

            }


        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    class PrizedMember implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            // Toast.makeText(v.getContext(), "Your choose :" + chit[position], Toast.LENGTH_SHORT).show();
            memberIdStr = memberSpin.getSelectedItemPosition() + 1;
            memberCodeStr = memberSpin.getItemAtPosition(position).toString() + 1;
            MemberModelItem member = (MemberModelItem) parent.getSelectedItem();
            memeberIdStrUp = member.getMemberid();
            mobile = member.getMobile();
            name = member.getName();
            String[] separated = name.split("-");
            String ticket = separated[0]; // this will contain "name"
            name1 = separated[1]; // this will contain " number"


        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    public void validateFields() {

        if (chitCodeStr.isEmpty() || chitCodeStr.equals("select ChitCode")) {
            Toast.makeText(ReceiptActivity.this, "Please select ChitCode", Toast.LENGTH_SHORT).show();

            return;

        } else if (payAmount.isEmpty()) {
            Toast.makeText(ReceiptActivity.this, "Please enter Amount", Toast.LENGTH_SHORT).show();

            return;
        }

        postDataUsingVolley(chitCodeStr, chitIdStrUp, memeberIdStrUp, status, payAmount, changedDate, createdBy, radioValue, remarksStr);


    }


    private void getReceiptDetails(Integer chitId) {
        RequestQueue queue = Volley.newRequestQueue(ReceiptActivity.this);
        //api/Chits/ReceiptDetails/{compid}/{userid}/{chitid}/{Receiptid}
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_RECEIPT_COMPLETE_DETAILS + "/" + compId + "/" + userId + "/" + chitId + "/" + 0, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                receiptCompleteDetailsModelItems = new ArrayList<ReceiptCompleteDetailsModelItem>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
                        ReceiptCompleteDetailsModelItem auctionDefine = new ReceiptCompleteDetailsModelItem();
                        JSONObject object = (JSONObject) array.get(i);
                        auctionDefine.setChitid(object.getInt("chitid"));
                        auctionDefine.setAmount(object.getInt("amount"));
                        auctionDefine.setRectype(object.getString("rectype"));
                        auctionDefine.setCreatedby(object.getInt("createdby"));
                        auctionDefine.setCreateddate(object.getString("createddate"));
                        auctionDefine.setChitcode(object.getString("chitcode"));
                        auctionDefine.setName(object.getString("name"));
                        auctionDefine.setReceiptdate(object.getString("receiptdate"));
                        auctionDefine.setReceiptid(object.getInt("receiptid"));
                        auctionDefine.setSubscriberid(object.getInt("subscriberid"));

                        if (object.isNull("memberid")) {
                            object.put("memberid", "");

                        } else {
                            auctionDefine.setMemberid(object.getInt("memberid"));


                        }
                        receiptCompleteDetailsModelItems.add(auctionDefine);
                    }


                } catch (Exception e) {
                    Toast.makeText(ReceiptActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }
                if (receiptCompleteDetailsModelItems.isEmpty()) {
                    nodata.setVisibility(View.VISIBLE);

                } else {
                    nodata.setVisibility(View.GONE);


                }
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ReceiptActivity.this);
                recyclerView.setLayoutManager(linearLayoutManager);

                receiptAdapter = new ReceiptAdapter(ReceiptActivity.this, receiptCompleteDetailsModelItems, ReceiptActivity.this::deleteRowClick,ReceiptActivity.this::onItemClick);
                recyclerView.setAdapter(receiptAdapter);
                receiptAdapter.notifyDataSetChanged();

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ReceiptActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void getChitid() {
        RequestQueue queue = Volley.newRequestQueue(ReceiptActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_AUCTION_CHITS_DETAILS + "/" + compId + "/" + userId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // chitDefineModelItemArrayList = new ArrayList<ChitDefineModelItem>();
                chitList = new ArrayList<>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {


                        // ChitDefineModelItem chitDefine = new ChitDefineModelItem();
                        JSONObject object = (JSONObject) array.get(i);
                        String chitname = object.getString("chitcode");
                        Integer chitid = object.getInt("chitid");
                        chitList.add(new ChitModelItem(chitid, chitname));


                    }


                    if (listData == null) {
                        listData.add(0, "No data");
                    } else {
                        chitaa = new ArrayAdapter(ReceiptActivity.this, android.R.layout.simple_spinner_item, chitList);
                        chitaa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner
                        listData.add(0, "select ChitCode");
                        select_chitCode.setAdapter(chitaa);
                        select_chitCode.setOnItemSelectedListener(new ReceiptActivity.ChitSpinnerClass());

/*
                        if(update){

                            select_chitCode.setSelection(getChitIndex(select_chitCode, chit1));

                        }
*/

                    }


                } catch (Exception e) {
                    Toast.makeText(ReceiptActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ReceiptActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }


    private void getMemberDetails(Integer chitId) {
        RequestQueue queue = Volley.newRequestQueue(ReceiptActivity.this);
        //api/Chits/SubscriberDetails/{compid}/{userid}/{chitid}/{subscriberid}
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_SUBSCRIBER_DETAILS + "/" + compId + "/" + userId + "/" + chitId + "/" + 0, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // subscriberDetailsList = new ArrayList<SubscriberDetailsModelItem>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = (JSONObject) array.get(i);

                        String membername = object.getString("ticketno") + "-" + object.getString("name");
                        Integer memberid = object.getInt("subscriberid");
                        String mobile = object.getString("mobile1");

                        memberList.add(new MemberModelItem(mobile, memberid, membername));

                    }

                    if (memberList == null) {
                        prizedlistData.add(0, "No data");
                    } else {
                        memberbb = new ArrayAdapter(ReceiptActivity.this, android.R.layout.simple_spinner_item, memberList);
                        memberbb.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner
                        memberSpin.setAdapter(memberbb);
                        memberSpin.setOnItemSelectedListener(new ReceiptActivity.PrizedMember());

                    }


                } catch (Exception e) {
                    Toast.makeText(ReceiptActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ReceiptActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }


    private void postDataUsingVolley(String chitCodep, Integer chitIdp, Integer memeberId, String status, String payAmount, String payDate, String createdBy, String radioValue, String remarksStr) {
        if (!checkNetworkAndShowError(this)) {
            return;
        }
        progressBar.setVisibility(View.VISIBLE);


        String updateData;
        if (update) {
            updateData = "U";
            receiptIdPara = receiptIdUp;

        } else {
            updateData = "N";
            receiptIdPara = 0;

        }
        // creating a new variable for our request queue
        RequestQueue queue = Volley.newRequestQueue(ReceiptActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_RECEIPT_SAVE_DETAILS + "/" + compId + "/" + userId + "/" + updateData, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String json = response;
                String rest = json.substring(1, json.length() - 1);


                if (rest.equals("Success")) {
                    // on below line we are displaying a success toast message.
                    if (updateData.equals("U")) {
                        progressBar.setVisibility(View.GONE);
                        getChitid();

                        //adding Commn spinner
                        ArrayAdapter selectCommn = new ArrayAdapter(ReceiptActivity.this, R.layout.spinner_text, recTypes);
                        selectCommn.setDropDownViewResource(R.layout.spinner_text);
                        recType.setAdapter(selectCommn);
                        recType.setOnItemSelectedListener(new CommnSpinnerClass());


                        getReceiptDetails(Integer.valueOf(chitIdStrUp));

                        Toast.makeText(ReceiptActivity.this, "Receipt Updated SuccessFully", Toast.LENGTH_SHORT).show();
                        update = false;
                        Gson gson = new Gson();
                        List<MemberModelItem> todolist1 = new ArrayList<MemberModelItem>(memberList);
                        String jsonText = gson.toJson(todolist1);
                        Bundle args = new Bundle();
                        args.putString("mobileList", mobile);
                        args.putString("chitCodep", chitCodep);
                        args.putString("name", name1);
                        args.putString("auctionamountp", payAmount);
                        args.putString("auctionDatep", changedDate);


                        MessageReceDialog dialog = new MessageReceDialog();
                        dialog.setCancelable(false);
                        FragmentActivity activity = (FragmentActivity) (ReceiptActivity.this);
                        FragmentManager fm = activity.getSupportFragmentManager();
                        dialog.setArguments(args);
                        dialog.show(fm, "MyDialogFragment");


                    } else {
                        progressBar.setVisibility(View.GONE);
                        getChitid();
                        //adding Commn spinner
                        ArrayAdapter selectCommn = new ArrayAdapter(ReceiptActivity.this, R.layout.spinner_text, recTypes);
                        selectCommn.setDropDownViewResource(R.layout.spinner_text);
                        recType.setAdapter(selectCommn);
                        recType.setOnItemSelectedListener(new CommnSpinnerClass());


                        getReceiptDetails(Integer.valueOf(chitIdStrUp));

                        Toast.makeText(ReceiptActivity.this, "Receipt Saved SuccessFully", Toast.LENGTH_SHORT).show();
                        Gson gson = new Gson();
                        List<MemberModelItem> todolist1 = new ArrayList<MemberModelItem>(memberList);
                        String jsonText = gson.toJson(todolist1);
                        Bundle args = new Bundle();
                        args.putString("mobileList", mobile);
                        args.putString("chitCodep", chitCodep);
                        args.putString("name", name1);

                        args.putString("auctionamountp", payAmount);
                        args.putString("auctionDatep", changedDate);


                        MessageReceDialog dialog = new MessageReceDialog();
                        dialog.setCancelable(false);
                        FragmentActivity activity = (FragmentActivity) (ReceiptActivity.this);
                        FragmentManager fm = activity.getSupportFragmentManager();
                        dialog.setArguments(args);
                        dialog.show(fm, "MyDialogFragment");


                    }
                }else {
                    Toast.makeText(ReceiptActivity.this, rest, Toast.LENGTH_SHORT).show();


                }


                dateTxt.setText((auctiondayStr));
                recAmountTxt.setText("");


            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ReceiptActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // below line we are creating a map for
                // storing our values in key and value pair.
                Map<String, String> params = new HashMap<String, String>();

                // on below line we are passing our key
                // and value pair to our parameters.
                params.put("rectid", String.valueOf(receiptIdPara));
                params.put("chitcode", chitCodep);
                params.put("chitId", String.valueOf(chitIdp));
                params.put("memberid", String.valueOf(memeberId));
                params.put("status", recTypeStr);
                params.put("recamount", payAmount);
                params.put("recdate", payDate);
                params.put("createdby", userId);
                params.put("paythrough", radioValue);
                params.put("remarks", remarksStr);


                // at last we are
                // returning our params.
                return params;
            }
        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }

    private void getUpdatedReceiptDetails(Integer chitId, Integer receiptId) {
        RequestQueue queue = Volley.newRequestQueue(ReceiptActivity.this);
        //api/Chits/ReceiptDetails/{compid}/{userid}/{chitid}/{Receiptid}
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_RECEIPT_COMPLETE_DETAILS + "/" + compId + "/" + userId + "/" + chitId + "/" + receiptId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                receiptCompleteDetailsModelItems = new ArrayList<ReceiptCompleteDetailsModelItem>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
                        ReceiptCompleteDetailsModelItem auctionDefine = new ReceiptCompleteDetailsModelItem();
                        JSONObject object = (JSONObject) array.get(i);
                        auctionDefine.setChitid(object.getInt("chitid"));
                        auctionDefine.setAmount(object.getInt("amount"));
                        auctionDefine.setRectype(object.getString("rectype"));
                        auctionDefine.setCreatedby(object.getInt("createdby"));
                        auctionDefine.setCreateddate(object.getString("createddate"));
                        auctionDefine.setChitcode(object.getString("chitcode"));
                        auctionDefine.setName(object.getString("name"));
                        auctionDefine.setReceiptdate(object.getString("receiptdate"));
                        auctionDefine.setReceiptid(object.getInt("receiptid"));
                        auctionDefine.setSubscriberid(object.getInt("subscriberid"));
                        auctionDefine.setTicketno(object.getString("ticketno"));
                        String chit = object.getString("chitcode");


                        if (object.isNull("memberid")) {
                            object.put("memberid", "");

                        } else {
                            auctionDefine.setMemberid(object.getInt("memberid"));


                        }
                        receiptCompleteDetailsModelItems.add(auctionDefine);

                        updatedetails();
                    }


                } catch (Exception e) {
                    Toast.makeText(ReceiptActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ReceiptActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }


    private void updatedetails() {
        chit1 = receiptCompleteDetailsModelItems.get(0).getChitcode();
        select_chitCode.setSelection(getChitIndex(select_chitCode, chit1));
/*
        if (chit1 != null) {
            int spinnerPosition = chitaa.getPosition(chit1);
            select_chitCode.setSelection(spinnerPosition);
        }
*/

/*
        select_chitCode.setSelection(((ArrayAdapter<String>)select_chitCode.getAdapter()).getPosition(chit1));
*/


        String dateDob = receiptCompleteDetailsModelItems.get(0).getReceiptdate();

        SimpleDateFormat dateFormatprev = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date d = null;
        try {
            d = dateFormatprev.parse(dateDob);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dobchanged = dateFormat.format(d);
        dateTxt.setText(dobchanged);

        recAmountTxt.setText(String.valueOf(receiptCompleteDetailsModelItems.get(0).getAmount()));
        String name = receiptCompleteDetailsModelItems.get(0).getName();
        subId = receiptCompleteDetailsModelItems.get(0).getTicketno();
        subTick = subId + "-" + name;
        memberSpin.setSelection(getSubIndex(memberSpin, subTick));


        String recTyp = receiptCompleteDetailsModelItems.get(0).getRectype();
        recType.setSelection(getRecTypeIndex(recType, recTyp));


    }

    private int getChitIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                return i;
            }
        }

        return 0;
    }

    private int getRecTypeIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                return i;
            }
        }

        return 0;
    }


    private int getSubIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(String.valueOf(myString))) {
                return i;
            }
        }

        return 0;
    }

    private int getRecIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(String.valueOf(myString))) {
                return i;
            }
        }

        return 0;
    }


    private void getDeleteApi(Integer chitId, Integer receiptId, String ChitCode) {
        // api/Chits/Deletes/{compid}/{userid}/{chitid}/{chitcode}/{Receipt}/{receiptId}
        RequestQueue queue = Volley.newRequestQueue(ReceiptActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_DELETE + "/" + compId + "/" + userId + "/" + chitId + "/" + ChitCode + "/" + "Receipt" + "/" + receiptId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    Toast.makeText(ReceiptActivity.this, "Receipt Deleted Successfully", Toast.LENGTH_SHORT).show();

                    if (rest.equals("Fail")) {
                        Toast.makeText(ReceiptActivity.this, "This Auction is used in subscribers list", Toast.LENGTH_SHORT).show();

                    } else {
                        getReceiptDetails(valueOf(chitIdStrUp));


                    }


                } catch (Exception e) {
                    Toast.makeText(ReceiptActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ReceiptActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_menu, menu);
        return true;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logOut) {
            Intent intent1 = new Intent(ReceiptActivity.this, LoginActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
            startActivity(intent1);
            finish();


        }
        return super.onOptionsItemSelected(item);


    }

    public static boolean checkNetworkAndShowError(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean isConnected = cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
        if (!isConnected) {
            showNetWorkError(context);
        }

        return isConnected;
    }

    private static void showNetWorkError(Context context) {
        new AlertDialog.Builder(context)
                .setPositiveButton("ok", null)
                .setMessage("Please check Internet Connection!!")
                .show();

    }


}