package com.fairsoft.chitfund.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fairsoft.chitfund.api.Urls;
import com.fairsoft.chitfund.model.ActiveChitsResponseModelItem;
import com.fairsoft.chitfund.model.AddContactResponse;
import com.fairsoft.chitfund.model.AuctionChitModelItem;
import com.fairsoft.chitfund.model.ChitModelItem;
import com.fairsoft.chitfund.model.ChitUpdateDetailsModelItem;
import com.fairsoft.chitfund.model.SubscriberModelItem;
import com.fairsoft.chitfund.R;
import com.fairsoft.chitfund.adapter.AuctionAdapter;

import com.fairsoft.chitfund.model.AuctionCompleteDetailsModelItem;


import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;


import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static java.lang.Integer.valueOf;

public class AuctionActivity extends AppCompatActivity implements AuctionAdapter.deleteRow,AuctionAdapter.onItemClickListener {
    //id's assigning
    Spinner select_chitCode, prized_subscriber;
    /*AutoCompleteTextView autoCompleteTextView;*/
    private List<AuctionCompleteDetailsModelItem> auctionCompleteDetailsModelItemList;
    private List<AuctionCompleteDetailsModelItem> auctionCompleteDetailsModelItemList1;

    private String json;
    String rest;
    String jsonStr;
    RecyclerView recyclerView;
    AuctionAdapter auctionAdapter;
    Integer chitIdStr;
    Integer chitCodePos;
    String chitCodeStr;
    String prizeCodeStr, searchSubScriber;
    int prizeIdStr;
    EditText currDateTxt, auction_amount;
    TextView currTimeTxt;
    CardView saveButton, cancelCardView;
    private List<String> listData = new ArrayList<String>();
    private List<String> prizedlistData = new ArrayList<String>();
    Calendar calendar;
    int currentHour;
    int currentMinute;
    TimePickerDialog timePickerDialog;
    int t1Hrs, t2min;
    String currentTime;


    ArrayAdapter chitaa;
    ArrayAdapter prizedbb;
    boolean update = false;
    Integer auctionId;
    Integer auctionIdPara;
    String auctiondate, auctiontime;
    Integer chitIdUp, position, auctionIdUp;
    String chitCodeUp, nameStr;
    String changedDate;
    String auctiondayStr, auctionamount;
    Integer memeberIdStrUp, chitIdStrUp, chitIdStr1;
    private List<ChitUpdateDetailsModelItem> chitUpdateDetailsList;
    Integer chitAmount, chitCommn;
    Integer auctionAmtval, valAmt;
    String compId, userId, userName, compName, chitperAmt;
    SharedPreferences pref;
    SharedPreferences.Editor editor;


    final Calendar myCalendar = Calendar.getInstance();

    TextView sendTxt;
    ArrayList<String> contactResponses;
    ArrayList<SubscriberModelItem> memberList1;
    ArrayList<ChitModelItem> chitList;
    ArrayList<ActiveChitsResponseModelItem> chitList1;


    ArrayList<String> chitCodeList;
    String longval, chitTypeStr;
    TextView noData;
    double chitAmnt, commn;
    long longChitAmnt;

    private HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
    Integer subId;
    String subsc, commPerStr, ticket, chit, name;
    Integer longvalint, noOfMemStr;
    TextView chitAmtPerTxt, chitCommPerTxt;
    String MemberId;
    ProgressBar progressBar;
    public static final int MY_DEFAULT_TIMEOUT = 15000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auction);
        getSupportActionBar().setTitle("Auction");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();


        compId = String.valueOf(pref.getInt("compId", 0));
        userId = String.valueOf(pref.getInt("userId", 0));
        userName = String.valueOf(pref.getString("userName", null));
        compName = String.valueOf(pref.getString("compName", null));

        //bundle
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();


        //method calling
        init();


        auctiondayStr = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        currDateTxt.setText((auctiondayStr));
        currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        currTimeTxt.setText((currentTime));
        getChitid();
        if (extras != null) {
            chitIdUp = extras.getInt("chitId");
            position = extras.getInt("position");
            chitCodeUp = extras.getString("chitCode");
            nameStr = extras.getString("name");
            auctionIdUp = extras.getInt("auctionId");
            update = true;
            getChitid();

            getUpdatedAuctionDetails(valueOf(chitIdUp), auctionIdUp);

        }
        auctionDatePicker();
        updateLabel();
        timePicker();
        memeberIdStrUp = 0;

        auction_amount.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                auction_amount.removeTextChangedListener(this);

                try {
                    String givenstring = s.toString();
                    if (givenstring.contains(",")) {
                        givenstring = givenstring.replaceAll(",", "");
                    }
                    longval = String.valueOf(Long.parseLong(givenstring));
                    DecimalFormat formatter = new DecimalFormat("#,###,###");
                    String formattedString = formatter.format(longval);
                    auction_amount.setText(formattedString);
                    auction_amount.setSelection(auction_amount.getText().length());


                    // to place the cursor at the end of text
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                auction_amount.addTextChangedListener(this);

            }
        });


        //saving data
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auctiondate = currDateTxt.getText().toString();
                currentTime = currTimeTxt.getText().toString();
                auctionamount = auction_amount.getText().toString();
/*
                prizeCodeStr = autoCompleteTextView.getText().toString();
*/


                validateFields();
            }
        });
        cancelCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AuctionActivity.this, DashBoardActivity.class);
                startActivity(intent);


            }
        });

    }


    public void init() {
        prized_subscriber = findViewById(R.id.prized_subscriber);
        recyclerView = findViewById(R.id.auctionRecyclerView);
        select_chitCode = findViewById(R.id.select_chitCode);
        currDateTxt = findViewById(R.id.currDateTxt);
        currTimeTxt = findViewById(R.id.currTimeTxt);
        auction_amount = findViewById(R.id.auction_amount);
        auction_amount.setHint(Html.fromHtml("Amount <font color =\"#cc0029\" >*</font>"));

        saveButton = findViewById(R.id.saveButton);
        sendTxt = findViewById(R.id.sendTxt);
        noData = findViewById(R.id.noData);
        chitAmtPerTxt = findViewById(R.id.chitAmtPerTxt);
        chitCommPerTxt = findViewById(R.id.chitCommPerTxt);
/*
        autoCompleteTextView = findViewById(R.id.autoCompleteTextView);
*/
        cancelCardView = findViewById(R.id.cancelCardView);
        progressBar = findViewById(R.id.progressBar);


    }

    //datePicker
    @SuppressLint("ClickableViewAccessibility")
    public void auctionDatePicker() {
        DatePickerDialog.OnDateSetListener date = new
                DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateLabel();
                    }

                };


        currDateTxt.setOnTouchListener(new View.OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(AuctionActivity.this, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
                return false;
            }
        });


    }

    private void updateLabel() {

        String myFormat = "dd-MM-yyyy";

        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        auctiondayStr = sdf.format(myCalendar.getTime());
        currDateTxt.setText((auctiondayStr));

        currDateTxt.setSelection(currDateTxt.getText().length());

        SimpleDateFormat dateFormatprev = new SimpleDateFormat("dd-MM-yyyy");
        Date d = null;
        try {
            d = dateFormatprev.parse(auctiondayStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd");
        changedDate = dateFormat.format(d);


    }

    public void timePicker() {

        currTimeTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                int seconds = mcurrentTime.get(Calendar.SECOND);


                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(AuctionActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        currTimeTxt.setText(selectedHour + ":" + selectedMinute);
/*
                        currTimeTxt.setSelection(currTimeTxt.getText().length());
*/

                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle(currentTime);
                mTimePicker.show();

            }
        });


    }

    @Override
    public void deleteRowClick(Integer chitId, Integer auctionId, String chitCode) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                AuctionActivity.this);

        // set title
        alertDialogBuilder.setTitle("Delete");

        // set dialog message
        alertDialogBuilder
                .setMessage("Are You Sure To Delete?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        getDeleteApi(chitId, auctionId, chitCodeUp);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();


    }

    @Override
    public void onItemClick(Integer chitId, Integer auctionId, String chitCode) {
        update = true;
        auctionIdUp = auctionId;
        getUpdatedAuctionDetails(valueOf(chitId), auctionId);


    }


    class ChitSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            // Toast.makeText(v.getContext(), "Your choose :" + chit[position], Toast.LENGTH_SHORT).show();
            chitIdStr = select_chitCode.getSelectedItemPosition();
            chitCodeStr = select_chitCode.getItemAtPosition(position).toString();
            chitCodePos = select_chitCode.getSelectedItemPosition();
            AuctionChitModelItem chit = (AuctionChitModelItem) parent.getSelectedItem();
            chitIdStrUp = chit.getMemberid();
            chitAmnt = chit.getAmount();
            commPerStr = chit.getCommnperamount();
            noOfMemStr = chit.getNoofmembers();
            commn = chit.getCommn();
            chitTypeStr = chit.getChittype();
            if (chitTypeStr.equals("Incremental")) {
                auction_amount.setVisibility(View.GONE);
            } else {
                auction_amount.setVisibility(View.VISIBLE);


            }


            if (position >= 0) {
                // getSubScriberDetails(chitIdStrUp);
                if (update) {
                    getPrizedDetails(chitIdStrUp);
                } else {
                    getNonPrizedDetails(chitIdStrUp);

                }
                getAuctionDetails(valueOf(chitIdStrUp));
                if (commPerStr.equals("%")) {
                    commn = chitAmnt * commn / 100;

                }
                chitAmtPerTxt.setText("Amnt - " + chitAmnt + "," + "Members - " + noOfMemStr.toString() + "\n" + "commnAmonut is" + "-" + commn + "\n" + "ChitType is" + "-" + chitTypeStr);
/*
                chitCommPerTxt.setText("commnAmonut is" + "-" + commn);
*/

                // Toast.makeText(AuctionActivity.this, chitIdStrUp.toString(), Toast.LENGTH_SHORT).show();
            }


/*
            chitAmtPerTxt.setText("Amnt - " + chitAmnt + "," + "Comm/Per - " + commPerStr + "," + "commnAmonut" + "-" + commn + "," + "Members - " + noOfMemStr.toString());
*/


        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    class PrizedMember implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            // Toast.makeText(v.getContext(), "Your choose :" + chit[position], Toast.LENGTH_SHORT).show();
            prizeIdStr = prized_subscriber.getSelectedItemPosition() + 1;
            prizeCodeStr = prized_subscriber.getItemAtPosition(position).toString();
            prizeCodeStr = parent.getItemAtPosition(position).toString();
            SubscriberModelItem member = (SubscriberModelItem) parent.getSelectedItem();
            memeberIdStrUp = member.getMemberid();

            //  chitCodePos = auctionPrizedSpin.getSelectedItemPosition();


        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }

    }

    public void validateFields() {

        if (auctiondate.equals("")) {
            Toast.makeText(AuctionActivity.this, "Please enter date", Toast.LENGTH_SHORT).show();

            return;
        } else if (currentTime.equals("")) {
            Toast.makeText(AuctionActivity.this, "Please enter time", Toast.LENGTH_SHORT).show();

            return;
        } else if (chitCodeStr.isEmpty() || chitCodeStr.equals("select ChitCode")) {
            Toast.makeText(AuctionActivity.this, "Please select ChitCode", Toast.LENGTH_SHORT).show();
            return;

        } else if (memeberIdStrUp.equals(0)) {
            Toast.makeText(AuctionActivity.this, "There is No non-prized Members for This chit", Toast.LENGTH_SHORT).show();
            return;

        } else if (!chitTypeStr.equals("Incremental")) {
            if (auctionamount.isEmpty()) {
                Toast.makeText(AuctionActivity.this, "Please enter amount", Toast.LENGTH_SHORT).show();
                return;
            } else if (update) {
                if (chitTypeStr.equals("Incremental")) {
                    longval = String.valueOf(0);

                    postDataUsingVolley(auctionId, chitCodeStr, chitIdStrUp, changedDate, currentTime, longval.toString());
                } else if (!auctionamount.isEmpty() || auctionamount != "null") {
                    getUpdateMemberDetails(chitIdStrUp);
                    return;
                }


            } else {

                if (chitTypeStr.equals("Incremental")) {
                    longval = String.valueOf(0);
                    auctionId = 0;

                    postDataUsingVolley(auctionId, chitCodeStr, chitIdStrUp, changedDate, currentTime, longval.toString());
                } else if (!auctionamount.isEmpty() || auctionamount != "null") {
                    getUpdateMemberDetails(chitIdStrUp);
                    return;
                }
            }


        } else if (update) {
            if (chitTypeStr.equals("Incremental")) {
                longval = String.valueOf(0);
                auctionId = 0;


                postDataUsingVolley(auctionId, chitCodeStr, chitIdStrUp, changedDate, currentTime, longval.toString());
            } else if (!auctionamount.isEmpty() || auctionamount != "null") {
                getUpdateMemberDetails(chitIdStrUp);
                return;
            }


        } else {

                if (chitTypeStr.equals("Incremental")) {
                    longval = String.valueOf(0);
                    auctionId = 0;
                    postDataUsingVolley(auctionId, chitCodeStr, chitIdStrUp, changedDate, currentTime, longval.toString());
                } else if (!auctionamount.isEmpty() || auctionamount != "null") {
                    getUpdateMemberDetails(chitIdStrUp);
                    return;
                }
            }




    }








/*
        postDataUsingVolley(auctionId, chitCodeStr, chitIdStrUp, changedDate, currentTime, longval.toString(), memeberIdStrUp);
*/


    boolean contains(ArrayList<AuctionCompleteDetailsModelItem> list, String name) {
        for (AuctionCompleteDetailsModelItem item : list) {
            if (item.getSubscribertick().equals(name)) {
                return true;
            }
        }
        return false;
    }

    private void getChitid() {
        RequestQueue queue = Volley.newRequestQueue(AuctionActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_AUCTION_CHITS_DETAILS + "/" + compId + "/" + userId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ArrayList<AuctionChitModelItem> chitList = new ArrayList<>();
                chitList1 = new ArrayList<>();
                chitCodeList = new ArrayList<>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = (JSONObject) array.get(i);
                        ActiveChitsResponseModelItem activeChitsResponseModelItem = new ActiveChitsResponseModelItem();
                        String chitname = object.getString("chitcode");
                        Integer chitid = object.getInt("chitid");
                        Integer amount = object.getInt("chitamt");
                        String commPer = object.getString("commnperamount");
                        Integer noOfMem = object.getInt("noofmembers");
                        double commn = object.getInt("commn");
                        String chitType = object.getString("chittype");


                        chitList.add(new AuctionChitModelItem(chitid, chitname, amount, commPer, noOfMem, commn, chitType));
                        activeChitsResponseModelItem.setChitcode(object.getString("chitcode"));
                        activeChitsResponseModelItem.setChitid(object.getInt("chitid"));
                        activeChitsResponseModelItem.setChitamt(object.getInt("chitamt"));

                        chitList1.add(activeChitsResponseModelItem);
                        chitCodeList.add(object.getString("chitcode"));

                        hashMap.put(object.getInt("chitid"), object.getString("chitcode"));

                    }


                    if (listData == null) {
                        listData.add(0, "No data");
                    } else {
                        listData.add(0, "select ChitCode");

                        chitaa = new ArrayAdapter(AuctionActivity.this, android.R.layout.simple_spinner_item, chitList);
                        chitaa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner
                        select_chitCode.setAdapter(chitaa);
                        select_chitCode.setOnItemSelectedListener(new AuctionActivity.ChitSpinnerClass());
                    }


                } catch (Exception e) {
                    Toast.makeText(AuctionActivity.this, "Something Went wrong" + e.toString(), Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(AuctionActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }


    private void getPrizedDetails(Integer chitId) {
        RequestQueue queue = Volley.newRequestQueue(AuctionActivity.this);
        //api/Chits/SubscriberDetails/{compid}/{userid}/{chitid}/{subscriberid}
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_SUBSCRIBER_DETAILS + "/" + compId + "/" + userId + "/" + chitId + "/" + 0, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // subscriberDetailsList = new ArrayList<SubscriberDetailsModelItem>();
                memberList1 = new ArrayList<>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
/*
                        SubscriberModelItem SubscriberModelItem = new SubscriberModelItem();
*/

                        JSONObject object = (JSONObject) array.get(i);


                        String membername = object.getString("ticketno") + "-" + object.getString("name");
                        Integer memberid = object.getInt("subscriberid");
                        String mobile1 = object.getString("mobile1");
                        String name = object.getString("name");
/*
                        SubscriberModelItem.setContactName(object.getString("mobile1"));
*/


                        memberList1.add(new SubscriberModelItem(memberid, membername, mobile1, name));
/*
                        memberList1.add(SubscriberModelItem);
*/


                    }

                    if (memberList1 == null) {
                    } else {

                        prizedbb = new ArrayAdapter(AuctionActivity.this, android.R.layout.simple_spinner_item, memberList1);
                        prizedbb.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner
                        prized_subscriber.setAdapter(prizedbb);
                        prized_subscriber.setOnItemSelectedListener(new AuctionActivity.PrizedMember());

                        if (update) {
                            prized_subscriber.setSelection(getSubIndex(prized_subscriber, subsc));

                        }
                       /* AutoCompleteCountryAdapter adapter = new AutoCompleteCountryAdapter(AuctionActivity.this, memberList1);
                        autoCompleteTextView.setAdapter(adapter);


                        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                memeberIdStrUp = adapter.getItem(i).getMemberid();


                            }
                        });
*/
                    }


                } catch (Exception e) {
                    Toast.makeText(AuctionActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(AuctionActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void getNonPrizedDetails(Integer chitId) {
        RequestQueue queue = Volley.newRequestQueue(AuctionActivity.this);
        //api/Chits/SubscriberDetails/{compid}/{userid}/{chitid}/{subscriberid}
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_PRIZED_MEMBER_DETAILS + "/" + compId + "/" + userId + "/" + chitId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // subscriberDetailsList = new ArrayList<SubscriberDetailsModelItem>();
                memberList1 = new ArrayList<>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    if (rest.equals("[]")) {
                        prizedbb = new ArrayAdapter(AuctionActivity.this, android.R.layout.simple_spinner_item, memberList1);
                        prizedbb.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner
                        prized_subscriber.setAdapter(prizedbb);
                        prizedbb.clear();
                        memeberIdStrUp = 0;

                        //  Toast.makeText(AuctionActivity.this, "There is  No non-prized  Members For This Chit", Toast.LENGTH_SHORT).show();

                    } else {


                        String json1 = rest.replace("[", "");
                        String json2 = json1.replace("]", "");
                        jsonStr = rest.replace("\\", "");
                        JSONArray array = new JSONArray(jsonStr);
                        for (int i = 0; i < array.length(); i++) {
                            AddContactResponse addContactResponse = new AddContactResponse();

                            JSONObject object = (JSONObject) array.get(i);

                            String membername = object.getString("ticketno") + "-" + object.getString("name");
                            Integer memberid = object.getInt("subscriberid");
                            String name = object.getString("name");
                            String mobile1 = object.getString("mobile1");

                            memberList1.add(new SubscriberModelItem(memberid, membername, mobile1, name));


                        }
                        if (memberList1 == null) {

                        } else {

                            prizedbb = new ArrayAdapter(AuctionActivity.this, android.R.layout.simple_spinner_item, memberList1);
                            prizedbb.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            //Setting the ArrayAdapter data on the Spinner
                            prized_subscriber.setAdapter(prizedbb);
                            prized_subscriber.setOnItemSelectedListener(new AuctionActivity.PrizedMember());
                          /*  AutoCompleteCountryAdapter adapter = new AutoCompleteCountryAdapter(AuctionActivity.this, memberList1);
                            autoCompleteTextView.setAdapter(adapter);


                            autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    memeberIdStrUp = adapter.getItem(i).getMemberid();


                                }
                            });
*/
                        }

                    }


                } catch (Exception e) {
                    Toast.makeText(AuctionActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(AuctionActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }


    private void getAuctionDetails(Integer chitId) {
        RequestQueue queue = Volley.newRequestQueue(AuctionActivity.this);
        //api/Chits/AuctionDetails/{compid}/{userid}/{chitid}/{Auctionid}
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_AUCTION_COMPLETE_DETAILS + "/" + compId + "/" + userId + "/" + chitId + "/" + 0, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                auctionCompleteDetailsModelItemList = new ArrayList<AuctionCompleteDetailsModelItem>();
                auctionCompleteDetailsModelItemList1 = new ArrayList<AuctionCompleteDetailsModelItem>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
                        AuctionCompleteDetailsModelItem auctionDefine = new AuctionCompleteDetailsModelItem();
                        JSONObject object = (JSONObject) array.get(i);
                        auctionDefine.setAuctionid(object.getInt("auctionid"));
                        auctionDefine.setChitid(object.getInt("chitid"));
                        auctionDefine.setAuctionamount(object.getInt("auctionamount"));
                        auctionDefine.setAuctiontime(object.getString("auctiontime"));
                        auctionDefine.setChitcode(object.getString("chitcode"));
                        auctionDefine.setAuctionday(object.getString("auctionday"));
                        auctionDefine.setName(object.getString("name"));
                        auctionDefine.setTicketno(object.getString("ticketno"));

                        if (object.isNull("prizedmemberid")) {
                            object.put("prizedmemberid", "");

                        } else {
                            auctionDefine.setPrizedmemberid(object.getInt("prizedmemberid"));


                        }
                        String subscriber = object.getString("ticketno") + "-" + object.getString("name");
                        auctionDefine.setSubscribertick(subscriber);

                        auctionCompleteDetailsModelItemList.add(auctionDefine);
                    }


                } catch (Exception e) {
                    Toast.makeText(AuctionActivity.this, "error is" + e.toString(), Toast.LENGTH_SHORT).show();

                }
                if (auctionCompleteDetailsModelItemList == null && auctionCompleteDetailsModelItemList.isEmpty()) {
                    Toast.makeText(AuctionActivity.this, "No data", Toast.LENGTH_SHORT).show();
                }
                recyclerView = findViewById(R.id.auctionRecyclerView);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(AuctionActivity.this);
                recyclerView.setLayoutManager(linearLayoutManager);
              /*  recyclerView.setAdapter(auctionAdapter);
                auctionAdapter.notifyDataSetChanged();
              */
                //filter by id
                for (AuctionCompleteDetailsModelItem billDefine : auctionCompleteDetailsModelItemList) {
                    Integer chit = billDefine.getChitid();


                    if (chit.equals(chitId)) {
                        auctionCompleteDetailsModelItemList1.add(billDefine);
                        noData.setVisibility(View.GONE);

                    } else {
                        noData.setVisibility(View.VISIBLE);


                    }

                }
                //instatiate adapter a
                auctionAdapter = new AuctionAdapter(AuctionActivity.this, auctionCompleteDetailsModelItemList1, AuctionActivity.this::deleteRowClick,AuctionActivity.this::onItemClick);
                noData.setVisibility(View.GONE);
                recyclerView.setAdapter(auctionAdapter);
                auctionAdapter.notifyDataSetChanged();


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(AuctionActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void postDataUsingVolley(Integer auctionIdp, String chitCodep, Integer chitIdp, String auctionDatep, String auctionTimeP, String auctionamountp) {
        progressBar.setVisibility(View.VISIBLE);


        String updateData;
        if (update) {
            updateData = "U";
            auctionIdPara = auctionIdUp;
           /* if (autoCompleteTextView.isFocused()) {
                memeberIdStrUp = memeberIdStrUp;


            } else {
                memeberIdStrUp = Integer.valueOf(MemberId);


            }*/


        } else {
            updateData = "N";
            auctionIdPara = 0;

        }
        // creating a new variable for our request queue
        RequestQueue queue = Volley.newRequestQueue(AuctionActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_AUCTION_SAVE_DETAILS + "/" + compId + "/" + userId + "/" + updateData, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String json = response;
                String rest = json.substring(1, json.length() - 1);


                // on below line we are displaying a success toast message.
                auction_amount.setText("");
                String phonestr, nameStr;

                Gson gson = new Gson();
                List<SubscriberModelItem> todolist1 = new ArrayList<SubscriberModelItem>(memberList1);
                String jsonText = gson.toJson(todolist1);


/*
                for (int i = 0; i < memberList1.size(); i++) {

                    phonestr = "91" + memberList1.get(i).getContactName();
                    nameStr = memberList1.get(i).getSubScriber();
                    String messagestr = "Auction Amount For this" + " " + chitCodep + " " + auctionamountp + " " + "on" + " " + auctionDatep;

*/
/*
                    sendMsgToWhatsapp(phonestr.toString(), messagestr, nameStr);
*//*


                    sendSmsMsgFnc(phonestr,messagestr);

                }
*/
                if (rest.equals("Success")) {
                    if (updateData.equals("U")) {
                        auction_amount.setText("");
                        getChitid();
/*
                    autoCompleteTextView.setText("");
*/
                        Toast.makeText(AuctionActivity.this, "Auction Updated SuccessFully", Toast.LENGTH_SHORT).show();
                        update=false;
                        progressBar.setVisibility(View.GONE);

                        getAuctionDetails(valueOf(chitIdStrUp));
                        Bundle args = new Bundle();
                        args.putString("mobileList", jsonText);
                        args.putString("chitCodep", chitCodep);
                        args.putString("auctionamountp", auctionamountp);
                        args.putString("auctionDatep", auctionDatep);


                        MessageDialog dialog = new MessageDialog();
                        dialog.setCancelable(false);
                        FragmentActivity activity = (FragmentActivity) (AuctionActivity.this);
                        FragmentManager fm = activity.getSupportFragmentManager();
                        dialog.setArguments(args);
                        dialog.show(fm, "MyDialogFragment");

                    } else {
                        auction_amount.setText("");
/*
                    autoCompleteTextView.setText("");
*/
                        Toast.makeText(AuctionActivity.this, "Auction Saved SuccessFully", Toast.LENGTH_SHORT).show();
                        getChitid();
                        progressBar.setVisibility(View.GONE);
                        getAuctionDetails(valueOf(chitIdStrUp));
                        Bundle args = new Bundle();
                        args.putString("mobileList", jsonText);
                        args.putString("chitCodep", chitCodep);
                        args.putString("auctionamountp", auctionamountp);
                        args.putString("auctionDatep", auctionDatep);


                        MessageDialog dialog = new MessageDialog();
                        dialog.setCancelable(false);
                        FragmentActivity activity = (FragmentActivity) (AuctionActivity.this);
                        FragmentManager fm = activity.getSupportFragmentManager();
                        dialog.setArguments(args);
                        dialog.show(fm, "MyDialogFragment");


                    }
                }


                // to send message


            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(AuctionActivity.this, "Fail to get response = " + error, Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // below line we are creating a map for
                // storing our values in key and value pair.
                Map<String, String> params = new HashMap<String, String>();

                // on below line we are passing our key
                // and value pair to our parameters.

                params.put("auctionId", String.valueOf(auctionIdPara));
                params.put("chitCode", chitCodep);
                params.put("chitId", String.valueOf(chitIdp));
                params.put("auctionDate", auctionDatep);
                params.put("auctiontime", currentTime);
                params.put("auctionamount", String.valueOf(auctionamountp));
                params.put("prizedSubscriber", String.valueOf(memeberIdStrUp));


                // at last we are
                // returning our params.
                return params;
            }
        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }

    public void sendLongSMS(String number, String messages) {
        String phoneNumber = number;
        String message = messages;
        SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> parts = smsManager.divideMessage(message);
        smsManager.sendMultipartTextMessage(phoneNumber, null, parts, null, null);
    }

    void sendSmsMsgFnc(String mblNumVar, String smsMsgVar) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
            try {
                SmsManager smsMgrVar = SmsManager.getDefault();
                smsMgrVar.sendTextMessage(mblNumVar, null, smsMsgVar, null, null);
/*
                Toast.makeText(AuctionActivity.this, "Message Sent",
                        Toast.LENGTH_LONG).show();
*/
            } catch (Exception ErrVar) {
                Toast.makeText(AuctionActivity.this, ErrVar.getMessage().toString(),
                        Toast.LENGTH_LONG).show();
                ErrVar.printStackTrace();
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.SEND_SMS}, 10);
            }
        }

    }

    private void getUpdatedAuctionDetails(Integer chitId, Integer auctionId) {
        //api/Chits/AuctionDetails/{compid}/{userid}/{chitid}/{Auctionid}
        RequestQueue queue = Volley.newRequestQueue(AuctionActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_AUCTION_COMPLETE_DETAILS + "/" + compId + "/" + userId + "/" + chitId + "/" + auctionId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                auctionCompleteDetailsModelItemList = new ArrayList<AuctionCompleteDetailsModelItem>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
                        AuctionCompleteDetailsModelItem auctionDefine = new AuctionCompleteDetailsModelItem();
                        JSONObject object = (JSONObject) array.get(i);
                        auctionDefine.setAuctionid(object.getInt("auctionid"));
                        auctionDefine.setChitid(object.getInt("chitid"));
                        auctionDefine.setAuctionamount(object.getInt("auctionamount"));
                        auctionDefine.setAuctiontime(object.getString("auctiontime"));
                        auctionDefine.setChitcode(object.getString("chitcode"));
                        auctionDefine.setAuctionday(object.getString("auctionday"));
                        auctionDefine.setName(object.getString("name"));
                        auctionDefine.setTicketno(object.getString("ticketno"));
                        auctionDefine.setSubscriberid(object.getInt("subscriberid"));


                        if (object.isNull("prizedmemberid")) {
                            object.put("prizedmemberid", "");

                        } else {
                            auctionDefine.setPrizedmemberid(object.getInt("prizedmemberid"));


                        }
                        auctionCompleteDetailsModelItemList.add(auctionDefine);
                        updatedetails();
                    }


                } catch (Exception e) {
                    Toast.makeText(AuctionActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(AuctionActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }


    private void updatedetails() {

        chitaa = (ArrayAdapter) select_chitCode.getAdapter();
        String date = auctionCompleteDetailsModelItemList.get(0).getAuctionday();

        SimpleDateFormat dateFormatprev = new SimpleDateFormat("yyy-MM-dd");
        Date d = null;
        try {
            d = dateFormatprev.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String changedDay = dateFormat.format(d);
        currDateTxt.setText(changedDay);


        currTimeTxt.setText(auctionCompleteDetailsModelItemList.get(0).getAuctiontime());
        String amount = String.valueOf(auctionCompleteDetailsModelItemList.get(0).getAuctionamount());
        double bill1 = Double.parseDouble(amount);
        long amountTxt = (long) bill1;
        auction_amount.setText(String.valueOf(amountTxt));

        Integer chitId = auctionCompleteDetailsModelItemList.get(0).getChitid();
        chit = auctionCompleteDetailsModelItemList.get(0).getChitcode();
        select_chitCode.setSelection(getChitIndex(select_chitCode, chit));

        ticket = auctionCompleteDetailsModelItemList.get(0).getTicketno();
        subId = auctionCompleteDetailsModelItemList.get(0).getSubscriberid();
        name = auctionCompleteDetailsModelItemList.get(0).getName();
        MemberId = String.valueOf(auctionCompleteDetailsModelItemList.get(0).getSubscriberid());

        subsc = ticket + "-" + name;
        if (update) {
            getPrizedDetails(chitId);
        } else {
            getNonPrizedDetails(chitId);

        }

/*
        autoCompleteTextView.setText(subsc);
*/


    }

    private int getChitIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                return i;
            }
        }

        return 0;
    }

    private int getSubIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(String.valueOf(myString))) {
                return i;
            }
        }

        return 0;
    }


    private void getUpdateMemberDetails(Integer chitId) {
        RequestQueue queue = Volley.newRequestQueue(AuctionActivity.this);
        if (!checkNetworkAndShowError(this)) {
            return;
        }
        progressBar.setVisibility(View.VISIBLE);

        if (update) {
            if (auction_amount.isFocused()) {
                auctionAmtval = Integer.valueOf(longval.toString().toString());
                longval = longval;

            } else {
                double bill = Double.parseDouble(auctionamount);
                long longAuctionAmnt = (long) bill;
                auctionAmtval = Integer.valueOf(String.valueOf(longAuctionAmnt));
                longval = String.valueOf(Long.valueOf(auctionAmtval));

            }
        } else {
            double bill = Double.parseDouble(auctionamount);
            long longAuctionAmnt = (long) bill;
            auctionAmtval = Integer.valueOf(String.valueOf(longAuctionAmnt));
            longval = String.valueOf(Long.valueOf(auctionAmtval));

        }

        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_CHIT_COMPLETE_DETAILS + "/" + compId + "/" + userId + "/" + chitId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                chitUpdateDetailsList = new ArrayList<ChitUpdateDetailsModelItem>();

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
                        ChitUpdateDetailsModelItem memeberDefine = new ChitUpdateDetailsModelItem();
                        JSONObject object = (JSONObject) array.get(i);
                        memeberDefine.setChitcode(object.getString("chitcode"));
                        memeberDefine.setChitid(object.getInt("chitid"));
                        memeberDefine.setNoofmembers(object.getInt("noofmembers"));
                        memeberDefine.setFrequency(object.getString("frequency"));
                        memeberDefine.setCommn(object.getInt("commn"));
                        memeberDefine.setIsownchit(object.getString("isownchit"));
                        memeberDefine.setDividend(object.getString("dividend"));
                        memeberDefine.setAuctionday(object.getString("auctionday"));
                        memeberDefine.setCommnPerAmount(object.getString("commnperamount"));

                        chitUpdateDetailsList.add(memeberDefine);
                        chitperAmt = object.getString("commnperamount");

                        chitAmount = Integer.valueOf(valueOf(String.valueOf(object.getInt("chitamt"))));
                        chitCommn = Integer.valueOf(valueOf(String.valueOf(object.getInt("commn"))));
                        BigDecimal x = new BigDecimal(chitAmount.toString());
                        Log.e("xamount", x.toString());


                        if (chitperAmt.equals("%")) {

                            valAmt = chitCommn * chitAmount / 100;
                            Log.e("chitCommn", chitCommn.toString());

                            Log.e("chitAmount", chitAmount.toString());
                            Log.e("valAmt", valAmt.toString());
                            Log.e("longval", longval.toString());
                            longvalint = Integer.parseInt(longval);


                            auctionAmtval = Integer.valueOf(longval.toString().toString());
                            double bill = chitAmnt;
                            longChitAmnt = (long) bill;

                            if (auctionAmtval < valAmt) {
                                Toast.makeText(AuctionActivity.this, "Auction amount should  be Greater than  or Equal to Commn Amount", Toast.LENGTH_LONG).show();
                                progressBar.setVisibility(View.GONE);

                                return;
                            } else if (longvalint >= longChitAmnt) {
                                Toast.makeText(AuctionActivity.this, "Auction amount should  Less Than Chit amount", Toast.LENGTH_LONG).show();
                                progressBar.setVisibility(View.GONE);

                                return;
                            }
                            postDataUsingVolley(auctionId, chitCodeStr, chitIdStrUp, changedDate, auctiontime, longval.toString());


                        } else {
                            valAmt = chitCommn;
                            int prevComm = valAmt;
                            auctionAmtval = Integer.valueOf(longval.toString());
                            int auction = auctionAmtval;
                            longvalint = Integer.parseInt(longval);
                            auctionAmtval = Integer.valueOf(longval.toString().toString());
                            double bill = chitAmnt;
                            longChitAmnt = (long) bill;


                            if (auction < prevComm) {
                                Toast.makeText(AuctionActivity.this, "Auction amount should  be Greater than  or Equal to Commn Amount", Toast.LENGTH_LONG).show();
                                progressBar.setVisibility(View.GONE);


                                return;
                            } else if (longvalint >= longChitAmnt) {
                                Toast.makeText(AuctionActivity.this, "Auction amount should  Less Than Chit amount", Toast.LENGTH_LONG).show();
                                progressBar.setVisibility(View.GONE);


                                return;
                            }
                            postDataUsingVolley(auctionId, chitCodeStr, chitIdStrUp, changedDate, auctiontime, longval.toString());


                        }


                    }


                } catch (Exception e) {
                    Toast.makeText(AuctionActivity.this, "error is" + e.toString(), Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);


                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(AuctionActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }


    private void getDeleteApi(Integer chitId, Integer auctionId, String ChitCode) {
        // api/Chits/Deletes/{compid}/{userid}/{chitid}/{chitcode}/{Auction}/{auctionId}
        RequestQueue queue = Volley.newRequestQueue(AuctionActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_DELETE + "/" + compId + "/" + userId + "/" + chitId + "/" + ChitCode + "/" + "Auction" + "/" + auctionId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    if (rest.equals("Fail")) {
                        Toast.makeText(AuctionActivity.this, "This Auction is used in subscribers list", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AuctionActivity.this, "Auction Deleted Successfully", Toast.LENGTH_SHORT).show();
                        getAuctionDetails(valueOf(chitIdStrUp));
                        getNonPrizedDetails(chitIdStrUp);



                    }


                } catch (Exception e) {
                    Toast.makeText(AuctionActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(AuctionActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_menu, menu);
        return true;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logOut) {
            Intent intent1 = new Intent(AuctionActivity.this, LoginActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
            startActivity(intent1);
            finish();


        }
        return super.onOptionsItemSelected(item);


    }

    private void sendMsgToWhatsapp(String number, String message, String name) {

        RequestQueue queue = Volley.newRequestQueue(AuctionActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_SEND_AUCTION_WHATSAP, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(AuctionActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // below line we are creating a map for
                // storing our values in key and value pair.
                Map<String, String> params = new HashMap<String, String>();

                // on below line we are passing our key
                // and value pair to our parameters.

                params.put("WANo", String.valueOf(number));
                params.put("name", name);
                params.put("msg", String.valueOf(message));


                // at last we are
                // returning our params.
                return params;
            }


        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    public static boolean checkNetworkAndShowError(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean isConnected = cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
        if (!isConnected) {
            showNetWorkError(context);
        }

        return isConnected;
    }

    private static void showNetWorkError(Context context) {
        new AlertDialog.Builder(context)
                .setPositiveButton("ok", null)
                .setMessage("Please check Internet Connection!!")
                .show();

    }


}