package com.fairsoft.chitfund.activity;

import com.fairsoft.chitfund.model.Requests;
import com.fairsoft.chitfund.model.WhatsappResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface Retrofit {
    @POST("918142323365")
    Call<Requests> getConfigUpdate(@Header("Authorization") String authorization,
                                   @Body WhatsappResponse  customParams);

}
