package com.fairsoft.chitfund.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;

import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fairsoft.chitfund.api.Urls;
import com.fairsoft.chitfund.model.CustomerContactModel;
import com.fairsoft.chitfund.model.MemberIdModelItem;
import com.fairsoft.chitfund.model.StateModelItem;
import com.fairsoft.chitfund.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class DefineNewMemberActivity extends AppCompatActivity {

    EditText nameTxt, adharTxt, mobile_one, mobile_two, landLineOffTxt, landLineResTxt, emailTxt, addressTxt, addressOneTxt, cityTxt, pinTxt, dobTxt, domTxt, refByTxt, refMobTxt;
    CardView saveBtn, clearCardView;
    String nameStr, adharStr, mobileOneStr, mobileTwoStr, landOffStr, landResStr, emailStr, addressStr, addressOneStr, cityStr, stateStr, pinStr, dobStr, domStr, refByStr, refMobStr;
    final Calendar myCalendar = Calendar.getInstance();
    final Calendar myCalendar2 = Calendar.getInstance();
    private List<MemberIdModelItem> memeberDetailsList;
    private String json;
    String rest;
    String jsonStr;
    Integer memeberIdUp;
    Integer memberIdPara;
    Integer position;
    String today_date;
    boolean update = false;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String compId, userId, userName, compName;
    String MobilePattern = "[0-9]{10}";
    String dobDate, domDate, dobchangedDate, domchangedDate;
    TextView textView1;
    Spinner select_state;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    ArrayAdapter stateAdapter;
    ArrayList<StateModelItem> stateList;
    String stateCodeStr;
    ProgressBar progressBar;
    public static final int MY_DEFAULT_TIMEOUT = 15000;
    String name, mobile;
    private ArrayList<CustomerContactModel> customerContactModelArrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_define_new_member);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Sharedpreference
        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        compId = String.valueOf(pref.getInt("compId", 0));
        userId = String.valueOf(pref.getInt("userId", 0));
        userName = String.valueOf(pref.getString("userName", null));
        compName = String.valueOf(pref.getString("compName", null));

        //id initializing
        init();

        //state Api
        getStateList();



        //bundle
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            memeberIdUp = extras.getInt("memeberId");

            if (memeberIdUp == 0) {
                name = extras.getString("name");
                mobile = extras.getString("number");
                update = false;
                nameTxt.setText(name);
                mobile_one.setText(mobile);


            } else {
                position = extras.getInt("position");
                update = true;
                getMemberDetails(memeberIdUp);


            }

        }


        // creating a variable for gson.
        Gson gson = new Gson();

        // below line is to get to string present from our
        // shared prefs if not present setting it as null.
        String json = pref.getString("openCashModel", null);

        // below line is to get the type of our array list.
        Type type = new TypeToken<ArrayList<CustomerContactModel>>() {
        }.getType();

        // in below line we are getting data from gson
        // and saving it to our array list
        customerContactModelArrayList = gson.fromJson(json, type);

        // checking below if the array list is empty or not
        if (customerContactModelArrayList == null) {
            // if the array list is empty
            // creating a new array list.
            customerContactModelArrayList = new ArrayList<>();
        }



        if (update) {
            getSupportActionBar().setTitle("Update Member");
            textView1.setText("Add Updated Member");

        } else {
            getSupportActionBar().setTitle("New Member");
            textView1.setText("Add New Member");


        }
        //Methods calling
        dobDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        dobTxt.setText((dobDate));
        domDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        domTxt.setText((domDate));
        auctionDatePicker();
        startDatePicker();

        //saving details
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nameStr = nameTxt.getText().toString();
                adharStr = adharTxt.getText().toString();
                mobileOneStr = mobile_one.getText().toString();
                mobileTwoStr = mobile_two.getText().toString();
                landOffStr = landLineOffTxt.getText().toString();
                landResStr = landLineResTxt.getText().toString();
                emailStr = emailTxt.getText().toString();
                addressStr = addressTxt.getText().toString();
                dobStr = dobTxt.getText().toString();
                domStr = domTxt.getText().toString();

                SimpleDateFormat dateFormatprev = new SimpleDateFormat("dd-MM-yyyy");
                Date d = null;
                try {
                    d = dateFormatprev.parse(dobStr);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd");
                dobchangedDate = dateFormat.format(d);

                Date d1 = null;
                try {
                    d1 = dateFormatprev.parse(domStr);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyy-MM-dd");
                domchangedDate = dateFormat.format(d1);
                refByStr = refByTxt.getText().toString();
                refMobStr = refMobTxt.getText().toString();
                addressOneStr = addressOneTxt.getText().toString();
                cityStr = cityTxt.getText().toString();
/*
                stateStr = stateTxt.getText().toString();
*/
                pinStr = pinTxt.getText().toString();


                validateFields();
            }
        });
        clearCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameTxt.setText("");
                adharTxt.setText("");
                mobile_one.setText("");
                mobile_two.setText("");
                landLineOffTxt.setText("");
                landLineResTxt.setText("");
                emailTxt.setText("");
                addressTxt.setText("");
                dobTxt.setText("");
                domTxt.setText("");
                refByTxt.setText("");
                refMobTxt.setText("");
                addressOneTxt.setText("");
                cityTxt.setText("");
/*
                stateTxt.setText("");
*/
                pinTxt.setText("");

            }
        });

    }

    public void init() {

        nameTxt = (EditText) findViewById(R.id.nameTxt);
        nameTxt.setHint(Html.fromHtml("Name <font color =\"#cc0029\" >*</font>"));

        adharTxt = (EditText) findViewById(R.id.adharTxt);
        adharTxt.setHint(Html.fromHtml("Adhar Number <font color =\"#cc0029\" >*</font>"));


        mobile_one = (EditText) findViewById(R.id.mobile_one);
        mobile_one.setHint(Html.fromHtml("Mobile Number <font color =\"#cc0029\" >*</font>"));

        mobile_two = (EditText) findViewById(R.id.mobile_two);
        landLineOffTxt = (EditText) findViewById(R.id.landLineOffTxt);
        landLineResTxt = (EditText) findViewById(R.id.landLineResTxt);
        emailTxt = (EditText) findViewById(R.id.emailTxt);
        addressTxt = (EditText) findViewById(R.id.addressTxt);
        dobTxt = (EditText) findViewById(R.id.dobTxt);
        domTxt = (EditText) findViewById(R.id.domTxt);
        refByTxt = (EditText) findViewById(R.id.refByTxt);
        refMobTxt = findViewById(R.id.refMobTxt);
        addressOneTxt = findViewById(R.id.addressOneTxt);
        select_state = findViewById(R.id.select_state);
        cityTxt = findViewById(R.id.cityTxt);
        pinTxt = findViewById(R.id.pinTxt);

        saveBtn = findViewById(R.id.saveCardView);
        textView1 = findViewById(R.id.textView1);
        clearCardView = findViewById(R.id.clearCardView);
        progressBar = findViewById(R.id.progressBar);


    }

    //datePicker
    @SuppressLint("ClickableViewAccessibility")
    public void auctionDatePicker() {
        DatePickerDialog.OnDateSetListener date = new
                DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateLabel();
                    }

                };


        dobTxt.setOnTouchListener(new View.OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(DefineNewMemberActivity.this, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
                return false;
            }
        });


    }

    private void updateLabel() {

        String myFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        today_date = sdf.format(myCalendar.getTime());
        dobTxt.setText(today_date);


    }

    @SuppressLint("ClickableViewAccessibility")
    public void startDatePicker() {
        DatePickerDialog.OnDateSetListener startDates = new
                DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar2.set(Calendar.YEAR, year);
                        myCalendar2.set(Calendar.MONTH, monthOfYear);
                        myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        startDate();
                    }

                };

        domTxt.setOnTouchListener(new View.OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(DefineNewMemberActivity.this, startDates, myCalendar2
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar2.get(Calendar.DAY_OF_MONTH)).show();
                }
                return false;
            }
        });


    }

    private void startDate() {

        String myFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        domStr = sdf.format(myCalendar2.getTime());
        domTxt.setText(domStr);


    }

    //validating fields
    public void validateFields() {

        if (nameStr.isEmpty()) {
            Toast.makeText(DefineNewMemberActivity.this, "Please enter Name", Toast.LENGTH_SHORT).show();
            return;


        } else if (adharStr.isEmpty()) {
            Toast.makeText(DefineNewMemberActivity.this, "Please enter Adhar Number", Toast.LENGTH_SHORT).show();
            return;


        } else if (adharStr.length() < 12) {
            Toast.makeText(DefineNewMemberActivity.this, "Adhar Number Not Valid Must be 12", Toast.LENGTH_SHORT).show();

            return;
        } else if (mobileOneStr.isEmpty()) {
            Toast.makeText(DefineNewMemberActivity.this, "Please enter mobile Number", Toast.LENGTH_SHORT).show();
            return;


        } else if (mobileOneStr.length() < 10) {
            Toast.makeText(DefineNewMemberActivity.this, "Mobile Number Not Valid", Toast.LENGTH_SHORT).show();

            return;
        } else if (!mobileTwoStr.isEmpty()) {
            if (mobileTwoStr.length() < 10) {
                Toast.makeText(DefineNewMemberActivity.this, "Mobile Number Not Valid", Toast.LENGTH_SHORT).show();
                return;
            } else if (!emailStr.isEmpty()) {
                if (!emailStr.matches(emailPattern)) {
                    Toast.makeText(DefineNewMemberActivity.this, "Invalid Email", Toast.LENGTH_SHORT).show();
                    return;


                }
            }
        } else if (!emailStr.isEmpty()) {
            if (!emailStr.matches(emailPattern)) {
                Toast.makeText(DefineNewMemberActivity.this, "Invalid Email", Toast.LENGTH_SHORT).show();
                return;


            }
        }
        if (update) {
            postMemberDataUsingVolley(nameStr, adharStr, mobileOneStr, mobileTwoStr, landOffStr, landResStr, emailStr, addressStr, addressOneStr, cityStr, stateStr, pinStr, dobchangedDate, domchangedDate, refByStr, refMobStr);


        } else {
            postExistingMemberUsingVolley(nameStr);
        }


    }

    private void getStateList() {
        //https://fssservices.bookxpert.co/api/Chits/getStatesList
        RequestQueue queue = Volley.newRequestQueue(DefineNewMemberActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_STATE_LIST, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //   chitDefineModelItemArrayList = new ArrayList<ChitDefineModelItem>();
                stateList = new ArrayList<>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {


                        JSONObject object = (JSONObject) array.get(i);
                        String state = object.getString("statename");
                        Integer stateId = object.getInt("stateid");
                        stateList.add(new StateModelItem(stateId, state));


                    }


                    stateAdapter = new ArrayAdapter(DefineNewMemberActivity.this, R.layout.state_spinner_text, stateList);
                    stateAdapter.setDropDownViewResource(R.layout.state_spinner_text);
                    //Setting the ArrayAdapter data on the Spinner
                    select_state.setAdapter(stateAdapter);
                    select_state.setOnItemSelectedListener(new DefineNewMemberActivity.ChitSpinnerClass());


                } catch (Exception e) {
                    Toast.makeText(DefineNewMemberActivity.this, "error is" + e.toString(), Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(DefineNewMemberActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }

    class ChitSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            stateCodeStr = select_state.getItemAtPosition(position).toString();


        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }


    private void postMemberDataUsingVolley(String nameStr, String adharStr, String mobileOneStr, String mobileTwoStr, String landOffStr, String landResStr, String emailStr, String addressStr, String addressTwoStr, String cityStr, String stateStr, String pinStr, String dobStr, String domStr, String refByStr, String refMobStr) {
        //https://fssservices.bookxpert.co/api/Chits/MemberDetails/compId/userId/updateData(N/U)
        RequestQueue queue = Volley.newRequestQueue(DefineNewMemberActivity.this);
        if (!checkNetworkAndShowError(this)) {
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        String updateData;
        if (update) {
            updateData = "U";
            memberIdPara = memeberIdUp;
        } else {
            updateData = "N";
            memberIdPara = 0;
        }
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_UPDATE_DETAILS + "/" + compId + "/" + userId + "/" + updateData, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                nameTxt.setText("");
                adharTxt.setText("");
                mobile_one.setText("");
                mobile_two.setText("");
                landLineOffTxt.setText("");
                landLineResTxt.setText("");
                emailTxt.setText("");
                addressTxt.setText("");
                dobTxt.setText("");
                domTxt.setText("");
                refByTxt.setText("");
                refMobTxt.setText("");
                addressOneTxt.setText("");
                cityTxt.setText("");
/*
                stateTxt.setText("");
*/
                pinTxt.setText("");
                if (updateData.equals("U")) {
                    progressBar.setVisibility(View.GONE);

                    Toast.makeText(DefineNewMemberActivity.this, "Member Updated SuccessFully", Toast.LENGTH_SHORT).show();

                } else {
                    progressBar.setVisibility(View.GONE);

                    Toast.makeText(DefineNewMemberActivity.this, "Member Saved SuccessFully", Toast.LENGTH_SHORT).show();

                }


                Intent intent = new Intent(DefineNewMemberActivity.this, MemberEditActivity.class);
                startActivity(intent);


            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(DefineNewMemberActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Name", nameStr);
                params.put("Aadhar", adharStr);
                params.put("Mobile", mobileOneStr);
                params.put("Mobile1", mobileTwoStr);
                params.put("Land", landOffStr);
                params.put("LandRes", landResStr);
                params.put("Email", emailStr);
                params.put("Address", addressStr);
                params.put("Address1", addressTwoStr);
                params.put("City", cityStr);
                params.put("State", stateCodeStr);
                params.put("PIN", pinStr);
                params.put("Dob", dobStr);
                params.put("Dom", domStr);
                params.put("Ref", refByStr);
                params.put("RefMobile", refMobStr);
                params.put("memberId", String.valueOf(memberIdPara));

                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(request);


    }

    private void postExistingMemberUsingVolley(String nameStr) {
        //https://fssservices.bookxpert.co/api/Chits/MemberDetails/compId/userId/name
        if (!checkNetworkAndShowError(this)) {
            return;
        }
        progressBar.setVisibility(View.VISIBLE);

        String myparam = nameStr;
        String url = Urls.URL_IS_EXISTING_UPDATE_DETAILS + "/" + compId + "/" + userId + "/" + myparam;
        RequestQueue queue = Volley.newRequestQueue(DefineNewMemberActivity.this);
        StringRequest request = new StringRequest(Request.Method.GET, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                String json = response;
                String rest = json.substring(1, json.length() - 1);

                if (rest.equals("0")) {
                    postMemberDataUsingVolley(nameStr, adharStr, mobileOneStr, mobileTwoStr, landOffStr, landResStr, emailStr, addressStr, addressOneStr, cityStr, stateStr, pinStr, dobchangedDate, domchangedDate, refByStr, refMobStr);
                } else {
                    Toast.makeText(DefineNewMemberActivity.this, "This Member already exist", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);


                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(DefineNewMemberActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(request);


    }

    private void getMemberDetails(Integer memberId) {


        RequestQueue queue = Volley.newRequestQueue(DefineNewMemberActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_MEMBER_DETAILS + "/" + compId + "/" + userId + "/" + memberId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                memeberDetailsList = new ArrayList<MemberIdModelItem>();

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
                        MemberIdModelItem memeberDefine = new MemberIdModelItem();


                        JSONObject object = (JSONObject) array.get(i);
                        memeberDefine.setDom1(object.getString("dom1"));
                        memeberDefine.setPhoneres(object.getString("phoneres"));
                        memeberDefine.setCity1(object.getString("city1"));
                        memeberDefine.setDom(object.getString("dom"));
                        memeberDefine.setRefmobile(object.getString("refmobile"));
                        memeberDefine.setCity(object.getString("city"));
                        memeberDefine.setMobile1(object.getString("mobile1"));
                        memeberDefine.setMobile11(object.getString("mobile2"));
                        memeberDefine.setPhoneoff(object.getString("phoneoff"));
                        memeberDefine.setPin(object.getString("pin"));
                        memeberDefine.setDob1(object.getString("dob1"));
                        memeberDefine.setDob(object.getString("dob"));
                        memeberDefine.setMobile2(object.getString("mobile2"));
                        memeberDefine.setName(object.getString("name"));
                        memeberDefine.setAadhar(object.getString("aadhar"));
                        memeberDefine.setState(object.getString("state"));
                        memeberDefine.setAdd2(object.getString("add2"));
                        memeberDefine.setAdd1(object.getString("add1"));
                        memeberDefine.setEmail(object.getString("email"));
                        memeberDefine.setMemberid(object.getInt("memberid"));
                        memeberDefine.setRefby(object.getString("refby"));
                        memeberDetailsList.add(memeberDefine);

                    }
                    updatedetails();


                } catch (Exception e) {
                    Toast.makeText(DefineNewMemberActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);


                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(DefineNewMemberActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }


    private void updatedetails() {
        if (memeberDetailsList.get(0).getName() == "null") {
            nameTxt.setText("");

        } else {
            nameTxt.setText(memeberDetailsList.get(0).getName());

        }
        if (memeberDetailsList.get(0).getAadhar() == "null") {
            adharTxt.setText("");

        } else {
            adharTxt.setText(memeberDetailsList.get(0).getAadhar());


        }
        String mobile1 = memeberDetailsList.get(0).getMobile1();
        if (mobile1 == "null") {
            mobile_one.setText("");

        } else {
            mobile_one.setText(mobile1);


        }
        String mobile2 = memeberDetailsList.get(0).getMobile2();
        if (mobile2 == "null") {
            mobile_two.setText("");

        } else {
            mobile_two.setText(mobile2);


        }
        if (memeberDetailsList.get(0).getPhoneoff() == "null") {
            landLineOffTxt.setText("");

        } else {
            landLineOffTxt.setText(memeberDetailsList.get(0).getPhoneoff());


        }
        if (memeberDetailsList.get(0).getPhoneres() == "null") {
            landLineResTxt.setText("");

        } else {
            landLineResTxt.setText(memeberDetailsList.get(0).getPhoneres());


        }
        if (memeberDetailsList.get(0).getEmail() == "null") {
            emailTxt.setText("");

        } else {
            emailTxt.setText(memeberDetailsList.get(0).getEmail());


        }
        if (memeberDetailsList.get(0).getAdd1() == "null") {
            addressTxt.setText("");

        } else {
            addressTxt.setText(memeberDetailsList.get(0).getAdd1());


        }
        if (memeberDetailsList.get(0).getDob() == "null") {
            dobTxt.setText("");

        } else {
            String dateDob = memeberDetailsList.get(0).getDob();

            SimpleDateFormat dateFormatprev = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date d = null;
            try {
                d = dateFormatprev.parse(dateDob);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            String dobchanged = dateFormat.format(d);
            dobTxt.setText(dobchanged);


        }
        if (memeberDetailsList.get(0).getDom() == "null") {

            domTxt.setText("");

        } else {
            String dateDom = memeberDetailsList.get(0).getDom();

            SimpleDateFormat dateFormatprev = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date d = null;
            try {
                d = dateFormatprev.parse(dateDom);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            String domchanged = dateFormat.format(d);
            domTxt.setText(domchanged);


        }
        if (memeberDetailsList.get(0).getRefby() == "null") {
            refByTxt.setText("");

        } else {
            refByTxt.setText(memeberDetailsList.get(0).getRefby());


        }
        if (memeberDetailsList.get(0).getRefmobile() == "null") {
            refMobTxt.setText("");

        } else {
            refMobTxt.setText(memeberDetailsList.get(0).getRefmobile());


        }
        if (memeberDetailsList.get(0).getAdd2() == "null") {
            addressOneTxt.setText("");

        } else {
            addressOneTxt.setText(memeberDetailsList.get(0).getAdd2());


        }
        if (memeberDetailsList.get(0).getCity() == "null") {
            cityTxt.setText("");

        } else {
            cityTxt.setText(memeberDetailsList.get(0).getCity());


        }
        if (memeberDetailsList.get(0).getState() == "null") {

        } else {
            String state = memeberDetailsList.get(0).getState();
            select_state.setSelection(getStateIndex(select_state, state));


        }
        if (memeberDetailsList.get(0).getPin() == "null") {
            pinTxt.setText("");

        } else {
            pinTxt.setText(memeberDetailsList.get(0).getPin());


        }


    }

    private int getStateIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                return i;
            }
        }

        return 0;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_menu, menu);
        return true;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logOut) {
            Intent intent1 = new Intent(DefineNewMemberActivity.this, LoginActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
            startActivity(intent1);
            finish();


        }
        return super.onOptionsItemSelected(item);


    }

    public static boolean checkNetworkAndShowError(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean isConnected = cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
        if (!isConnected) {
            showNetWorkError(context);
        }

        return isConnected;
    }

    private static void showNetWorkError(Context context) {
        new AlertDialog.Builder(context)
                .setPositiveButton("ok", null)
                .setMessage("Please check Internet Connection!!")
                .show();

    }

    public void onBackPressed() {
        Intent intent1 = new Intent(DefineNewMemberActivity.this, DashBoardActivity.class);
        startActivity(intent1);
    }


}