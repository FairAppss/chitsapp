package com.fairsoft.chitfund.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;

import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fairsoft.chitfund.api.Urls;
import com.fairsoft.chitfund.model.ChitModelItem;
import com.fairsoft.chitfund.model.MemberModelItem;
import com.fairsoft.chitfund.model.PaymentCompleteDetailsModelItem;
import com.fairsoft.chitfund.R;

import com.fairsoft.chitfund.adapter.PaymentAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static java.lang.Integer.valueOf;

public class PaymentActivity extends AppCompatActivity implements PaymentAdapter.deleteRow,PaymentAdapter.onItemClickListener {
    TextView sendTxt;
    Spinner select_chitCode, memberSpin;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    Integer chitIdStr;
    Integer chitCodePos;
    String chitCodeStr;
    private String json;
    String rest;
    String jsonStr;
    private List<PaymentCompleteDetailsModelItem> paymentCompleteDetailsModelItems;
    ArrayAdapter chitaa;
    ArrayAdapter memberbb;
    String memberCodeStr;
    Integer memberIdStr;
    private List<String> listData = new ArrayList<String>();
    private List<String> prizedlistData = new ArrayList<String>();
    ArrayList<MemberModelItem> memberList = new ArrayList<>();

    PaymentAdapter paymentAdapter;
    CardView saveButton;
    boolean update = false;
    Integer paymentIdPara;

    String interestGet, amountGet, remarksStr;
    String createdBy;
    EditText dateTxt;
    final Calendar myCalendar = Calendar.getInstance();
    String auctiondayStr;
    String changedDate, chitCodeUp,mobile,name,name1;
    EditText intrestTxt, amountTxt;
    Integer chitIdUp, position, paymentIdUp;
    Integer memeberIdStrUp, chitIdStrUp;
    String compId, userId, userName, compName;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    EditText remarksEd;
    TextView nodata;
    String subId, subTick;
    ProgressBar progressBar;
    public static final int MY_DEFAULT_TIMEOUT = 15000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        getSupportActionBar().setTitle("Payment");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();


        compId = String.valueOf(pref.getInt("compId", 0));
        userId = String.valueOf(pref.getInt("userId", 0));
        userName = String.valueOf(pref.getString("userName", null));
        compName = String.valueOf(pref.getString("compName", null));


        Intent intent = getIntent();
        Bundle extras = intent.getExtras();


        //method calling
        init();
        getChitid();
        if (extras != null) {
            chitIdUp = extras.getInt("chitId");
            position = extras.getInt("position");
            chitCodeUp = extras.getString("chitCode");
            paymentIdUp = extras.getInt("paymentId");
            update = true;
            getUpdatedPaymentDetails(Integer.valueOf(chitIdUp), paymentIdUp);
        }
        auctiondayStr = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        dateTxt.setText((auctiondayStr));
        auctionDatePicker();
        updateLabel();


        //saving details
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interestGet = intrestTxt.getText().toString();
                amountGet = amountTxt.getText().toString();
                remarksStr = remarksEd.getText().toString();
                validateFields();

            }
        });


        sendTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messagestr = "hloo";
                String phonestr = "+91 8074051219";


                // Getting the text
                // from edit text


                // Calling the function
                // to send message
                sendMessage(messagestr);


            }
        });


    }

    private void sendMessage(String message) {

        // Creating new intent
        Intent intent
                = new Intent(
                Intent.ACTION_SEND);

        intent.setType("text/plain");
        intent.setPackage("com.whatsapp");

        // Give your message here
        intent.putExtra(
                Intent.EXTRA_TEXT,
                message);

        // Checking whether Whatsapp
        // is installed or not
        if (intent
                .resolveActivity(
                        getPackageManager())
                == null) {
            Toast.makeText(
                    this,
                    "Please install whatsapp first.",
                    Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        // Starting Whatsapp
        startActivity(intent);
    }

    public void init() {
        recyclerView = findViewById(R.id.paymentRecyclerView);
        memberSpin = (Spinner) findViewById(R.id.member);
        select_chitCode = (Spinner) findViewById(R.id.select_chitCode);
        sendTxt = findViewById(R.id.sendMsg);
        saveButton = findViewById(R.id.saveButton);
        dateTxt = findViewById(R.id.dateTxt);
        intrestTxt = findViewById(R.id.intrestTxt);
        amountTxt = findViewById(R.id.amountTxt);
        amountTxt.setHint(Html.fromHtml("Amount <font color =\"#cc0029\" >*</font>"));

        remarksEd = findViewById(R.id.remarksEd);

        nodata = findViewById(R.id.nodata);
        progressBar = findViewById(R.id.progressBar);


    }

    private boolean isWhatappInstalled() {

        PackageManager packageManager = getPackageManager();
        boolean whatsappInstalled;

        try {

            packageManager.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
            whatsappInstalled = true;


        } catch (PackageManager.NameNotFoundException e) {

            whatsappInstalled = false;

        }

        return whatsappInstalled;

    }

    //datePicker
    @SuppressLint("ClickableViewAccessibility")
    public void auctionDatePicker() {
        DatePickerDialog.OnDateSetListener date = new
                DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateLabel();
                    }

                };


        dateTxt.setOnTouchListener(new View.OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(PaymentActivity.this, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
                return false;
            }
        });


    }

    public void validateFields() {

        if (chitCodeStr.isEmpty() || chitCodeStr.equals("select ChitCode")) {
            progressBar.setVisibility(View.GONE);

            Toast.makeText(PaymentActivity.this, "Please select ChitCode", Toast.LENGTH_SHORT).show();

            return;

        } else if (changedDate.isEmpty()) {
            progressBar.setVisibility(View.GONE);

            Toast.makeText(PaymentActivity.this, "Please select date", Toast.LENGTH_SHORT).show();

            return;
        } else if (amountGet.isEmpty()) {
            progressBar.setVisibility(View.GONE);

            Toast.makeText(PaymentActivity.this, "Please enter Amount", Toast.LENGTH_SHORT).show();

            return;

        }
        postDataUsingVolley(chitCodeStr, chitIdStrUp, memeberIdStrUp, interestGet, amountGet, changedDate, createdBy, remarksStr);


    }

    @Override
    public void deleteRowClick(Integer chitId, Integer paymentId, String chitCode) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                PaymentActivity.this);

        // set title
        alertDialogBuilder.setTitle("Delete");

        // set dialog message
        alertDialogBuilder
                .setMessage("Are You Sure To Delete?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        getDeleteApi(chitId, paymentId, chitCode);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();


    }

    @Override
    public void onItemClick(Integer chitId, Integer paymentId, String chitCode) {
        update = true;
        paymentIdUp=paymentId;
        getUpdatedPaymentDetails(Integer.valueOf(chitId), paymentId);

    }

    class ChitSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            // Toast.makeText(v.getContext(), "Your choose :" + chit[position], Toast.LENGTH_SHORT).show();
            chitIdStr = select_chitCode.getSelectedItemPosition();
            chitCodeStr = select_chitCode.getItemAtPosition(position).toString();
            chitCodePos = select_chitCode.getSelectedItemPosition();
            ChitModelItem chit = (ChitModelItem) parent.getSelectedItem();
            chitIdStrUp = chit.getMemberid();

            if (position >= 0) {
                memberList.clear();
                getSubScriberDetails(chitIdStrUp);
                getPaymentDetails(Integer.valueOf(chitIdStrUp));

            }


        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    class PrizedMember implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            // Toast.makeText(v.getContext(), "Your choose :" + chit[position], Toast.LENGTH_SHORT).show();
            memberIdStr = memberSpin.getSelectedItemPosition() + 1;
            memberCodeStr = memberSpin.getItemAtPosition(position).toString() + 1;
            MemberModelItem member = (MemberModelItem) parent.getSelectedItem();
            memeberIdStrUp = member.getMemberid();
            mobile = member.getMobile();
            name = member.getName();
            String[] separated = name.split("-");
            String ticket=  separated[0]; // this will contain "name"
            name1=  separated[1]; // this will contain " number"




        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private void updateLabel() {

        String myFormat = "dd-MM-yyyy";

        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        auctiondayStr = sdf.format(myCalendar.getTime());
        dateTxt.setText((auctiondayStr));

        dateTxt.setSelection(dateTxt.getText().length());

        SimpleDateFormat dateFormatprev = new SimpleDateFormat("dd-MM-yyyy");
        Date d = null;
        try {
            d = dateFormatprev.parse(auctiondayStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd");
        changedDate = dateFormat.format(d);


    }


    private void getChitid() {
        RequestQueue queue = Volley.newRequestQueue(PaymentActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_AUCTION_CHITS_DETAILS + "/" + compId + "/" + userId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                /// chitDefineModelItemArrayList = new ArrayList<ChitDefineModelItem>();
                ArrayList<ChitModelItem> chitList = new ArrayList<>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject object = (JSONObject) array.get(i);
                        String chitname = object.getString("chitcode");
                        Integer chitid = object.getInt("chitid");
                        chitList.add(new ChitModelItem(chitid, chitname));

                    }

                    if (listData == null) {
                        listData.add(0, "No data");
                    } else {
                        listData.add(0, "select ChitCode");

                        chitaa = new ArrayAdapter(PaymentActivity.this, android.R.layout.simple_spinner_item, chitList);
                        chitaa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner
                        select_chitCode.setAdapter(chitaa);
                        select_chitCode.setOnItemSelectedListener(new PaymentActivity.ChitSpinnerClass());
                    }


                } catch (Exception e) {
                    Toast.makeText(PaymentActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(PaymentActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }

    private void getSubScriberDetails(Integer chitId) {
        RequestQueue queue = Volley.newRequestQueue(PaymentActivity.this);
        //api/Chits/SubscriberDetails/{compid}/{userid}/{chitid}/{subscriberid}
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_SUBSCRIBER_DETAILS + "/" + compId + "/" + userId + "/" + "/" + chitId + "/" + 0, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // subscriberDetailsList = new ArrayList<SubscriberDetailsModelItem>();
                ArrayList<MemberModelItem> memberList = new ArrayList<>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = (JSONObject) array.get(i);

                        String membername = object.getString("ticketno") + "-" + object.getString("name");
                        Integer memberid = object.getInt("subscriberid");
                        String mobile = object.getString("mobile1");

                        memberList.add(new MemberModelItem(mobile,memberid, membername));

                    }

                    if (prizedlistData == null) {
                        prizedlistData.add(0, "No data");
                    } else {
                        memberbb = new ArrayAdapter(PaymentActivity.this, android.R.layout.simple_spinner_item, memberList);
                        memberbb.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner
                        memberSpin.setAdapter(memberbb);
                        memberSpin.setOnItemSelectedListener(new PaymentActivity.PrizedMember());

                    }


                } catch (Exception e) {
                    Toast.makeText(PaymentActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(PaymentActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void getPaymentDetails(Integer chitId) {
        RequestQueue queue = Volley.newRequestQueue(PaymentActivity.this);
        //api/Chits/PaymentDetails/{compid}/{userid}/{chitid}/{paymentid}
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_PAYMENT_COMPLETE_DETAILS + "/" + compId + "/" + userId + "/" + chitId + "/" + 0, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                paymentCompleteDetailsModelItems = new ArrayList<PaymentCompleteDetailsModelItem>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
                        PaymentCompleteDetailsModelItem auctionDefine = new PaymentCompleteDetailsModelItem();
                        JSONObject object = (JSONObject) array.get(i);

                        auctionDefine.setAmount(object.getInt("amount"));
                        auctionDefine.setChitid(object.getInt("chitid"));
                        auctionDefine.setPaymentdate(object.getString("paymentdate"));
                        auctionDefine.setPaymentid(object.getInt("paymentid"));
                        auctionDefine.setPaytype(object.getString("paytype"));
                        auctionDefine.setChitcode(object.getString("chitcode"));
                        auctionDefine.setInterest(object.getInt("interest"));
                        auctionDefine.setName(object.getString("name"));
                        auctionDefine.setSubscriberid(object.getInt("subscriberid"));

                        if (object.isNull("memberid")) {
                            object.put("memberid", "");

                        } else {
                            auctionDefine.setMemberid(object.getInt("memberid"));


                        }
                        paymentCompleteDetailsModelItems.add(auctionDefine);
                    }


                } catch (Exception e) {
                    Toast.makeText(PaymentActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }
                if (paymentCompleteDetailsModelItems.isEmpty()) {
                    nodata.setVisibility(View.VISIBLE);

                } else {
                    nodata.setVisibility(View.GONE);


                }
                linearLayoutManager = new LinearLayoutManager(PaymentActivity.this);
                recyclerView.setLayoutManager(linearLayoutManager);
                paymentAdapter = new PaymentAdapter(PaymentActivity.this, paymentCompleteDetailsModelItems, PaymentActivity.this::deleteRowClick,PaymentActivity.this::onItemClick);
                recyclerView.setAdapter(paymentAdapter);
                recyclerView.getAdapter().notifyDataSetChanged();


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(PaymentActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void postDataUsingVolley(String chitCodep, Integer chitIdp, Integer memeberId, String interest, String payAmount, String payDate, String createdBy, String remarksStr) {
        if (!checkNetworkAndShowError(this)) {
            return;
        }
        progressBar.setVisibility(View.VISIBLE);


        String updateData;
        if (update) {
            updateData = "U";
            paymentIdPara = paymentIdUp;

        } else {
            updateData = "N";
            paymentIdPara = 0;

        }
        // creating a new variable for our request queue
        RequestQueue queue = Volley.newRequestQueue(PaymentActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_PAYMENT_SAVE_DETAILS + "/" + compId + "/" + userId + "/" + updateData, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);
                String json = response;
                String rest = json.substring(1, json.length() - 1);


                // on below line we are displaying a success toast message.
                if (rest.equals("")) {
                    Toast.makeText(PaymentActivity.this, "Payment Not Updated", Toast.LENGTH_SHORT).show();



                } else if (rest.equals("Success")) {

                    if (updateData.equals("U")) {
                        getChitid();
                        getPaymentDetails(Integer.valueOf(chitIdStrUp));


                        Toast.makeText(PaymentActivity.this, "Payment Updated SuccessFully", Toast.LENGTH_SHORT).show();
                        update = false;
                        Bundle args = new Bundle();
                        args.putString("mobileList", mobile);
                        args.putString("chitCodep", chitCodep);
                        args.putString("name", name1);
                        args.putString("auctionamountp", payAmount);
                        args.putString("auctionDatep", changedDate);


                        MessagePayDialog dialog = new MessagePayDialog();
                        dialog.setCancelable(false);
                        FragmentActivity activity = (FragmentActivity) (PaymentActivity.this);
                        FragmentManager fm = activity.getSupportFragmentManager();
                        dialog.setArguments(args);
                        dialog.show(fm, "MyDialogFragment");
                        progressBar.setVisibility(View.GONE);


                    } else {
                        getChitid();
                        getPaymentDetails(Integer.valueOf(chitIdStrUp));


                        Toast.makeText(PaymentActivity.this, "Payment Saved SuccessFully", Toast.LENGTH_SHORT).show();
                        Bundle args = new Bundle();
                        args.putString("mobileList", mobile);
                        args.putString("chitCodep", chitCodep);
                        args.putString("name", name1);

                        args.putString("auctionamountp", payAmount);
                        args.putString("auctionDatep", changedDate);


                        MessagePayDialog dialog = new MessagePayDialog();
                        dialog.setCancelable(false);
                        FragmentActivity activity = (FragmentActivity) (PaymentActivity.this);
                        FragmentManager fm = activity.getSupportFragmentManager();
                        dialog.setArguments(args);
                        dialog.show(fm, "MyDialogFragment");
                        progressBar.setVisibility(View.GONE);


                    }
                }


                dateTxt.setText((auctiondayStr));
                amountTxt.setText("");
                intrestTxt.setText("");


            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(PaymentActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // below line we are creating a map for
                // storing our values in key and value pair.
                Map<String, String> params = new HashMap<String, String>();

                // on below line we are passing our key
                // and value pair to our parameters.
                params.put("payid", String.valueOf(paymentIdPara));
                params.put("chitcode", chitCodep);
                params.put("chitId", String.valueOf(chitIdp));
                params.put("memberid", String.valueOf(memeberId));
                params.put("interest", interest);
                params.put("payamount", payAmount);
                params.put("paydate", payDate);
                params.put("createdby", userId);
                params.put("remarks", remarksStr);


                // at last we are
                // returning our params.
                return params;
            }
        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }

    private void getUpdatedPaymentDetails(Integer chitId, Integer paymentId) {
        RequestQueue queue = Volley.newRequestQueue(PaymentActivity.this);
        //api/Chits/PaymentDetails/{compid}/{userid}/{chitid}/{paymentid}
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_PAYMENT_COMPLETE_DETAILS + "/" + compId + "/" + userId + "/" + chitId + "/" + paymentId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                paymentCompleteDetailsModelItems = new ArrayList<PaymentCompleteDetailsModelItem>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
                        PaymentCompleteDetailsModelItem auctionDefine = new PaymentCompleteDetailsModelItem();
                        JSONObject object = (JSONObject) array.get(i);

                        auctionDefine.setAmount(object.getInt("amount"));
                        auctionDefine.setChitid(object.getInt("chitid"));
                        auctionDefine.setPaymentdate(object.getString("paymentdate"));
                        auctionDefine.setPaymentid(object.getInt("paymentid"));
                        auctionDefine.setPaytype(object.getString("paytype"));
                        auctionDefine.setChitcode(object.getString("chitcode"));
                        auctionDefine.setInterest(object.getInt("interest"));
                        auctionDefine.setName(object.getString("name"));
                        auctionDefine.setSubscriberid(object.getInt("subscriberid"));
                        auctionDefine.setTicketno(object.getString("ticketno"));


                        if (object.isNull("memberid")) {
                            object.put("memberid", "");

                        } else {
                            auctionDefine.setMemberid(object.getInt("memberid"));


                        }
                        paymentCompleteDetailsModelItems.add(auctionDefine);
                        updatedetails();
                    }


                } catch (Exception e) {
                    Toast.makeText(PaymentActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(PaymentActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void updatedetails() {
        String chit = paymentCompleteDetailsModelItems.get(0).getChitcode();
        select_chitCode.setSelection(getChitIndex(select_chitCode, chit));




        listData.add(0, chitCodeUp);
        dateTxt.setText(paymentCompleteDetailsModelItems.get(0).getPaymentdate());
        amountTxt.setText(String.valueOf(paymentCompleteDetailsModelItems.get(0).getAmount()));
        intrestTxt.setText(String.valueOf(paymentCompleteDetailsModelItems.get(0).getInterest()));
        String name = paymentCompleteDetailsModelItems.get(0).getName();
        subId = paymentCompleteDetailsModelItems.get(0).getTicketno();
        subTick = subId + "-" + name;
        memberSpin.setSelection(getSubIndex(memberSpin, subTick));



    }

    private int getChitIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                return i;
            }
        }

        return 0;
    }

    private int getSubIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(String.valueOf(myString))) {
                return i;
            }
        }

        return 0;
    }

    private void getDeleteApi(Integer chitId, Integer paymentId, String ChitCode) {
        // api/Chits/Deletes/{compid}/{userid}/{chitid}/{chitcode}/{Payment}/{paymentId}
        RequestQueue queue = Volley.newRequestQueue(PaymentActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_DELETE + "/" + compId + "/" + userId + "/" + chitId + "/" + ChitCode + "/" + "Payment" + "/" + paymentId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    Toast.makeText(PaymentActivity.this, "Payment Deleted Successfully", Toast.LENGTH_SHORT).show();

                    if (rest.equals("Fail")) {
                        Toast.makeText(PaymentActivity.this, "This Auction is used in subscribers list", Toast.LENGTH_SHORT).show();

                    } else {
                        getPaymentDetails(valueOf(chitIdStrUp));


                    }


                } catch (Exception e) {
                    Toast.makeText(PaymentActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(PaymentActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_menu, menu);
        return true;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logOut) {
            Intent intent1 = new Intent(PaymentActivity.this, LoginActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
            startActivity(intent1);
            finish();


        }
        return super.onOptionsItemSelected(item);


    }

    public static boolean checkNetworkAndShowError(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean isConnected = cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
        if (!isConnected) {
            showNetWorkError(context);
        }

        return isConnected;
    }

    private static void showNetWorkError(Context context) {
        new AlertDialog.Builder(context)
                .setPositiveButton("ok", null)
                .setMessage("Please check Internet Connection!!")
                .show();

    }


}