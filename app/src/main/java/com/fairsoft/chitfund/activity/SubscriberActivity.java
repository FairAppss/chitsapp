package com.fairsoft.chitfund.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fairsoft.chitfund.R;

import com.fairsoft.chitfund.adapter.SubscriberAdapter;
import com.fairsoft.chitfund.api.Urls;
import com.fairsoft.chitfund.model.MemberModelItem;
import com.fairsoft.chitfund.model.SubScriberChitModelItem;
import com.fairsoft.chitfund.model.SubscriberDetailsModelItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import java.util.List;


public class SubscriberActivity extends AppCompatActivity implements SubscriberAdapter.onItemClickListener, SubscriberAdapter.deleteRow {
    Spinner selectChit, memberSpin;
    SubscriberAdapter subscriberAdapter;
    String memberIdStr, chitCodeStr;
    Integer chitIdStr;
    Integer memeberIdStr;
    Integer memeberIdStrUp, chitIdStrUp, memberNum;

    Integer chitCodePos;
    Integer memeberIdPos;
    String chitIdPos;
    String memberCodePs;


    Integer subScribeId;
    Integer chitIdUp;
    Integer position;

    CardView addCardView;
    RadioButton active, broken;
    RadioGroup radioGroup;
    String status;
    private String json;
    String rest;
    String jsonStr;

    private List<String> listData = new ArrayList<String>();
    private List<String> memberListData = new ArrayList<String>();
    private List<SubscriberDetailsModelItem> subscriberDetailsList;
    RecyclerView recyclerView;
    String spChitCode, spMemberId, spDate;
    ArrayList<SubScriberChitModelItem> chitList;
    ArrayList<MemberModelItem> memberList;

    ArrayAdapter aa;
    ArrayAdapter bb;

    boolean update = false;
    Integer subscriberIdPara;
    String compId, userId, userName, compName,chitCodeStrUp,memeberUp;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    TextView nodata;
    String subId, chit;
    ProgressBar progressBar;
    public static final int MY_DEFAULT_TIMEOUT = 15000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscriber);
        getSupportActionBar().setTitle(" Add Subscriber");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();


        compId = String.valueOf(pref.getInt("compId", 0));
        userId = String.valueOf(pref.getInt("userId", 0));
        userName = String.valueOf(pref.getString("userName", null));
        compName = String.valueOf(pref.getString("compName", null));


        init();
        getChitid();
        getMemberId();
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        if (extras != null) {
            chitIdUp = extras.getInt("chitId");
            subScribeId = extras.getInt("subscriberId");
            position = extras.getInt("position");
            chitIdPos = extras.getString("chitCode");
            memeberIdPos = extras.getInt("memberId");
            memberCodePs = extras.getString("memberCode");
            update = true;
/*
            getSelectedSubScriberDetails(chitIdUp, subScribeId);
*/
        }


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {


            }
        });

        addCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = ((RadioButton) findViewById(radioGroup.getCheckedRadioButtonId()))
                        .getText().toString();
                validateFields();


            }
        });


    }

    @Override
    public void deleteRowClick(Integer chitId, Integer subscriberId, String chitCode) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                SubscriberActivity.this);

        // set title
        alertDialogBuilder.setTitle("Delete");

        // set dialog message
        alertDialogBuilder
                .setMessage("Are You Sure To Delete?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        getDeleteApi(chitId, subscriberId, chitCode);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onItemClick(Integer chitId, Integer subScriberId, String chitCode,String member) {
        update = true;
        subScribeId = subScriberId;
        chitCodeStrUp=chitCode;
        memeberUp=member;
        updateChitCode();
/*
        getSelectedSubScriberDetails(chitId, subScribeId);
*/


    }


    class ChitSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            // Toast.makeText(v.getContext(), "Your choose :" + chit[position], Toast.LENGTH_SHORT).show();
            chitIdStr = selectChit.getSelectedItemPosition();
            chitCodeStr = selectChit.getItemAtPosition(position).toString();
            chitCodePos = selectChit.getSelectedItemPosition();
            SubScriberChitModelItem chit = (SubScriberChitModelItem) parent.getSelectedItem();
            chitIdStrUp = chit.getMemberid();
            memberNum = chit.getMembers();
            getSubScriberDetails(chitIdStrUp, memberNum);


        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    class memberSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            //  Toast.makeText(v.getContext(), "Your choose :" + members[position], Toast.LENGTH_SHORT).show();
            memeberIdStr = memberSpin.getSelectedItemPosition();
            memberIdStr = memberSpin.getItemAtPosition(position).toString();
            MemberModelItem member = (MemberModelItem) parent.getSelectedItem();
            memeberIdStrUp = member.getMemberid();

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    public void init() {
        active = findViewById(R.id.active);
        broken = findViewById(R.id.broken);
        radioGroup = findViewById(R.id.radioGroup);
        selectChit = (Spinner) findViewById(R.id.select_chit);
        memberSpin = (Spinner) findViewById(R.id.member);
        addCardView = findViewById(R.id.addCardView);
        nodata = findViewById(R.id.nodata);
        progressBar = findViewById(R.id.progressBar);


    }

    private void getChitid() {
        if (!checkNetworkAndShowError(this)) {
            return;
        }
        progressBar.setVisibility(View.VISIBLE);

        RequestQueue queue = Volley.newRequestQueue(SubscriberActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_CHIT_DETAILS + "/" + compId + "/" + userId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //   chitDefineModelItemArrayList = new ArrayList<ChitDefineModelItem>();
                chitList = new ArrayList<>();
                progressBar.setVisibility(View.GONE);


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {


                        JSONObject object = (JSONObject) array.get(i);
                        String chitname = object.getString("chitcode");
                        Integer chitid = object.getInt("chitid");
                        Integer members = object.getInt("noofmembers");

                        chitList.add(new SubScriberChitModelItem(chitid, chitname, members));


                    }


                    if (listData == null) {
                        listData.add(0, "No data");
                    } else {
                        listData.add(0, "select ChitCode");
                        aa = new ArrayAdapter(SubscriberActivity.this, android.R.layout.simple_spinner_item, chitList);
                        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner
                        selectChit.setAdapter(aa);
                        SubScriberChitModelItem chit = (SubScriberChitModelItem) selectChit.getSelectedItem();
                        chitIdStrUp = chit.getMemberid();
                        memberNum = chit.getMembers();
                        getSubScriberDetails(chitIdStrUp, memberNum);
                        selectChit.setOnItemSelectedListener(new ChitSpinnerClass());
                    }


                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);

                    Toast.makeText(SubscriberActivity.this, "error is" + e.toString(), Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(SubscriberActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(request);


    }

    private void getMemberId() {
        if (!checkNetworkAndShowError(this)) {
            return;
        }
        progressBar.setVisibility(View.VISIBLE);

        RequestQueue queue = Volley.newRequestQueue(SubscriberActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_MEMBER_DETAILS + "/" + compId + "/" + userId + "/" + 0, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);

                memberList = new ArrayList<>();

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = (JSONObject) array.get(i);
                        String membername = object.getString("name");
                        Integer memberid = object.getInt("memberid");
                        String mobile = object.getString("mobile1");

                        memberList.add(new MemberModelItem(mobile, memberid, membername));


                    }
                    if (memberListData == null) {
                        memberListData.add(0, "No data");
                    } else {
                        memberListData.add(0, "select Member");


                        bb = new ArrayAdapter(SubscriberActivity.this, android.R.layout.simple_spinner_item, memberList);
                        bb.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner
                        memberSpin.setAdapter(bb);
                        memberSpin.setOnItemSelectedListener(new memberSpinnerClass());
                    }


                } catch (Exception e) {
                    Toast.makeText(SubscriberActivity.this, "error is" + e.toString(), Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);


                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(SubscriberActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);

    }

    public void validateFields() {

        if (chitCodeStr.isEmpty()) {
            Toast.makeText(SubscriberActivity.this, "Please enter Chit Id", Toast.LENGTH_SHORT).show();

            return;
        } else if (memberIdStr.isEmpty()) {
            Toast.makeText(SubscriberActivity.this, "Please enter Member Id", Toast.LENGTH_SHORT).show();

            return;
        } else if (subscriberDetailsList == null || subscriberDetailsList.size() == 0) {
            postDataUsingVolley(chitIdStrUp, chitCodeStr, memeberIdStrUp, status);
            return;


        } else {
            if (update) {
                postDataUsingVolley(chitIdStrUp, chitCodeStr, memeberIdStrUp, status);
                return;

            } else {
                int size = subscriberDetailsList.size();
                if (size >= memberNum) {
                    progressBar.setVisibility(View.GONE);

                    Toast.makeText(SubscriberActivity.this, "Subscribers Should Not be Greater than Chit Members", Toast.LENGTH_SHORT).show();
                    return;

                } else {
                    postDataUsingVolley(chitIdStrUp, chitCodeStr, memeberIdStrUp, status);
                    return;


                }
            }

        }


    }


    private void postDataUsingVolley(Integer chitId, String chitCodeStr, Integer memeberIdStr, String status) {
        //api/Chits/UpdateSubscriber/{compid}/{userid}/{chitcode}/{chitid}/{subscriberid}/{memberid}/{status}/{updStatus}
        if (!checkNetworkAndShowError(this)) {
            return;
        }
        progressBar.setVisibility(View.VISIBLE);


        String updateData;
        if (update) {
            updateData = "U";
            subscriberIdPara = subScribeId;

        } else {
            updateData = "N";
            subscriberIdPara = 0;

        }

        String myparam = chitCodeStr + "/" + chitId + "/" + subscriberIdPara + "/" + memeberIdStr + "/" + status + "/" + updateData;
        String url = Urls.URL_SUBSCRIBE_MEMBER + "/" + compId + "/" + userId + "/" + myparam;
        // creating a new variable for our request queue
        RequestQueue queue = Volley.newRequestQueue(SubscriberActivity.this);
        StringRequest request = new StringRequest(Request.Method.GET, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (updateData.equals("U")) {
                    progressBar.setVisibility(View.GONE);

                    Toast.makeText(SubscriberActivity.this, "Subscriber Updated SuccessFully", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(SubscriberActivity.this, SubscriberActivity.class);
                    startActivity(intent);

                } else {
                    progressBar.setVisibility(View.GONE);

                    Toast.makeText(SubscriberActivity.this, "Subscriber Saved SuccessFully", Toast.LENGTH_SHORT).show();


                }


                // on below line we are displaying a success toast message.
                getSubScriberDetails(chitIdStrUp, memberNum);


            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(SubscriberActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        });
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }


    private void getSubScriberDetails(Integer chitId, Integer memNum) {
        RequestQueue queue = Volley.newRequestQueue(SubscriberActivity.this);
        //api/Chits/SubscriberDetails/{compid}/{userid}/{chitid}/{subscriberid}
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_SUBSCRIBER_DETAILS + "/" + compId + "/" + userId + "/" + chitId + "/" + 0, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                subscriberDetailsList = new ArrayList<SubscriberDetailsModelItem>();

                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace(
                            "[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {
                        SubscriberDetailsModelItem memeberDefine = new SubscriberDetailsModelItem();
                        JSONObject object = (JSONObject) array.get(i);
                        memeberDefine.setReference(object.getString("reference"));
                        memeberDefine.setChitid(object.getInt("chitid"));
                        memeberDefine.setCreatedby(object.getInt("createdby"));
                        memeberDefine.setCreateddate(object.getString("createddate"));
                        memeberDefine.setCity(object.getString("city"));
                        memeberDefine.setChitcode(object.getString("chitcode"));
                        memeberDefine.setName(object.getString("member"));
                        memeberDefine.setSubscriberid(object.getInt("subscriberid"));
                        memeberDefine.setMemberid(object.getInt("memberid"));
                        memeberDefine.setStatus(object.getString("status"));
                        memeberDefine.setTicketno(object.getString("ticketno"));
                        subscriberDetailsList.add(memeberDefine);


                    }


                } catch (Exception e) {
                    Toast.makeText(SubscriberActivity.this, "error is" + e.toString(), Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);


                }
                if (subscriberDetailsList.isEmpty()) {
                    nodata.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);


                } else {
                    nodata.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);


                }
                recyclerView = findViewById(R.id.subscriberRecyclerView);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SubscriberActivity.this);
                recyclerView.setLayoutManager(linearLayoutManager);
                subscriberAdapter = new SubscriberAdapter(SubscriberActivity.this, subscriberDetailsList, SubscriberActivity.this::onItemClick, SubscriberActivity.this::deleteRowClick);
                recyclerView.setAdapter(subscriberAdapter);

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(SubscriberActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void getSelectedSubScriberDetails(Integer chitId, Integer subScribeId) {
        RequestQueue queue = Volley.newRequestQueue(SubscriberActivity.this);
        //api/Chits/SubscriberDetails/{compid}/{userid}/{chitid}/{subscriberid}
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_SUBSCRIBER_DETAILS + "/" + compId + "/" + userId + "/" + chitId + "/" + subScribeId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                subscriberDetailsList = new ArrayList<SubscriberDetailsModelItem>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {

                        SubscriberDetailsModelItem memeberDefine = new SubscriberDetailsModelItem();
                        JSONObject object = (JSONObject) array.get(i);
                        memeberDefine.setReference(object.getString("reference"));
                        memeberDefine.setChitid(object.getInt("chitid"));
                        memeberDefine.setCreatedby(object.getInt("createdby"));
                        memeberDefine.setCreateddate(object.getString("createddate"));
                        memeberDefine.setChitcode(object.getString("chitcode"));
                        memeberDefine.setMember(object.getString("member"));
                        memeberDefine.setSubscriberid(object.getInt("subscriberid"));
                        memeberDefine.setMemberid(object.getInt("memberid"));
                        memeberDefine.setStatus(object.getString("status"));
                        memeberDefine.setTicketno(object.getString("ticketno"));
                        subscriberDetailsList.add(memeberDefine);
/*
                        updateChitCode();
*/
                    }


                } catch (Exception e) {
                    Toast.makeText(SubscriberActivity.this, "error is" + e.toString(), Toast.LENGTH_SHORT).show();

                }
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(SubscriberActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }


    private void updateChitCode() {
      /*  chit = subscriberDetailsList.get(0).getChitcode();
        subId = subscriberDetailsList.get(0).getMember();*/
        selectChit.setSelection(getChitIndex(selectChit, chitCodeStrUp));
        memberSpin.setSelection(getSubIndex(memberSpin, memeberUp));


    }

    private int getChitIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                return i;
            }
        }

        return 0;
    }

    private int getSubIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(String.valueOf(myString))) {
                return i;
            }
        }

        return 0;
    }

    private void getDeleteApi(Integer chitId, Integer subscriberId, String ChitCode) {
        // api/Chits/Deletes/{compid}/{userid}/{chitid}/{chitcode}/{type}/{typeID}
        RequestQueue queue = Volley.newRequestQueue(SubscriberActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_DELETE + "/" + compId + "/" + userId + "/" + chitId + "/" + ChitCode + "/" + "Subscriber" + "/" + subscriberId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    if (rest.equals("Success")) {
                        Toast.makeText(SubscriberActivity.this, "Subscriber Deleted Sussessfully", Toast.LENGTH_SHORT).show();
                        getSubScriberDetails(chitIdStrUp, memberNum);
                    } else if (rest.equals("This Subscriber is used in Auctions list")) {
                        Toast.makeText(SubscriberActivity.this, "This Subscriber is used in Auctions", Toast.LENGTH_SHORT).show();

                    }

                } catch (Exception e) {
                    Toast.makeText(SubscriberActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(SubscriberActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_menu, menu);
        return true;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logOut) {
            Intent intent1 = new Intent(SubscriberActivity.this, LoginActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
            startActivity(intent1);
            finish();


        }
        return super.onOptionsItemSelected(item);


    }

    public static boolean checkNetworkAndShowError(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean isConnected = cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
        if (!isConnected) {
            showNetWorkError(context);
        }

        return isConnected;
    }

    private static void showNetWorkError(Context context) {
        new AlertDialog.Builder(context)
                .setPositiveButton("ok", null)
                .setMessage("Please check Internet Connection!!")
                .show();

    }


}