package com.fairsoft.chitfund.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;

import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.BuildConfig;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.fairsoft.chitfund.api.Urls;
import com.fairsoft.chitfund.model.LoginResponseModelItem;
import com.fairsoft.chitfund.R;

import com.google.android.play.core.appupdate.AppUpdateManager;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


public class LoginActivity extends AppCompatActivity {

    CardView click;
    EditText passwordTxt;
    EditText userNameTxt;
    String userNameStr, passwordNameStr;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    /*
        String deviceId;
    */
    private List<LoginResponseModelItem> loginResponseModelItems;
    private String json;
    String rest;
    String jsonStr;

    String compName, userName, vesrionStr;
    Integer compId, userId;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    TextView vesrionTxt;
    ProgressBar progressBar;
    public static final int MY_DEFAULT_TIMEOUT = 15000;
    private AppUpdateManager appUpdateManager;
    private static final int RC_APP_UPDATE = 100;
    String currentVersion = "";


    @SuppressLint({"CommitPrefEdits", "SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Objects.requireNonNull(getSupportActionBar()).setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().hide();


        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        //method calling
        init();
        vesrionStr = BuildConfig.VERSION_NAME;
        vesrionTxt.setText("Version :" + " " + vesrionStr);
        getDeviceId();
        postDeviceIdDataUsingVolley(getDeviceId());


        //validation
        click.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                userNameStr = userNameTxt.getText().toString();
                passwordNameStr = passwordTxt.getText().toString();
                validateFields();
            }
        });
    }


    public void init() {
        click = findViewById(R.id.click);
        userNameTxt = findViewById(R.id.userName);
        passwordTxt = findViewById(R.id.password);
        vesrionTxt = findViewById(R.id.version);
        passwordTxt.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        progressBar = findViewById(R.id.progressBar);


    }


    private String getDeviceId() {
        // testing code
/*
        if (true) {
            return "ac67cae1287ee185";
        }
*/
        // ac67cae1287ee185

        @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return android_id;
    }


    public static class AsteriskPasswordTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new PasswordCharSequence(source);
        }

        private static class PasswordCharSequence implements CharSequence {
            private final CharSequence mSource;

            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }

            public char charAt(int index) {
                return '*'; // This is the important part
            }

            public int length() {
                return mSource.length(); // Return default
            }

            public @NotNull CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    }


    @SuppressLint("ResourceAsColor")
    public void validateFields() {
        if (userNameStr.isEmpty()) {
            Toast.makeText(LoginActivity.this, "Please enter Email Id", Toast.LENGTH_SHORT).show();

            return;
        } else if (passwordNameStr.isEmpty()) {
            Toast.makeText(LoginActivity.this, "Please enter password", Toast.LENGTH_SHORT).show();

            return;
        } else if (userNameStr.matches(emailPattern)) {
        } else {
            Toast.makeText(getApplicationContext(), "Invalid email address", Toast.LENGTH_SHORT).show();
            return;
        }

        postLoginDataUsingVolley(userNameStr, passwordNameStr, getDeviceId());

    }

    //calling device id api
    private void postDeviceIdDataUsingVolley(String devideid) {
        //https://fssservices.bookxpert.co/api/Chits/CheckDevice/deviceId
        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
        if (!checkNetworkAndShowError(this)) {
            return;
        }
        progressBar.setVisibility(View.VISIBLE);


        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_Login_DEVID_ID + "/" + devideid, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBar.setVisibility(View.GONE);


                loginResponseModelItems = new ArrayList<LoginResponseModelItem>();
                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    userNameTxt.setText(rest);

                    if (rest.equals("0")) {

                        new AlertDialog.Builder(LoginActivity.this)
                                .setTitle("Device Details")
                                .setMessage("Please register this id in server : \n" + getDeviceId())
                                .setPositiveButton("ok", (dialogInterface, i) -> finish())
                                .setNegativeButton("Copy", (dialogInterface, i) -> {

                                    ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                                    ClipData clip = ClipData.newPlainText("id", getDeviceId());
                                    clipboard.setPrimaryClip(clip);
                                    showError("Copied!!");
                                    finish();
                                })
                                .setCancelable(false)
                                .show();


                    } else {
                        userNameTxt.setText(rest);
                        try {
                            currentVersion = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0).versionName;

                            Log.e("Current Version", "::" + currentVersion);
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }

                        new GetVersionCode().execute();


                    }


                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
                    builder1.setMessage(" Your Device  " + devideid + "  Not  Registered");
                    builder1.setCancelable(false);

                    builder1.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });


                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }


            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(LoginActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

                Log.e("error is", error.toString());
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(request);


    }

    private void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    //calling Login api
    private void postLoginDataUsingVolley(String nameStr, String passwordStr, String devideid) {
        //https://fssservices.bookxpert.co/api/Chits/isValidLogin
        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
        if (!checkNetworkAndShowError(this)) {
            return;
        }
        progressBar.setVisibility(View.VISIBLE);

        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_Login_DETAILS, new com.android.volley.Response.Listener<String>() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onResponse(String response) {

/*
                userNameTxt.setText("");
*/
                passwordTxt.setText("");
                json = response;
                rest = json.substring(1, json.length() - 1);

                loginResponseModelItems = new ArrayList<LoginResponseModelItem>();
                if (rest.equals("Invalid User")) {
                    progressBar.setVisibility(View.GONE);

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
                    builder1.setMessage("Invalid User");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });


                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                } else {

                    try {
                        String json1 = rest.replace("[", "");
                        String json2 = json1.replace("]", "");
                        jsonStr = rest.replace("\\", "");
                        JSONArray array = new JSONArray(jsonStr);
                        for (int i = 0; i < array.length(); i++) {
                            LoginResponseModelItem memeberDefine = new LoginResponseModelItem();
                            JSONObject object = (JSONObject) array.get(i);
                            memeberDefine.setRole(object.getString("role"));
                            memeberDefine.setCompanyname(object.getString("companyname"));
                            memeberDefine.setCompid(object.getInt("compid"));
                            memeberDefine.setUserid(object.getInt("userid"));
                            memeberDefine.setUsername(object.getString("username"));
                            loginResponseModelItems.add(memeberDefine);
                        }


                        compId = loginResponseModelItems.get(0).getCompid();
                        compName = loginResponseModelItems.get(0).getCompanyname();
                        userName = loginResponseModelItems.get(0).getUsername();
                        userId = loginResponseModelItems.get(0).getUserid();
                       String role = loginResponseModelItems.get(0).getRole();



                        editor.putInt("compId", compId);        // Saving integer
                        editor.putInt("userId", userId);    // Saving Int
                        editor.putString("compName", compName);      // Saving String
                        editor.putString("userName", userName);  // Saving string

                        // Save the changes in SharedPreferences
                        editor.apply(); // commit changes
                        if (role.equals("Receipts")){
                            Toast.makeText(LoginActivity.this, "Login Successfully", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LoginActivity.this, ReceiptActivity.class);
                            startActivity(intent);

                        }else {
                            Toast.makeText(LoginActivity.this, "Login Successfully", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class);
                            startActivity(intent);
                        }


                    } catch (Exception e) {
                        Toast.makeText(LoginActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);


                    }
                }


            }
        }, new com.android.volley.Response.ErrorListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(LoginActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);


                Log.e("error is", error.toString());
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", nameStr);
                params.put("pwd", passwordStr);
                params.put("deviceid", devideid);

                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(request);


    }

    //checking network connection
    public static boolean checkNetworkAndShowError(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean isConnected = cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
        if (!isConnected) {
            showNetWorkError(context);
        }

        return isConnected;
    }

    private static void showNetWorkError(Context context) {
        new AlertDialog.Builder(context)
                .setPositiveButton("ok", null)
                .setMessage("Please check Internet Connection!!")
                .show();

    }

    public void onBackPressed() {
        finish();
    }

    public class GetVersionCode extends AsyncTask<Void, String, String> {

        @Override

        protected String doInBackground(Void... voids) {

            String newVersion = null;

            try {
                Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                newVersion = sibElemet.text();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return newVersion;

        }


        @Override

        protected void onPostExecute(String onlineVersion) {

            super.onPostExecute(onlineVersion);

            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                vesrionStr = vesrionTxt.getText().toString();

                if (onlineVersion.equals(currentVersion)) {

                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                    alertDialog.setTitle("Update");
                    alertDialog.setIcon(R.mipmap.ic_launcher);
                    alertDialog.setMessage("New Update is available");

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Update", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + LoginActivity.this.getPackageName())));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + LoginActivity.this.getPackageName())));
                            }
                        }
                    });

                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    alertDialog.show();
                }

            }

            Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);

        }
    }


}