package com.fairsoft.chitfund.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;

import android.os.StrictMode;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.fairsoft.chitfund.adapter.ChitLevelReportAdapter;
import com.fairsoft.chitfund.api.Urls;
import com.fairsoft.chitfund.imagestopdf.PDFEngine;
import com.fairsoft.chitfund.model.ChitLevelReportsModelItem;
import com.fairsoft.chitfund.model.ChitModelItem;
import com.fairsoft.chitfund.model.CustomParamsItem;
import com.fairsoft.chitfund.model.MultipleWhatsappRequest;
import com.fairsoft.chitfund.model.ReceiversItem;
import com.fairsoft.chitfund.R;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class ChitLevelReportActivity extends AppCompatActivity {
    Spinner select_chitCode;
    Integer chitIdStr;
    Integer chitCodePos;
    String chitCodeStr;
    Integer chitIdStrUp;
    private String json;
    String rest;
    String jsonStr;
    ArrayAdapter chitaa;
    private List<ChitLevelReportsModelItem> chitLevelReportsModelItems;
    RecyclerView recyclerView;
    ChitLevelReportAdapter chitLevelReportAdapter;
    TextView nodataTxt;
    LinearLayout totalLL;
    String compId, userId, userName, compName;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String date, time;
    public final static String PREFS_NAME = "my_prefs";
    Button btn_send_wa_all_print, btn_send_mail_all_print, btn_save_all_print, send, sendWhatsapp, btnSendMultiple;
    String EMAIL = "email";
    EditText mobileNumber;


    File finalPdfFile;
    File finalPdfFile1;
    private List<MultipleWhatsappRequest> whatsappResponseList = new ArrayList<MultipleWhatsappRequest>();
    private List<ReceiversItem> receiversItems = new ArrayList<ReceiversItem>();
    private List<CustomParamsItem> customParamsItemList = new ArrayList<CustomParamsItem>();
    Uri uri;

    ProgressDialog pd;
    String encoded;
    WebView webView;

    int retry = 4;
    public static final String PARAM_PDF_PATH = "param_pdf_path";
    public static final String PARAM_TITLE = "param_title";
    String fileName;
    public static final int MY_DEFAULT_TIMEOUT = 15000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chit_level_report);

        select_chitCode = findViewById(R.id.select_chitCode);
        nodataTxt = findViewById(R.id.noData);
        totalLL = findViewById(R.id.totalLL);
        btn_send_wa_all_print = findViewById(R.id.btn_send_wa_all_print);
        btn_send_mail_all_print = findViewById(R.id.btn_send_mail_all_print);
        btn_save_all_print = findViewById(R.id.btn_save_all_print);
        send = findViewById(R.id.send);
        sendWhatsapp = findViewById(R.id.sendWhatsapp);
        btnSendMultiple = findViewById(R.id.btnSendMultiple);
        mobileNumber = findViewById(R.id.mobileNumber);


        getSupportActionBar().setTitle("Chit Level Reports");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();


        compId = String.valueOf(pref.getInt("compId", 0));
        userId = String.valueOf(pref.getInt("userId", 0));
        userName = String.valueOf(pref.getString("userName", null));
        compName = String.valueOf(pref.getString("compName", null));

        getChitid();

        btn_save_all_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkCheckPermission()) {
                    // savePetrolData();
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());
                    createPDF();

                }
            }
        });

        btn_send_wa_all_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finalPdfFile == null) {
                    Toast.makeText(ChitLevelReportActivity.this, "PDF File Not Found Click On Create Pdf", Toast.LENGTH_SHORT).show();
                    return;
                }

                uri = FileProvider.getUriForFile(ChitLevelReportActivity.this, getApplication().getPackageName() + ".provider", finalPdfFile);
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("application/pdf");
                share.putExtra(Intent.EXTRA_STREAM, uri);
                share.setPackage("com.whatsapp");
                share.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(share);
            }
        });
        btn_send_mail_all_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finalPdfFile1 == null) {
                    Toast.makeText(ChitLevelReportActivity.this, "PDF File Not Found Click On Create Pdf", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setType("application/pdf");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
                        new String[]{getStr(EMAIL)});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Report Generated from Chit App");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
                emailIntent.putExtra(Intent.EXTRA_STREAM,
                        // Uri.fromFile(file));
                        FileProvider.getUriForFile(ChitLevelReportActivity.this, getPackageName() + ".provider", finalPdfFile1));
                emailIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            }
        });


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String numberTxt = mobileNumber.getText().toString();

                addContact(numberTxt);
            }
        });

        sendWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String numberTxt = mobileNumber.getText().toString();


                sendMsgToWhatsapp(numberTxt);
            }
        });

        btnSendMultiple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String numberTxt = mobileNumber.getText().toString();

                if (finalPdfFile == null) {
                    Toast.makeText(ChitLevelReportActivity.this, "PDF File Not Found Click On Create Pdf", Toast.LENGTH_SHORT).show();
                    return;
                } else {


                    encoded = encodeFileToBase64Binary(finalPdfFile);
                }

                uri = FileProvider.getUriForFile(ChitLevelReportActivity.this, getApplication().getPackageName() + ".provider", finalPdfFile);


                sendMsgToMultipleWhatsapp(numberTxt, String.valueOf(encoded));
            }
        });


    }


    class ChitSpinnerClass implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
            // Toast.makeText(v.getContext(), "Your choose :" + chit[position], Toast.LENGTH_SHORT).show();
            chitIdStr = select_chitCode.getSelectedItemPosition();
            chitCodeStr = select_chitCode.getItemAtPosition(position).toString();
            chitCodePos = select_chitCode.getSelectedItemPosition();
            ChitModelItem chit = (ChitModelItem) parent.getSelectedItem();
            chitIdStrUp = chit.getMemberid();
            getChitReportsDetails(chitIdStrUp);


        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private void getChitid() {
        RequestQueue queue = Volley.newRequestQueue(ChitLevelReportActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_CHIT_COMPLETE_DETAILS + "/" + compId + "/" + userId + "/" + "0", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // chitDetailsList = new ArrayList<ChitCompleDetailsModelItem>();
                ArrayList<ChitModelItem> chitList = new ArrayList<>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    String json1 = rest.replace("[", "");
                    String json2 = json1.replace("]", "");
                    jsonStr = rest.replace("\\", "");
                    JSONArray array = new JSONArray(jsonStr);
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject object = (JSONObject) array.get(i);

                        String chitname = object.getString("chitcode");
                        Integer chitid = object.getInt("chitid");
                        chitList.add(new ChitModelItem(chitid, chitname));


                    }


                    chitaa = new ArrayAdapter(ChitLevelReportActivity.this, android.R.layout.simple_spinner_item, chitList);
                    chitaa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    //Setting the ArrayAdapter data on the Spinner
                    select_chitCode.setAdapter(chitaa);
                    select_chitCode.setOnItemSelectedListener(new ChitLevelReportActivity.ChitSpinnerClass());


                } catch (Exception e) {
                    Toast.makeText(ChitLevelReportActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();

                }


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ChitLevelReportActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }

    private void getChitReportsDetails(Integer chitId) {
        RequestQueue queue = Volley.newRequestQueue(ChitLevelReportActivity.this);
        //api/Chits/AuctionDetails/{compid}/{userid}/{chitid}/{Auctionid}
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_CHIT_LEVEL_REPORTS + "/" + compId + "/" + userId + "/" + chitId, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                chitLevelReportsModelItems = new ArrayList<ChitLevelReportsModelItem>();


                try {
                    json = response;
                    rest = json.substring(1, json.length() - 1);
                    if (rest.equals("")) {
                        nodataTxt.setVisibility(View.VISIBLE);
                    } else {

                        jsonStr = rest.replace("\\", "");
                        JSONArray array = new JSONArray(jsonStr);
                        for (int i = 0; i < array.length(); i++) {
                            ChitLevelReportsModelItem auctionDefine = new ChitLevelReportsModelItem();
                            JSONObject object = (JSONObject) array.get(i);
                            auctionDefine.setReceivables(object.getString("Receivables"));
                            auctionDefine.setReceived(object.getString("Received"));
                            auctionDefine.setRecPending(object.getString("RecPending"));
                            auctionDefine.setPaid(object.getString("Paid"));
                            auctionDefine.setPayable(object.getString("Payable"));
                            auctionDefine.setPayPending(object.getString("PayPending"));
                            auctionDefine.setName(object.getString("Name"));
                            auctionDefine.setTicketNo(object.getString("TicketNo"));

                            chitLevelReportsModelItems.add(auctionDefine);
                        }
                        nodataTxt.setVisibility(View.GONE);

                    }


                } catch (Exception e) {


                    Toast.makeText(ChitLevelReportActivity.this, "error is" + e.toString(), Toast.LENGTH_SHORT).show();

                }
                if (chitLevelReportsModelItems.isEmpty()) {
                    nodataTxt.setVisibility(View.VISIBLE);
                }


                recyclerView = findViewById(R.id.chitLevelRecyclerView);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ChitLevelReportActivity.this);
                recyclerView.setLayoutManager(linearLayoutManager);

                chitLevelReportAdapter = new ChitLevelReportAdapter();
                chitLevelReportAdapter.setListData(ChitLevelReportActivity.this, chitLevelReportsModelItems);

                recyclerView.setAdapter(chitLevelReportAdapter);
                chitLevelReportAdapter.notifyDataSetChanged();


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ChitLevelReportActivity.this, "Something Went wrong", Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer_menu, menu);
        return true;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logOut) {
            Intent intent1 = new Intent(ChitLevelReportActivity.this, LoginActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);// clear back stack
            startActivity(intent1);
            finish();


        }
        return super.onOptionsItemSelected(item);


    }


    public String getStr(String key) {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
        return prefs.getString(key, "");
    }

    private boolean checkCheckPermission() {
        if (ContextCompat.checkSelfPermission(
                ChitLevelReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                ChitLevelReportActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            // You can use the API that requires the permission.
            return true;
        } else {
            // You can directly ask for the permission.
            // The registered ActivityResultCallback gets the result of this request.
            ActivityCompat.requestPermissions(ChitLevelReportActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1011);
            return false;
        }
    }

    private void createPDF() {
        date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        time = new SimpleDateFormat("HH-mm-ss", Locale.getDefault()).format(new Date());

        View u = findViewById(R.id.vertical_scroll_view);

        ScrollView z = findViewById(R.id.vertical_scroll_view);
        int totalHeight = z.getChildAt(0).getHeight();
        int totalWidth = z.getChildAt(0).getWidth();

        Bitmap bitmap = takeScreenShot(u, totalHeight, totalWidth);
/*
        Bitmap bitmap = takeScreenShot();
*/
        Bitmap[] bitmaps = new Bitmap[]{bitmap};

        fileName = "ChitLevelReport_" + date + "-" + time + ".pdf";

        PDFEngine.getInstance().createPDF(ChitLevelReportActivity.this,
                bitmaps,
                fileName,
                (pdfFile, numOfImages) -> {
                    if (pdfFile == null) {
                        Toast.makeText(ChitLevelReportActivity.this, "Error in PDF Generation Error.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Toast.makeText(ChitLevelReportActivity.this, "PDF File generated", Toast.LENGTH_SHORT).show();
                    finalPdfFile = pdfFile;
                    PDFEngine.getInstance().openPDF(ChitLevelReportActivity.this, finalPdfFile);


                    if (finalPdfFile == null) {
                        Toast.makeText(ChitLevelReportActivity.this, "PDF File Not Found", Toast.LENGTH_SHORT).show();
                        return;
                    }


                    btn_send_wa_all_print.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (finalPdfFile == null) {
                                Toast.makeText(ChitLevelReportActivity.this, "PDF File Not Found", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            uri = FileProvider.getUriForFile(ChitLevelReportActivity.this, getApplication().getPackageName() + ".provider", finalPdfFile);
                            Intent share = new Intent(Intent.ACTION_SEND);
                            share.setType("application/pdf");
                            share.putExtra(Intent.EXTRA_STREAM, uri);
                            share.setPackage("com.whatsapp");
                            share.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(share);
                        }
                    });
                    finalPdfFile1 = pdfFile;
                    btn_send_mail_all_print.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (finalPdfFile1 == null) {
                                Toast.makeText(ChitLevelReportActivity.this, "PDF File Not Found", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                            emailIntent.setType("application/pdf");
                            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
                                    new String[]{getStr(EMAIL)});
                            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Report Generated from Chit App");
                            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
                            emailIntent.putExtra(Intent.EXTRA_STREAM,
                                    // Uri.fromFile(file));
                                    FileProvider.getUriForFile(ChitLevelReportActivity.this, getPackageName() + ".provider", finalPdfFile1));
                            emailIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                        }
                    });


                    pdfFile = pdfFile;
                    // PDFEngine.getInstance().sharePDF(MainActivity.this, pdfFile);
                });
    }

    protected Bitmap takeScreenShot(View view, int totalHeight, int totalWidth) {

        Bitmap returnedBitmap = Bitmap.createBitmap(totalWidth, totalHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return returnedBitmap;


    }


    private void addContact(String number) {

        RequestQueue queue = Volley.newRequestQueue(ChitLevelReportActivity.this);
        StringRequest request = new StringRequest(Request.Method.GET, Urls.URL_WHATSAPP_CONTACT_ADD + "/" + number + "/" + "Rajitha", new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Toast.makeText(ChitLevelReportActivity.this, response.toString(), Toast.LENGTH_SHORT).show();


            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ChitLevelReportActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {

        };
        // below line is to make
        // a json object request.
        queue.add(request);

    }


    private void sendMsgToWhatsapp(String number) {

        RequestQueue queue = Volley.newRequestQueue(ChitLevelReportActivity.this);
        StringRequest request = new StringRequest(Request.Method.GET, Urls.URL_SEND_WHATSAP + "/" + number + "/" + "Rajitha" + "/" + "Hello", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(ChitLevelReportActivity.this, response.toString(), Toast.LENGTH_SHORT).show();

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ChitLevelReportActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {


        };
        // below line is to make
        // a json object request.
        queue.add(request);

    }

    private void sendMsgToMultipleWhatsapp(String number, String binaryString) {


        RequestQueue queue = Volley.newRequestQueue(ChitLevelReportActivity.this);
        StringRequest request = new StringRequest(Request.Method.POST, Urls.URL_SEND_WHATSAP_FILE, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Toast.makeText(ChitLevelReportActivity.this, response.toString(), Toast.LENGTH_SHORT).show();

            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // method to handle errors.
                Toast.makeText(ChitLevelReportActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {


            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("WANo", "918142323365");
                params.put("msg", "hello");
                params.put("fileName", String.valueOf(fileName));
                params.put("filebinary", String.valueOf(binaryString));


                return params;
            }


        };
        // below line is to make
        // a json object request.
        request.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }


    private String encodeFileToBase64Binary(File yourFile) {
        int size = (int) yourFile.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(yourFile));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        String encoded = Base64.encodeToString(bytes, Base64.NO_WRAP);
        appendLog(encoded);
        return encoded;
    }

    public static void appendLog(String text) {
        File logFile = new File(android.os.Environment.getExternalStorageDirectory(), "log.txt");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {

                e.printStackTrace();
            }
        }
        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void uploadDocs(String base64String) {
        if (!Consts.checkNetworkAndShowError(this)) {
            return;
        }
        if (!Consts.checkNetworkAndShowError(this)) {
            return;
        }

        startProgress("Uploading Doc File...");


    }


    private void startProgress(String message) {
        try {
            if (pd != null && pd.isShowing()) {
                if (message.isEmpty())
                    pd.setMessage("Loading...");
                return;
            }
            pd = new ProgressDialog(this);
            pd.setCancelable(true);
            if (message.isEmpty())
                pd.setMessage("Loading...");
            else pd.setMessage(message);
            pd.setCancelable(false);
            pd.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}