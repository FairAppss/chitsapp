package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SingleChitModel{

	@SerializedName("SingleChitModel")
	private List<SingleChitModelItem> singleChitModel;

	public void setSingleChitModel(List<SingleChitModelItem> singleChitModel){
		this.singleChitModel = singleChitModel;
	}

	public List<SingleChitModelItem> getSingleChitModel(){
		return singleChitModel;
	}
}