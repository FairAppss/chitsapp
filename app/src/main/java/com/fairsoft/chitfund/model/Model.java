package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Model{

	@SerializedName("ids")
	private List<String> ids;

	public List<String> getIds(){
		return ids;
	}
}