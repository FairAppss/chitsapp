package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AuctionCompleteDetailsModel{

	@SerializedName("AuctionCompleteDetailsModel")
	private List<AuctionCompleteDetailsModelItem> auctionCompleteDetailsModel;

	public void setAuctionCompleteDetailsModel(List<AuctionCompleteDetailsModelItem> auctionCompleteDetailsModel){
		this.auctionCompleteDetailsModel = auctionCompleteDetailsModel;
	}

	public List<AuctionCompleteDetailsModelItem> getAuctionCompleteDetailsModel(){
		return auctionCompleteDetailsModel;
	}
}