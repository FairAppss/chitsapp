package com.fairsoft.chitfund.model;

import java.util.List;

public class MemberIdModel{
	private List<MemberIdModelItem> memberIdModel;

	public void setMemberIdModel(List<MemberIdModelItem> memberIdModel){
		this.memberIdModel = memberIdModel;
	}

	public List<MemberIdModelItem> getMemberIdModel(){
		return memberIdModel;
	}
}