package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SingleChitResponseModel{

	@SerializedName("SingleChitResponseModel")
	private List<SingleChitResponseModelItem> singleChitResponseModel;

	public void setSingleChitResponseModel(List<SingleChitResponseModelItem> singleChitResponseModel){
		this.singleChitResponseModel = singleChitResponseModel;
	}

	public List<SingleChitResponseModelItem> getSingleChitResponseModel(){
		return singleChitResponseModel;
	}
}