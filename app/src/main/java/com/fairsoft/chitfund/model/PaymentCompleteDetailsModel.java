package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class PaymentCompleteDetailsModel{

	@SerializedName("PaymentCompleteDetailsModel")
	private List<PaymentCompleteDetailsModelItem> paymentCompleteDetailsModel;

	public void setPaymentCompleteDetailsModel(List<PaymentCompleteDetailsModelItem> paymentCompleteDetailsModel){
		this.paymentCompleteDetailsModel = paymentCompleteDetailsModel;
	}

	public List<PaymentCompleteDetailsModelItem> getPaymentCompleteDetailsModel(){
		return paymentCompleteDetailsModel;
	}
}