package com.fairsoft.chitfund.model;

import com.google.gson.annotations.SerializedName;

public class PrizedModelItem {

    @SerializedName("chitid")
    private int chitid;

    @SerializedName("nomonths")
    private int nomonths;

    @SerializedName("chitdate")
    private String chitdate;

    @SerializedName("commn")
    private double commn;

    @SerializedName("isownchit")
    private String isownchit;

}
