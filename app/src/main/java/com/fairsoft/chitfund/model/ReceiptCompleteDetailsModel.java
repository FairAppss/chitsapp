package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ReceiptCompleteDetailsModel{

	@SerializedName("ReceiptCompleteDetailsModel")
	private List<ReceiptCompleteDetailsModelItem> receiptCompleteDetailsModel;

	public void setReceiptCompleteDetailsModel(List<ReceiptCompleteDetailsModelItem> receiptCompleteDetailsModel){
		this.receiptCompleteDetailsModel = receiptCompleteDetailsModel;
	}

	public List<ReceiptCompleteDetailsModelItem> getReceiptCompleteDetailsModel(){
		return receiptCompleteDetailsModel;
	}
}