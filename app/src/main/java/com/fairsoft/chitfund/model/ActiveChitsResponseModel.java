package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ActiveChitsResponseModel{

	@SerializedName("ActiveChitsResponseModel")
	private List<ActiveChitsResponseModelItem> activeChitsResponseModel;

	public List<ActiveChitsResponseModelItem> getActiveChitsResponseModel(){
		return activeChitsResponseModel;
	}
}