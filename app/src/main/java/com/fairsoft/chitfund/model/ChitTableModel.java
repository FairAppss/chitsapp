package com.fairsoft.chitfund.model;

public class ChitTableModel {
    public String chitdate;
    public int chitid;
    public String chitcode;
    public double chitamt;
    public int nomonths;
    public int noofmembers;
    public String frequency;
    public double commn;
    public String dividend;
    public String auctionday;
    public String bidamount;
    public String completed;
    public String receivable;
    public String payable;
    public String lastbid;

    public String getChitdate() {
        return chitdate;
    }

    public void setChitdate(String chitdate) {
        this.chitdate = chitdate;
    }

    public int getChitid() {
        return chitid;
    }

    public void setChitid(int chitid) {
        this.chitid = chitid;
    }

    public String getChitcode() {
        return chitcode;
    }

    public void setChitcode(String chitcode) {
        this.chitcode = chitcode;
    }

    public double getChitamt() {
        return chitamt;
    }

    public void setChitamt(double chitamt) {
        this.chitamt = chitamt;
    }

    public int getNomonths() {
        return nomonths;
    }

    public void setNomonths(int nomonths) {
        this.nomonths = nomonths;
    }

    public int getNoofmembers() {
        return noofmembers;
    }

    public void setNoofmembers(int noofmembers) {
        this.noofmembers = noofmembers;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public double getCommn() {
        return commn;
    }

    public void setCommn(double commn) {
        this.commn = commn;
    }

    public String getIsownchit() {
        return isownchit;
    }

    public void setIsownchit(String isownchit) {
        this.isownchit = isownchit;
    }

    public String getDividend() {
        return dividend;
    }

    public void setDividend(String dividend) {
        this.dividend = dividend;
    }

    public String getAuctionday() {
        return auctionday;
    }

    public void setAuctionday(String auctionday) {
        this.auctionday = auctionday;
    }

    public String getBidamount() {
        return bidamount;
    }

    public void setBidamount(String bidamount) {
        this.bidamount = bidamount;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public String getReceivable() {
        return receivable;
    }

    public void setReceivable(String receivable) {
        this.receivable = receivable;
    }

    public String getPayable() {
        return payable;
    }

    public void setPayable(String payable) {
        this.payable = payable;
    }

    public String getLastbid() {
        return lastbid;
    }

    public void setLastbid(String lastbid) {
        this.lastbid = lastbid;
    }

    public String isownchit;

    public ChitTableModel() {

    }

    public ChitTableModel(String chitdate, int chitid, String chitcode, double chitamt, int nomonths, int noofmembers, String frequency, double commn, String isownchit, String dividend, String auctionday, String bidamount, String completed, String receivable, String payable, String lastbid) {
        this.chitdate = chitdate;
        this.chitid = chitid;
        this.chitcode = chitcode;
        this.chitamt = chitamt;
        this.nomonths = nomonths;
        this.noofmembers = noofmembers;
        this.frequency = frequency;
        this.commn = commn;
        this.isownchit = isownchit;
        this.dividend = dividend;
        this.auctionday = auctionday;
        this.bidamount = bidamount;
        this.completed = completed;
        this.receivable = receivable;
        this.payable = payable;
        this.lastbid = lastbid;
    }


}