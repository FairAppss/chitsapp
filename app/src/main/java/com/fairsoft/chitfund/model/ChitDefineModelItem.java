package com.fairsoft.chitfund.model;

import com.google.gson.annotations.SerializedName;

public class ChitDefineModelItem {

    @SerializedName("bidamount")
    private String bidamount;

    @SerializedName("chitdate")
    private String chitdate;

    @SerializedName("commn")
    private Double commn;

    @SerializedName("chitamt")
    private Double chitamt;

    @SerializedName("chitcode")
    private String chitcode;

    @SerializedName("auctionday")
    private String auctionday;

    @SerializedName("completed")
    private String completed;

    @SerializedName("receivable")
    private String receivable;

    @SerializedName("noofmembers")
    private Integer noofmembers;

    @SerializedName("frequency")
    private String frequency;

    @SerializedName("chitid")
    private Integer chitid;

    @SerializedName("nomonths")
    private Integer nomonths;

    @SerializedName("payable")
    private String payable;

    @SerializedName("isownchit")
    private String isownchit;

    @SerializedName("lastbid")
    private String lastbid;

    @SerializedName("dividend")
    private String dividend;

    public String getBidamount() {
        return bidamount;
    }

    public String getChitdate() {
        return chitdate;
    }

    public Double getCommn() {
        return commn;
    }

    public Double getChitamt() {
        return chitamt;
    }

    public String getChitcode() {
        return chitcode;
    }

    public String getAuctionday() {
        return auctionday;
    }

    public String getCompleted() {
        return completed;
    }

    public String getReceivable() {
        return receivable;
    }

    public void setBidamount(String bidamount) {
        this.bidamount = bidamount;
    }

    public void setChitdate(String chitdate) {
        this.chitdate = chitdate;
    }

    public void setCommn(Double commn) {
        this.commn = commn;
    }

    public void setChitamt(Double chitamt) {
        this.chitamt = chitamt;
    }

    public void setChitcode(String chitcode) {
        this.chitcode = chitcode;
    }

    public void setAuctionday(String auctionday) {
        this.auctionday = auctionday;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public void setReceivable(String receivable) {
        this.receivable = receivable;
    }

    public void setNoofmembers(Integer noofmembers) {
        this.noofmembers = noofmembers;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public void setChitid(Integer chitid) {
        this.chitid = chitid;
    }

    public void setNomonths(Integer nomonths) {
        this.nomonths = nomonths;
    }

    public void setPayable(String payable) {
        this.payable = payable;
    }

    public void setIsownchit(String isownchit) {
        this.isownchit = isownchit;
    }

    public void setLastbid(String lastbid) {
        this.lastbid = lastbid;
    }

    public void setDividend(String dividend) {
        this.dividend = dividend;
    }

    public Integer getNoofmembers() {
        return noofmembers;
    }

    public String getFrequency() {
        return frequency;
    }

    public Integer getChitid() {
        return chitid;
    }

    public Integer getNomonths() {
        return nomonths;
    }

    public String getPayable() {
        return payable;
    }

    public String getIsownchit() {
        return isownchit;
    }

    public String getLastbid() {
        return lastbid;
    }

    public String getDividend() {
        return dividend;
    }
}