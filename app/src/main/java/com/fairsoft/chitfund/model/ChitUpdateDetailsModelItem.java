package com.fairsoft.chitfund.model;

import com.google.gson.annotations.SerializedName;

public class ChitUpdateDetailsModelItem{

	@SerializedName("chitid")
	private int chitid;

	@SerializedName("nomonths")
	private int nomonths;

	@SerializedName("chitdate")
	private String chitdate;

	@SerializedName("commn")
	private double commn;

	@SerializedName("isownchit")
	private String isownchit;

	@SerializedName("chitamt")
	private double chitamt;

	@SerializedName("chitcode")
	private String chitcode;

	@SerializedName("auctionday")
	private String auctionday;

	@SerializedName("dividend")
	private String dividend;

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	@SerializedName("noofmembers")
	private int noofmembers;

	@SerializedName("frequency")
	private String frequency;

	@SerializedName("commnPerAmount")
	private String commnPerAmount;

	@SerializedName("startdate")
	private String startdate;
	@SerializedName("chittype")
	private String chittype;

	public String getChittype() {
		return chittype;
	}

	public void setChittype(String chittype) {
		this.chittype = chittype;
	}

	public String getOwnchitmonth() {
		return ownchitmonth;
	}

	public void setOwnchitmonth(String ownchitmonth) {
		this.ownchitmonth = ownchitmonth;
	}

	public String getPaymentafter() {
		return paymentafter;
	}

	public void setPaymentafter(String paymentafter) {
		this.paymentafter = paymentafter;
	}

	public String getReceiptafter() {
		return receiptafter;
	}

	public void setReceiptafter(String receiptafter) {
		this.receiptafter = receiptafter;
	}

	public String getAuctweek() {
		return auctweek;
	}

	public void setAuctweek(String auctweek) {
		this.auctweek = auctweek;
	}

	public String getSamenext() {
		return samenext;
	}

	public void setSamenext(String samenext) {
		this.samenext = samenext;
	}

	public String getDateday() {
		return dateday;
	}

	public void setDateday(String dateday) {
		this.dateday = dateday;
	}

	@SerializedName("ownchitmonth")
	private String ownchitmonth;

	@SerializedName("paymentafter")
	private String paymentafter;

	public String getAuctiontime() {
		return auctiontime;
	}

	public void setAuctiontime(String auctiontime) {
		this.auctiontime = auctiontime;
	}

	@SerializedName("receiptafter")
	private String receiptafter;

	@SerializedName("auctweek")
	private String auctweek;

	@SerializedName("samenext")
	private String samenext;

	@SerializedName("dateday")
	private String dateday;

	@SerializedName("auctiontime")
	private String auctiontime;









	public void setChitid(int chitid){
		this.chitid = chitid;
	}

	public int getChitid(){
		return chitid;
	}

	public void setNomonths(int nomonths){
		this.nomonths = nomonths;
	}

	public int getNomonths(){
		return nomonths;
	}

	public void setChitdate(String chitdate){
		this.chitdate = chitdate;
	}

	public String getChitdate(){
		return chitdate;
	}

	public void setCommn(double commn){
		this.commn = commn;
	}

	public double getCommn(){
		return commn;
	}

	public void setIsownchit(String isownchit){
		this.isownchit = isownchit;
	}

	public String getIsownchit(){
		return isownchit;
	}

	public void setChitamt(double chitamt){
		this.chitamt = chitamt;
	}

	public double getChitamt(){
		return chitamt;
	}

	public void setChitcode(String chitcode){
		this.chitcode = chitcode;
	}

	public String getChitcode(){
		return chitcode;
	}

	public void setAuctionday(String auctionday){
		this.auctionday = auctionday;
	}

	public String getAuctionday(){
		return auctionday;
	}

	public void setDividend(String dividend){
		this.dividend = dividend;
	}

	public String getDividend(){
		return dividend;
	}

	public void setNoofmembers(int noofmembers){
		this.noofmembers = noofmembers;
	}

	public int getNoofmembers(){
		return noofmembers;
	}

	public void setFrequency(String frequency){
		this.frequency = frequency;
	}

	public String getFrequency(){
		return frequency;
	}

	public String getCommnPerAmount() {
		return commnPerAmount;
	}

	public void setCommnPerAmount(String commnPerAmount) {
		this.commnPerAmount = commnPerAmount;
	}
}