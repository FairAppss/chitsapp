package com.fairsoft.chitfund.model;

import com.google.gson.annotations.SerializedName;

public class SubscriberDetailsModelItem{

	@SerializedName("reference")
	private String reference;

	@SerializedName("chitid")
	private int chitid;

	@SerializedName("createdby")
	private int createdby;

	@SerializedName("createddate")
	private String createddate;

	@SerializedName("city")
	private String city;

	@SerializedName("chitcode")
	private String chitcode;

	@SerializedName("name")
	private String name;

	@SerializedName("member")
	private String member;

	@SerializedName("subscriberid")
	private int subscriberid;

	@SerializedName("memberid")
	private int memberid;

	public String getTicketno() {
		return ticketno;
	}

	public void setTicketno(String ticketno) {
		this.ticketno = ticketno;
	}

	@SerializedName("status")
	private String status;

	@SerializedName("ticketno")
	private String ticketno;



	public String getReference(){
		return reference;
	}

	public int getChitid(){
		return chitid;
	}

	public int getCreatedby(){
		return createdby;
	}

	public String getCreateddate(){
		return createddate;
	}

	public String getCity(){
		return city;
	}

	public String getChitcode(){
		return chitcode;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public void setChitid(int chitid) {
		this.chitid = chitid;
	}

	public void setCreatedby(int createdby) {
		this.createdby = createdby;
	}

	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setChitcode(String chitcode) {
		this.chitcode = chitcode;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMember(String member) {
		this.member = member;
	}

	public void setSubscriberid(int subscriberid) {
		this.subscriberid = subscriberid;
	}

	public void setMemberid(int memberid) {
		this.memberid = memberid;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName(){
		return name;
	}

	public String getMember(){
		return member;
	}

	public int getSubscriberid(){
		return subscriberid;
	}

	public int getMemberid(){
		return memberid;
	}

	public String getStatus(){
		return status;
	}
}