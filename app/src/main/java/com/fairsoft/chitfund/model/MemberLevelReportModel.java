package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class MemberLevelReportModel{

	@SerializedName("MemberLevelReportModel")
	private List<MemberLevelReportModelItem> memberLevelReportModel;

	public void setMemberLevelReportModel(List<MemberLevelReportModelItem> memberLevelReportModel){
		this.memberLevelReportModel = memberLevelReportModel;
	}

	public List<MemberLevelReportModelItem> getMemberLevelReportModel(){
		return memberLevelReportModel;
	}
}