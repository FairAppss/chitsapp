package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ChitLevelReportsModel{

	@SerializedName("ChitLevelReportsModel")
	private List<ChitLevelReportsModelItem> chitLevelReportsModel;

	public void setChitLevelReportsModel(List<ChitLevelReportsModelItem> chitLevelReportsModel){
		this.chitLevelReportsModel = chitLevelReportsModel;
	}

	public List<ChitLevelReportsModelItem> getChitLevelReportsModel(){
		return chitLevelReportsModel;
	}
}