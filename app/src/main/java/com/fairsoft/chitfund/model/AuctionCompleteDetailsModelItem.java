package com.fairsoft.chitfund.model;

import com.google.gson.annotations.SerializedName;

public class AuctionCompleteDetailsModelItem{

	public int getSubscriberid() {
		return subscriberid;
	}

	public void setSubscriberid(int subscriberid) {
		this.subscriberid = subscriberid;
	}

	@SerializedName("auctionid")
	private int auctionid;

	@SerializedName("chitid")
	private int chitid;
	@SerializedName("subscriberid")
	private int subscriberid;


	@SerializedName("auctionamount")
	private double auctionamount;

	@SerializedName("auctiontime")
	private String auctiontime;

	public String getTicketno() {
		return ticketno;
	}

	public void setTicketno(String ticketno) {
		this.ticketno = ticketno;
	}

	@SerializedName("chitcode")
	private String chitcode;

	@SerializedName("ticketno")
	private String ticketno;


	@SerializedName("auctionday")
	private String auctionday;

	@SerializedName("name")
	private String name;

	public String getSubscribertick() {
		return subscribertick;
	}

	public void setSubscribertick(String subscribertick) {
		this.subscribertick = subscribertick;
	}

	@SerializedName("prizedmemberid")
	private int prizedmemberid;

	@SerializedName("subscribertick")
	private String subscribertick;

	public void setAuctionid(int auctionid){
		this.auctionid = auctionid;
	}

	public int getAuctionid(){
		return auctionid;
	}

	public void setChitid(int chitid){
		this.chitid = chitid;
	}

	public int getChitid(){
		return chitid;
	}

	public void setAuctionamount(double auctionamount){
		this.auctionamount = auctionamount;
	}

	public double getAuctionamount(){
		return auctionamount;
	}

	public void setAuctiontime(String auctiontime){
		this.auctiontime = auctiontime;
	}

	public String getAuctiontime(){
		return auctiontime;
	}

	public void setChitcode(String chitcode){
		this.chitcode = chitcode;
	}

	public String getChitcode(){
		return chitcode;
	}

	public void setAuctionday(String auctionday){
		this.auctionday = auctionday;
	}

	public String getAuctionday(){
		return auctionday;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setPrizedmemberid(int prizedmemberid){
		this.prizedmemberid = prizedmemberid;
	}

	public int getPrizedmemberid(){
		return prizedmemberid;
	}
}