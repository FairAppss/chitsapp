package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ReceiversItem{

	@SerializedName("whatsappNumber")
	private String whatsappNumber;

	@SerializedName("customParams")
	private List<CustomParamsItem> customParams;

	public void setWhatsappNumber(String whatsappNumber){
		this.whatsappNumber = whatsappNumber;
	}

	public String getWhatsappNumber(){
		return whatsappNumber;
	}

	public void setCustomParams(List<CustomParamsItem> customParams){
		this.customParams = customParams;
	}

	public List<CustomParamsItem> getCustomParams(){
		return customParams;
	}
}