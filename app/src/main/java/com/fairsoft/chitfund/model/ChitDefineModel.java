package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ChitDefineModel{

	@SerializedName("ChitDefineModel")
	private List<ChitDefineModelItem> chitDefineModel;

	public List<ChitDefineModelItem> getChitDefineModel(){
		return chitDefineModel;
	}
}