package com.fairsoft.chitfund.model;

import com.google.gson.annotations.SerializedName;

public class ReceiptsNPaymentsModelItem{

	@SerializedName("chitid")
	private int chitid;

	@SerializedName("amount")
	private double amount;

	@SerializedName("trDate")
	private String trDate;

	@SerializedName("createdby")
	private int createdby;

	@SerializedName("createddate")
	private String createddate;

	public int getReceiptid() {
		return receiptid;
	}

	public void setReceiptid(int receiptid) {
		this.receiptid = receiptid;
	}

	@SerializedName("paymentid")
	private int paymentid;
	@SerializedName("receiptid")
	private int receiptid;


	@SerializedName("chitcode")
	private String chitcode;

	@SerializedName("trtype")
	private Object trtype;

	public int getSubscriberid() {
		return subscriberid;
	}

	public void setSubscriberid(int subscriberid) {
		this.subscriberid = subscriberid;
	}

	@SerializedName("name")
	private String name;

	@SerializedName("memberid")
	private int memberid;

	@SerializedName("subscriberid")
	private int subscriberid;





	public void setChitid(int chitid){
		this.chitid = chitid;
	}

	public int getChitid(){
		return chitid;
	}

	public void setAmount(double amount){
		this.amount = amount;
	}

	public double getAmount(){
		return amount;
	}

	public void setTrDate(String trDate){
		this.trDate = trDate;
	}

	public String getTrDate(){
		return trDate;
	}

	public void setCreatedby(int createdby){
		this.createdby = createdby;
	}

	public int getCreatedby(){
		return createdby;
	}

	public void setCreateddate(String createddate){
		this.createddate = createddate;
	}

	public String getCreateddate(){
		return createddate;
	}

	public void setPaymentid(int paymentid){
		this.paymentid = paymentid;
	}

	public int getPaymentid(){
		return paymentid;
	}

	public void setChitcode(String chitcode){
		this.chitcode = chitcode;
	}

	public String getChitcode(){
		return chitcode;
	}

	public void setTrtype(Object trtype){
		this.trtype = trtype;
	}

	public Object getTrtype(){
		return trtype;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setMemberid(int memberid){
		this.memberid = memberid;
	}

	public int getMemberid(){
		return memberid;
	}
}