package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class MultipleWhatsappRequest{

	@SerializedName("template_name")
	private String templateName;

	@SerializedName("receivers")
	private List<ReceiversItem> receivers;

	@SerializedName("broadcast_name")
	private String broadcastName;

	public void setTemplateName(String templateName){
		this.templateName = templateName;
	}

	public String getTemplateName(){
		return templateName;
	}

	public void setReceivers(List<ReceiversItem> receivers){
		this.receivers = receivers;
	}

	public List<ReceiversItem> getReceivers(){
		return receivers;
	}

	public void setBroadcastName(String broadcastName){
		this.broadcastName = broadcastName;
	}

	public String getBroadcastName(){
		return broadcastName;
	}
}