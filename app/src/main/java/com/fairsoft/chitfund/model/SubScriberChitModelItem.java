package com.fairsoft.chitfund.model;

public class SubScriberChitModelItem {

    private String name;

    private int chitid;

    public int getChitid() {
        return chitid;
    }

    public void setChitid(int chitid) {
        this.chitid = chitid;
    }

    private int members;


    public int getMembers() {
        return members;
    }

    public void setMembers(int members) {
        this.members = members;
    }

    public SubScriberChitModelItem(Integer memberid, String name, Integer members) {
        this.chitid = memberid;
        this.name = name;
        this.members = members;

    }
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void setMemberid(int memberid) {
        this.chitid = memberid;
    }

    public int getMemberid() {
        return chitid;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof SubScriberChitModelItem){
            SubScriberChitModelItem c = (SubScriberChitModelItem)obj;
            if(c.getName().equals(name) && c.getMemberid()==chitid ) return true;
        }

        return false;
    }


}
