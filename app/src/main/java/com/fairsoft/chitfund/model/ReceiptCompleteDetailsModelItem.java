package com.fairsoft.chitfund.model;

import com.google.gson.annotations.SerializedName;

public class ReceiptCompleteDetailsModelItem {

    @SerializedName("chitid")
    private int chitid;

    @SerializedName("amount")
    private double amount;

    @SerializedName("rectype")
    private String rectype;

    @SerializedName("createdby")
    private int createdby;

    @SerializedName("createddate")
    private String createddate;

    @SerializedName("chitcode")
    private String chitcode;

    @SerializedName("name")
    private String name;

    @SerializedName("receiptdate")
    private String receiptdate;

    @SerializedName("receiptid")
    private int receiptid;

    @SerializedName("memberid")
    private int memberid;

    public int getSubscriberid() {
        return subscriberid;
    }

    public void setSubscriberid(int subscriberid) {
        this.subscriberid = subscriberid;
    }

    @SerializedName("subscriberid")
    private int subscriberid;

    @SerializedName("ticketno")
    private String ticketno;

    public String getTicketno() {
        return ticketno;
    }

    public void setTicketno(String ticketno) {
        this.ticketno = ticketno;
    }


    public void setChitid(int chitid) {
        this.chitid = chitid;
    }

    public int getChitid() {
        return chitid;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    public void setRectype(String rectype) {
        this.rectype = rectype;
    }

    public String getRectype() {
        return rectype;
    }

    public void setCreatedby(int createdby) {
        this.createdby = createdby;
    }

    public int getCreatedby() {
        return createdby;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setChitcode(String chitcode) {
        this.chitcode = chitcode;
    }

    public String getChitcode() {
        return chitcode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setReceiptdate(String receiptdate) {
        this.receiptdate = receiptdate;
    }

    public String getReceiptdate() {
        return receiptdate;
    }

    public void setReceiptid(int receiptid) {
        this.receiptid = receiptid;
    }

    public int getReceiptid() {
        return receiptid;
    }

    public void setMemberid(int memberid) {
        this.memberid = memberid;
    }

    public int getMemberid() {
        return memberid;
    }
}