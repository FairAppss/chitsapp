package com.fairsoft.chitfund.model;

public class AuctionChitModelItem {

    private String name;

    private int chitid;
    private String commnperamount;
    private int noofmembers;
    private String chittype;

    public String getChittype() {
        return chittype;
    }

    public void setChittype(String chittype) {
        this.chittype = chittype;
    }

    public String getCommnperamount() {
        return commnperamount;
    }

    public void setCommnperamount(String commnperamount) {
        this.commnperamount = commnperamount;
    }

    public int getNoofmembers() {
        return noofmembers;
    }

    public void setNoofmembers(int noofmembers) {
        this.noofmembers = noofmembers;
    }

    public int getChitid() {
        return chitid;
    }

    public void setChitid(int chitid) {
        this.chitid = chitid;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    private int amount;
    private double commn;


    public double getCommn() {
        return commn;
    }

    public void setCommn(double commn) {
        this.commn = commn;
    }

    public AuctionChitModelItem(Integer memberid, String name, Integer amount, String commnperamount, Integer noofmembers, double commn,String chittype) {
        this.chitid = memberid;
        this.name = name;
        this.amount = amount;
        this.commnperamount = commnperamount;
        this.noofmembers = noofmembers;
        this.commn = commn;
        this.chittype = chittype;




    }
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void setMemberid(int memberid) {
        this.chitid = memberid;
    }

    public int getMemberid() {
        return chitid;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof AuctionChitModelItem){
            AuctionChitModelItem c = (AuctionChitModelItem)obj;
            if(c.getName().equals(name) && c.getMemberid()==chitid && c.getAmount()==amount) return true;
        }

        return false;
    }


}
