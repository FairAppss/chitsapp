package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class WhatsappResponse{

	@SerializedName("customParams")
	private List<CustomParamsItem> customParams;

	@SerializedName("name")
	private String name;

	public void setCustomParams(List<CustomParamsItem> customParams){
		this.customParams = customParams;
	}

	public List<CustomParamsItem> getCustomParams(){
		return customParams;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}
}