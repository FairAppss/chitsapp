package com.fairsoft.chitfund.model;

public class SubscriberModelItem {

    private String name;
    private String subScriber;


    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    private String contactName;


    private int memberid;

    public String getSubScriber() {
        return subScriber;
    }

    public void setSubScriber(String subScriber) {
        this.subScriber = subScriber;
    }

    public SubscriberModelItem(Integer memberid, String name, String contactName, String subScriber) {
        this.memberid = memberid;
        this.name = name;
        this.contactName=contactName;
        this.subScriber=subScriber;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void setMemberid(int memberid) {
        this.memberid = memberid;
    }

    public int getMemberid() {
        return memberid;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof SubscriberModelItem){
            SubscriberModelItem c = (SubscriberModelItem)obj;
            if(c.getName().equals(name) && c.getMemberid()==memberid &&c.getContactName()==contactName ) return true;
        }

        return false;
    }


}
