package com.fairsoft.chitfund.model;

import com.google.gson.annotations.SerializedName;

public class ChitCompleDetailsModelItem{

	@SerializedName("bidamount")
	private String bidamount;

	@SerializedName("chitdate")
	private String chitdate;

	@SerializedName("commn")
	private double commn;

	@SerializedName("chitamt")
	private double chitamt;

	@SerializedName("chitcode")
	private String chitcode;

	@SerializedName("auctionday")
	private String auctionday;

	@SerializedName("enddt")
	private String enddt;

	@SerializedName("completed")
	private String completed;

	@SerializedName("receivable")
	private String receivable;

	@SerializedName("noofmembers")
	private int noofmembers;

	@SerializedName("frequency")
	private String frequency;

	@SerializedName("Comp")
	private String comp;

	@SerializedName("chitid")
	private int chitid;

	@SerializedName("nomonths")
	private int nomonths;

	@SerializedName("payable")
	private String payable;

	@SerializedName("isownchit")
	private String isownchit;

	@SerializedName("lastbid")
	private String lastbid;

	@SerializedName("dividend")
	private String dividend;

	public void setBidamount(String bidamount){
		this.bidamount = bidamount;
	}

	public String getBidamount(){
		return bidamount;
	}

	public void setChitdate(String chitdate){
		this.chitdate = chitdate;
	}

	public String getChitdate(){
		return chitdate;
	}

	public void setCommn(double commn){
		this.commn = commn;
	}

	public double getCommn(){
		return commn;
	}

	public void setChitamt(double chitamt){
		this.chitamt = chitamt;
	}

	public double getChitamt(){
		return chitamt;
	}

	public void setChitcode(String chitcode){
		this.chitcode = chitcode;
	}

	public String getChitcode(){
		return chitcode;
	}

	public void setAuctionday(String auctionday){
		this.auctionday = auctionday;
	}

	public String getAuctionday(){
		return auctionday;
	}

	public void setEnddt(String enddt){
		this.enddt = enddt;
	}

	public String getEnddt(){
		return enddt;
	}

	public void setCompleted(String completed){
		this.completed = completed;
	}

	public String getCompleted(){
		return completed;
	}

	public void setReceivable(String receivable){
		this.receivable = receivable;
	}

	public String getReceivable(){
		return receivable;
	}

	public void setNoofmembers(int noofmembers){
		this.noofmembers = noofmembers;
	}

	public int getNoofmembers(){
		return noofmembers;
	}

	public void setFrequency(String frequency){
		this.frequency = frequency;
	}

	public String getFrequency(){
		return frequency;
	}

	public void setComp(String comp){
		this.comp = comp;
	}

	public String getComp(){
		return comp;
	}

	public void setChitid(int chitid){
		this.chitid = chitid;
	}

	public int getChitid(){
		return chitid;
	}

	public void setNomonths(int nomonths){
		this.nomonths = nomonths;
	}

	public int getNomonths(){
		return nomonths;
	}

	public void setPayable(String payable){
		this.payable = payable;
	}

	public String getPayable(){
		return payable;
	}

	public void setIsownchit(String isownchit){
		this.isownchit = isownchit;
	}

	public String getIsownchit(){
		return isownchit;
	}

	public void setLastbid(String lastbid){
		this.lastbid = lastbid;
	}

	public String getLastbid(){
		return lastbid;
	}

	public void setDividend(String dividend){
		this.dividend = dividend;
	}

	public String getDividend(){
		return dividend;
	}
}