package com.fairsoft.chitfund.model;

public class ContactsModelItem {

    private String name;
    private String value;

    public ContactsModelItem() {

    }


    public ContactsModelItem(String name, String value) {
        this.name = name;
        this.value = value;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setMsg(String msg) {
        this.value = msg;
    }
}
