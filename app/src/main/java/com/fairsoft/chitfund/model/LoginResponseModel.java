package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class LoginResponseModel{

	@SerializedName("LoginResponseModel")
	private List<LoginResponseModelItem> loginResponseModel;

	public void setLoginResponseModel(List<LoginResponseModelItem> loginResponseModel){
		this.loginResponseModel = loginResponseModel;
	}

	public List<LoginResponseModelItem> getLoginResponseModel(){
		return loginResponseModel;
	}
}