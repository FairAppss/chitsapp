package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Contact{

	@SerializedName("allowSMS")
	private boolean allowSMS;

	@SerializedName("lastFlowId")
	private Object lastFlowId;

	@SerializedName("customParams")
	private List<CustomParamsItem> customParams;

	@SerializedName("created")
	private String created;

	@SerializedName("fullName")
	private String fullName;

	@SerializedName("photo")
	private Object photo;

	@SerializedName("source")
	private Object source;

	@SerializedName("teamIds")
	private List<String> teamIds;

	@SerializedName("tags")
	private List<Object> tags;

	@SerializedName("firstName")
	private String firstName;

	@SerializedName("lastUpdated")
	private String lastUpdated;

	@SerializedName("wAid")
	private String wAid;

	@SerializedName("optedIn")
	private boolean optedIn;

	@SerializedName("contactStatus")
	private String contactStatus;

	@SerializedName("isDeleted")
	private boolean isDeleted;

	@SerializedName("isInFlow")
	private boolean isInFlow;

	@SerializedName("phone")
	private String phone;

	@SerializedName("currentFlowNodeId")
	private Object currentFlowNodeId;

	@SerializedName("id")
	private String id;

	@SerializedName("allowBroadcast")
	private boolean allowBroadcast;

	public boolean isAllowSMS(){
		return allowSMS;
	}

	public Object getLastFlowId(){
		return lastFlowId;
	}

	public List<CustomParamsItem> getCustomParams(){
		return customParams;
	}

	public String getCreated(){
		return created;
	}

	public String getFullName(){
		return fullName;
	}

	public Object getPhoto(){
		return photo;
	}

	public Object getSource(){
		return source;
	}

	public List<String> getTeamIds(){
		return teamIds;
	}

	public List<Object> getTags(){
		return tags;
	}

	public String getFirstName(){
		return firstName;
	}

	public String getLastUpdated(){
		return lastUpdated;
	}

	public String getWAid(){
		return wAid;
	}

	public boolean isOptedIn(){
		return optedIn;
	}

	public String getContactStatus(){
		return contactStatus;
	}

	public boolean isIsDeleted(){
		return isDeleted;
	}

	public boolean isIsInFlow(){
		return isInFlow;
	}

	public String getPhone(){
		return phone;
	}

	public Object getCurrentFlowNodeId(){
		return currentFlowNodeId;
	}

	public String getId(){
		return id;
	}

	public boolean isAllowBroadcast(){
		return allowBroadcast;
	}
}