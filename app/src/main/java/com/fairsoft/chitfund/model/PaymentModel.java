package com.fairsoft.chitfund.model;

public class PaymentModel {
    String Dt;
    String Amt;
    String Mth;
    String Completed;

    public String getDt() {
        return Dt;
    }

    public void setDt(String dt) {
        Dt = dt;
    }

    public String getAmt() {
        return Amt;
    }

    public void setAmt(String amt) {
        Amt = amt;
    }

    public String getMth() {
        return Mth;
    }

    public void setMth(String mth) {
        Mth = mth;
    }

    public String getCompleted() {
        return Completed;
    }

    public void setCompleted(String completed) {
        Completed = completed;
    }

    public String getReceivable() {
        return Receivable;
    }

    public void setReceivable(String receivable) {
        Receivable = receivable;
    }

    public String getPayable() {
        return Payable;
    }

    public void setPayable(String payable) {
        Payable = payable;
    }

    public String getLastBid() {
        return lastBid;
    }

    public void setLastBid(String lastBid) {
        this.lastBid = lastBid;
    }

    String Receivable;
    String Payable;

    public PaymentModel(String dt) {
        Dt = dt;

    }

    String lastBid;
}
