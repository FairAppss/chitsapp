package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ChitCompleDetailsModel{

	@SerializedName("ChitCompleDetailsModel")
	private List<ChitCompleDetailsModelItem> chitCompleDetailsModel;

	public void setChitCompleDetailsModel(List<ChitCompleDetailsModelItem> chitCompleDetailsModel){
		this.chitCompleDetailsModel = chitCompleDetailsModel;
	}

	public List<ChitCompleDetailsModelItem> getChitCompleDetailsModel(){
		return chitCompleDetailsModel;
	}
}