package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ReceiptsNPaymentsModel{

	@SerializedName("ReceiptsNPaymentsModel")
	private List<ReceiptsNPaymentsModelItem> receiptsNPaymentsModel;

	public void setReceiptsNPaymentsModel(List<ReceiptsNPaymentsModelItem> receiptsNPaymentsModel){
		this.receiptsNPaymentsModel = receiptsNPaymentsModel;
	}

	public List<ReceiptsNPaymentsModelItem> getReceiptsNPaymentsModel(){
		return receiptsNPaymentsModel;
	}
}