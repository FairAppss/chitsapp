package com.fairsoft.chitfund.model;

public class StateModelItem {

    private String state;

    private int stateid;

    public StateModelItem(Integer stateid, String state) {
        this.stateid = stateid;
        this.state = state;
    }
    public void setName(String name) {
        this.state = name;
    }

    public String getName() {
        return state;
    }


    public void setMemberid(int memberid) {
        this.stateid = memberid;
    }

    public int getMemberid() {
        return stateid;
    }

    @Override
    public String toString() {
        return state;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof StateModelItem){
            StateModelItem c = (StateModelItem)obj;
            if(c.getName().equals(state) && c.getMemberid()==stateid ) return true;
        }

        return false;
    }


}
