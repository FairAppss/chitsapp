package com.fairsoft.chitfund.model;

import com.google.gson.annotations.SerializedName;

public class ParameteresItem{

	@SerializedName("name")
	private String name;

	@SerializedName("value")
	private String value;

	public String getName(){
		return name;
	}

	public String getValue(){
		return value;
	}
}