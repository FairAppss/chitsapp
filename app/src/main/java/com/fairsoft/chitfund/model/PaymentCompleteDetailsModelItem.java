package com.fairsoft.chitfund.model;

import com.google.gson.annotations.SerializedName;

public class PaymentCompleteDetailsModelItem {

    @SerializedName("chitid")
    private int chitid;

    @SerializedName("paymentdate")
    private String paymentdate;

    @SerializedName("amount")
    private double amount;

    @SerializedName("interest")
    private double interest;

    @SerializedName("paymentid")
    private int paymentid;

    @SerializedName("chitcode")
    private String chitcode;

    public String getTicketno() {
        return ticketno;
    }

    public void setTicketno(String ticketno) {
        this.ticketno = ticketno;
    }


    @SerializedName("name")
    private String name;

    @SerializedName("ticketno")
    private String ticketno;


    @SerializedName("paytype")
    private Object paytype;

    @SerializedName("memberid")
    private int memberid;

    public int getSubscriberid() {
        return subscriberid;
    }

    public void setSubscriberid(int subscriberid) {
        this.subscriberid = subscriberid;
    }

    @SerializedName("subscriberid")
    private int subscriberid;


    public void setChitid(int chitid) {
        this.chitid = chitid;
    }

    public int getChitid() {
        return chitid;
    }

    public void setPaymentdate(String paymentdate) {
        this.paymentdate = paymentdate;
    }

    public String getPaymentdate() {
        return paymentdate;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

    public double getInterest() {
        return interest;
    }

    public void setPaymentid(int paymentid) {
        this.paymentid = paymentid;
    }

    public int getPaymentid() {
        return paymentid;
    }

    public void setChitcode(String chitcode) {
        this.chitcode = chitcode;
    }

    public String getChitcode() {
        return chitcode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPaytype(Object paytype) {
        this.paytype = paytype;
    }

    public Object getPaytype() {
        return paytype;
    }

    public void setMemberid(int memberid) {
        this.memberid = memberid;
    }

    public int getMemberid() {
        return memberid;
    }
}