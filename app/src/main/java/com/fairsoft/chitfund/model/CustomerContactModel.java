package com.fairsoft.chitfund.model;

public class CustomerContactModel {
    private String name;
    private String Mobile;
    boolean isSelected;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return Mobile;
    }


    public void setMobile(String mobile) {
        Mobile = mobile;
    }
    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
    public CustomerContactModel(boolean isSelected,String name,String mobile) {
        this.isSelected = isSelected;
        this.name = name;
        this.Mobile = mobile;

/*
        this.transId = transId;
*/



    }
    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CustomerContactModel) {
            CustomerContactModel c = (CustomerContactModel) obj;
            if (c.getName().equals(name) && c.getMobile() == getMobile()) return true;
        }

        return false;
    }

}
