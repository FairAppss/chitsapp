package com.fairsoft.chitfund.model;

import com.google.gson.annotations.SerializedName;

public class SingleChitModelItem{

	@SerializedName("reference")
	private String reference;

	@SerializedName("chitid")
	private int chitid;

	@SerializedName("createdby")
	private int createdby;

	@SerializedName("createddate")
	private String createddate;

	@SerializedName("city")
	private String city;

	@SerializedName("chitcode")
	private String chitcode;

	@SerializedName("name")
	private String name;

	@SerializedName("member")
	private String member;

	@SerializedName("subscriberid")
	private int subscriberid;

	@SerializedName("memberid")
	private int memberid;

	@SerializedName("status")
	private String status;

	public void setReference(String reference){
		this.reference = reference;
	}

	public String getReference(){
		return reference;
	}

	public void setChitid(int chitid){
		this.chitid = chitid;
	}

	public int getChitid(){
		return chitid;
	}

	public void setCreatedby(int createdby){
		this.createdby = createdby;
	}

	public int getCreatedby(){
		return createdby;
	}

	public void setCreateddate(String createddate){
		this.createddate = createddate;
	}

	public String getCreateddate(){
		return createddate;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setChitcode(String chitcode){
		this.chitcode = chitcode;
	}

	public String getChitcode(){
		return chitcode;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setMember(String member){
		this.member = member;
	}

	public String getMember(){
		return member;
	}

	public void setSubscriberid(int subscriberid){
		this.subscriberid = subscriberid;
	}

	public int getSubscriberid(){
		return subscriberid;
	}

	public void setMemberid(int memberid){
		this.memberid = memberid;
	}

	public int getMemberid(){
		return memberid;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}