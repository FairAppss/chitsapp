package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SendMessageResponse{

	@SerializedName("result")
	private boolean result;

	@SerializedName("validWhatsAppNumber")
	private boolean validWhatsAppNumber;

	@SerializedName("template_name")
	private String templateName;

	@SerializedName("parameteres")
	private List<ParameteresItem> parameteres;

	@SerializedName("contact")
	private Contact contact;

	@SerializedName("phone_number")
	private String phoneNumber;

	@SerializedName("model")
	private Model model;

	public boolean isResult(){
		return result;
	}

	public boolean isValidWhatsAppNumber(){
		return validWhatsAppNumber;
	}

	public String getTemplateName(){
		return templateName;
	}

	public List<ParameteresItem> getParameteres(){
		return parameteres;
	}

	public Contact getContact(){
		return contact;
	}

	public String getPhoneNumber(){
		return phoneNumber;
	}

	public Model getModel(){
		return model;
	}
}