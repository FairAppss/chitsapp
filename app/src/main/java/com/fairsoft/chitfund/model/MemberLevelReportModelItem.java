package com.fairsoft.chitfund.model;

import com.google.gson.annotations.SerializedName;

public class MemberLevelReportModelItem{

	@SerializedName("Receivables")
	private String receivables;

	@SerializedName("chitid")
	private String chitid;

	@SerializedName("RecPending")
	private String recPending;

	@SerializedName("Received")
	private String received;

	@SerializedName("PayPending")
	private String payPending;

	@SerializedName("Paid")
	private String paid;

	@SerializedName("ChitCode")
	private String chitCode;

	@SerializedName("Payable")
	private String payable;

	public void setReceivables(String receivables){
		this.receivables = receivables;
	}

	public String getReceivables(){
		return receivables;
	}

	public void setChitid(String chitid){
		this.chitid = chitid;
	}

	public String getChitid(){
		return chitid;
	}

	public void setRecPending(String recPending){
		this.recPending = recPending;
	}

	public String getRecPending(){
		return recPending;
	}

	public void setReceived(String received){
		this.received = received;
	}

	public String getReceived(){
		return received;
	}

	public void setPayPending(String payPending){
		this.payPending = payPending;
	}

	public String getPayPending(){
		return payPending;
	}

	public void setPaid(String paid){
		this.paid = paid;
	}

	public String getPaid(){
		return paid;
	}

	public void setChitCode(String chitCode){
		this.chitCode = chitCode;
	}

	public String getChitCode(){
		return chitCode;
	}

	public void setPayable(String payable){
		this.payable = payable;
	}

	public String getPayable(){
		return payable;
	}
}