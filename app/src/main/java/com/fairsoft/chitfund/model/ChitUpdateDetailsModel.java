package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ChitUpdateDetailsModel{

	@SerializedName("ChitUpdateDetailsModel")
	private List<ChitUpdateDetailsModelItem> chitUpdateDetailsModel;

	public void setChitUpdateDetailsModel(List<ChitUpdateDetailsModelItem> chitUpdateDetailsModel){
		this.chitUpdateDetailsModel = chitUpdateDetailsModel;
	}

	public List<ChitUpdateDetailsModelItem> getChitUpdateDetailsModel(){
		return chitUpdateDetailsModel;
	}
}