package com.fairsoft.chitfund.model;

import com.google.gson.annotations.SerializedName;

public class Requests {

	@SerializedName("result")
	private boolean result;

	@SerializedName("contact")
	private Contact contact;

	public void setResult(boolean result) {
		this.result = result;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public boolean isResult(){
		return result;
	}

	public Contact getContact(){
		return contact;
	}
}