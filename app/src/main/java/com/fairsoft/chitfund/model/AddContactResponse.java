package com.fairsoft.chitfund.model;

import com.google.gson.annotations.SerializedName;

public class AddContactResponse {


    @SerializedName("contact")
    private Contact contact;


    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Contact getContact() {
        return contact;
    }
}