package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SubscriberDetailsModel{

	@SerializedName("SubscriberDetailsModel")
	private List<SubscriberDetailsModelItem> subscriberDetailsModel;

	public List<SubscriberDetailsModelItem> getSubscriberDetailsModel(){
		return subscriberDetailsModel;
	}
}