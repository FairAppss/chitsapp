package com.fairsoft.chitfund.model;

import com.google.gson.annotations.SerializedName;

public class ActiveChitsResponseModelItem{

	@SerializedName("chitid")
	private int chitid;

	@SerializedName("nomonths")
	private int nomonths;

	@SerializedName("chitdate")
	private String chitdate;

	@SerializedName("commn")
	private double commn;

	@SerializedName("isownchit")
	private String isownchit;

	@SerializedName("chitamt")
	private double chitamt;

	@SerializedName("commnPerAmount")
	private String commnPerAmount;

	@SerializedName("chitcode")
	private String chitcode;

	@SerializedName("auctionday")
	private String auctionday;

	@SerializedName("dividend")
	private Object dividend;

	@SerializedName("noofmembers")
	private int noofmembers;

	@SerializedName("chittype")
	private String chittype;

	public String getChittype() {
		return chittype;
	}

	public void setChittype(String chittype) {
		this.chittype = chittype;
	}

	public void setChitid(int chitid) {
		this.chitid = chitid;
	}

	public void setNomonths(int nomonths) {
		this.nomonths = nomonths;
	}

	public void setChitdate(String chitdate) {
		this.chitdate = chitdate;
	}

	public void setCommn(double commn) {
		this.commn = commn;
	}

	public void setIsownchit(String isownchit) {
		this.isownchit = isownchit;
	}

	public void setChitamt(double chitamt) {
		this.chitamt = chitamt;
	}

	public void setCommnPerAmount(String commnPerAmount) {
		this.commnPerAmount = commnPerAmount;
	}

	public void setChitcode(String chitcode) {
		this.chitcode = chitcode;
	}

	public void setAuctionday(String auctionday) {
		this.auctionday = auctionday;
	}

	public void setDividend(Object dividend) {
		this.dividend = dividend;
	}

	public void setNoofmembers(int noofmembers) {
		this.noofmembers = noofmembers;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	@SerializedName("frequency")
	private String frequency;

	public int getChitid(){
		return chitid;
	}

	public int getNomonths(){
		return nomonths;
	}

	public String getChitdate(){
		return chitdate;
	}

	public double getCommn(){
		return commn;
	}

	public String getIsownchit(){
		return isownchit;
	}

	public double getChitamt(){
		return chitamt;
	}

	public String getCommnPerAmount(){
		return commnPerAmount;
	}

	public String getChitcode(){
		return chitcode;
	}

	public String getAuctionday(){
		return auctionday;
	}

	public Object getDividend(){
		return dividend;
	}

	public int getNoofmembers(){
		return noofmembers;
	}

	public String getFrequency(){
		return frequency;
	}
}