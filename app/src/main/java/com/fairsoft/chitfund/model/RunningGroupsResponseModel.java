package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class RunningGroupsResponseModel{

	@SerializedName("RunningGroupsResponseModel")
	private List<RunningGroupsResponseModelItem> runningGroupsResponseModel;

	public void setRunningGroupsResponseModel(List<RunningGroupsResponseModelItem> runningGroupsResponseModel){
		this.runningGroupsResponseModel = runningGroupsResponseModel;
	}

	public List<RunningGroupsResponseModelItem> getRunningGroupsResponseModel(){
		return runningGroupsResponseModel;
	}
}