package com.fairsoft.chitfund.model;

public class MemberModelItem1 {

    private String name;

    private int memberid;
    private String mobile;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public MemberModelItem1( Integer memberid, String name) {
        this.memberid = memberid;
        this.name = name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void setMemberid(int memberid) {
        this.memberid = memberid;
    }

    public int getMemberid() {
        return memberid;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof MemberModelItem1){
            MemberModelItem1 c = (MemberModelItem1)obj;
            if(c.getName().equals(name) && c.getMemberid()==memberid ) return true;
        }

        return false;
    }


}
