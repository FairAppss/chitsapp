package com.fairsoft.chitfund.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class OverAllReportModel{

	@SerializedName("OverAllReportModel")
	private List<OverAllReportModelItem> overAllReportModel;

	public void setOverAllReportModel(List<OverAllReportModelItem> overAllReportModel){
		this.overAllReportModel = overAllReportModel;
	}

	public List<OverAllReportModelItem> getOverAllReportModel(){
		return overAllReportModel;
	}
}