package com.fairsoft.chitfund.model;

public class ChitModelItem {

    private String name;

    private int chitid;

    public ChitModelItem(Integer memberid, String name) {
        this.chitid = memberid;
        this.name = name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void setMemberid(int memberid) {
        this.chitid = memberid;
    }

    public int getMemberid() {
        return chitid;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ChitModelItem){
            ChitModelItem c = (ChitModelItem)obj;
            if(c.getName().equals(name) && c.getMemberid()==chitid ) return true;
        }

        return false;
    }


}
