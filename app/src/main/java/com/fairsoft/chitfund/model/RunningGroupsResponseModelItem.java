package com.fairsoft.chitfund.model;

import com.google.gson.annotations.SerializedName;

public class RunningGroupsResponseModelItem{

	@SerializedName("auctiontime")
	private String auctiontime;

	@SerializedName("commn")
	private double commn;

	@SerializedName("chitamt")
	private double chitamt;

	@SerializedName("auctionday")
	private String auctionday;

	@SerializedName("startdate")
	private String startdate;

	@SerializedName("auctweek")
	private String auctweek;

	@SerializedName("noofmembers")
	private int noofmembers;

	@SerializedName("frequency")
	private String frequency;

	@SerializedName("Comp")
	private String comp;

	@SerializedName("nomonths")
	private int nomonths;

	@SerializedName("payable")
	private String payable;

	@SerializedName("auctionday1")
	private String auctionday1;

	@SerializedName("lastbid")
	private String lastbid;

	@SerializedName("commnPerAmount")
	private String commnPerAmount;

	@SerializedName("dividend")
	private Object dividend;

	@SerializedName("paymentafter")
	private int paymentafter;

	@SerializedName("bidamount")
	private String bidamount;

	@SerializedName("chitdate")
	private String chitdate;

	@SerializedName("chitcode")
	private String chitcode;

	@SerializedName("dateday")
	private String dateday;

	@SerializedName("completed")
	private String completed;

	@SerializedName("receivable")
	private String receivable;

	@SerializedName("Column1")
	private String column1;

	@SerializedName("receiptafter")
	private int receiptafter;

	@SerializedName("chitid")
	private int chitid;

	@SerializedName("enddate")
	private String enddate;

	@SerializedName("isownchit")
	private String isownchit;

	@SerializedName("aucttimeAMPM")
	private String aucttimeAMPM;

	@SerializedName("samenext")
	private String samenext;

	public void setAuctiontime(String auctiontime){
		this.auctiontime = auctiontime;
	}

	public String getAuctiontime(){
		return auctiontime;
	}

	public void setCommn(double commn){
		this.commn = commn;
	}

	public double getCommn(){
		return commn;
	}

	public void setChitamt(double chitamt){
		this.chitamt = chitamt;
	}

	public double getChitamt(){
		return chitamt;
	}

	public void setAuctionday(String auctionday){
		this.auctionday = auctionday;
	}

	public String getAuctionday(){
		return auctionday;
	}

	public void setStartdate(String startdate){
		this.startdate = startdate;
	}

	public String getStartdate(){
		return startdate;
	}

	public void setAuctweek(String auctweek){
		this.auctweek = auctweek;
	}

	public String getAuctweek(){
		return auctweek;
	}

	public void setNoofmembers(int noofmembers){
		this.noofmembers = noofmembers;
	}

	public int getNoofmembers(){
		return noofmembers;
	}

	public void setFrequency(String frequency){
		this.frequency = frequency;
	}

	public String getFrequency(){
		return frequency;
	}

	public void setComp(String comp){
		this.comp = comp;
	}

	public String getComp(){
		return comp;
	}

	public void setNomonths(int nomonths){
		this.nomonths = nomonths;
	}

	public int getNomonths(){
		return nomonths;
	}

	public void setPayable(String payable){
		this.payable = payable;
	}

	public String getPayable(){
		return payable;
	}

	public void setAuctionday1(String auctionday1){
		this.auctionday1 = auctionday1;
	}

	public String getAuctionday1(){
		return auctionday1;
	}

	public void setLastbid(String lastbid){
		this.lastbid = lastbid;
	}

	public String getLastbid(){
		return lastbid;
	}

	public void setCommnPerAmount(String commnPerAmount){
		this.commnPerAmount = commnPerAmount;
	}

	public String getCommnPerAmount(){
		return commnPerAmount;
	}

	public void setDividend(Object dividend){
		this.dividend = dividend;
	}

	public Object getDividend(){
		return dividend;
	}

	public void setPaymentafter(int paymentafter){
		this.paymentafter = paymentafter;
	}

	public int getPaymentafter(){
		return paymentafter;
	}

	public void setBidamount(String bidamount){
		this.bidamount = bidamount;
	}

	public String getBidamount(){
		return bidamount;
	}

	public void setChitdate(String chitdate){
		this.chitdate = chitdate;
	}

	public String getChitdate(){
		return chitdate;
	}

	public void setChitcode(String chitcode){
		this.chitcode = chitcode;
	}

	public String getChitcode(){
		return chitcode;
	}

	public void setDateday(String dateday){
		this.dateday = dateday;
	}

	public String getDateday(){
		return dateday;
	}

	public void setCompleted(String completed){
		this.completed = completed;
	}

	public String getCompleted(){
		return completed;
	}

	public void setReceivable(String receivable){
		this.receivable = receivable;
	}

	public String getReceivable(){
		return receivable;
	}

	public void setColumn1(String column1){
		this.column1 = column1;
	}

	public String getColumn1(){
		return column1;
	}

	public void setReceiptafter(int receiptafter){
		this.receiptafter = receiptafter;
	}

	public int getReceiptafter(){
		return receiptafter;
	}

	public void setChitid(int chitid){
		this.chitid = chitid;
	}

	public int getChitid(){
		return chitid;
	}

	public void setEnddate(String enddate){
		this.enddate = enddate;
	}

	public String getEnddate(){
		return enddate;
	}

	public void setIsownchit(String isownchit){
		this.isownchit = isownchit;
	}

	public String getIsownchit(){
		return isownchit;
	}

	public void setAucttimeAMPM(String aucttimeAMPM){
		this.aucttimeAMPM = aucttimeAMPM;
	}

	public String getAucttimeAMPM(){
		return aucttimeAMPM;
	}

	public void setSamenext(String samenext){
		this.samenext = samenext;
	}

	public String getSamenext(){
		return samenext;
	}
}