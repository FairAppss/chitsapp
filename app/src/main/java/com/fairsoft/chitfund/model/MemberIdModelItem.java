package com.fairsoft.chitfund.model;

public class MemberIdModelItem{
	private String dom1;
	private String phoneres;
	private String city1;
	private String dom;
	private String refmobile;
	private String city;
	private String mobile1;
	private String mobile11;
	private String phoneoff;
	private String pin;
	private String dob1;
	private String dob;
	private String mobile2;
	private String name;
	private String aadhar;
	private String state;
	private String add2;
	private String add1;
	private String email;
	private int memberid;
	private String refby;

	public void setDom1(String dom1){
		this.dom1 = dom1;
	}

	public String getDom1(){
		return dom1;
	}

	public void setPhoneres(String phoneres){
		this.phoneres = phoneres;
	}

	public String getPhoneres(){
		return phoneres;
	}

	public void setCity1(String city1){
		this.city1 = city1;
	}

	public String getCity1(){
		return city1;
	}

	public void setDom(String dom){
		this.dom = dom;
	}

	public String getDom(){
		return dom;
	}

	public void setRefmobile(String refmobile){
		this.refmobile = refmobile;
	}

	public String getRefmobile(){
		return refmobile;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setMobile1(String mobile1){
		this.mobile1 = mobile1;
	}

	public String getMobile1(){
		return mobile1;
	}

	public void setMobile11(String mobile11){
		this.mobile11 = mobile11;
	}

	public String getMobile11(){
		return mobile11;
	}

	public void setPhoneoff(String phoneoff){
		this.phoneoff = phoneoff;
	}

	public String getPhoneoff(){
		return phoneoff;
	}

	public void setPin(String pin){
		this.pin = pin;
	}

	public String getPin(){
		return pin;
	}

	public void setDob1(String dob1){
		this.dob1 = dob1;
	}

	public String getDob1(){
		return dob1;
	}

	public void setDob(String dob){
		this.dob = dob;
	}

	public String getDob(){
		return dob;
	}

	public void setMobile2(String mobile2){
		this.mobile2 = mobile2;
	}

	public String getMobile2(){
		return mobile2;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setAadhar(String aadhar){
		this.aadhar = aadhar;
	}

	public String getAadhar(){
		return aadhar;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	public void setAdd2(String add2){
		this.add2 = add2;
	}

	public String getAdd2(){
		return add2;
	}

	public void setAdd1(String add1){
		this.add1 = add1;
	}

	public String getAdd1(){
		return add1;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setMemberid(int memberid){
		this.memberid = memberid;
	}

	public int getMemberid(){
		return memberid;
	}

	public void setRefby(String refby){
		this.refby = refby;
	}

	public String getRefby(){
		return refby;
	}
}
