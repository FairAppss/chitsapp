package com.fairsoft.chitfund.model;

import com.google.gson.annotations.SerializedName;

public class LoginResponseModelItem{

	@SerializedName("role")
	private String role;

	@SerializedName("compid")
	private int compid;

	@SerializedName("companyname")
	private String companyname;

	@SerializedName("userid")
	private int userid;

	@SerializedName("username")
	private String username;

	public void setRole(String role){
		this.role = role;
	}

	public String getRole(){
		return role;
	}

	public void setCompid(int compid){
		this.compid = compid;
	}

	public int getCompid(){
		return compid;
	}

	public void setCompanyname(String companyname){
		this.companyname = companyname;
	}

	public String getCompanyname(){
		return companyname;
	}

	public void setUserid(int userid){
		this.userid = userid;
	}

	public int getUserid(){
		return userid;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}
}