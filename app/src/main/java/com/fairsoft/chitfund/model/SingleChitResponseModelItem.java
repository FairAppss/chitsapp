package com.fairsoft.chitfund.model;

import com.google.gson.annotations.SerializedName;

public class SingleChitResponseModelItem{

	@SerializedName("Receivables")
	private String receivables;

	@SerializedName("MemberID")
	private String memberID;

	@SerializedName("Installments")
	private Object installments;

	@SerializedName("RecPending")
	private String recPending;

	@SerializedName("prized")
	private String prized;

	@SerializedName("Received")
	private String received;

	@SerializedName("PayPending")
	private String payPending;

	@SerializedName("Paid")
	private String paid;

	@SerializedName("Payable")
	private String payable;

	@SerializedName("Name")
	private String name;

	public void setReceivables(String receivables){
		this.receivables = receivables;
	}

	public String getReceivables(){
		return receivables;
	}

	public void setMemberID(String memberID){
		this.memberID = memberID;
	}

	public String getMemberID(){
		return memberID;
	}

	public Integer setInstallments(Object installments){
		this.installments = installments;
		return null;
	}

	public Object getInstallments(){
		return installments;
	}

	public void setRecPending(String recPending){
		this.recPending = recPending;
	}

	public String getRecPending(){
		return recPending;
	}

	public void setPrized(String prized){
		this.prized = prized;
	}

	public String getPrized(){
		return prized;
	}

	public void setReceived(String received){
		this.received = received;
	}

	public String getReceived(){
		return received;
	}

	public void setPayPending(String payPending){
		this.payPending = payPending;
	}

	public String getPayPending(){
		return payPending;
	}

	public void setPaid(String paid){
		this.paid = paid;
	}

	public String getPaid(){
		return paid;
	}

	public void setPayable(String payable){
		this.payable = payable;
	}

	public String getPayable(){
		return payable;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}
}