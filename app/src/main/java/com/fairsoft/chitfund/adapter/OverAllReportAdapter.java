package com.fairsoft.chitfund.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.fairsoft.chitfund.R;
import com.fairsoft.chitfund.model.OverAllReportModelItem;

import java.text.DecimalFormat;
import java.util.List;

public class OverAllReportAdapter extends RecyclerView.Adapter<OverAllReportAdapter.MyViewHolder> {
    List<OverAllReportModelItem> personNames;
    Context context;
    private static int lastClickedPosition = -1;
    private int selectedItem;
    OverAllReportModelItem model;
    String memberId, chitCode, chintName;
    Integer chitId;


    public OverAllReportAdapter(Context context, List<OverAllReportModelItem> personNames) {
        this.context = context;
        this.personNames = personNames;
        selectedItem = 0;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.over_all_level_row_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // set the data in items

        if (personNames != null && personNames.size() > 0) {
            model = personNames.get(position);
            holder.subscriber_name.setText(String.valueOf(model.getChitCode()));

            DecimalFormat formatter = new DecimalFormat("#,###,###");

            String receivable = String.valueOf(model.getReceivables());
            double d;
            if (!receivable.isEmpty()) {
                d = Double.parseDouble(receivable);
                holder.receivable_chit_level.setText(formatter.format(d));
            }

            String received = String.valueOf(model.getReceived());
            if (!received.isEmpty()) {
                double d1 = Double.parseDouble(received);
                holder.received_chit_level.setText(formatter.format(d1));
            }

            String recPending = String.valueOf(model.getRecPending());

            if (!recPending.isEmpty()) {
                double d2 = Double.parseDouble(recPending);
                holder.due_chit_level.setText(formatter.format(d2));
            }

            String payable = String.valueOf(model.getPayable());

            if (!payable.isEmpty()) {
                double d3 = Double.parseDouble(payable);
                holder.payable_chit_level.setText(formatter.format(d3));
            }

            String paid = String.valueOf(model.getPaid());

            if (!paid.isEmpty()) {
                double d4 = Double.parseDouble(paid);
                holder.paid_chit_level.setText(formatter.format(d4));
            }
            String payPending = String.valueOf(model.getPayPending());

            if (!payPending.isEmpty()) {
                double d5 = Double.parseDouble(payPending);
                holder.bal_chit_level.setText(formatter.format(d5));
            }


        }
        if (position % 2 == 0) {
            holder.subScribeLayout.setBackgroundColor(Color.WHITE);
        } else {
            holder.subScribeLayout.setBackgroundResource(R.color.fadeBlue);


        }

    }

    @Override
    public int getItemCount() {
        return personNames.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView subscriber_name, receivable_chit_level, received_chit_level, due_chit_level, paid_chit_level, payable_chit_level, bal_chit_level;// init the item view's
        LinearLayout subScribeLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            // get the reference of item view's
            subscriber_name = (TextView) itemView.findViewById(R.id.subscriber_name);
            receivable_chit_level = (TextView) itemView.findViewById(R.id.receivable_chit_level);
            received_chit_level = (TextView) itemView.findViewById(R.id.received_chit_level);
            due_chit_level = (TextView) itemView.findViewById(R.id.due_chit_level);
            paid_chit_level = (TextView) itemView.findViewById(R.id.paid_chit_level);
            payable_chit_level = (TextView) itemView.findViewById(R.id.payable_chit_level);
            bal_chit_level = (TextView) itemView.findViewById(R.id.bal_chit_level);


            subScribeLayout = itemView.findViewById(R.id.subScribeLayout);


        }

    }


}
