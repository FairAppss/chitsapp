package com.fairsoft.chitfund.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.fairsoft.chitfund.model.ReceiptCompleteDetailsModelItem;
import com.fairsoft.chitfund.R;


import java.util.List;

public class ReceiptAdapter extends RecyclerView.Adapter<ReceiptAdapter.MyViewHolder> {
    List<ReceiptCompleteDetailsModelItem> personNames;
    Context context;
    private onItemClickListener onItemClickListener;
    private static int lastClickedPosition = -1;
    private int selectedItem;
    String memberId, chitCode, chintName;
    Integer chitId;
    deleteRow listeres;



    public void setOnItemClickListener(ReceiptAdapter.onItemClickListener onItemClickListener, deleteRow deleteRow) {
        this.onItemClickListener = onItemClickListener;
        this.listeres = deleteRow;

    }

    public ReceiptAdapter(Context context, List<ReceiptCompleteDetailsModelItem> personNames, deleteRow listeners,onItemClickListener onItemClickListener) {
        this.context = context;
        this.personNames = personNames;
        this.onItemClickListener = onItemClickListener;
        selectedItem = 0;
        this.listeres = listeners;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.receipt_row_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v, onItemClickListener,listeres); // pass the view to View Holder
        return vh;
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // set the data in items

        if (personNames != null && personNames.size() > 0) {
            ReceiptCompleteDetailsModelItem model = personNames.get(position);
            holder.chitAmtTxt.setText(String.valueOf(model.getAmount()));
            holder.chitDtTxt.setText((model.getChitcode()));
            holder.receiptId.setText(String.valueOf(model.getName()));
            holder.receiptType.setText(String.valueOf(model.getRectype()));
            holder.memeberIdRec.setText(String.valueOf(model.getMemberid()));
            chitCode = personNames.get(position).getChitcode();
            chitId = model.getChitid();


        }
        if (position % 2 == 0) {
            holder.tableLayout.setBackgroundColor(Color.WHITE);
        } else {
            holder.tableLayout.setBackgroundResource(R.color.fadeBlue);


        }

    }

    @Override
    public int getItemCount() {
        return personNames.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView chitAmtTxt, receiptId, receiptType, memeberIdRec;// init the item view's
        LinearLayout tableLayout,editLL;
        TextView chitDtTxt;
        ImageView edit, deleteImageView;
        deleteRow deleteRow;


        public MyViewHolder(View itemView, onItemClickListener listener, deleteRow deleteRow) {
            super(itemView);
            // get the reference of item view's
            chitDtTxt = (TextView) itemView.findViewById(R.id.chitcodeTxt);
            chitAmtTxt = (TextView) itemView.findViewById(R.id.chitIdRec);
            receiptId = (TextView) itemView.findViewById(R.id.receiptId);
            receiptType = (TextView) itemView.findViewById(R.id.receiptType);
            memeberIdRec = (TextView) itemView.findViewById(R.id.memeberIdRec);
            tableLayout = itemView.findViewById(R.id.tableLayout);
            editLL = itemView.findViewById(R.id.editLL);

            edit = itemView.findViewById(R.id.edit);

            deleteImageView = itemView.findViewById(R.id.deleteImageView);
            this.deleteRow = deleteRow;


            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                   /* Intent intent = new Intent(context, ReceiptActivity.class);
                    intent.putExtra("chitId", personNames.get(position).getChitid());
                    intent.putExtra("receiptId", personNames.get(position).getReceiptid());
                    intent.putExtra("chitCode", personNames.get(position).getChitcode());
                    intent.putExtra("position", position);
                    context.startActivity(intent);*/
                    Integer chitId=personNames.get(position).getChitid();
                    Integer receiptid=personNames.get(position).getReceiptid();
                    String chitCode=personNames.get(position).getChitcode();

                    onItemClickListener.onItemClick(chitId,receiptid,chitCode);



                }
            });
            deleteImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    Integer chitId=personNames.get(position).getChitid();
                    Integer receiptid=personNames.get(position).getReceiptid();
                    String chitCode=personNames.get(position).getChitcode();

                    listeres.deleteRowClick(chitId,receiptid,chitCode);



                }
            });



        }

    }

    public interface onItemClickListener {
        void onItemClick(Integer chitId,Integer receiptId,String chitCode);

    }

    public interface deleteRow {
        void deleteRowClick(Integer chitId,Integer receiptId,String chitCode);

    }


}
