package com.fairsoft.chitfund.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.fairsoft.chitfund.R;
import com.fairsoft.chitfund.model.SingleChitResponseModelItem;

import java.text.DecimalFormat;
import java.util.List;

public class SigleChitAdapter extends RecyclerView.Adapter<SigleChitAdapter.MyViewHolder> {
    List<SingleChitResponseModelItem> personNames;

    Context context;
    private onItemClickListener onItemClickListener;
    private static int lastClickedPosition = -1;
    private int selectedItem;


    public void setOnItemClickListener(SigleChitAdapter.onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public SigleChitAdapter(Context context, List<SingleChitResponseModelItem> personNames) {
        this.context = context;
        this.personNames = personNames;
        this.onItemClickListener = onItemClickListener;
        selectedItem = 0;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_chit_row_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v, onItemClickListener); // pass the view to View Holder
        return vh;
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // set the data in items

        if (personNames != null && personNames.size() > 0) {
            SingleChitResponseModelItem model = personNames.get(position);
            holder.memberTxt.setText((model.getName()));

            DecimalFormat formatter = new DecimalFormat("#,###,###");

            String receivable = String.valueOf(model.getReceivables());
            double d;
            if (!receivable.isEmpty()) {
                d = Double.parseDouble(receivable);
                holder.receivableTxt.setText(formatter.format(d));
            }





            String received = String.valueOf(model.getReceived());
            if (!received.isEmpty()) {
                double d1 = Double.parseDouble(received);
                holder.receivedTxt.setText(formatter.format(d1));
            }


            String recPending = String.valueOf(model.getRecPending());

            if (!recPending.isEmpty()) {
                double d2 = Double.parseDouble(recPending);
                holder.pendingTxt.setText(formatter.format(d2));
            }


            String payable = String.valueOf(model.getPayable());

            if (!payable.isEmpty()) {
                double d3 = Double.parseDouble(payable);
                holder.payableTxt.setText(formatter.format(d3));
            }


            String paid = String.valueOf(model.getPaid());

            if (!paid.isEmpty()) {
                double d4 = Double.parseDouble(paid);
                holder.paidTxt.setText(formatter.format(d4));
            }


            String payPending = String.valueOf(model.getPayPending());

            if (!payPending.isEmpty()) {
                double d5 = Double.parseDouble(payPending);
                holder.PaypendingTxt.setText(formatter.format(d5));
            }

            String prized = String.valueOf(model.getPrized());

            if (!prized.isEmpty()) {
                double d6 = Double.parseDouble(prized);
                holder.prizedTxt.setText(formatter.format(d6));
            }


            if (position % 2 == 0) {
                holder.tableLayout.setBackgroundColor(Color.WHITE);
            } else {
                holder.tableLayout.setBackgroundResource(R.color.fadeBlue);


            }


        }

    }

    @Override
    public int getItemCount() {
        return personNames.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView receivableTxt, memberTxt, receivedTxt, pendingTxt, prizedTxt, prizedInstTxt, payableTxt, paidTxt, PaypendingTxt;// init the item view's
        LinearLayout tableLayout;

        public MyViewHolder(View itemView, onItemClickListener listener) {
            super(itemView);
            // get the reference of item view's
            receivableTxt = (TextView) itemView.findViewById(R.id.receivableTxt);
            memberTxt = (TextView) itemView.findViewById(R.id.memberTxt);
            pendingTxt = (TextView) itemView.findViewById(R.id.pendingTxt);
            receivedTxt = (TextView) itemView.findViewById(R.id.receivedTxt);
            prizedTxt = (TextView) itemView.findViewById(R.id.prizedTxt);
/*
            prizedInstTxt = (TextView) itemView.findViewById(R.id.prizedInstTxt);
*/
            payableTxt = (TextView) itemView.findViewById(R.id.payableTxt);
            paidTxt = (TextView) itemView.findViewById(R.id.paidTxt);
            PaypendingTxt = (TextView) itemView.findViewById(R.id.PaypendingTxt);


            tableLayout = (LinearLayout) itemView.findViewById(R.id.tableLayout);


        }

    }

    public interface onItemClickListener {
        void onItemClick(int position);

    }

}
