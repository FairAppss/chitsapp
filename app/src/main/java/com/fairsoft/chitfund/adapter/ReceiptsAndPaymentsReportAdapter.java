package com.fairsoft.chitfund.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.fairsoft.chitfund.R;
import com.fairsoft.chitfund.model.ReceiptsNPaymentsModelItem;

import java.text.DecimalFormat;
import java.util.List;

public class ReceiptsAndPaymentsReportAdapter extends RecyclerView.Adapter<ReceiptsAndPaymentsReportAdapter.MyViewHolder> {
    List<ReceiptsNPaymentsModelItem> personNames;
    Context context;
    private static int lastClickedPosition = -1;
    private int selectedItem;
    ReceiptsNPaymentsModelItem model;
    String memberId, chitCode, chintName;
    Integer chitId;


    public ReceiptsAndPaymentsReportAdapter(Context context, List<ReceiptsNPaymentsModelItem> personNames) {
        this.context = context;
        this.personNames = personNames;
        selectedItem = 0;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.receipt_payment_row_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;
    }



    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // set the data in items

        if (personNames != null && personNames.size() > 0) {
            model = personNames.get(position);
            holder.date_rp.setText(String.valueOf(model.getCreateddate()));

            DecimalFormat formatter = new DecimalFormat("#,###,###");


            String received = String.valueOf(model.getAmount());
            if (!received.isEmpty()) {
                double d1 = Double.parseDouble(received);
                holder.amount_rp.setText(formatter.format(d1));
            }
            holder.from_rp.setText(String.valueOf(model.getName()));
            String trTypr=String.valueOf(model.getTrtype());
            if (trTypr.equals("null")) {
                holder.to_rp.setText("");
            }else {
                holder.to_rp.setText(trTypr);


            }

        }
        if (position % 2 == 0) {
            holder.subScribeLayout.setBackgroundColor(Color.WHITE);
        } else {
            holder.subScribeLayout.setBackgroundResource(R.color.fadeBlue);


        }

    }

    @Override
    public int getItemCount() {
        return personNames.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView date_rp, amount_rp, from_rp, to_rp, paid_chit_level, payable_chit_level, bal_chit_level;// init the item view's
        LinearLayout subScribeLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            // get the reference of item view's
            date_rp = (TextView) itemView.findViewById(R.id.date_rp);
            amount_rp = (TextView) itemView.findViewById(R.id.amount_rp);
            from_rp = (TextView) itemView.findViewById(R.id.from_rp);
            to_rp = (TextView) itemView.findViewById(R.id.to_rp);


            subScribeLayout = itemView.findViewById(R.id.subScribeLayout);


        }

    }


}
