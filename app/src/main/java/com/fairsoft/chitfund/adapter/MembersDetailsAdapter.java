package com.fairsoft.chitfund.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.fairsoft.chitfund.R;
import com.fairsoft.chitfund.activity.DefineNewMemberActivity;
import com.fairsoft.chitfund.model.MemberIdModelItem;

import java.util.List;

public class MembersDetailsAdapter extends RecyclerView.Adapter<MembersDetailsAdapter.MyViewHolder> {
    List<MemberIdModelItem> chitDetails;
    Context context;
    private static int lastClickedPosition = -1;
    private int selectedItem;
    deleteRow listeres;

    String nameUp, aadharUp, mobile1Up, mobile2Up, landOffUp, landResUp, emailUp, address1Up, address2Up, cityUp, stateUp, pinCodeUp, dobUp, domUp, refByUp, refMobUp, memeberId;
    public void setOnItemClickListener( deleteRow deleteRow) {
        this.listeres = deleteRow;
    }



    public MembersDetailsAdapter(Context context, List<MemberIdModelItem> chitDetails, deleteRow listeners) {
        this.context = context;
        this.chitDetails = chitDetails;
        selectedItem = 0;
        this.listeres = listeners;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.member_details_row_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v,listeres); // pass the view to View Holder
        return vh;
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // set the data in items

        if (chitDetails != null && chitDetails.size() > 0) {
            MemberIdModelItem model = chitDetails.get(position);
            if (model.getName() == "null") {
                holder.nameTxt.setText("");

            } else {
                holder.nameTxt.setText(model.getName());


            }
            if (model.getAdd1() == "null") {
                holder.address1Txt.setText("");

            } else {
                holder.address1Txt.setText(model.getAdd1());


            }
            holder.address2Txt.setText(String.valueOf(model.getMemberid()));


            if (model.getMobile1() == "null") {
                holder.mobile1Txt.setText("");

            } else {
                holder.mobile1Txt.setText(model.getMobile1());


            }
            // holder.mobile2Txt.setText((model.getMobile2()).toString());
            nameUp = model.getName();
            aadharUp = model.getAadhar();
            mobile1Up = model.getMobile1();
            mobile2Up = model.getMobile2();
            landOffUp = model.getPhoneoff();
            landResUp = model.getPhoneres();
            emailUp = model.getEmail();
            address1Up = model.getAdd1();
            address2Up = model.getAdd2();
            cityUp = model.getCity();
            stateUp = model.getState();
            dobUp = model.getDob();
            domUp = model.getDom();
            refByUp = model.getRefby();
            refMobUp = model.getRefmobile();
            pinCodeUp = model.getPin();
            memeberId = String.valueOf(model.getMemberid());

        }
        if (position % 2 == 0) {
            holder.tableLayout.setBackgroundColor(Color.WHITE);
        } else {
            holder.tableLayout.setBackgroundResource(R.color.fadeBlue);


        }

    }

    @Override
    public int getItemCount() {
        return chitDetails.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nameTxt, mobile1Txt, mobile2Txt, address1Txt, address2Txt;// init the item view's
        LinearLayout tableLayout;
        ImageView edit, deleteImageView;
        deleteRow deleteRow;


        public MyViewHolder(View itemView, deleteRow deleteRow) {
            super(itemView);
            // get the reference of item view's
            nameTxt = (TextView) itemView.findViewById(R.id.name);
            mobile1Txt = (TextView) itemView.findViewById(R.id.mobile1);
            edit = itemView.findViewById(R.id.edit);
            address1Txt = (TextView) itemView.findViewById(R.id.address1);
            address2Txt = (TextView) itemView.findViewById(R.id.address2);
            tableLayout = (LinearLayout) itemView.findViewById(R.id.tableLayout);
            deleteImageView = itemView.findViewById(R.id.deleteImageView);
            this.deleteRow = deleteRow;



            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Intent intent = new Intent(context, DefineNewMemberActivity.class);
                    intent.putExtra("memeberId", chitDetails.get(position).getMemberid());
                    intent.putExtra("position", position);
                    context.startActivity(intent);


                }
            });

            deleteImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                  /*  Intent intent = new Intent(context, MemberEditActivity.class);
                    intent.putExtra("memberId", chitDetails.get(position).getMemberid());
                    intent.putExtra("position", position);
                    intent.putExtra("chitCode", "");
                    context.startActivity(intent);*/
                    Integer memberId=chitDetails.get(position).getMemberid();
                    listeres.deleteRowClick(memberId);



                }
            });


        }

    }

    public interface deleteRow {
        void deleteRowClick(Integer memberId);

    }



}
