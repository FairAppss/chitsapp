package com.fairsoft.chitfund.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.fairsoft.chitfund.R;
import com.fairsoft.chitfund.model.AuctionCompleteDetailsModelItem;

import java.text.DecimalFormat;
import java.util.List;

public class AuctionAdapter extends RecyclerView.Adapter<AuctionAdapter.MyViewHolder> {
    List<AuctionCompleteDetailsModelItem> personNames;
    Context context;
    private onItemClickListener onItemClickListener;
    private static int lastClickedPosition = -1;
    private int selectedItem;
    AuctionCompleteDetailsModelItem model;
    String memberId, chitCode, chintName;
    Integer chitId;
    deleteRow listeres;


    public void setOnItemClickListener(AuctionAdapter.onItemClickListener onItemClickListener, deleteRow listeners) {
        this.onItemClickListener = onItemClickListener;
        this.listeres = listeners;
    }

    public AuctionAdapter(Context context, List<AuctionCompleteDetailsModelItem> personNames, deleteRow listeners,onItemClickListener onItemClickListener) {
        this.context = context;
        this.personNames = personNames;
        this.onItemClickListener = onItemClickListener;
        selectedItem = 0;
        this.listeres = listeners;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.auction_row_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v, onItemClickListener, listeres); // pass the view to View Holder
        return vh;
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // set the data in items

        if (personNames != null && personNames.size() > 0) {
            model = personNames.get(position);
            holder.aucName.setText(String.valueOf(model.getName()));
            holder.aucId.setText(String.valueOf(model.getAuctionday()));
            holder.aucChitCode.setText(String.valueOf(model.getChitcode()));

            DecimalFormat formatter = new DecimalFormat("#,###,###");


            String received = String.valueOf(model.getAuctionamount());
            if (!received.isEmpty()) {
                double d1 = Double.parseDouble(received);
                holder.aucChitId.setText(formatter.format(d1));
            }
            //  memberId = String.valueOf(personNames.get(position).get());
            chitCode = personNames.get(position).getChitcode();
            chitId = model.getChitid();


        }
        if (position % 2 == 0) {
            holder.subScribeLayout.setBackgroundColor(Color.WHITE);
        } else {
            holder.subScribeLayout.setBackgroundResource(R.color.fadeBlue);


        }

    }

    @Override
    public int getItemCount() {
        return personNames.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView aucName, aucChitId, aucChitCode, aucId;// init the item view's
        LinearLayout subScribeLayout;
        onItemClickListener onItemClickListener;
        ImageView deleteImageView, edit;
        deleteRow deleteRow;
        LinearLayout editLL;


        public MyViewHolder(View itemView, onItemClickListener onItemClickListener, deleteRow deleteRow) {
            super(itemView);
            // get the reference of item view's
            aucName = (TextView) itemView.findViewById(R.id.aucTime);
            aucChitId = (TextView) itemView.findViewById(R.id.aucAmount);
            aucChitCode = (TextView) itemView.findViewById(R.id.aucChitCode);
            aucId = (TextView) itemView.findViewById(R.id.aucDay);
            subScribeLayout = itemView.findViewById(R.id.subScribeLayout);
            deleteImageView = itemView.findViewById(R.id.deleteImageView);
            edit = itemView.findViewById(R.id.edit);
            editLL = itemView.findViewById(R.id.editLL);
            this.deleteRow = deleteRow;


            editLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                   /* Intent intent = new Intent(context, AuctionActivity.class);
                    intent.putExtra("chitId", personNames.get(position).getChitid());
                    intent.putExtra("auctionId", personNames.get(position).getAuctionid());
                    intent.putExtra("chitCode", personNames.get(position).getChitcode());
                    intent.putExtra("name", personNames.get(position).getName());

                    intent.putExtra("position", position);
                    context.startActivity(intent);*/
                    Integer chitId = personNames.get(position).getChitid();
                    Integer auctionId = personNames.get(position).getAuctionid();
                    String chitCode = personNames.get(position).getChitcode();

                    onItemClickListener.onItemClick(chitId, auctionId, chitCode);



                }
            });

            deleteImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                   /* Intent intent = new Intent(context, AuctionActivity.class);
                    intent.putExtra("chitId", personNames.get(position).getChitid());
                    intent.putExtra("auctionId", personNames.get(position).getAuctionid());
                    intent.putExtra("chitCode", personNames.get(position).getChitcode());
                    intent.putExtra("position", position);
                    context.startActivity(intent);*/
                    Integer chitId = personNames.get(position).getChitid();
                    Integer auctionId = personNames.get(position).getAuctionid();
                    String chitCode = personNames.get(position).getChitcode();

                    listeres.deleteRowClick(chitId, auctionId, chitCode);


                }
            });


        }

    }

    public interface onItemClickListener {
        void onItemClick(Integer chitId, Integer auctionId, String chitCode);

    }

    public interface deleteRow {
        void deleteRowClick(Integer chitId, Integer auctionId, String chitCode);

    }


}
