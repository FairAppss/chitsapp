package com.fairsoft.chitfund.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.fairsoft.chitfund.R;
import com.fairsoft.chitfund.model.PaymentModel;

import java.util.List;

public class GetMonthsAdapter extends RecyclerView.Adapter<GetMonthsAdapter.MyViewHolder> {
    List<PaymentModel> personNames;
    Context context;


    public GetMonthsAdapter(Context context, List<PaymentModel> personNames) {
        this.context = context;
        this.personNames = personNames;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_text, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.aucName.setText(personNames.get(position).getDt());
        // set the data in items


    }

    @Override
    public int getItemCount() {
        return personNames.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView aucName;
        LinearLayout subScribeLayout;
        onItemClickListener onItemClickListener;

        public MyViewHolder(View itemView) {
            super(itemView);
            // get the reference of item view's



        }

    }

    public interface onItemClickListener {
        void onItemClick(int position, String chitCode, String memberId, String date);

    }

}
