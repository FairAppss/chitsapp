package com.fairsoft.chitfund.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.fairsoft.chitfund.R;
import com.fairsoft.chitfund.model.RunningGroupsResponseModelItem;

import java.text.DecimalFormat;
import java.util.List;

public class RunningGroupsReportAdapter extends RecyclerView.Adapter<RunningGroupsReportAdapter.MyViewHolder> {
    List<RunningGroupsResponseModelItem> personNames;
    Context context;
    private static int lastClickedPosition = -1;
    private int selectedItem;
    RunningGroupsResponseModelItem model;


    public RunningGroupsReportAdapter(Context context, List<RunningGroupsResponseModelItem> personNames) {
        this.context = context;
        this.personNames = personNames;
        selectedItem = 0;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.running_group_row_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // set the data in items

        if (personNames != null && personNames.size() > 0) {
            model = personNames.get(position);
            holder.chitCodeRunn.setText(String.valueOf(model.getChitcode()));
            DecimalFormat formatter = new DecimalFormat("#,###,###");


            String received = String.valueOf(model.getChitamt());
            if (!received.isEmpty()) {
                double d1 = Double.parseDouble(received);
                holder.startDateRunn.setText(formatter.format(d1));
            }
            holder.memberRunn.setText(String.valueOf(model.getStartdate()));
            holder.amountRunn.setText(String.valueOf(model.getNoofmembers()));
            holder.compleRunn.setText(String.valueOf(model.getFrequency()));
            holder.frequencyRunn.setText(String.valueOf(model.getComp()));
            holder.enddateRunn.setText(String.valueOf(model.getEnddate()));


        }
        if (position % 2 == 0) {
            holder.subScribeLayout.setBackgroundColor(Color.WHITE);
        } else {
            holder.subScribeLayout.setBackgroundResource(R.color.fadeBlue);


        }

    }

    @Override
    public int getItemCount() {
        return personNames.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView chitCodeRunn, startDateRunn, memberRunn, amountRunn, compleRunn, frequencyRunn, enddateRunn;// init the item view's
        LinearLayout subScribeLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            // get the reference of item view's
            chitCodeRunn = (TextView) itemView.findViewById(R.id.chitCodeRunn);
            startDateRunn = (TextView) itemView.findViewById(R.id.startDateRunn);
            memberRunn = (TextView) itemView.findViewById(R.id.memberRunn);
            amountRunn = (TextView) itemView.findViewById(R.id.amountRunn);
            compleRunn = (TextView) itemView.findViewById(R.id.compleRunn);
            frequencyRunn = (TextView) itemView.findViewById(R.id.frequencyRunn);
            enddateRunn = (TextView) itemView.findViewById(R.id.enddateRunn);


            subScribeLayout = itemView.findViewById(R.id.subScribeLayout);


        }

    }


}
