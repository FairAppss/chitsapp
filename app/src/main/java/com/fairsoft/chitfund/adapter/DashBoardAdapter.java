package com.fairsoft.chitfund.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.fairsoft.chitfund.activity.SingleChitSummaryActivity;
import com.fairsoft.chitfund.model.ChitDefineModelItem;

import com.fairsoft.chitfund.R;
import com.fairsoft.chitfund.model.SingleChitModelItem;

import java.text.DecimalFormat;
import java.util.List;


public class DashBoardAdapter extends RecyclerView.Adapter<DashBoardAdapter.MyViewHolder> {
    List<ChitDefineModelItem> chitDetails;
    Context context;
    private onItemClickListener onItemClickListener;
    private static int lastClickedPosition = -1;
    private int selectedItem;
    List<SingleChitModelItem> personNames;


    public void setOnItemClickListener(DashBoardAdapter.onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public DashBoardAdapter(Context context, List<ChitDefineModelItem> chitDetails) {
        this.context = context;
        this.chitDetails = chitDetails;
        this.onItemClickListener = onItemClickListener;
        selectedItem = 0;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.dash_rowlayout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v, onItemClickListener); // pass the view to View Holder
        return vh;
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // set the data in items

        if (chitDetails != null && chitDetails.size() > 0) {
            ChitDefineModelItem model = chitDetails.get(position);
            holder.chitAmtTxt.setText((model.getChitdate().toString()));
            holder.chitDtTxt.setText((model.getChitcode()));
            holder.receivableTxt.setText((model.getReceivable()));
            holder.payableTxt.setText((model.getPayable()));


            DecimalFormat formatter = new DecimalFormat("#,###,###");


            String received = String.valueOf(model.getChitamt());
            if (!received.isEmpty()) {
                double d1 = Double.parseDouble(received);
                holder.AmtTxt.setText(formatter.format(d1));
            }

            holder.completedTxt.setText((model.getCompleted()));

            holder.noOfMonths.setText((model.getNomonths()).toString());

            holder.lastBidTxt.setText((model.getLastbid()));

            if (position % 2 == 0) {
                holder.tableLayout.setBackgroundColor(Color.WHITE);
            } else {
                holder.tableLayout.setBackgroundResource(R.color.fadeBlue);


            }


        }

    }

    @Override
    public int getItemCount() {
        return chitDetails.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView chitDtTxt, chitAmtTxt, AmtTxt, noOfMonths, completedTxt, receivableTxt, payableTxt, lastBidTxt;// init the item view's
        LinearLayout tableLayout;
        ImageView edit;
        RecyclerView saveChiefComplaintsRecyclerView;

        public MyViewHolder(View itemView, onItemClickListener listener) {
            super(itemView);
            // get the reference of item view's
            chitDtTxt = (TextView) itemView.findViewById(R.id.chitIdTxt);
            chitAmtTxt = (TextView) itemView.findViewById(R.id.chitAmtTxt);
            AmtTxt = (TextView) itemView.findViewById(R.id.AmtTxt);

            noOfMonths = (TextView) itemView.findViewById(R.id.noOfMonths);
            completedTxt = (TextView) itemView.findViewById(R.id.completedTxt);
            receivableTxt = (TextView) itemView.findViewById(R.id.receivableIdTxt);
            payableTxt = (TextView) itemView.findViewById(R.id.payableTxt);
            lastBidTxt = (TextView) itemView.findViewById(R.id.lastBidTxt);
            tableLayout = (LinearLayout) itemView.findViewById(R.id.tableLayout);
            saveChiefComplaintsRecyclerView = (RecyclerView) itemView.findViewById(R.id.saveChiefComplaintsRecyclerView);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Intent intent = new Intent(context, SingleChitSummaryActivity.class);
                    intent.putExtra("chitDt", chitDetails.get(position).getChitdate());
                    intent.putExtra("chitAmt", chitDetails.get(position).getChitamt());
                    intent.putExtra("receivable", chitDetails.get(position).getReceivable());
                    intent.putExtra("payable", chitDetails.get(position).getPayable());
                    intent.putExtra("completed", chitDetails.get(position).getBidamount());
                    intent.putExtra("mths", chitDetails.get(position).getNomonths());
                    intent.putExtra("chitId", chitDetails.get(position).getChitid());
                    intent.putExtra("chitCode", chitDetails.get(position).getChitcode());

                    intent.putExtra("lastBid", chitDetails.get(position).getLastbid());
                    intent.putExtra("position", position);
                    context.startActivity(intent);

                   /* saveChiefComplaintsRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                    SigleChitAdapter sub_adapter = new SigleChitAdapter(context, personNames);
                    saveChiefComplaintsRecyclerView.setAdapter(sub_adapter);
                   */

                }
            });


        }

    }

    public interface onItemClickListener {
        void onItemClick(int position);

    }

}
