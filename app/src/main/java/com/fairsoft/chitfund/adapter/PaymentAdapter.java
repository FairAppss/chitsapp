package com.fairsoft.chitfund.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.fairsoft.chitfund.model.PaymentCompleteDetailsModelItem;
import com.fairsoft.chitfund.R;

import java.util.List;

public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.MyViewHolder> {
    List<PaymentCompleteDetailsModelItem> personNames;
    Context context;
    private onItemClickListener onItemClickListener;
    private static int lastClickedPosition = -1;
    private int selectedItem;
    PaymentCompleteDetailsModelItem model;
    String memberId, chitCode, chintName;
    Integer chitId;
    deleteRow listeres;



    public void setOnItemClickListener(PaymentAdapter.onItemClickListener onItemClickListener, deleteRow deleteRow) {
        this.onItemClickListener = onItemClickListener;
        this.listeres = deleteRow;

    }

    public PaymentAdapter(Context context, List<PaymentCompleteDetailsModelItem> personNames, deleteRow listeners,onItemClickListener onItemClickListener) {
        this.context = context;
        this.personNames = personNames;
        this.onItemClickListener = onItemClickListener;
        selectedItem = 0;
        this.listeres = listeners;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_row_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v, onItemClickListener,listeres); // pass the view to View Holder
        return vh;
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // set the data in items

        if (personNames != null && personNames.size() > 0) {
            model = personNames.get(position);
            holder.chitCodePay.setText(String.valueOf(model.getChitcode()));
            holder.chitIdPay.setText(String.valueOf(model.getChitcode()));
            holder.paymentId.setText(String.valueOf(model.getName()));
            holder.interest.setText(String.valueOf(model.getAmount()));
            //  memberId = String.valueOf(personNames.get(position).get());
            chitCode = personNames.get(position).getChitcode();
            chitId = model.getChitid();


        }
        if (position % 2 == 0) {
            holder.subScribeLayout.setBackgroundColor(Color.WHITE);
        } else {
            holder.subScribeLayout.setBackgroundResource(R.color.fadeBlue);


        }

    }

    @Override
    public int getItemCount() {
        return personNames.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView chitIdPay, chitCodePay, paymentId, interest;// init the item view's
        LinearLayout subScribeLayout,editLL;
        onItemClickListener onItemClickListener;
        ImageView edit,deleteImageView;
        deleteRow deleteRow;



        public MyViewHolder(View itemView, onItemClickListener onItemClickListener,deleteRow deleteRow) {
            super(itemView);
            // get the reference of item view's
            chitIdPay = (TextView) itemView.findViewById(R.id.chitIdPay);
            chitCodePay = (TextView) itemView.findViewById(R.id.chitCodePay);
            paymentId = (TextView) itemView.findViewById(R.id.paymentId);
            interest = (TextView) itemView.findViewById(R.id.interest);
            subScribeLayout = itemView.findViewById(R.id.subScribeLayout);
            edit=itemView.findViewById(R.id.edit);
            editLL=itemView.findViewById(R.id.editLL);

            deleteImageView=itemView.findViewById(R.id.deleteImageView);
            this.deleteRow = deleteRow;
            this.onItemClickListener=onItemClickListener;



            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                   /* Intent intent = new Intent(context, PaymentActivity.class);
                    intent.putExtra("chitId", personNames.get(position).getChitid());
                    intent.putExtra("paymentId", personNames.get(position).getPaymentid());
                    intent.putExtra("chitCode", personNames.get(position).getChitcode());
                    intent.putExtra("position", position);
                    context.startActivity(intent);
*/
                    Integer chitId=personNames.get(position).getChitid();
                    Integer paymentId=personNames.get(position).getPaymentid();
                    String chitCode=personNames.get(position).getChitcode();

                    onItemClickListener.onItemClick(chitId,paymentId,chitCode);



                }
            });

            deleteImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    Integer chitId=personNames.get(position).getChitid();
                    Integer paymentId=personNames.get(position).getPaymentid();
                    String chitCode=personNames.get(position).getChitcode();

                    listeres.deleteRowClick(chitId,paymentId,chitCode);




                }
            });



        }

    }

    public interface onItemClickListener {
        void onItemClick(Integer chitId,Integer paymentId,String chitCode);

    }
    public interface deleteRow {
        void deleteRowClick(Integer chitId,Integer paymentId,String chitCode);

    }


}
