package com.fairsoft.chitfund.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.fairsoft.chitfund.R;


import java.util.ArrayList;
import java.util.List;

public class ChitBidCalculationAdapter extends RecyclerView.Adapter<ChitBidCalculationAdapter.MyViewHolder> {
    private List<Double> monthlistData = new ArrayList<Double>();
    private List<Double> interestBid = new ArrayList<Double>();

    Context context;
    Integer chitId;


    public ChitBidCalculationAdapter(Context context, List<Double> monthlistData,  List<Double> interestBid ) {
        this.context = context;
        this.monthlistData = monthlistData;
        this.interestBid = interestBid;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.bid_row_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // set the data in items

        if (monthlistData != null && monthlistData.size() > 0) {
            holder.bidTxt.setText(String.valueOf(monthlistData.get(position)));
            holder.interestTxt.setText(String.valueOf(interestBid.get(position)));


        }
        if (position % 2 == 0) {
            holder.subScribeLayout.setBackgroundColor(Color.WHITE);
        } else {
            holder.subScribeLayout.setBackgroundResource(R.color.fadeBlue);


        }

    }

    @Override
    public int getItemCount() {
        return monthlistData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView bidTxt, interestTxt, aucChitCode, aucId;// init the item view's
        LinearLayout subScribeLayout;
        onItemClickListener onItemClickListener;

        public MyViewHolder(View itemView) {
            super(itemView);
            // get the reference of item view's
            bidTxt = (TextView) itemView.findViewById(R.id.bidTxt);
            interestTxt = (TextView) itemView.findViewById(R.id.interestTxt);
            subScribeLayout = itemView.findViewById(R.id.subScribeLayout);


        }

    }

    public interface onItemClickListener {
        void onItemClick(int position, String chitCode, String memberId, String date);

    }

}
