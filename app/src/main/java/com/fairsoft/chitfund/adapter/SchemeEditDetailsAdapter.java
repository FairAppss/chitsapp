package com.fairsoft.chitfund.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.fairsoft.chitfund.activity.SchemeDefActivity;
import com.fairsoft.chitfund.R;
import com.fairsoft.chitfund.model.ChitCompleDetailsModelItem;

import java.text.DecimalFormat;
import java.util.List;

public class SchemeEditDetailsAdapter extends RecyclerView.Adapter<SchemeEditDetailsAdapter.MyViewHolder> {
    List<ChitCompleDetailsModelItem> chitDetails;
    Context context;
    private onChitItemClickListener onChitItemClickListener;
    private static int lastClickedPosition = -1;
    private int selectedItem;
    deleteRow listeres;
    int position;

    String dateUp, codeUp, id1Up, amt2Up, nomonthsUp, nomembersUp, frequencyUp, commnUp, isOwnchitUp, dividendUp, auctiondayUp, bidamtUp, compleUp, receivableUp, payableUp, lastBidUp, compup, enddtUp;


    public void setOnItemClickListener(SchemeEditDetailsAdapter.onChitItemClickListener onChitItemClickListener, SchemeEditDetailsAdapter.deleteRow deleteRow) {
        this.onChitItemClickListener = onChitItemClickListener;
        this.listeres = deleteRow;
    }

    public SchemeEditDetailsAdapter(Context context, List<ChitCompleDetailsModelItem> chitDetails, SchemeEditDetailsAdapter.onChitItemClickListener onChitItemClickListener, deleteRow listeners) {
        this.context = context;
        this.chitDetails = chitDetails;
        this.onChitItemClickListener = onChitItemClickListener;
        selectedItem = 0;
        this.listeres = listeners;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.scheme_details_row_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v, onChitItemClickListener, listeres); // pass the view to View Holder
        return vh;
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // set the data in items

        if (chitDetails != null && chitDetails.size() > 0) {
            ChitCompleDetailsModelItem model = chitDetails.get(position);
            holder.chitCode.setText((model.getChitcode().toString()));
            DecimalFormat formatter = new DecimalFormat("#,###,###");


            String received = String.valueOf(model.getChitamt());
            if (!received.isEmpty()) {
                double d1 = Double.parseDouble(received);
                holder.chitId.setText(formatter.format(d1));
            }
            holder.ChitDate.setText((model.getChitdate()));
            holder.Payable.setText(String.valueOf(model.getNoofmembers()));
            holder.receivable.setText(String.valueOf(model.getChitid()));
            dateUp = model.getChitdate();
            id1Up = String.valueOf(model.getChitid());
            codeUp = model.getChitcode();
            amt2Up = String.valueOf(model.getChitamt());
            nomembersUp = String.valueOf(model.getNoofmembers());
            nomonthsUp = String.valueOf(model.getNomonths());
            frequencyUp = model.getFrequency();
            commnUp = String.valueOf(model.getCommn());
            isOwnchitUp = model.getIsownchit();
            dividendUp = model.getDividend();
            auctiondayUp = model.getAuctionday();
            bidamtUp = model.getBidamount();
            compleUp = model.getCompleted();
            receivableUp = model.getReceivable();
            payableUp = model.getPayable();
            lastBidUp = model.getLastbid();
            compup = model.getComp();
            enddtUp = model.getEnddt();

        }
        if (position % 2 == 0) {
            holder.tableLayout.setBackgroundColor(Color.WHITE);
        } else {
            holder.tableLayout.setBackgroundResource(R.color.fadeBlue);


        }

    }

    @Override
    public int getItemCount() {
        return chitDetails.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView chitCode, chitId, ChitDate, Payable, receivable;// init the item view's
        LinearLayout tableLayout;
        ImageView edit, deleteImageView;
        onChitItemClickListener onChitItemClickListener;
        deleteRow deleteRow;

        public MyViewHolder(View itemView, onChitItemClickListener onChitItemClickListener, deleteRow deleteRow) {
            super(itemView);
            // get the reference of item view's
            chitCode = (TextView) itemView.findViewById(R.id.chitCode);
            chitId = (TextView) itemView.findViewById(R.id.chitId);
            ChitDate = (TextView) itemView.findViewById(R.id.ChitDate);
            Payable = (TextView) itemView.findViewById(R.id.Payable);
            receivable = (TextView) itemView.findViewById(R.id.receivable);
            edit = itemView.findViewById(R.id.edit);
            deleteImageView = itemView.findViewById(R.id.deleteImageView);


            tableLayout = (LinearLayout) itemView.findViewById(R.id.tableLayout);
            this.onChitItemClickListener = onChitItemClickListener;
            this.deleteRow = deleteRow;


            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Intent intent = new Intent(context, SchemeDefActivity.class);
                    intent.putExtra("chitId", chitDetails.get(position).getChitid());
                    intent.putExtra("position", position);
                    intent.putExtra("chitCode", chitDetails.get(position).getChitcode());

                    context.startActivity(intent);


                }
            });

            deleteImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    position = getAdapterPosition();
                    Integer chitId = chitDetails.get(position).getChitid();
                    String chitCode = chitDetails.get(position).getChitcode();
                    listeres.deleteRowClick(chitId, chitCode, position);
                   /* chitDetails.remove(position);
                    notifyDataSetChanged();
*/

                }
            });


        }

    }

    public interface onChitItemClickListener {
        void onMemItemClick(int position, String cdateUp, String cid1Up, String ccodeUp, String camt2Up, String cnomonthsUp, String cnomembersUp, String cfrequencyUp, String cCommnUp, String cisOwnchitUp, String cdividendUp, String cauctiondayUp, String cbidamtUp, String cCompleUp, String creceivableUp, String cpayableUp, String clastBidUp, String compup, String enddtUp);

    }

    public interface deleteRow {
        void deleteRowClick(Integer chitId, String chitCode, Integer position);

    }

    public void deleteRow(
            int position1
    ) {

       chitDetails.remove(position1);
        notifyDataSetChanged();


    }


}
