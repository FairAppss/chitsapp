package com.fairsoft.chitfund.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.fairsoft.chitfund.R;
import com.fairsoft.chitfund.model.SubscriberDetailsModelItem;

import java.util.List;

public class SubscriberAdapter extends RecyclerView.Adapter<SubscriberAdapter.MyViewHolder> {
    List<SubscriberDetailsModelItem> personNames;
    Context context;
    private onItemClickListener onItemClickListener;
    private static int lastClickedPosition = -1;
    private int selectedItem;
    SubscriberDetailsModelItem model;
    String memberId, chitCode, chintName;
    deleteRow listeres;



    public void setOnItemClickListener(SubscriberAdapter.onItemClickListener onItemClickListener, deleteRow listeners) {
        this.onItemClickListener = onItemClickListener;
        this.listeres=listeners;

    }

    public SubscriberAdapter(Context context, List<SubscriberDetailsModelItem> personNames, SubscriberAdapter.onItemClickListener onItemClickListener, deleteRow listeners) {
        this.context = context;
        this.personNames = personNames;
        this.onItemClickListener = onItemClickListener;
        selectedItem = 0;
        this.listeres = listeners;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.subscriber_row_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder vh = new MyViewHolder(v, onItemClickListener,listeres); // pass the view to View Holder
        return vh;
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // set the data in items

        if (personNames != null && personNames.size() > 0) {
            model = personNames.get(position);
            holder.memberIdSub.setText(String.valueOf(model.getMemberid()));
            holder.chidCodeSub.setText((model.getChitcode()));
            holder.chitIdSub.setText(String.valueOf(model.getName()));
            holder.statusSub.setText((model.getStatus()));
            memberId = String.valueOf(personNames.get(position).getMemberid());
            chitCode = personNames.get(position).getChitcode();
            chintName = model.getCreateddate();
            holder.memberIdSubT.setText((model.getTicketno()));


        }
        if (position % 2 == 0) {
            holder.subScribeLayout.setBackgroundColor(Color.WHITE);
        } else {
            holder.subScribeLayout.setBackgroundResource(R.color.fadeBlue);


        }

    }

    @Override
    public int getItemCount() {
        return personNames.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nameSub, memberIdSubT, memberIdSub, chitIdSub, chidCodeSub, referenceSub, citySub, createdBy, createdDate, subscrberId, statusSub;// init the item view's
        LinearLayout subScribeLayout,editLL;
        onItemClickListener onItemClickListener;
        ImageView edit,deleteImageView;
        deleteRow deleteRow;


        public MyViewHolder(View itemView, onItemClickListener onItemClickListener, deleteRow deleteRow) {
            super(itemView);
            // get the reference of item view's
            memberIdSub = (TextView) itemView.findViewById(R.id.memberIdSubT);
            chitIdSub = (TextView) itemView.findViewById(R.id.chitIdSub);
            chidCodeSub = (TextView) itemView.findViewById(R.id.chitCodeSub);
            statusSub = (TextView) itemView.findViewById(R.id.statusSub);
            memberIdSubT = (TextView) itemView.findViewById(R.id.memberIdSubT);

            subScribeLayout = itemView.findViewById(R.id.subScribeLayout);
            edit = itemView.findViewById(R.id.edit);
            deleteImageView=itemView.findViewById(R.id.deleteImageView);
            editLL=itemView.findViewById(R.id.editLL);
            this.deleteRow = deleteRow;
            this.onItemClickListener = onItemClickListener;




            editLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    /*Intent intent = new Intent(context, SubscriberActivity.class);
                    intent.putExtra("chitId", personNames.get(position).getChitid());
                    intent.putExtra("subscriberId", personNames.get(position).getSubscriberid());
                    intent.putExtra("chitCode", personNames.get(position).getChitcode());
                    intent.putExtra("memberId", personNames.get(position).getMemberid());
                    intent.putExtra("memberCode", personNames.get(position).getMember());

                    intent.putExtra("position", position);
                    context.startActivity(intent);*/
                    Integer chitId = personNames.get(position).getChitid();
                    Integer subscriberid = personNames.get(position).getSubscriberid();
                    String chitCode = personNames.get(position).getChitcode();
                    String member = personNames.get(position).getName();


                    onItemClickListener.onItemClick(chitId, subscriberid, chitCode,member);



                }
            });

            deleteImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                   /* Intent intent = new Intent(context, AuctionActivity.class);
                    intent.putExtra("chitId", personNames.get(position).getChitid());
                    intent.putExtra("auctionId", personNames.get(position).getAuctionid());
                    intent.putExtra("chitCode", personNames.get(position).getChitcode());
                    intent.putExtra("position", position);
                    context.startActivity(intent);*/
                    Integer chitId=personNames.get(position).getChitid();
                    Integer subscriberid=personNames.get(position).getSubscriberid();
                    String chitCode=personNames.get(position).getChitcode();

                    listeres.deleteRowClick(chitId,subscriberid,chitCode);



                }
            });



        }

    }

    public interface onItemClickListener {
        void onItemClick(Integer chitId, Integer subScriberId, String chitCode,String member);

    }
    public interface deleteRow {
        void deleteRowClick(Integer chitId,Integer auctionId,String chitCode);

    }


}
