package com.fairsoft.chitfund.api;


public class Urls {

    private static final String ROOT_URL = "https://fssservices.bookxpert.co/api/Chits/";
    public static final String URL_Login_DETAILS = ROOT_URL + "isValidLogin";
    public static final String URL_Login_DEVID_ID = ROOT_URL + "CheckDevice";
    public static final String URL_CHIT_DETAILS = ROOT_URL + "ChitDetails";
    public static final String URL_SAVE_CHIT_DETAILS = ROOT_URL + "SaveChitDetails";
    public static final String URL_IS_EXISTING_CODE = ROOT_URL + "isChittExists";
    public static final String URL_UPDATE_DETAILS = ROOT_URL + "UpdateMember";
    public static final String URL_IS_EXISTING_UPDATE_DETAILS = ROOT_URL + "isMemberExists";
    public static final String URL_SUBSCRIBE_MEMBER = ROOT_URL + "UpdateSubscriber";
    public static final String URL_MEMBER_DETAILS = ROOT_URL + "MemberDetails";
    public static final String URL_SUBSCRIBER_DETAILS = ROOT_URL + "SubscriberDetails";
    public static final String URL_CHIT_COMPLETE_DETAILS = ROOT_URL + "ChitCompleteDetails";
    public static final String URL_AUCTION_COMPLETE_DETAILS = ROOT_URL + "AuctionDetails";
    public static final String URL_RECEIPT_COMPLETE_DETAILS = ROOT_URL + "ReceiptDetails";
    public static final String URL_PAYMENT_COMPLETE_DETAILS = ROOT_URL + "PaymentDetails";
    public static final String URL_PAYMENT_SAVE_DETAILS = ROOT_URL + "SavePayments";
    public static final String URL_AUCTION_SAVE_DETAILS = ROOT_URL + "SaveAuctions";
    public static final String URL_RECEIPT_SAVE_DETAILS = ROOT_URL + "SaveReceipts";
    public static final String URL_CHIT_SUMMARY__DETAILS = ROOT_URL + "ChitSummaries";
    public static final String URL_SINGLE_CHIT_SUMMARY__DETAILS = ROOT_URL + "SingleChitSummary";
    public static final String URL_CHIT_LEVEL_REPORTS = ROOT_URL + "ChitReceivablesNpayables";
    public static final String URL_MEMBER_LEVEL_REPORTS = ROOT_URL + "MemberReceivablesNpayables";
    public static final String URL_OVERALL_LEVEL_REPORTS = ROOT_URL + "OverallReceivablesNpayables";
    public static final String URL_RECEIPTS_ANDZ_PAYMENTS_REPORTS = ROOT_URL + "ReceiptsNPayments";
    public static final String URL_RUNNING_GROUP_REPORTS = ROOT_URL + "RunningGroups";
    public static final String URL_PRIZED_MEMBER_DETAILS = ROOT_URL + "getNotPrizedSubscribersByChits";
    public static final String URL_ACTIVE_CHITS_DETAILS = ROOT_URL + "getActiveChits";
    public static final String URL_AUCTION_CHITS_DETAILS = ROOT_URL + "getAuctionChits";

    public static final String URL_DELETE = ROOT_URL + "Deletes";
    public static final String URL_MONTHS_VALID = ROOT_URL + "ValidMonths";
    public static final String URL_STATE_LIST = ROOT_URL + "getStatesList";




    public static final String URL_WHATSAPP_CONTACT_ADD = "https://FSSServices.bookxpert.co/api/WA/AddContact";

    public static final String URL_SEND_WHATSAP = "https://FSSServices.bookxpert.co/api/WA/SendMessage";
    public static final String URL_SEND_AUCTION_WHATSAP = "https://FSSServices.bookxpert.co/api/WA/SendWAMessage";


    public static final String URL_SEND_WHATSAP_FILE = "https://FSSServices.bookxpert.co/api/WA/SendMessageAndFile";


}




